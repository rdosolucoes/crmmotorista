SELECT C.NUNOTA,C.NUMNOTA, P.NOMEPARC, C.DTMOV,
       LPAD(C.HRMOV,6,0) HRMOV,
       DTMOV || ' ' || SUBSTR(TO_CHAR(LPAD(C.HRMOV,6,0)),1,2) || '&' ||
                       SUBSTR(TO_CHAR(LPAD(C.HRMOV,6,0)),3,2) || '&' ||
                       SUBSTR(TO_CHAR(LPAD(C.HRMOV,6,0)),5,2) HRENTSAI,
       CASE WHEN (TRUNC(1440 * (SYSDATE - TO_DATE((C.DTMOV || LPAD(C.HRMOV,6,0)),'DD/MM/YYHH24MISS' ))) >
                   (SELECT TEMPOSEPARA
                      FROM CRMPARAMETROS PR
                     WHERE (PR.CODCOLIGADA = C.CODEMP)
                   )
                 )
            THEN 'A'
            ELSE ''
        END ATRASO,
       S.SETORA,
       S.SETORB,
       S.SETORC,
       V.APELIDO,
       R.NOMEREG,S.STATUS
  FROM TGFCAB C
       INNER JOIN TGFPAR P ON C.CODPARC = P.CODPARC
       INNER JOIN TGFVEN V ON C.CODVEND = V.CODVEND
       INNER JOIN TSIREG R ON R.CODREG  = P.CODREG 
       LEFT OUTER JOIN CRMSEPARACAO S ON S.NUNOTA = C.NUNOTA AND S.CODEMP = C.CODEMP
 WHERE /* Foi acrescentadas as top's 4, 12, 444 e 1034 como pendente, acredito que ter� que criar uma nova coluna na crmparametros com as top's que
          dever�o exibir somente as pendentes, j� que n�o poder� fixar as top's, no fonte */
         (    (     C.CODTIPOPER IN ( 4,12,444,1034 )
                AND C.PENDENTE = 'S'
              )
           OR ( C.CODTIPOPER IN ( 300,1012 ) )
         )
       
   AND C.STATUSNOTA = 'L'
   AND C.CODEMP = &CODEMP
   AND C.AD_CODPARC IS NULL
   AND (S.STATUS IS NULL OR S.STATUS = 'R')

UNION

SELECT C.NUNOTA,
       C.NUMNOTA,
       P.NOMEPARC,
       C.DTMOV,
       LPAD(C.HRMOV,6,0) HRMOV,DTMOV || ' ' || 
                SUBSTR(TO_CHAR(LPAD(C.HRMOV,6,0)),1,2) || '&' || SUBSTR(TO_CHAR(LPAD(C.HRMOV,6,0)),3,2) || '&' ||
                SUBSTR(TO_CHAR(LPAD(C.HRMOV,6,0)),5,2)HRENTSAI,
       CASE WHEN  (TRUNC(1440 * (SYSDATE - TO_DATE((C.DTMOV || LPAD(C.HRMOV,6,0)),'DD/MM/YYHH24MISS' ))) >
                  (SELECT TEMPOSEPARA FROM CRMPARAMETROS PR WHERE (PR.CODCOLIGADA = C.CODEMP )))   THEN 'A' ELSE '' END ATRASO,
       S.SETORA,
       S.SETORB,
       S.SETORC,
       V.APELIDO,
       R.NOMEREG,
       S.STATUS
  FROM TGFCAB C
       INNER JOIN TGFPAR P ON C.AD_CODPARC = P.CODPARC
       INNER JOIN TGFVEN V ON C.CODVEND = V.CODVEND
       INNER JOIN TSIREG R ON R.CODREG  = P.CODREG 
       LEFT OUTER JOIN CRMSEPARACAO S ON S.NUNOTA = C.NUNOTA AND S.CODEMP = C.CODEMP
 WHERE /* Foi acrescentadas as top's 4, 12, 444 e 1034 como pendente, acredito que ter� que criar uma nova coluna na crmparametros com as top's que
          dever�o exibir somente as pendentes, j� que n�o poder� fixar as top's, no fonte */
         (    (     C.CODTIPOPER IN ( 4,12,444,1034 )
                AND C.PENDENTE = 'S'
              )
           OR ( C.CODTIPOPER IN ( 300,1012 ) )
         )
   AND C.PENDENTE = 'S'
   AND C.STATUSNOTA = 'L' 
   AND C.CODEMP = &CODEMP   
   AND (S.STATUS IS NULL OR S.STATUS = 'R')