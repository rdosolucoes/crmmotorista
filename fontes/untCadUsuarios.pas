unit untCadUsuarios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untBaseCad, JvExControls, JvLabel, dxGDIPlusClasses, JvXPCore,
  JvXPButtons, StdCtrls, ComCtrls, ExtCtrls, DBCtrls, Mask, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore,dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, Data.DB,
  cxDBData, cxCheckBox, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  dxSkiniMaginary, dxSkinSilver;

type
  TfrmCadUsuarios = class(TfrmBaseCad)
    btnRedifinirSenha: TJvXPButton;
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    grbLogin: TGroupBox;
    Label7: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    edtUsuario: TDBEdit;
    edtSenha: TDBEdit;
    edtSenha2: TEdit;
    GroupBox3: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit1: TDBEdit;
    Panel3: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    edtIdentificador: TDBEdit;
    edtNome: TDBEdit;
    chkAtivo: TDBCheckBox;
    chkMaster: TDBCheckBox;
    GroupBox1: TGroupBox;
    dblcEmpresa: TcxDBLookupComboBox;
    cxDBLookupComboBox2: TcxDBLookupComboBox;
    GroupBox2: TGroupBox;
    dblcMotorista: TcxDBLookupComboBox;
    DBCheckBox1: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure pSetaDireitos; override;
    //
    procedure pNovo; override;
    procedure pEditar; override;
    procedure pExcluir; override;
    procedure pRefresh; override;
    //--
    Function fValidaCampos :boolean; override;
    Function pGravar:Boolean; override;
    procedure pCancelar; override;
  end;

var
  frmCadUsuarios: TfrmCadUsuarios;

implementation

uses udmUsuarios,untPrincipal, untTrocaSenha,untFuncoes;

{$R *.dfm}

{ TfrmCadUsuarios }

procedure TfrmCadUsuarios.btnAtualizarClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TfrmTrocaSenha, frmTrocaSenha);
  frmTrocaSenha.userName := dmUsuarios.cdsUsu.FieldByName('NOME').AsString;
  frmTrocaSenha.userPass := dmUsuarios.cdsUsu.FieldByName('USERNAME').AsString;
  frmTrocaSenha.userID   := dmUsuarios.cdsUsu.FieldByName('IDCRMUSUARIO').AsString;
  frmTrocaSenha.Caption := 'Redefinir a Senha';
  frmTrocaSenha.ShowModal;
end;

procedure TfrmCadUsuarios.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  frmCadUsuarios := nil;
end;

procedure TfrmCadUsuarios.FormCreate(Sender: TObject);
begin
  inherited;
  cdsPad := dmUsuarios.cdsUsu;
end;

procedure TfrmCadUsuarios.FormShow(Sender: TObject);
begin
  inherited;
  edtSenha2.Clear;
  edtsenha.Enabled := (edtIdentificador.Text = '');
  edtsenha2.Enabled := (edtIdentificador.Text = '');
  pRegistros;
end;

function TfrmCadUsuarios.fValidaCampos: boolean;
var
  erro :string;
begin
  result := true;
  erro := '';
  if (edtNome.Text = '') then
    erro := erro + 'Informar o Nome;'+#13;
  if (edtUsuario.Text = '') then
    erro := erro + 'Informar o Usu�rio de login;'+#13;

  if (dblcEmpresa.Text = '') and not(chkMaster.Checked) then
    erro := erro + 'Informar a Empresa;'+#13;

  if dmUsuarios.cdsUsu.State = dsInsert then
  begin
    if (edtSenha.Text = '') then
      erro := erro + 'Informar a Senha de login;'+#13;
    if (edtSenha2.Text = '') then
      erro := erro + 'Redigitar a Senha de login;'+#13;
    if (edtSenha.Text <> edtSenha2.Text) then
      erro := erro + 'Senhas n�o conferem;'+#13;
  end;

  if (erro <> '') then
  begin
    MessageDlg(Erro+'Corrija os erros acima e tente novamente.', mtInformation, [mbOK], 0);
    result := false;
  end;
end;


procedure TfrmCadUsuarios.pCancelar;
begin
  inherited;
  with dmUsuarios.cdsUsu do
    if (State in [dsInsert, dsEdit]) then
    begin
      Cancel;
      RevertRecord;
    end;
end;

procedure TfrmCadUsuarios.pEditar;
begin
  inherited;
  with dmUsuarios.cdsUsu do
    if (FieldByName('IDCRMUSUARIO').AsInteger > 0) then
      Edit;
end;

procedure TfrmCadUsuarios.pExcluir;
begin
  inherited;
  with dmUsuarios.cdsUsu do
    if (FieldByName('IDCRMUSUARIO').AsInteger > 0) then
    begin
      Delete;
      if (ApplyUpdates(0) > 0) then
        raise Exception.Create('Erro ao excluir registro');
    end;
end;

Function TfrmCadUsuarios.pGravar:Boolean;
begin
  inherited;
  result := false;
  with dmUsuarios.cdsUsu do
  begin
    Post;
    if (ApplyUpdates(0) > 0) then
      raise Exception.Create('Erro ao incluir registro');
    result := true;
  end;
end;

procedure TfrmCadUsuarios.pNovo;
begin
  inherited;
  with dmUsuarios.cdsUsu do
    if (State in [dsBrowse]) then
      Append
    else if (State in [dsInsert, dsEdit]) then
      raise Exception.Create('Erro salve o registro antes')
    else //if State in [dsInactive] then
      raise Exception.Create('Erro ao criar registro');
end;

procedure TfrmCadUsuarios.pRefresh;
var
  chv :Integer;
begin
  inherited;
  with dmUsuarios.cdsUsu do
  begin
    chv := FieldByName('IDCRMUSUARIO').AsInteger;
    Close;
    Open;
    Locate('IDCRMUSUARIO', chv, []);
  end;
end;

procedure TfrmCadUsuarios.pSetaDireitos;
begin
  inherited;
  btnAdicionar.Enabled := frmprincipal.A00102.Enabled;
  btnEditar.Enabled := frmprincipal.A00103.Enabled;
  dmUsuarios.dtsUsu.AutoEdit := frmprincipal.A00103.Enabled;
  btnExcluir.Enabled := frmprincipal.A00104.Enabled;
  btnConfirmar.Enabled := frmprincipal.A00101.Enabled;
  btnRedifinirSenha.Enabled := frmprincipal.A00106.Enabled;
end;

end.
