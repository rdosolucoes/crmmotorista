unit untCadRomaneio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseCad, JvExControls, JvLabel,
  dxGDIPlusClasses, JvXPCore, JvXPButtons, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.ExtCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinSilver, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, Data.DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxMaskEdit, dxSkiniMaginary, cxLabel, cxContainer,
  cxGroupBox, cxRadioGroup, cxDBEdit, cxTextEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, Vcl.Mask, Vcl.DBCtrls, cxDBLabel,
  cxImageComboBox, JvGIF, JvExExtCtrls, JvExtComponent, JvCaptionPanel,SQLExpr,
  cxMemo, dxSkinOffice2013White, cxNavigator, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmCadRomaneio = class(TfrmBaseCad)
    GroupBox2: TGroupBox;
    pnlSel: TPanel;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    Timer1: TTimer;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1NUMNOTA: TcxGridDBColumn;
    cxGridDBTableView1NOMEPARC: TcxGridDBColumn;
    cxGridDBTableView1NOMEREG: TcxGridDBColumn;
    cxGridDBTableView1TEMPO: TcxGridDBColumn;
    Panel5: TPanel;
    Label5: TLabel;
    DBText1: TDBText;
    DBRGTipo1: TDBRadioGroup;
    btnRoteirizar: TJvXPButton;
    DBText2: TDBText;
    Label6: TLabel;
    pnlPedReal: TPanel;
    GroupBox1: TGroupBox;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1NUMNOTA: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEPARC: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEREG: TcxGridDBColumn;
    cxGrid1DBTableView1TEMPO: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Panel3: TPanel;
    Label3: TLabel;
    edtPesq: TEdit;
    Panel6: TPanel;
    btnCarrega: TJvXPButton;
    btnVolta: TJvXPButton;
    Panel4: TPanel;
    btnIncluiPedidos: TJvXPButton;
    btnReprovaPedidos: TJvXPButton;
    pnlPedido: TJvCaptionPanel;
    lblCliLoja: TLabel;
    JvXPButton3: TJvXPButton;
    JvXPButton4: TJvXPButton;
    dblcMotorista: TcxDBLookupComboBox;
    lblMotorista: TLabel;
    lcCliente: TcxDBLookupComboBox;
    rgCliLojaold: TDBRadioGroup;
    lcLoja: TcxDBLookupComboBox;
    rgCliLoja2: TDBRadioGroup;
    btnImprimir: TJvXPButton;
    dblcbMotoboy: TcxDBLookupComboBox;
    rgMotorista2: TDBRadioGroup;
    cxDBMemo1: TcxDBMemo;
    Label4: TLabel;
    cxGridDBTableView1OBSERVACOES: TcxGridDBColumn;
    NUNOTA: TcxGridDBColumn;
    cxGridDBTableView1NUNOTA: TcxGridDBColumn;
    rgCliLoja: TcxRadioGroup;
    rgMotorista: TcxDBRadioGroup;
    DBRGTipo: TcxDBRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure btnCarregaClick(Sender: TObject);
    procedure cxGrid1DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure btnVoltaClick(Sender: TObject);
    procedure edtPesqChange(Sender: TObject);
    procedure cxGridDBTableView1CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure btnRoteirizarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure rgCliLojaoldClick(Sender: TObject);
    procedure btnIncluiPedidosClick(Sender: TObject);
    procedure JvXPButton4Click(Sender: TObject);
    procedure rgCliLoja2Click(Sender: TObject);
    procedure JvXPButton3Click(Sender: TObject);
    procedure btnReprovaPedidosClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure cxGrid1DBTableView1MouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure cxGridDBTableView1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnAdicionarClick(Sender: TObject);
    procedure rgCliLojaClick(Sender: TObject);
    procedure rgMotoristaClick(Sender: TObject);
    procedure DBRGTipoPropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    sNuNota :String;
    bSetorA,bSetorB,bSetorC :Boolean;
    procedure pNovo; override;
    procedure pEditar; override;
    procedure pExcluir; override;
    procedure pCancelar; override;
    Function fValidaCampos :boolean; override;
    Function pGravar:Boolean; override;
    procedure pSetaDireitos; override;
    //
    procedure pRefresh; override;
    procedure AtualizaPedidos;
    function GeraIdPercurso(idControle :Integer): Integer;
  end;

var
  frmCadRomaneio: TfrmCadRomaneio;

implementation

{$R *.dfm}

uses udmRomaneio,udmPrincipal, untPrincipal,untFuncoes,untLogin, untReprovaConf,cxGridRows;

procedure TfrmCadRomaneio.AtualizaPedidos;
var
 chv :Integer;
begin
  chv := dmRomaneio.cdsPedidosCODREG.AsInteger;
  dmRomaneio.cdsPedidos.DisableControls;
  dmRomaneio.cdsPedidos.Close;
  dmRomaneio.sqlPedidos.ParamByName('CODEMP').AsInteger := fGetColigada;
 // dmRomaneio.sqlPedidos.ParamByName('CODTIPOPER').AsInteger  := dmPrincipal.cdsParametros.FieldByName('CODTIPOPER').AsInteger;
 // dmRomaneio.sqlPedidos.ParamByName('CODTIPOPERTRANSF').AsInteger  := dmPrincipal.cdsParametros.FieldByName('CODTIPOPERTRANSF').AsInteger;
  dmRomaneio.cdsPedidos.Open;
  dmRomaneio.cdsPedidos.Locate('CODREG',chv,[]);
  dmRomaneio.cdsPedidos.EnableControls;
end;

procedure TfrmCadRomaneio.cxGrid1DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  if (AViewInfo.Item.Index = cxGrid1DBTableView1TEMPO.Index) then
  begin
    if dmPrincipal.cdsParametros.FieldByName('TEMPOLIBERA').AsInteger >  AViewInfo.GridRecord.Values[cxGrid1DBTableView1TEMPO.Index]  then
    begin
      ACanvas.Font.Color  := clGreen;
      ACanvas.Font.Style :=  ACanvas.Font.Style + [fsBold];
    end
    else
    begin
      ACanvas.Font.Color  := clRed;
      ACanvas.Font.Style := ACanvas.Font.Style + [fsBold];
    end;
  end;

end;

procedure TfrmCadRomaneio.cxGrid1DBTableView1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  AHitTest: TcxCustomGridHitTest;
  AList: TList;
  I, ARowIndex: Integer;
begin
  AHitTest := cxGrid1DBTableView1.ViewInfo.GetHitTest(X, Y);
  if (AHitTest.ViewInfo is TcxGridGroupRowViewInfo) and (AHitTest.HitTestCode <> htExpandButton) then
    with TcxGridDBTableView(TcxGridSite(Sender).GridView), DataController do
    begin
      AList := TList.Create;
      try
        Groups.LoadRecordIndexesByRowIndex(AList, FocusedRowIndex);
        if AList.Count > 0 then
        begin
          Controller.ClearSelection;
          for I := 0 to AList.Count - 1 do
          begin
            ARowIndex := GetRowIndexByRecordIndex(Integer(AList[I]), True);
            SelectRows(ARowIndex, ARowIndex);
          end;
        end;
      finally
        AList.Free;
      end;
    end;
end;

procedure TfrmCadRomaneio.cxGridDBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
   if (AViewInfo.Item.Index = cxGridDBTableView1TEMPO.Index) then
  begin
    if dmPrincipal.cdsParametros.FieldByName('TEMPOLIBERA').AsInteger >  AViewInfo.GridRecord.Values[cxGridDBTableView1TEMPO.Index]  then
    begin
      ACanvas.Font.Color  := clGreen;
      ACanvas.Font.Style :=  ACanvas.Font.Style + [fsBold];
    end
    else
    begin
      ACanvas.Font.Color  := clRed;
      ACanvas.Font.Style := ACanvas.Font.Style + [fsBold];
    end;
  end;
end;

procedure TfrmCadRomaneio.cxGridDBTableView1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  AHitTest: TcxCustomGridHitTest;
  AList: TList;
  I, ARowIndex: Integer;
begin
  AHitTest := cxGridDBTableView1.ViewInfo.GetHitTest(X, Y);
  if (AHitTest.ViewInfo is TcxGridGroupRowViewInfo) and (AHitTest.HitTestCode <> htExpandButton) then
    with TcxGridDBTableView(TcxGridSite(Sender).GridView), DataController do
    begin
      AList := TList.Create;
      try
        Groups.LoadRecordIndexesByRowIndex(AList, FocusedRowIndex);
        if AList.Count > 0 then
        begin
          Controller.ClearSelection;
          for I := 0 to AList.Count - 1 do
          begin
            ARowIndex := GetRowIndexByRecordIndex(Integer(AList[I]), True);
            SelectRows(ARowIndex, ARowIndex);
          end;
        end;
      finally
        AList.Free;
      end;
    end;
end;



procedure TfrmCadRomaneio.DBRGTipoPropertiesChange(Sender: TObject);
begin
  inherited;
  if DBRGTipo.ItemIndex = 1 then
  begin
    dblcbMotoboy.Enabled := False;
    dblcMotorista.Enabled := False;
    dblcbMotoboy.Clear;
    dblcMotorista.Clear;
  end
  else
  begin
    dblcbMotoboy.Enabled := True;

    if (dmRomaneio.sdsMotoboy.Active) and (dmRomaneio.sdsMotoboy.RecordCount > 0) then
      dblcbMotoboy.ItemIndex := 0;

    dblcMotorista.Enabled := True;
  end;

end;


procedure TfrmCadRomaneio.edtPesqChange(Sender: TObject);
begin
  inherited;
  if (edtPesq.Text <> '') then
    dmRomaneio.cdsPedidos.Locate('NUMNOTA',edtPesq.Text,[]);
end;

procedure TfrmCadRomaneio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  frmCadRomaneio := nil;
end;

procedure TfrmCadRomaneio.FormCreate(Sender: TObject);
begin
  inherited;
  cdsPad := dmRomaneio.cdsRomaneio;
end;

procedure TfrmCadRomaneio.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
   if (Key = #13) and (PageControl1.ActivePageIndex = 1) then
    btnConfirmarClick(nil);
end;


procedure TfrmCadRomaneio.FormShow(Sender: TObject);
begin
  inherited;
  pRegistros;
  dmPrincipal.cdsParametros.Close;
  dmPrincipal.cdsParametros.Params[0].AsInteger := fGetColigada;
  dmPrincipal.cdsParametros.Open;
  Timer1.Enabled := True;
  AtualizaPedidos;
  cxGrid1DBTableView1.DataController.Groups.FullExpand;
  btnRoteirizar.visible := dmromaneio.cdsRomaneioSTATUS.AsString = 'A';
  pnlPedReal.Visible :=  (dmromaneio.cdsRomaneioSTATUS.AsString = 'A') or (dmRomaneio.cdsRomaneio.State = dsInsert);
  pnlSel.Visible :=  (dmromaneio.cdsRomaneioSTATUS.AsString = 'A') or (dmRomaneio.cdsRomaneio.State = dsInsert);
  sNuNota := '';
  rgCliLojaClick(nil);
  //rgCliLoja.Enabled := dmromaneio.cdsItensRomaneio.IsEmpty;
  rgMotoristaClick(nil);

  frmCadRomaneio.DBRGTipoPropertiesChange(nil);
  end;

function TfrmCadRomaneio.fValidaCampos: boolean;
begin
  result := true;
end;

procedure TfrmCadRomaneio.btnIncluiPedidosClick(Sender: TObject);
var
 result:integer;
begin
  inherited;
  //Application.CreateForm(TfrmLogin,frmLogin);
  //result := frmLogin.ShowModal;

  //if result = 6 then
  //begin
    if dmPrincipal.cdsUsuario.FieldByName('PERMITEINCPEDIDO').AsInteger <> 1 then
    begin
     Application.MessageBox('Voc� n�o tem permiss�o para Incluir Pedido!!','SANKHYA',MB_OK + mb_iconwarning);
     Exit;
    end;

    AlinharPanel(frmCadRomaneio,pnlPedido,true);
    pnlPedido.Visible := True;
    rgCliLoja2Click(nil);
    rgCliLoja2.Enabled := dmRomaneio.cdsItensRomaneio.IsEmpty;
  //end;
end;

procedure TfrmCadRomaneio.btnReprovaPedidosClick(Sender: TObject);
var result :integer;
    sSql,sID :String;
    qAux :TSQLQuery;
begin
  inherited;
  if dmRomaneio.cdsRomaneio.State = dsInsert then
    btnConfirmarClick(nil);

  //Application.CreateForm(TfrmLogin,frmLogin);
  //result := frmLogin.ShowModal;

  //if result = 6 then
  //begin
    frmReprovaConf := TfrmReprovaConf.Create(frmCadRomaneio,frmCadRomaneio);
    result := frmReprovaConf.ShowModal;

    if result = 6 then
    begin
      With dmRomaneio do
      begin
        sID := cdsPedidosNUNOTA.AsString;

        sSQL := 'SELECT NUNOTA FROM CRMSEPARACAO WHERE NUNOTA = ' +  sID +  ' AND CODEMP = ' + IntToStr(fGetColigada) ;
        CriaQuery(qAux);
        qAux.SQL.Text := sSQL;
        qAux.Open;

        if qAux.IsEmpty then
        begin
          Application.MessageBox('Pedido ainda n�o Separado.','SANKHYA',mb_ok + mb_iconwarning);
          FreeAndNil(qAux);
          Abort;
        end;


        sSQL := 'UPDATE CRMSEPARACAO SET STATUS = ''R'', DATAREPROVACAO = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                ',''DD/MM/YYYY HH24:MI:SS'') WHERE NUNOTA = ' + sID + ' AND CODEMP = ' + IntToStr(fGetColigada);
        SQLExecute(sSQL);

        if bSetorA then
        begin
          sSQL := 'UPDATE CRMSEPARACAO SET SETORA = 2 WHERE NUNOTA = ' + sID + ' AND SETORA = 1  AND CODEMP = ' + IntToStr(fGetColigada);
          SQLExecute(sSQL);
        end;

        if bSetorB then
        begin
          sSQL := 'UPDATE CRMSEPARACAO SET SETORB = 2 WHERE NUNOTA = ' + sID + ' AND SETORB = 1 AND CODEMP = ' + IntToStr(fGetColigada);
          SQLExecute(sSQL);
        end;

        if bSetorC then
        begin
          sSQL := 'UPDATE CRMSEPARACAO SET SETORC = 2 WHERE NUNOTA = ' + sID + ' AND SETORC = 1 AND CODEMP = ' + IntToStr(fGetColigada);
          SQLExecute(sSQL);
        end;
      end;
       btnAtualizarClick(nil);
       FreeAndNil(qAux);
    end;
  //end;
end;

procedure TfrmCadRomaneio.btnImprimirClick(Sender: TObject);
begin
  inherited;
   CarregaRelatorio('Relat�rio de Romaneio',['CODEMP','IDROMANEIO'],[dmRomaneio.cdsRomaneioCODEMP.AsString,dmRomaneio.cdsRomaneioIDROMANEIO.AsString]);
end;

procedure TfrmCadRomaneio.JvXPButton3Click(Sender: TObject);
begin
  inherited;
  With dmRomaneio do
  begin
    if cdsItensRomaneio.State <> dsInsert then
      cdsItensRomaneio.Append;

    cdsItensRomaneioNUMNOTA.Value := 99999;
    cdsItensRomaneioCODEMP.AsInteger := fGetColigada;
    cdsItensRomaneioTEMPO.Value := 0;
    cdsItensRomaneioIDROMANEIO.AsInteger := cdsRomaneioIDROMANEIO.AsInteger;
    cdsItensRomaneioNUNOTA.Value := 99999;

    if rgCliLoja2.ItemIndex = 0 then
    begin
      sdsCliente.Locate('CODPARC',cdsItensRomaneioCODPARC.AsInteger,[]);
      cdsItensRomaneioCODREG.Value := sdsClienteCodReg.Value;
    end
    else
    begin
      sdsLoja.Locate('CODPARC',cdsItensRomaneioCODPARC.AsInteger,[]);
      cdsItensRomaneioCODREG.Value :=  sdsLojaCodReg.Value;
    end;

    cdsItensRomaneio.Post;

     if dmRomaneio.cdsRomaneio.State = dsInsert then
      btnConfirmarClick(nil);

      cdsItensRomaneio.ApplyUpdates(0);
  end;
  pnlPedido.Visible := False;
  rgCliLojaClick(nil);

  if dmRomaneio.cdsRomaneio.State = dsEdit then
    btnConfirmarClick(nil);

  btnAtualizarClick(nil);
end;

procedure TfrmCadRomaneio.JvXPButton4Click(Sender: TObject);
begin
  inherited;
  pnlPedido.Visible := False;
end;

procedure TfrmCadRomaneio.btnAdicionarClick(Sender: TObject);
var
  strMsg :PWideChar;
begin
  strMsg := 'Voc� tem certeza que deseja incluir um novo registro? Dados n�o salvos ser�o perdidos !!';

  if Application.MessageBox(strMsg,'SANKHYA',MB_YESNO + MB_ICONQUESTION) = IDYES  then
    inherited;

end;

procedure TfrmCadRomaneio.btnCarregaClick(Sender: TObject);
var
  I : Integer;
  Selecionados: Integer;
  Row : Integer;
begin
  inherited;
  With dmRomaneio do
  begin
    if cdsRomaneioSTATUS.AsString <> 'A' then
    begin
      Application.MessageBox('N�o � Poss�vel Alterar um Romaneio j� Roteirizado!!','SANKHYA', mb_ok + mb_iconerror);
      Abort;
    end;



   // Comentado a pedido da Laise 26/03/2015 - Rafael Domingues de Oliveira
  {  if not cdsItensRomaneio.IsEmpty then
    begin
      if cdsItensRomaneioNOMEREG.AsString <> cdsPedidosNOMEREG.AsString then
      begin
        Application.MessageBox('N�o � Poss�vel Incluir Pedidos de Regi�es Diferentes.','SANKHYA',mb_ok + mb_iconwarning);
        Abort;
      end;
    end;  }


    Selecionados := cxGrid1DBTableView1.ViewData.DataController.GetSelectedCount;
    for I := 0 to Selecionados - 1 do
    begin
      Row := cxGrid1DBTableView1.ViewData.DataController.GetSelectedRowIndex(I);
      cdsPedidos.Locate('NUNOTA',cxGrid1DBTableView1.ViewData.Records[Row].Values[cxGrid1DBTableView1.GetColumnByFieldName('NUNOTA').Index],[]);

      if cdsPedidosSTATUS.AsString <> 'A' then
      begin
        Application.MessageBox('N�o � Poss�vel Incluir um Pedido n�o Aprovado.','SANKHYA', mb_ok + mb_iconerror);
        Continue;
      end;

      cdsItensRomaneio.Append;
      cdsItensRomaneioIDROMANEIO.AsInteger := cdsRomaneioIDROMANEIO.AsInteger;
      cdsItensRomaneioCODEMP.AsInteger  := fGetColigada;
      cdsItensRomaneioNUNOTA.AsInteger  := cdsPedidosNUNOTA.AsInteger;
      cdsItensRomaneioNUMNOTA.AsString  := cdsPedidosNUMNOTA.AsString;
      cdsItensRomaneioNOMEPARC.AsString := cdsPedidosNOMEPARC.AsString;
      cdsItensRomaneioNOMEREG.AsString  := cdsPedidosNOMEREG.AsString;
      cdsItensRomaneioCODREG.AsInteger  := cdsPedidosCODREG.AsInteger;
      cdsItensRomaneioCODPARC.AsInteger := cdsPedidosCODPARC.AsInteger;
      cdsItensRomaneioTEMPO.AsInteger   := cdsPedidosTEMPO.AsInteger;
      cdsItensRomaneio.Post;
    end;



    if dmRomaneio.cdsRomaneio.State = dsInsert then
      btnConfirmarClick(nil);


    cdsItensRomaneio.ApplyUpdates(0);
    AtualizaPedidos;

   { end
    else
    begin
      if sNuNota = '' then
        sNuNota := 'NUNOTA <> '  + cdsPedidosNUNOTA.AsString
      else
        sNuNota := sNuNota + ' AND NUNOTA <> ' + cdsPedidosNUNOTA.AsString;

      dmRomaneio.cdsPedidos.Filter := sNuNota;
      dmRomaneio.cdsPedidos.Filtered := true;
    end;     }

 //   cxGridDBTableView1.DataController.Groups.FullExpand;
  end;
  ActiveControl := nil;
end;

procedure TfrmCadRomaneio.btnVoltaClick(Sender: TObject);
var
  I : Integer;
  Selecionados,nunota: Integer;
  Row : Integer;
  AList :TStringList;
begin
  inherited;
 { if dmRomaneio.cdsRomaneio.State = dsInsert then
  begin
    sNuNota := StringReplace(sNuNota,' AND NUNOTA <> ' + dmRomaneio.cdsItensRomaneioNuNota.asString,'',[]);
    sNuNota := StringReplace(sNuNota,'NUNOTA <> ' + dmRomaneio.cdsItensRomaneioNuNota.asString,'',[]);

    if Copy(sNuNota,0,4) = ' AND' then
      sNuNota := StringReplace(sNuNota,'AND','',[]);

    dmRomaneio.cdsPedidos.Filtered := false;

    if (sNuNota <> '') or (sNuNota <> ' ')  then
    begin
      dmRomaneio.cdsPedidos.Filter := sNuNota;
      dmRomaneio.cdsPedidos.Filtered := true;
    end;
  end;    }

  AList := TStringList.Create();

  if dmromaneio.cdsRomaneioSTATUS.AsString <> 'A' then
  begin
    Application.MessageBox('N�o � Poss�vel Alterar um Romaneio j� Roteirizado!!','SANKHYA', mb_ok + mb_iconerror);
    Abort;
  end;

  if dmRomaneio.cdsRomaneio.State = dsInsert then
    btnConfirmarClick(nil);


   Selecionados := cxGridDBTableView1.ViewData.DataController.GetSelectedCount;
   for I := 0 to Selecionados - 1 do
   begin
     Row := cxGridDBTableView1.ViewData.DataController.GetSelectedRowIndex(I);
     nunota := cxGridDBTableView1.ViewData.Records[Row].Values[cxGridDBTableView1.GetColumnByFieldName('NUNOTA').Index];
     AList.Add(IntToStr(nunota));
   end;

   for i := 0 to AList.Count - 1 do
   begin
     dmRomaneio.cdsItensRomaneio.Locate('NUNOTA',AList[I],[]);
     dmRomaneio.cdsItensRomaneio.Delete;
   end;

   dmRomaneio.cdsItensRomaneio.ApplyUpdates(0);


  AtualizaPedidos;
  ActiveControl := nil;
end;

procedure TfrmCadRomaneio.btnRoteirizarClick(Sender: TObject);
var
  sSQL :String;
  result,IdControle :integer;
begin
  inherited;

  With dmRomaneio do
  begin
    if cdsItensRomaneio.IsEmpty then
      Exit;

   // Application.CreateForm(TfrmLogin,frmLogin);
   // result := frmLogin.ShowModal;

   // if result = 6  then
  //  begin
      if (cdsRomaneioCODPARC.AsString = '') and (DBRGTipo.ItemIndex = 0) then
      begin
        Application.MessageBox('Informar o Entregador/Motorista.','SANKHYA',MB_ok + MB_ICONWARNING);
        Abort;
      end;

      Screen.Cursor := crHourGlass;
      cdsItensRomaneio.DisableControls;
      cdsItensRomaneio.First;

      While not cdsItensRomaneio.Eof  do
      begin
        sSQL := ' UPDATE TGFCAB SET AD_ID_ROMANEIO = ' + cdsRomaneioIDROMANEIO.AsString +
                ' WHERE NUNOTA = ' + cdsItensRomaneioNUNOTA.AsString +
                ' AND CODEMP = ' + cdsItensRomaneioCODEMP.AsString;

        SQLExecute(sSQL);
        cdsItensRomaneio.Next;
      end;

      cdsItensRomaneio.First;
      cdsItensRomaneio.EnableControls;

      sSQL := ' UPDATE CRMROMANEIO SET STATUS = ''R'', DATAROTEIRIZACAO = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
              ',''DD/MM/YYYY HH24:MI:SS''),USUARIOROTEIRIZACAO = ' + QuotedStr(fGetNomeUsuario) +
              ' WHERE IDROMANEIO = ' + cdsRomaneioIDROMANEIO.AsString + ' AND CODEMP = ' + IntToStr(fGetColigada);
      SQLExecute(sSQL);


      if DBRGTipo.ItemIndex = 0 then
      begin
         sdsMotoboy.Locate('CODPARC',cdsRomaneioCODPARC.AsString,[]);
         IdControle := sdsMotoboy.FieldByName('IDCONTROLE').AsInteger;

          if rgMotorista.ItemIndex = 0 then
          begin
            sSQL := ' UPDATE CRMCONTROLEMOTOBOY SET STATUS = ''P'' WHERE IDCONTROLE = ' + IntToStr(IdControle) +
                    ' AND CODEMP = ' + IntToStr(fGetColigada);
            SQLExecute(sSQL);

            sSQL := ' INSERT INTO CRMPERCURSOMOTOBOY (CODEMP,IDCONTROLE,IDPERCURSO,IDROMANEIO,STATUS) ' +
                    ' VALUES (' + IntToStr(fGetColigada) + ',' + IntToStr(IdControle) + ',' +
                    IntToStr(GeraIdPercurso(IdControle)) + ',' + cdsRomaneioIDROMANEIO.AsString + ',''A'' )';

            SQLExecute(sSQL);
          end;
      end;

      btnAtualizarClick(nil);

      if DBRGTipo.ItemIndex = 0 then
        CarregaRelatorio('Relat�rio de Romaneio',['CODEMP','IDROMANEIO'],[dmRomaneio.cdsRomaneioCODEMP.AsString,dmRomaneio.cdsRomaneioIDROMANEIO.AsString],True);

      Screen.Cursor := crDefault;
    end;
  //end;
end;

procedure TfrmCadRomaneio.pCancelar;
begin
 inherited;
  with dmRomaneio.cdsRomaneio do
  if (State in [dsInsert, dsEdit]) then
  begin
    Cancel;
    RevertRecord;
  end;
end;

procedure TfrmCadRomaneio.pEditar;
begin
  inherited;
  with dmRomaneio.cdsRomaneio do
    if (FieldByName('IDROMANEIO').AsInteger > 0) then
      Edit;
end;

procedure TfrmCadRomaneio.pExcluir;
var
  sSQL,sID :String;
begin
 inherited;
  with dmRomaneio.cdsRomaneio do
    if (FieldByName('IDROMANEIO').AsInteger > 0) then
    begin
      sID := FieldByName('IDROMANEIO').AsString;
      Delete;

      sSQL := 'DELETE FROM CRMITENSROMANEIO WHERE IDROMANEIO = ' + sID +
            ' AND CODEMP = ' + IntToStr(fGetColigada);

      SQLExecute(sSQL);

      if (ApplyUpdates(0) > 0) then
        raise Exception.Create('Erro ao excluir registro');
    end;
end;

function TfrmCadRomaneio.pGravar: Boolean;
begin
  result := false;
  with dmRomaneio.cdsRomaneio do
  begin
    Post;
    if (ApplyUpdates(0) > 0) then
      raise Exception.Create('Erro ao incluir registro');

    result := true;

    pRefresh;
  end;
end;

procedure TfrmCadRomaneio.pNovo;
begin
  inherited;
  with dmRomaneio.cdsRomaneio do
    if (State in [dsBrowse]) then
      Append
    else if (State in [dsInsert, dsEdit]) then
      raise Exception.Create('Erro salve o registro antes')
    else //if State in [dsInactive] then
      raise Exception.Create('Erro ao criar registro');

end;

procedure TfrmCadRomaneio.pRefresh;
var
  chv :Integer;
begin
  inherited;
  with dmRomaneio.cdsRomaneio do
  begin
    chv := FieldByName('IDROMANEIO').AsInteger;
    Close;
    Params[0].AsInteger := fGetColigada;
    Open;
    Locate('IDROMANEIO', chv, []);
  end;
  AtualizaPedidos;

end;

procedure TfrmCadRomaneio.pSetaDireitos;
begin
  inherited;
  btnAdicionar.Enabled := frmprincipal.A01802.Enabled;
  btnEditar.Enabled := frmprincipal.A01803.Enabled;
  dmRomaneio.dsRomaneio.AutoEdit := frmprincipal.A01803.Enabled;
  btnExcluir.Enabled := frmprincipal.A01804.Enabled;
  panel1.Enabled := frmprincipal.A01805.Enabled;
  btnIncluiPedidos.Enabled := frmprincipal.A01806.Enabled;
  btnReprovaPedidos.Enabled := frmprincipal.A01807.Enabled;
end;

procedure TfrmCadRomaneio.rgMotoristaClick(Sender: TObject);
begin
  inherited;
  if rgMotorista.ItemIndex = 0 then
  begin
    lblMotorista.Caption := 'Motoboy';
    dblcMotorista.Visible := False;
    dblcbMotoboy.Visible := True;
    dblcMotorista.Clear;
    dmRomaneio.sdsMotoboy.Close;
    dmRomaneio.sdsMotoboy.Open;
    if not dmRomaneio.sdsMotoboy.IsEmpty then
      dblcbMotoboy.ItemIndex := 0;
  end
  else
  begin
    lblMotorista.Caption := 'Motorista';
    dblcMotorista.Visible := True;
    dblcbMotoboy.Visible := False;
    dblcbMotoboy.Clear;
    dmRomaneio.sdsMotorista.Close;
    dmRomaneio.sdsMotorista.Open;
  end;

  DBRGTipoPropertiesChange(nil);
end;

procedure TfrmCadRomaneio.rgCliLoja2Click(Sender: TObject);
begin
  inherited;
  if rgCliLoja2.ItemIndex = 0 then
  begin
    dmRomaneio.sdsCliente.Close;
    dmRomaneio.sdsCliente.Open;
    lblCliLoja.Caption := 'Cliente';
    lcCliente.Visible := True;
    lcLoja.Visible := False;
  end
  else
  begin
    dmRomaneio.sdsLoja.Close;
    dmRomaneio.sdsLoja.Open;
    lblCliLoja.Caption := 'Loja';
    lcCliente.Visible := False;
    lcLoja.Visible := True;
  end;


end;

procedure TfrmCadRomaneio.rgCliLojaClick(Sender: TObject);
begin
  inherited;

  dmRomaneio.cdsPedidos.Filtered := False;

  if rgCliLoja.ItemIndex = 1 then
  begin
  {  if dmRomaneio.cdsRomaneio.State = dsInsert then
    begin
      sNuNota := sNuNota +  ' AND CLIENTE = ''S''';
      dmRomaneio.cdsPedidos.Filter := sNuNota;
    end              8
    else    }
    dmRomaneio.cdsPedidos.Filter := 'CODTIPOPER = ' +  dmPrincipal.cdsParametros.FieldByName('CODTIPOPER').AsString;
    dmRomaneio.cdsPedidos.Filtered := True;
  end
  else
  if rgCliLoja.ItemIndex = 2 then
  begin
 { if dmRomaneio.cdsRomaneio.State = dsInsert then
  begin
    sNuNota := sNuNota +  ' AND AD_LOJA = ''S''';
    dmRomaneio.cdsPedidos.Filter := sNuNota;
  end
  else      }
    dmRomaneio.cdsPedidos.Filter := 'CODTIPOPER = ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOPERTRANSF').AsString;
    dmRomaneio.cdsPedidos.Filtered := True;
  end
end;

procedure TfrmCadRomaneio.rgCliLojaoldClick(Sender: TObject);
begin
  inherited;
  dmRomaneio.cdsPedidos.Filtered := False;

  if rgCliLoja.ItemIndex = 1 then
  begin
  {  if dmRomaneio.cdsRomaneio.State = dsInsert then
    begin
      sNuNota := sNuNota +  ' AND CLIENTE = ''S''';
      dmRomaneio.cdsPedidos.Filter := sNuNota;
    end              8
    else    }
    dmRomaneio.cdsPedidos.Filter := 'CODTIPOPER = ' +  dmPrincipal.cdsParametros.FieldByName('CODTIPOPER').AsString;
    dmRomaneio.cdsPedidos.Filtered := True;
  end
  else
  if rgCliLoja.ItemIndex = 2 then
  begin
 { if dmRomaneio.cdsRomaneio.State = dsInsert then
  begin
    sNuNota := sNuNota +  ' AND AD_LOJA = ''S''';
    dmRomaneio.cdsPedidos.Filter := sNuNota;
  end
  else      }
    dmRomaneio.cdsPedidos.Filter := 'CODTIPOPER = ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOPERTRANSF').AsString;
    dmRomaneio.cdsPedidos.Filtered := True;
  end


end;

procedure TfrmCadRomaneio.Timer1Timer(Sender: TObject);
begin
  inherited;
  AtualizaPedidos;
end;

function TfrmCadRomaneio.GeraIdPercurso(idControle :Integer): Integer;
var
  qAux :TSQLQuery;
begin
  CriaQuery(qAux);
  qAux.SQL.Add('SELECT NVL(MAX(IDPERCURSO),0) + 1 FROM CRMPERCURSOMOTOBOY');
  qAux.SQL.Add(' WHERE IDCONTROLE = :IDCONTROLE AND CODEMP = :CODEMP ');
  qAux.Params[0].AsInteger := idControle;
  qAux.Params[1].AsInteger := fGetColigada;
  qAux.Open;

  Result := qAux.Fields[0].AsInteger;
  FreeAndNil(qAux);
end;

end.
