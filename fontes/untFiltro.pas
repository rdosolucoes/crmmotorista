unit untFiltro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBase, Vcl.ComCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters,cxFilterControl, cxDBFilterControl, Vcl.StdCtrls,
  Vcl.Buttons, cxCalendar, cxTextEdit, dxGDIPlusClasses, JvExControls, JvXPCore,
  JvXPButtons, SqlExpr, DBClient, Vcl.ExtCtrls,DB, dxSkiniMaginary,
  dxSkinOffice2013White, dxSkinSilver;

type
  TfrmFiltro = class(TfrmBase)
    cxDBFilterControl1: TcxDBFilterControl;
    Panel1: TPanel;
    btnSalvar: TJvXPButton;
    btnTodos: TJvXPButton;
    btnOK: TJvXPButton;
    procedure btnSalvarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure btnTodosClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Tabela :String;
  end;

var
  frmFiltro: TfrmFiltro;

implementation
uses udmUsuarios,untFuncoes,udmFiltros;

{$R *.dfm}

procedure TfrmFiltro.btnFecharClick(Sender: TObject);
begin
  inherited;
  self.close;
end;

procedure TfrmFiltro.btnOKClick(Sender: TObject);
begin
  inherited;
  btnSalvarClick(nil);
  self.Close;
end;

procedure TfrmFiltro.btnSalvarClick(Sender: TObject);
var
   MS :TMemoryStream;
begin
  inherited;
  MS := TMemoryStream.Create;
  dmFiltros.cdsFiltro.Edit;
  frmfiltro.cxDBFilterControl1.SaveToStream(MS);
  MS.Position := 0;
 (dmFiltros.cdsFiltroLAYOUT as TBlobField).LoadFromStream(MS);
  MS.Free;
  dmFiltros.cdsFiltroFILTROTXT.Value := cxDBFilterControl1.FilterText;
  dmFiltros.cdsFiltro.Post;
end;

procedure TfrmFiltro.btnTodosClick(Sender: TObject);
begin
  inherited;
  self.close;
end;

procedure TfrmFiltro.FormShow(Sender: TObject);
var
  AStream: TMemoryStream;
begin
   inherited;
   if (dmFiltros.cdsFiltroLAYOUT.asstring <> '') then
   begin
      AStream := TMemoryStream.Create;
      try
        TBlobField(dmFiltros.cdsFiltroLAYOUT).SaveToStream(AStream);
        AStream.Position := 0;
        frmFiltro.cxDBFilterControl1.LoadFromStream(AStream);
      finally
        AStream.Free;
      end;
   end;
end;

end.
