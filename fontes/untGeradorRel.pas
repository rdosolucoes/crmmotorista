unit untGeradorRel;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBase, Vcl.ImgList, cxGraphics,
  dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons, Vcl.ExtCtrls,
  Vcl.ComCtrls, JvExComCtrls, JvComCtrls, Vcl.Grids, Vcl.DBGrids,Data.SqlExpr,
  Vcl.Menus,frxDsgnIntf,frxRes, frxClass, frxDCtrl, frxDesgn,Db,
  frxDBXComponents;

type
  TfrmGeradorRel = class(TfrmBase)
    treeAcoes: TJvTreeView;
    Panel1: TPanel;
    btnIncPasta: TJvXPButton;
    JvXPButton1: TJvXPButton;
    JvXPButton2: TJvXPButton;
    JvXPButton3: TJvXPButton;
    JvXPButton4: TJvXPButton;
    imgMenu: TcxImageList;
    PopupMenu1: TPopupMenu;
    ModoDesenho: TMenuItem;
    cxImageList1: TcxImageList;
    Visualizar1: TMenuItem;
    btnColapse: TJvXPButton;
    btnExpand: TJvXPButton;
    procedure btnIncPastaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure JvXPButton1Click(Sender: TObject);
    procedure treeAcoesEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure JvXPButton2Click(Sender: TObject);
    procedure JvXPButton4Click(Sender: TObject);
    procedure JvXPButton3Click(Sender: TObject);
    procedure treeAcoesGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure treeAcoesGetSelectedIndex(Sender: TObject; Node: TTreeNode);
    procedure ModoDesenhoClick(Sender: TObject);
    procedure GravaStream;
    procedure CarregaStream;
    procedure Visualizar1Click(Sender: TObject);
    procedure btnExpandClick(Sender: TObject);
    procedure btnColapseClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
  private
    { Private declarations }
    procedure CarregaTreeView;
    function NomePasta (Nome,Tipo :String;ID :Integer) :String;
  public
    { Public declarations }
  end;

var
  frmGeradorRel: TfrmGeradorRel;
  Index :Integer;

implementation

{$R *.dfm}

uses udmGeradorRelatorios,untFuncoes, udmPrincipal;

procedure TfrmGeradorRel.btnColapseClick(Sender: TObject);
begin
  inherited;
  pColapseTree(treeAcoes);
end;

procedure TfrmGeradorRel.btnExpandClick(Sender: TObject);
begin
  inherited;
  pExpandTree(treeAcoes);
end;

procedure TfrmGeradorRel.btnIncPastaClick(Sender: TObject);
var
  Nome :String;
begin
  inherited;
  Nome := NomePasta('Nova Pasta','P', 0);
  treeAcoes.Items.Add(nil,Nome);
  dmGeradorRelatorios.cdsPasta.Append;
  dmGeradorRelatorios.cdsPastaNOME.Value := Nome;
  dmGeradorRelatorios.cdsPasta.Post;
end;

procedure TfrmGeradorRel.CarregaStream;
var
  MS :TMemoryStream;
  frDataSet : TfrxDBXQuery;
  i,x:Integer;
begin
   dmGeradorRelatorios.cdsRel.Locate('NOME',treeAcoes.items.Item[treeAcoes.Selected.AbsoluteIndex].Text,[]);

   if dmGeradorRelatorios.cdsRelLAYOUT.IsNull then
    Exit;

   MS := TMemoryStream.Create;
   TBlobField(dmGeradorRelatorios.cdsRelLAYOUT).SaveToStream(MS);
   MS.Position := 0;
   dmGeradorRelatorios.frxReport1.LoadFromStream(MS);

   With dmGeradorRelatorios do
   begin
     for i:= 0 to frxReport1.DataSets.Count - 1 do
     begin
       if frxReport1.DataSets[i].DataSet is TfrxDBXQuery then
       begin
         frDataSet := TfrxDBXQuery( frxReport1.DataSets[i].DataSet );

         for x := 0 to frDataSet.Params.Count -1 do
         begin
           if frDataSet.Params[x].Name = 'CODEMP' then
             frDataSet.Params[x].Value := fGetColigada;
         end;
       end;
     end;
   end;
   MS.Free;
end;

procedure TfrmGeradorRel.CarregaTreeView;
var
 tn,n:TTreeNode;

begin
  treeAcoes.Items.Clear;
  With dmGeradorRelatorios do
  begin
    cdsPasta.First;
    while not cdsPasta.Eof do
    begin
      tn :=  treeAcoes.Items.Add(nil,cdsPastaNOME.Text);
      tn.ImageIndex := 0;
      cdsRel.Filtered := False;
      cdsRel.Filter := ' IDPASTA = ' + cdsPastaIDPASTA.AsString;
      cdsRel.Filtered := True;
      while not cdsRel.Eof do
      begin
        n := treeAcoes.Items.AddChild(tn,cdsRelNOME.AsString);
        n.ImageIndex := 1;
        cdsRel.Next;
      end;
      cdsPasta.Next;
    end;
    cdsRel.Filtered := False;
  end;
end;

procedure TfrmGeradorRel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  frmGeradorRel := nil;
  FreeAndNil(dmGeradorRelatorios);
end;

procedure TfrmGeradorRel.FormCreate(Sender: TObject);
begin
  inherited;
   treeAcoes.Items.Clear;
   Application.CreateForm(TdmGeradorRelatorios,dmGeradorRelatorios);
   CarregaTreeView;
end;

procedure TfrmGeradorRel.FormPaint(Sender: TObject);
begin
//inherited;

end;

procedure TfrmGeradorRel.GravaStream;
var
  MS : TMemoryStream;
begin
    MS := TMemoryStream.Create;
    dmgeradorRelatorios.frxReport1.SaveToStream(MS);
    MS.Position := 0;
    (dmGeradorRelatorios.cdsRelLAYOUT as TBlobField).LoadFromStream(MS);
    MS.Free;
end;

procedure TfrmGeradorRel.JvXPButton1Click(Sender: TObject);
var
 n,tn :TTreeNode;
 Nome :String;
begin
  inherited;
  if treeAcoes.Selected = nil then
    Exit;

  tn := treeAcoes.Selected;
  While tn.Parent <> nil do
    tn := tn.Parent;

  Nome := NomePasta('Novo Relat�rio','R', 0);
  n := treeAcoes.Items.AddChild(tn,Nome);
  n.ImageIndex := 1;


  With dmGeradorRelatorios do
  begin
    cdsPasta.Locate('NOME',tn.Text,[]);
    cdsRel.Append;
    cdsRelIDPASTA.Value := cdsPastaIDPASTA.Value;
    cdsrelNome.Value := Nome;
    cdsRel.Post;
  end;
  tn.Expanded := True;
end;

procedure TfrmGeradorRel.JvXPButton2Click(Sender: TObject);
begin
  inherited;
  if Application.MessageBox('Confirma a Exclus�o da Pasta e de todos seus Relat�rios ?','SANKHYA',mb_yesno + MB_ICONQUESTION) = idYes then
  begin
    if (treeAcoes.SelectedCount > 0) and (treeAcoes.items.Item[treeAcoes.Selected.AbsoluteIndex].Level = 0) then
    begin
      dmGeradorRelatorios.cdsPasta.Locate('NOME',treeAcoes.items.Item[treeAcoes.Selected.AbsoluteIndex].Text,[]);
      SQLExecute('DELETE FROM CRMREL WHERE IDPASTA = ' + dmGeradorRelatorios.cdsPastaIDPASTA.AsString );
      dmGeradorRelatorios.cdsPasta.Delete;
      treeAcoes.Items.Delete(treeAcoes.items.Item[treeAcoes.Selected.AbsoluteIndex]);
    end;
  end;
end;

procedure TfrmGeradorRel.JvXPButton3Click(Sender: TObject);
begin
  inherited;
  if (treeAcoes.SelectedCount > 0) and (treeAcoes.items.Item[treeAcoes.Selected.AbsoluteIndex].Level = 1) then
  begin
    dmGeradorRelatorios.cdsRel.Locate('NOME',treeAcoes.items.Item[treeAcoes.Selected.AbsoluteIndex].Text,[]);
    dmGeradorRelatorios.cdsRel.Delete;
    treeAcoes.Items.Delete(treeAcoes.items.Item[treeAcoes.Selected.AbsoluteIndex]);
  end;
end;

procedure TfrmGeradorRel.JvXPButton4Click(Sender: TObject);
begin
  inherited;
  close;
end;

function TfrmGeradorRel.NomePasta(Nome,Tipo :String;ID :Integer): String;
var
  qAux :TSQLQuery;
  s,NomeSQL,Tabela,campo: string;
  Maior,Num :integer;
begin
  CriaQuery(qAux);
  Maior := 0;
  Num := 0;

  if tipo = 'P' then
  begin
    tabela := 'CRMPASTA';
    campo := 'IDPASTA';
  end
  else
  begin
    tabela := 'CRMREL';
    campo := 'IDREL';
  end;

  qAux.SQL.Add('SELECT NOME FROM ' + tabela + ' WHERE NOME LIKE ' + QuotedStr(NOME + '%'));

  if ID > 0 then
    qAux.SQL.Add(' AND ' + campo + ' <> ' + IntToStr(ID) );

  qAux.Open;

  if qAux.RecordCount > 1 then
  begin
    while not qAux.Eof do
    begin
      NomeSQL := qAux.Fields[0].AsString;
      if Pos('_',NomeSQL) > 0 then
      begin
        Num := StrToInt(Copy(NomeSQL,Pos('_',NomeSQL) + 1,length(NomeSQL)));
        if Num > Maior then
          Maior := Num;
      end;
      qAux.Next;
    end;
  end;


  if Maior > 0 then
    Nome := Nome + '_' + IntToStr(Maior);

  qAux.SQL.Clear;
  qAux.Close;
  qAux.SQL.Add('SELECT * FROM ' + Tabela + ' WHERE NOME = ' + QuotedStr(NOME));
  qAux.Open;

  if not qAux.IsEmpty then
  begin
    if Pos('_',Nome) > 0 then
    begin
      s := Copy(Nome,Pos('_',Nome) + 1,length(Nome));
      Result := StringReplace(Nome,s,IntToStr(StrToInt(s) + 1),[])
    end
    else
      Result := Nome + '_1';
  end
  else
    Result := Nome;
  qAux.Close;
  FreeAndNil(qAux);
end;

procedure TfrmGeradorRel.ModoDesenhoClick(Sender: TObject);
begin
  inherited;
  With dmGeradorRelatorios do
  begin
    CarregaStream;
    dmgeradorRelatorios.frxReport1.DesignReport;
    if Application.MessageBox('Deseja Salvar as Altera��es ?','SANKHYA',mb_yesno + mb_iconquestion) = idyes then
    begin
      cdsRel.Edit;
      GravaStream;
      cdsRel.Post;
    end;
  end;
end;

procedure TfrmGeradorRel.PopupMenu1Popup(Sender: TObject);
begin
  inherited;
  if treeAcoes.items.Item[treeAcoes.Selected.AbsoluteIndex].Level = 0 then
    Abort;
end;

procedure TfrmGeradorRel.treeAcoesEdited(Sender: TObject; Node: TTreeNode;
  var S: string);
  var NovoNome :String;
begin
  inherited;
  With dmGeradorRelatorios do
  begin
    if node.Level = 0 then
    begin
      cdsPasta.Locate('NOME',NODE.Text,[]);
      NovoNome :=  NomePasta(s,'P',cdsPastaIDPASTA.AsInteger);
      cdsPasta.Edit;
      cdsPastaNOME.Value := NovoNome;
      cdsPasta.Post;
      S := NovoNome;
    end
    else
    begin
      cdsRel.Locate('NOME',NODE.Text,[]);
      NovoNome :=  NomePasta(s,'R',cdsRelIDREL.AsInteger);
      cdsRel.Edit;
      cdsRelNOME.Value := NovoNome;
      cdsRel.Post;
      S := NovoNome;
    end;
  end;
end;

procedure TfrmGeradorRel.treeAcoesGetImageIndex(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;
  if Node.Level = 0 then
  begin
    if Node.Expanded then
     Node.ImageIndex := 0 // <-- Icone da pasta aberta
    else
     Node.ImageIndex := 1 // <-- Icone da pasta fechada
  end
  else
    Node.ImageIndex := 2
end;

procedure TfrmGeradorRel.treeAcoesGetSelectedIndex(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;
  Node.SelectedIndex := Node.ImageIndex;
end;

procedure TfrmGeradorRel.Visualizar1Click(Sender: TObject);
begin
  inherited;
  if treeAcoes.items.Item[treeAcoes.Selected.AbsoluteIndex].Level = 1 then
  begin
    CarregaStream;
    dmgeradorRelatorios.frxReport1.ShowReport;
    dmGeradorRelatorios.frxReport1.clear;
  end;
end;

end.
