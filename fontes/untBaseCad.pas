unit untBaseCad;

interface

uses untBase, Classes, ActnList, PlatformDefaultStyleActnCtrls, ActnMan,
  JvExControls, JvLabel, dxGDIPlusClasses, JvXPCore, JvXPButtons, Controls,
  StdCtrls, ComCtrls, ExtCtrls, Dialogs, DBClient,Windows,Forms,db;

type
  TfrmBaseCad = class(TfrmBase)
    pnlBotoes: TPanel;
    pnlTrabalho: TPanel;
    PageControl1: TPageControl;
    tbs1: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    tbs2: TTabSheet;
    Panel2: TPanel;
    Label2: TLabel;
    btnConfirmar: TJvXPButton;
    btnCancelar: TJvXPButton;
    btnAdicionar: TJvXPButton;
    btnEditar: TJvXPButton;
    btnExcluir: TJvXPButton;
    btnAtualizar: TJvXPButton;
    pnlMovimento: TPanel;
    JvLabel2: TJvLabel;
    btnPrimeiro: TJvXPButton;
    btnAnterior: TJvXPButton;
    btnProximo: TJvXPButton;
    btnUltimo: TJvXPButton;
    btSair: TJvXPButton;
    procedure FormShow(Sender: TObject);
    procedure btnAdicionarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnPrimeiroClick(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnProximoClick(Sender: TObject);
    procedure btnUltimoClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    Procedure ControleBarras(ativa :TTabSheet);
  public
    { Public declarations }
    cdsPad: TClientDataSet;
    Acao :String;
    procedure pSetaDireitos; virtual; abstract;
    procedure pRegistros;
    //
    procedure pNovo; virtual; abstract;
    procedure pEditar; virtual; abstract;
    procedure pExcluir; virtual; abstract;
    procedure pRefresh; virtual; abstract;
    //--
    Function fValidaCampos :boolean; virtual; abstract;
    Function pGravar:Boolean; virtual; abstract;
    procedure pCancelar; virtual; abstract;
  end;

var
  frmBaseCad: TfrmBaseCad;

implementation

uses udmPrincipal, SysUtils;

{$R *.dfm}

procedure TfrmBaseCad.ControleBarras(ativa: TTabSheet);
var
  Component: TComponent;
begin
  for Component in Self do
    if (Component is TTabSheet) and ((Component.Name = 'tbs1') or (Component.Name = 'tbs2'))then
      TTabSheet(Component).TabVisible := false;

  TPageControl(ativa.Parent).ActivePage := ativa;
end;

procedure TfrmBaseCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if cdsPad.State in [dsInsert,dsEdit] then
  if Application.MessageBox('Deseja Salvar as Alterações ? ','SANKHYA',MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    if fValidaCampos then
      btnConfirmarClick(nil)
    else
      Abort;
  end
  else
   btnCancelarClick(nil);
end;

procedure TfrmBaseCad.FormShow(Sender: TObject);
begin
  inherited;
  if cdsPad.State in [dsInsert,dsEdit] then
    ControleBarras(tbs2)
  else
    ControleBarras(tbs1);
  pSetaDireitos;
end;

procedure TfrmBaseCad.btSairClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmBaseCad.pRegistros;
begin
  StatusBar1.Panels.Items[0].Text := 'Registros: '+
                                     IntToStr(cdsPad.RecNo)+'/'+
                                     IntToStr(cdsPad.RecordCount);
end;

procedure TfrmBaseCad.btnAdicionarClick(Sender: TObject);
begin
  inherited;
  ControleBarras(tbs2);
  pNovo;
end;

procedure TfrmBaseCad.btnAnteriorClick(Sender: TObject);
begin
  inherited;
  cdsPad.Prior;
  pRegistros;
end;

procedure TfrmBaseCad.btnAtualizarClick(Sender: TObject);
begin
  inherited;
  pRefresh;
  pRegistros;
end;

procedure TfrmBaseCad.btnCancelarClick(Sender: TObject);
begin
  inherited;
  pCancelar;
  ControleBarras(tbs1);
end;

procedure TfrmBaseCad.btnConfirmarClick(Sender: TObject);
begin
  inherited;
  if fValidaCampos then
    If pGravar then
    begin
      ControleBarras(tbs1);
      pRegistros;
    end;
end;

procedure TfrmBaseCad.btnEditarClick(Sender: TObject);
begin
  inherited;
  pEditar;
  ControleBarras(tbs2);
end;

procedure TfrmBaseCad.btnExcluirClick(Sender: TObject);
begin
  inherited;
  if Application.MessageBox('Deseja Realmente Excluir Registro(s) Selecionado(s) ?','SANKHYA',MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    pExcluir;
    pRegistros;
  end;
end;

procedure TfrmBaseCad.btnPrimeiroClick(Sender: TObject);
begin
  inherited;
  cdsPad.First;
  pRegistros;
end;

procedure TfrmBaseCad.btnProximoClick(Sender: TObject);
begin
  inherited;
  cdsPad.Next;
  pRegistros;
end;

procedure TfrmBaseCad.btnUltimoClick(Sender: TObject);
begin
  inherited;
  cdsPad.Last;
  pRegistros;
end;

end.
