unit untTrocaSenha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untBaseConfirmacao, dxGDIPlusClasses, JvExControls, JvXPCore,
  JvXPButtons, StdCtrls, ComCtrls, ExtCtrls, Mask, DBCtrls,SqlExpr;

type
  TfrmTrocaSenha = class(TfrmBaseConfirmacao)
    edtNome: TEdit;
    Label1: TLabel;
    grbLogin: TGroupBox;
    Label7: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    edtSenha2: TEdit;
    edtUsuario: TEdit;
    edtSenha: TEdit;
    btCript: TJvXPButton;
    procedure FormShow(Sender: TObject);
    procedure btCriptClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    userName, userPass, userID :String;

    Function fAcaoOk:boolean; override;
    Function fAcaoCancelar:boolean; override;
  end;

var
  frmTrocaSenha: TfrmTrocaSenha;

implementation

uses untFuncoes;

{$R *.dfm}

{ TfrmTrocaSenha }

function TfrmTrocaSenha.fAcaoCancelar: boolean;
begin
  result := true;
end;

function TfrmTrocaSenha.fAcaoOk: boolean;
var
  erro, txt :string;
begin
  result := true;
  erro := '';
  //
  if (edtUsuario.Text = '') then
    erro := erro + 'Informar o Usu�rio de login;'+#13;
  if (edtSenha.Text = '') then
    erro := erro + 'Informar a Senha de login;'+#13;
  if (edtSenha2.Text = '') then
    erro := erro + 'Redigitar a Senha de login;'+#13;
  if (edtSenha.Text <> edtSenha2.Text) then
    erro := erro + 'Senhas n�o conferem;'+#13;
  //
  if (erro <> '') then
  begin
    MessageDlg(Erro+'Corrija os erros acima e tente novamente.', mtInformation, [mbOK], 0);
    result := false;
  end
  else
  begin
    txt := 'UPDATE CRMUSUARIO SET USERNAME = '''+edtUsuario.Text+''', '+
                                'SENHA = '''+Cripto(edtSenha.Text)+''' '+
                          'WHERE IDCRMUSUARIO = '+userID;
    pExec(txt);
  end;
end;

procedure TfrmTrocaSenha.FormShow(Sender: TObject);
begin
  inherited;
  edtSenha.Clear;
  edtSenha2.Clear;
  edtNome.Text    := userName;
  edtUsuario.Text := userPass;
  btCript.Visible := UpperCase(fGetNomeUsuario) = 'MESTRE';
end;

procedure TfrmTrocaSenha.btCriptClick(Sender: TObject);
var
  qAux,
  qUpdate :TSQLQuery;
  begin
  inherited;
  Screen.Cursor :=  crHourGlass;
  CriaQuery(qAux);
  CriaQuery(qUpdate);
  qAux.SQL.Add(' SELECT IDCRMUSUARIO,SENHA,CODUSUARIO FROM CRMUSUARIO ');
  qAux.Open;

  while not qAux.Eof do
  begin
    if not(qAux.Fields[1].IsNull) and (UPPERCASE(qAux.FieldByName('CODUSUARIO').AsString) <> 'MESTRE') then
    begin
      qUpdate.SQL.Add('UPDATE CRMUSUARIO SET SENHA = :SENHA ');
      qUpdate.SQL.Add(' WHERE IDCRMUSUARIO = :ID ');
      qUpdate.ParamByName('SENHA').AsString :=  Cripto(qAux.Fields[1].Value);
      qUpdate.ParamByName('ID').AsInteger := qAux.Fields[0].AsInteger;
      qUpdate.ExecSQL;
    end;
    qAux.Next;
  end;
  FreeAndNil(qAux);
  FreeAndNil(qUpdate);
  Screen.Cursor :=  crDefault;
end;

end.
