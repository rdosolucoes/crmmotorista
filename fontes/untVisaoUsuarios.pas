unit untVisaoUsuarios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untBaseVisao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, StdCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ComCtrls, dxGDIPlusClasses, JvExControls, JvXPCore,
  JvXPButtons, JvLabel, ExtCtrls, cxCheckBox,dxSkinTheAsphaltWorld,
  dxSkinscxPCPainter,DBCommon,SqlExpr, dxSkinsCore;

type
  TfrmVisaoUsuarios = class(TfrmBaseVisao)
    cxGrid1DBTableView1ATIVO: TcxGridDBColumn;
    cxGrid1DBTableView1MASTERUSER: TcxGridDBColumn;
    cxGrid1DBTableView1NOME: TcxGridDBColumn;
    cxGrid1DBTableView1USERNAME: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid1DBTableView1IDCRMUSUARIO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure pSetaDireitos; override;
    //
    procedure pNovo; override;
    procedure pEditar; override;
    procedure pExcluir; override;
    procedure pRefresh; override;
    //--
    Procedure pAbrirTela;
  end;

var
  frmVisaoUsuarios: TfrmVisaoUsuarios;

implementation

uses udmUsuarios, untCadUsuarios, untPrincipal,untfiltro,untFuncoes;

{$R *.dfm}

{ TfrmVisaoUsuarios }

procedure TfrmVisaoUsuarios.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if frmCadUsuarios <> nil then
    frmCadUsuarios.Close;
  frmVisaoUsuarios := nil;
  dmUsuarios.Free;
end;

procedure TfrmVisaoUsuarios.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmUsuarios, dmUsuarios);
  cdsPad := dmUsuarios.cdsUsu;
  SqlPad := dmUsuarios.sqlUsu;
  Tabela := 'CRMUSUARIO';
end;

procedure TfrmVisaoUsuarios.FormShow(Sender: TObject);
begin
  inherited;
  pRefresh;
  btnFiltrar.Visible := false;
end;

procedure TfrmVisaoUsuarios.pAbrirTela;
begin
   pAbreCadastro(TfrmCadUsuarios,frmCadUsuarios);
end;

procedure TfrmVisaoUsuarios.pEditar;
begin
  inherited;
  if (dmUsuarios.cdsUsu.RecNo <= 0) then
    Exit;

  pAbrirTela;
  if dmUsuarios.cdsUsu.State <> dsBrowse then
    frmCadUsuarios.btnCancelarClick(self);
end;

procedure TfrmVisaoUsuarios.pExcluir;
var
 qAux :TSQLQuery;
begin
  inherited;
  with dmUsuarios.cdsUsu do
  begin
    CriaQuery(qAux);
    qAux.SQL.Add(' DELETE FROM CRMUSUARIORPR WHERE IDCRMUSUARIO = :ID ');
    qAux.Params[0].AsInteger := fieldbyname('IDCRMUSUARIO').AsInteger;
    qAux.ExecSQL;
    FreeAndNil(qAux);
    Delete;
    if ApplyUpdates(0) > 0 then
      raise Exception.Create('Erro ao Excluir Registro.');
  end;
end;


procedure TfrmVisaoUsuarios.pNovo;
begin
  inherited;
  pAbrirTela;
end;

procedure TfrmVisaoUsuarios.pRefresh;
var
  chv :Integer;
begin
  inherited;
  chv := -1;
  with dmUsuarios.cdsUsu do
  begin
    if Active then
      chv := FieldByName('IDCRMUSUARIO').AsInteger;

    Close;
    Open;
    if (chv > -1) then
      Locate('IDCRMUSUARIO', chv, []);
  end;
  pRegistros;
end;

procedure TfrmVisaoUsuarios.pSetaDireitos;
begin
  inherited;
  btnAdicionar.Enabled := frmprincipal.A00102.Enabled;
  btnEditar.Enabled := frmprincipal.A00103.Enabled;
  dmUsuarios.dtsUsu.AutoEdit := frmprincipal.A00103.Enabled;
  btnExcluir.Enabled := frmprincipal.A00104.Enabled;
  panel1.Enabled := frmprincipal.A00105.Enabled;
end;

end.
