unit untCadPerfil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untBaseCad, JvExControls, JvLabel, dxGDIPlusClasses, JvXPCore,
  JvXPButtons, StdCtrls, ComCtrls, ExtCtrls, Mask, DBCtrls, JvExComCtrls,
  JvComCtrls;

type
  TfrmCadPerfil = class(TfrmBaseCad)
    Label3: TLabel;
    edtIdentificador: TDBEdit;
    edtNome: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    edtCodigo: TDBEdit;
    treeAcoes: TJvTreeView;
    Label6: TLabel;
    btnExpand: TJvXPButton;
    btnColapse: TJvXPButton;
    chkExpandido: TCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure treeAcoesClick(Sender: TObject);
    procedure btnExpandClick(Sender: TObject);
    procedure btnColapseClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure pSetaDireitos; override;
    //
    procedure pNovo; override;
    procedure pEditar; override;
    procedure pExcluir; override;
    procedure pRefresh; override;
    //--
    Function fValidaCampos :boolean; override;
    Function pGravar:Boolean; override;
    procedure pCancelar; override;
    //
    procedure pPopulaTree;
    procedure pGrardarDireitos;
  end;

var
  frmCadPerfil: TfrmCadPerfil;

implementation

uses udmPerfil, DB, untPrincipal, ActnList, ActnMan, untFuncoes;

{$R *.dfm}

{ TfrmCadPerfil }

procedure TfrmCadPerfil.btnColapseClick(Sender: TObject);
begin
  inherited;
  pColapseTree(treeAcoes);
end;

procedure TfrmCadPerfil.btnExpandClick(Sender: TObject);
begin
  inherited;
  pExpandTree(treeAcoes);
end;

procedure TfrmCadPerfil.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  frmCadPerfil := nil;
end;

procedure TfrmCadPerfil.FormCreate(Sender: TObject);
begin
  inherited;
  cdsPad := dmPerfil.cdsPerfil;
  pPopulaTree;
end;

procedure TfrmCadPerfil.FormShow(Sender: TObject);
begin
  inherited;
  pRegistros;
end;

function TfrmCadPerfil.fValidaCampos: boolean;
var
  erro :string;
begin
  result := true;
  erro := '';
  if (edtCodigo.Text = '') then
    erro := erro + 'Informar o C�digo;'+#13;
  if (edtNome.Text = '') then
    erro := erro + 'Informar a Descri��o;'+#13;
  if (erro <> '') then
  begin
    MessageDlg(Erro+'Corrija os erros acima e tente novamente.', mtInformation, [mbOK], 0);
    result := false;
  end;
end;

procedure TfrmCadPerfil.pCancelar;
begin
  inherited;
  with dmPerfil.cdsPerfil do
    if (State in [dsInsert, dsEdit]) then
    begin
      Cancel;
      RevertRecord;
    end;
end;

procedure TfrmCadPerfil.pEditar;
begin
  inherited;
  with dmPerfil.cdsPerfil do
    if (FieldByName('IDCRMPERFIL').AsInteger > 0) then
      Edit;
end;

procedure TfrmCadPerfil.pExcluir;
begin
  inherited;
  with dmPerfil.cdsPerfil do
    if (FieldByName('IDCRMPERFIL').AsInteger > 0) then
    begin
      Delete;
      if (ApplyUpdates(0) > 0) then
        raise Exception.Create('Erro ao excluir registro');
    end;
end;

procedure TfrmCadPerfil.pGrardarDireitos;
var
  I, posi :Integer;
  aux, cod :String;
begin
  aux := '';
  for I := 0 to treeAcoes.Items.Count - 1 do
  begin
    if treeAcoes.Checked[treeAcoes.Items.Item[i]] then
    begin
      cod := treeAcoes.Items.Item[i].Text;
      posi := pos('. ', cod);
      if (posi > 0) then
        cod := copy(cod,1,posi);

      aux := aux + cod;
    end;
  end;//for

  dmPerfil.cdsPerfil.FieldByName('MENUS').AsString := aux;
end;

function TfrmCadPerfil.pGravar: Boolean;
begin
  result := false;
  with dmPerfil.cdsPerfil do
  begin
    pGrardarDireitos;
    Post;
    if (ApplyUpdates(0) > 0) then
      raise Exception.Create('Erro ao incluir registro');
    result := true;
  end;
end;

procedure TfrmCadPerfil.pNovo;
begin
  inherited;
  with dmPerfil.cdsPerfil do
    if (State in [dsBrowse]) then
      Append
    else if (State in [dsInsert, dsEdit]) then
      raise Exception.Create('Erro salve o registro antes')
    else //if State in [dsInactive] then
      raise Exception.Create('Erro ao criar registro');
end;

procedure TfrmCadPerfil.pPopulaTree;
var
  Component :TComponent;
  I, posi :integer;
  tn, n :TTreeNode;
  aux :string;
  cod: string;
begin
  with dmPerfil do
  begin
    if not(cdsAcoes.Active) then
      cdsAcoes.CreateDataSet
    else
      cdsAcoes.EmptyDataSet;

    Component := frmprincipal.actPrincipal;
    for I := 0 to TActionManager(Component).ActionCount - 1 do
    begin
      if TActionManager(Component).Actions[i] is TAction then
      begin
        If (TAction(TActionManager(Component).Actions[i]).Category <> 'Basico') then
        begin
          cdsAcoes.Append;
          cdsAcoesIndex.AsInteger := i+1;
          cdsAcoescodigo.AsString := TAction(TActionManager(Component).Actions[i]).Name;
          cdsAcoesTexto.AsString := TAction(TActionManager(Component).Actions[i]).Caption;
          cdsAcoes.Post;
        end;
      end;
    end;

    treeAcoes.Items.Clear;

    cdsAcoes.First;
    while not(cdsAcoes.Eof) do
    begin
      cod := cdsAcoescodigo.AsString;
      aux := cdsAcoesTexto.AsString;

      if (length(cod) = 4) then
      begin
        tn := treeAcoes.Items.Add(nil, cod+'. '+aux);
        n := tn;
      end
      else if (length(cod) = 6) then
      begin
        posi := pos('-',aux);
        if (posi > 0) then
          aux := Copy(aux,posi+2,length(aux)-posi+2);

        n := treeAcoes.Items.AddChild(tn, cod+'. '+aux);
      end;
      //
      if not(cdsPerfil.FieldByName('MENUS').IsNull) then
        if (pos(cod, cdsPerfil.FieldByName('MENUS').AsString) > 0) then
          treeAcoes.Checked[n] := true;

      cdsAcoes.Next;
    end;//while

  end;//with

  if (chkExpandido.Checked) then
    pExpandTree(treeAcoes);
end;

procedure TfrmCadPerfil.pRefresh;
var
  chv :Integer;
begin
  inherited;
  with dmPerfil.cdsPerfil do
  begin
    chv := FieldByName('IDCRMPERFIL').AsInteger;
    Close;
    Open;
    Locate('IDCRMPERFIL', chv, []);
  end;
end;

procedure TfrmCadPerfil.pSetaDireitos;
begin
  inherited;
 btnAdicionar.Enabled := frmprincipal.A00202.Enabled;
  btnEditar.Enabled := frmprincipal.A00203.Enabled;
  dmPerfil.dtsPerfil.AutoEdit := frmprincipal.A00203.Enabled;
  btnExcluir.Enabled := frmprincipal.A00204.Enabled;
  btnConfirmar.Enabled := frmprincipal.A00201.Enabled;
end;

procedure TfrmCadPerfil.treeAcoesClick(Sender: TObject);
begin
  inherited;
  if (dmPerfil.cdsPerfil.State = dsBrowse) and
     (dmPerfil.cdsPerfil.FieldByName('IDCRMPERFIL').AsInteger > 0) then
    dmPerfil.cdsPerfil.Edit;
end;

end.
