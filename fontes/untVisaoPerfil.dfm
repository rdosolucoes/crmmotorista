inherited frmVisaoPerfil: TfrmVisaoPerfil
  Caption = 'CRM - Vis'#227'o de Perfil'
  ClientHeight = 431
  ClientWidth = 767
  ExplicitWidth = 783
  ExplicitHeight = 470
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar1: TStatusBar
    Top = 408
    Width = 767
    ExplicitTop = 408
    ExplicitWidth = 767
  end
  inherited pnlBotoes: TPanel
    Height = 408
    ExplicitHeight = 408
    inherited pnlMovimento: TPanel
      Top = 379
      ExplicitTop = 379
    end
    inherited Panel1: TPanel
      Top = 350
      ExplicitTop = 350
      inherited JvLabel1: TJvLabel
        Height = 27
      end
    end
    inherited PageControl1: TPageControl
      Height = 350
      ExplicitHeight = 350
      inherited tbs1: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 24
        ExplicitWidth = 108
        ExplicitHeight = 322
        inherited Panel2: TPanel
          Height = 322
          ExplicitHeight = 322
        end
      end
    end
  end
  inherited pnlTrabalho: TPanel
    Width = 651
    Height = 408
    ExplicitWidth = 651
    ExplicitHeight = 408
    inherited cxGrid1: TcxGrid
      Width = 651
      Height = 377
      ExplicitWidth = 651
      ExplicitHeight = 377
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmPerfil.dtsPerfil
        object cxGrid1DBTableView1IDCRMPERFIL: TcxGridDBColumn
          DataBinding.FieldName = 'IDCRMPERFIL'
        end
        object cxGrid1DBTableView1CODPERFIL: TcxGridDBColumn
          DataBinding.FieldName = 'CODPERFIL'
          Width = 88
        end
        object cxGrid1DBTableView1NOME: TcxGridDBColumn
          DataBinding.FieldName = 'NOME'
          Width = 386
        end
      end
    end
    inherited Panel3: TPanel
      Width = 651
      ExplicitWidth = 651
    end
  end
end
