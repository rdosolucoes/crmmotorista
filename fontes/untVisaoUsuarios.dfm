inherited frmVisaoUsuarios: TfrmVisaoUsuarios
  Caption = 'CRM - Vis'#227'o de Usu'#225'rios'
  ClientHeight = 400
  ClientWidth = 786
  ExplicitWidth = 794
  ExplicitHeight = 431
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar1: TStatusBar
    Top = 377
    Width = 786
    ExplicitTop = 377
    ExplicitWidth = 786
  end
  inherited pnlBotoes: TPanel
    Height = 377
    ExplicitHeight = 377
    inherited pnlMovimento: TPanel
      Top = 348
      ExplicitTop = 348
    end
    inherited Panel1: TPanel
      Top = 319
      ExplicitTop = 319
      inherited JvLabel1: TJvLabel
        Height = 27
      end
    end
    inherited PageControl1: TPageControl
      Height = 319
      ExplicitHeight = 319
      inherited tbs1: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 24
        ExplicitWidth = 108
        ExplicitHeight = 291
        inherited Panel2: TPanel
          Height = 291
          ExplicitHeight = 291
        end
      end
    end
  end
  inherited pnlTrabalho: TPanel
    Width = 670
    Height = 377
    ExplicitWidth = 670
    ExplicitHeight = 377
    inherited cxGrid1: TcxGrid
      Width = 670
      Height = 346
      ExplicitWidth = 670
      ExplicitHeight = 346
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmUsuarios.dtsUsu
        object cxGrid1DBTableView1IDCRMUSUARIO: TcxGridDBColumn
          DataBinding.FieldName = 'IDCRMUSUARIO'
        end
        object cxGrid1DBTableView1NOME: TcxGridDBColumn
          DataBinding.FieldName = 'NOME'
          Width = 251
        end
        object cxGrid1DBTableView1USERNAME: TcxGridDBColumn
          DataBinding.FieldName = 'USERNAME'
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'EMAIL'
          Width = 211
        end
        object cxGrid1DBTableView1ATIVO: TcxGridDBColumn
          DataBinding.FieldName = 'ATIVO'
          Width = 63
        end
        object cxGrid1DBTableView1MASTERUSER: TcxGridDBColumn
          DataBinding.FieldName = 'MASTERUSER'
          Width = 91
        end
      end
    end
    inherited Panel3: TPanel
      Width = 670
      ExplicitWidth = 670
    end
  end
end
