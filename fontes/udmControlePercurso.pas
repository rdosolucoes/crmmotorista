unit udmControlePercurso;

interface

uses
  System.SysUtils, System.Classes, Data.DBXOracle, Data.FMTBcd, Data.SqlExpr,
  SimpleDS, Datasnap.Provider, Data.DB, Datasnap.DBClient;

type
  TdmControlePercurso = class(TDataModule)
    dsPercurso: TDataSource;
    cdsPercurso: TClientDataSet;
    dspPercurso: TDataSetProvider;
    dsMotorista: TDataSource;
    sdsMotorista: TSimpleDataSet;
    sdsMotoristaCODPARC: TFMTBCDField;
    sdsMotoristaNOMEPARC: TWideStringField;
    sqlPercurso: TSQLDataSet;
    sqlPercursoDATA: TSQLTimeStampField;
    sqlPercursoHORASAIDA: TWideStringField;
    sqlPercursoKMSAIDA: TFMTBCDField;
    sqlPercursoHORACHEGADA: TWideStringField;
    sqlPercursoKMCHEGADA: TFMTBCDField;
    sqlPercursoCODPARC: TFMTBCDField;
    sqlPercursoNOMEPARC: TWideStringField;
    cdsPercursoDATA: TSQLTimeStampField;
    cdsPercursoHORASAIDA: TWideStringField;
    cdsPercursoKMSAIDA: TFMTBCDField;
    cdsPercursoHORACHEGADA: TWideStringField;
    cdsPercursoKMCHEGADA: TFMTBCDField;
    cdsPercursoCODPARC: TFMTBCDField;
    cdsPercursoNOMEPARC: TWideStringField;
    sqlPercursoCODEMP: TFMTBCDField;
    sqlPercursoIDCONTROLE: TFMTBCDField;
    sqlPercursoIDPERCURSO: TFMTBCDField;
    cdsPercursoCODEMP: TFMTBCDField;
    cdsPercursoIDCONTROLE: TFMTBCDField;
    cdsPercursoIDPERCURSO: TFMTBCDField;
    sqlPercursoPERCURSO: TFMTBCDField;
    cdsPercursoPERCURSO: TFMTBCDField;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsPercursoAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FsqlPercurso,FOrder :String;
  end;

var
  dmControlePercurso: TdmControlePercurso;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmControlePercurso.cdsPercursoAfterPost(DataSet: TDataSet);
begin
 cdsPercurso.ApplyUpdates(0);
 cdsPercurso.Refresh();
end;

procedure TdmControlePercurso.DataModuleCreate(Sender: TObject);
begin
  FsqlPercurso :=  ' SELECT PC.CODEMP,PC.IDCONTROLE,PC.IDPERCURSO,C.DATA,TO_CHAR(HORASAIDA,''HH24:MI:SS'') HORASAIDA,KMSAIDA,  ' +
                   ' TO_CHAR(HORACHEGADA,''HH24:MI:SS'') HORACHEGADA,KMCHEGADA,KMCHEGADA - KMSAIDA PERCURSO,C.CODPARC,P.NOMEPARC ' +
                   ' FROM CRMPERCURSOMOTOBOY PC  ' +
                   ' INNER JOIN CRMCONTROLEMOTOBOY C ON C.IDCONTROLE = PC.IDCONTROLE ' +
                   ' INNER JOIN TGFPAR P ON P.CODPARC = C.CODPARC ';


  FOrder :=  ' ORDER BY C.DATA,P.NOMEPARC,HORASAIDA ';
end;


end.
