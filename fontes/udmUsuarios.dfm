object dmUsuarios: TdmUsuarios
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 139
  Width = 347
  object dspUsu: TDataSetProvider
    DataSet = sqlUsu
    Options = [poCascadeDeletes, poUseQuoteChar]
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 16
  end
  object cdsUsu: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspUsu'
    BeforePost = cdsUsuBeforePost
    AfterScroll = cdsUsuAfterScroll
    OnNewRecord = cdsUsuNewRecord
    Left = 122
    Top = 16
    object cdsUsuIDCRMUSUARIO: TFMTBCDField
      Tag = 1
      DisplayLabel = 'ID'
      FieldName = 'IDCRMUSUARIO'
      Required = True
      Precision = 32
    end
    object cdsUsuNOME: TWideStringField
      Tag = 1
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Size = 60
    end
    object cdsUsuUSERNAME: TWideStringField
      Tag = 1
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'USERNAME'
      Size = 15
    end
    object cdsUsuSENHA: TWideStringField
      Tag = 1
      DisplayLabel = 'Senha'
      FieldName = 'SENHA'
    end
    object cdsUsuEMAIL: TWideStringField
      Tag = 1
      DisplayLabel = 'Email'
      FieldName = 'EMAIL'
      FixedChar = True
      Size = 100
    end
    object cdsUsuATIVO: TWideStringField
      Tag = 1
      DisplayLabel = 'Ativo'
      FieldName = 'ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsUsuMASTERUSER: TWideStringField
      FieldName = 'MASTERUSER'
      FixedChar = True
      Size = 1
    end
    object cdsUsuTELEFONE: TWideStringField
      Tag = 1
      DisplayLabel = 'Telefone'
      FieldName = 'TELEFONE'
      Size = 30
    end
    object cdsUsuCELULAR: TWideStringField
      Tag = 1
      DisplayLabel = 'Celular'
      FieldName = 'CELULAR'
      Size = 30
    end
    object cdsUsuCODCOLIGADA: TFMTBCDField
      Tag = 1
      DisplayLabel = 'Empresa'
      FieldName = 'CODCOLIGADA'
      Precision = 32
    end
    object cdsUsuCODPARC: TFMTBCDField
      FieldName = 'CODPARC'
      Precision = 10
      Size = 0
    end
    object cdsUsuPERMITEINCPEDIDO: TFMTBCDField
      FieldName = 'PERMITEINCPEDIDO'
      Precision = 32
    end
  end
  object sqlUsu: TSQLDataSet
    SchemaName = 'dbo'
    CommandText = 'SELECT * FROM '#13#10'CRMUSUARIO'#13#10'ORDER BY IDCRMUSUARIO'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = dmPrincipal.Conn
    Left = 37
    Top = 16
    object sqlUsuIDCRMUSUARIO: TFMTBCDField
      FieldName = 'IDCRMUSUARIO'
      Required = True
      Precision = 32
    end
    object sqlUsuNOME: TWideStringField
      FieldName = 'NOME'
      Size = 60
    end
    object sqlUsuUSERNAME: TWideStringField
      FieldName = 'USERNAME'
      Size = 15
    end
    object sqlUsuSENHA: TWideStringField
      FieldName = 'SENHA'
    end
    object sqlUsuEMAIL: TWideStringField
      FieldName = 'EMAIL'
      FixedChar = True
      Size = 100
    end
    object sqlUsuATIVO: TWideStringField
      FieldName = 'ATIVO'
      FixedChar = True
      Size = 1
    end
    object sqlUsuMASTERUSER: TWideStringField
      FieldName = 'MASTERUSER'
      FixedChar = True
      Size = 1
    end
    object sqlUsuTELEFONE: TWideStringField
      FieldName = 'TELEFONE'
      Size = 30
    end
    object sqlUsuCELULAR: TWideStringField
      FieldName = 'CELULAR'
      Size = 30
    end
    object sqlUsuCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      Precision = 32
    end
    object sqlUsuCODPARC: TFMTBCDField
      FieldName = 'CODPARC'
      Precision = 10
      Size = 0
    end
    object sqlUsuPERMITEINCPEDIDO: TFMTBCDField
      FieldName = 'PERMITEINCPEDIDO'
      Precision = 32
    end
  end
  object dtsUsu: TDataSource
    DataSet = cdsUsu
    OnStateChange = dtsUsuStateChange
    Left = 166
    Top = 16
  end
  object sqlEmp: TSimpleDataSet
    Aggregates = <>
    Connection = dmPrincipal.Conn
    DataSet.CommandText = 
      'SELECT EMP1.CODEMP,EMP1.NOMEFANTASIA,EMP1.RAZAOSOCIAL'#13#10'FROM TGFE' +
      'MP EMP, TSIEMP EMP1'#13#10'WHERE EMP.CODEMP = EMP1.CODEMP'#13#10'ORDER BY EM' +
      'P1.CODEMP'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 32
    Top = 72
    object sqlEmpCODEMP: TFMTBCDField
      FieldName = 'CODEMP'
      Required = True
      Precision = 5
      Size = 0
    end
    object sqlEmpNOMEFANTASIA: TWideStringField
      FieldName = 'NOMEFANTASIA'
      Size = 40
    end
    object sqlEmpRAZAOSOCIAL: TWideStringField
      FieldName = 'RAZAOSOCIAL'
      Size = 40
    end
  end
  object dtsEmp: TDataSource
    DataSet = sqlEmp
    OnStateChange = dtsUsuStateChange
    Left = 94
    Top = 72
  end
  object sdsMotorista: TSimpleDataSet
    Aggregates = <>
    Connection = dmPrincipal.Conn
    DataSet.CommandText = 
      'SELECT CODPARC, NOMEPARC, AD_VEICULO, AD_REPASSE_ENTREGA'#13#10'FROM T' +
      'GFPAR'#13#10'WHERE '#13#10'   (MOTORISTA = '#39'S'#39')  OR'#13#10'    (AD_MOTOBOY = '#39'S'#39')'#13 +
      #10'AND ATIVO = '#39'S'#39
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 160
    Top = 72
  end
  object dsMotorista: TDataSource
    DataSet = sdsMotorista
    Left = 240
    Top = 70
  end
end
