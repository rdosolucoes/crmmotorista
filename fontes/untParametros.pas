unit untParametros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBase, Vcl.ComCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters,dxSkinscxPCPainter, Vcl.StdCtrls,
  Vcl.DBCtrls, cxPC, dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons,
  Vcl.ExtCtrls,DB, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxContainer, cxGroupBox,
  Vcl.Mask, JvDBLookup, JvExMask, JvToolEdit, JvDBControls, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  dxSkiniMaginary, dxSkinSilver, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxBarBuiltInMenu;

type
  TfrmParametros = class(TfrmBase)
    Panel2: TPanel;
    Label2: TLabel;
    btnConfirmar: TJvXPButton;
    btnCancelar: TJvXPButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    Label11: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    DBEdit6: TDBEdit;
    Label14: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit9: TDBEdit;
    procedure btnConfirmarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmParametros: TfrmParametros;

implementation

{$R *.dfm}

uses udmPrincipal,untFuncoes, udmParametros;

procedure TfrmParametros.btnCancelarClick(Sender: TObject);
begin
  inherited;
   if dmParametros.cdsParametros.State in [dsInsert,dsEdit]  then
    dmParametros.cdsParametros.Cancel;
end;

procedure TfrmParametros.btnConfirmarClick(Sender: TObject);
begin
  inherited;
  if dmParametros.cdsParametros.State in [dsInsert,dsEdit]  then
    dmParametros.cdsParametros.Post;

  dmPrincipal.cdsParametros.Close;
  dmPrincipal.cdsParametros.Params[0].Value := fGetColigada;
  dmPrincipal.cdsParametros.Open;
  close;
end;

procedure TfrmParametros.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  frmParametros := nil;
  FreeAndNil(dmParametros);
end;

procedure TfrmParametros.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmParametros,dmParametros);

  With dmParametros do
  begin
    cdsParametros.Close;
    cdsParametros.Params[0].Value := fGetColigada;
    cdsParametros.Open;
  end;

end;

procedure TfrmParametros.FormPaint(Sender: TObject);
begin
  inherited;
  self.Top := fAjustaTop(self.Height);
end;



end.
