unit untRastreaPedidos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBase, Vcl.ComCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinSilver, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.StdCtrls, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, dxGDIPlusClasses, JvExControls, JvXPCore,
  JvXPButtons, Vcl.ExtCtrls, cxContainer, cxTextEdit, cxCurrencyEdit, Vcl.Menus,
  cxButtons, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator;

type
  TfrmRastreaPedido = class(TfrmBase)
    Panel1: TPanel;
    Label2: TLabel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    edtPedido: TEdit;
    btnLocalizar: TJvXPButton;
    cxGrid1DBTableView1SITUACAO: TcxGridDBColumn;
    cxGrid1DBTableView1NUMNOTA: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEPARC: TcxGridDBColumn;
    cxGrid1DBTableView1DTMOV: TcxGridDBColumn;
    procedure btnLocalizarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure edtPedidoKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRastreaPedido: TfrmRastreaPedido;

implementation

{$R *.dfm}

uses udmRastreaPedido,untFuncoes,udmPrincipal;

procedure TfrmRastreaPedido.btnLocalizarClick(Sender: TObject);
begin
  inherited;
  dmRastreaPedido.cdsPedido.Close;

 // dmRastreaPedido.SQLPedido.Parambyname('CODTIPOPER').AsInteger := dmPrincipal.cdsParametros.FieldByName('CODTIPOPER').AsInteger ;

  if (edtPedido.Text <> '') then
    dmRastreaPedido.SQLPedido.Parambyname('NUMNOTA').AsInteger := StrToInt(edtPedido.text);

  dmRastreaPedido.SQLPedido.Parambyname('CODEMP').AsInteger := fGetColigada ;
  dmRastreaPedido.cdsPedido.Open;

  if dmRastreaPedido.cdsPedido.IsEmpty then
    Application.MessageBox('Pedido n�o Encontrado.','SANKHYA',mb_ok + MB_ICONEXCLAMATION);
end;

procedure TfrmRastreaPedido.edtPedidoKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if key = #13 then
    btnLocalizarClick(nil);

  if ((key in ['0'..'9'] = false) and (word(key) <> vk_back)) then
    key := #0;
end;

procedure TfrmRastreaPedido.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  frmRastreaPedido := nil;
  FreeAndNil(dmRastreaPedido);
end;

procedure TfrmRastreaPedido.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmRastreaPedido,dmRastreaPedido);
end;

end.
