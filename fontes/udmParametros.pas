unit udmParametros;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.SqlExpr, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, SimpleDS,Dialogs;

type
  TdmParametros = class(TDataModule)
    dsParametros: TDataSource;
    dspParametros: TDataSetProvider;
    cdsParametros: TClientDataSet;
    sqlParametros: TSQLDataSet;
    sqlParametrosCODCOLIGADA: TFMTBCDField;
    sqlParametrosENTERCOMOTAB: TFMTBCDField;
    sqlParametrosUSARFILTROANTES: TFMTBCDField;
    sqlParametrosTEMPOSEPARA: TFMTBCDField;
    sqlParametrosTEMPOCONFERE: TFMTBCDField;
    sqlParametrosTEMPOLIBERA: TFMTBCDField;
    sqlParametrosTEMPOENTREGA: TFMTBCDField;
    sqlParametrosDIASIMPORT: TFMTBCDField;
    sqlParametrosLIMITEREGISTROS: TFMTBCDField;
    sqlParametrosCODTIPOPER: TFMTBCDField;
    sqlParametrosCODTIPOPERTRANSF: TFMTBCDField;
    sqlParametrosCODTIPOSPER: TWideStringField;
    sqlParametrosCODTIPOSPERTODOS: TWideStringField;
    cdsParametrosCODCOLIGADA: TFMTBCDField;
    cdsParametrosENTERCOMOTAB: TFMTBCDField;
    cdsParametrosUSARFILTROANTES: TFMTBCDField;
    cdsParametrosTEMPOSEPARA: TFMTBCDField;
    cdsParametrosTEMPOCONFERE: TFMTBCDField;
    cdsParametrosTEMPOLIBERA: TFMTBCDField;
    cdsParametrosTEMPOENTREGA: TFMTBCDField;
    cdsParametrosDIASIMPORT: TFMTBCDField;
    cdsParametrosLIMITEREGISTROS: TFMTBCDField;
    cdsParametrosCODTIPOPER: TFMTBCDField;
    cdsParametrosCODTIPOPERTRANSF: TFMTBCDField;
    cdsParametrosCODTIPOSPER: TWideStringField;
    cdsParametrosCODTIPOSPERTODOS: TWideStringField;
    procedure cdsParametrosNewRecord(DataSet: TDataSet);
    procedure cdsParametrosAfterPost(DataSet: TDataSet);
    procedure cdsParametrosSENHALIBERACAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsParametrosSENHAUSUARIOBANCOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsParametrosSENHAUSUARIOBANCOSetText(Sender: TField;
      const Text: string);
    procedure cdsParametrosSENHALIBERACAOSetText(Sender: TField;
      const Text: string);
    procedure cdsParametrosReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsParametrosFTPPASSWORDGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsParametrosFTPPASSWORDSetText(Sender: TField;
      const Text: string);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmParametros: TdmParametros;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}
uses untFuncoes, udmPrincipal;

procedure TdmParametros.cdsParametrosAfterPost(DataSet: TDataSet);
begin
  cdsParametros.ApplyUpdates(0);
end;

procedure TdmParametros.cdsParametrosFTPPASSWORDGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := DeCripto(Sender.AsString);
end;

procedure TdmParametros.cdsParametrosFTPPASSWORDSetText(Sender: TField;
  const Text: string);
begin
   Sender.AsString := Cripto(Text);
end;

procedure TdmParametros.cdsParametrosNewRecord(DataSet: TDataSet);
begin
  cdsParametros.fieldbyname('CODCOLIGADA').Value := 0;
  cdsParametros.fieldbyname('ENTERCOMOTAB').Value := 0;
  cdsParametros.fieldbyname('USARFILTROANTES').Value := 0;
end;

procedure TdmParametros.cdsParametrosReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind;
  var Action: TReconcileAction);
begin
  showmessage(E.Message);
end;

procedure TdmParametros.cdsParametrosSENHALIBERACAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
   Text := DeCripto(Sender.AsString);
end;

procedure TdmParametros.cdsParametrosSENHALIBERACAOSetText(Sender: TField;
  const Text: string);
begin
  Sender.AsString := Cripto(Text);
end;

procedure TdmParametros.cdsParametrosSENHAUSUARIOBANCOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
   Text := DeCripto(Sender.AsString);
end;

procedure TdmParametros.cdsParametrosSENHAUSUARIOBANCOSetText(Sender: TField;
  const Text: string);
begin
  Sender.AsString := Cripto(Text);
end;

end.
