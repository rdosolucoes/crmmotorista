unit untReprovaConf;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseConfirmacao, dxGDIPlusClasses,
  JvExControls, JvXPCore, JvXPButtons, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TfrmReprovaConf = class(TfrmBaseConfirmacao)
    GroupBox1: TGroupBox;
    cbSetorA: TCheckBox;
    cbSetorB: TCheckBox;
    cbSetorC: TCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
    sID :String;
    Function fAcaoOk:boolean; override;
    Function fAcaoCancelar:boolean; override;
    constructor Create(Awner: TComponent;sForm: TForm); reintroduce; overload;
  end;

var
  frmReprovaConf: TfrmReprovaConf;
  Form :TForm;

implementation

{$R *.dfm}

uses untFuncoes,untConferencia, untCadRomaneio;

{ TfrmReprovaConf }

constructor TfrmReprovaConf.Create(Awner: TComponent;sForm: TForm);
begin
  Inherited Create(Awner);
  Form := sForm;
end;


function TfrmReprovaConf.fAcaoCancelar: boolean;
begin
  result := true;
  ModalResult := mrNo;
end;

function TfrmReprovaConf.fAcaoOk: boolean;
var
  sSQL,Erro :String;
begin
  result := true;
  ModalResult := mrYes;

  if Form = frmConferencia then
  begin
    frmConferencia.bSetorA := cbSetorA.Checked;
    frmConferencia.bSetorB := cbSetorB.Checked;
    frmConferencia.bSetorC := cbSetorC.Checked;
  end
  else
  if Form = frmCadRomaneio then
  begin
    frmCadRomaneio.bSetorA := cbSetorA.Checked;
    frmCadRomaneio.bSetorB := cbSetorB.Checked;
    frmCadRomaneio.bSetorC := cbSetorC.Checked;
  end;


  if not(cbSetorA.Checked) and not(cbSetorB.Checked) and not(cbSetorC.Checked) then
  begin
    Application.MessageBox('Escolha um Setor Para Reprovação.','SANKHYA',mb_ok + MB_ICONWARNING);
    result := false;
    ModalResult := mrNo;
  end;
end;

end.
