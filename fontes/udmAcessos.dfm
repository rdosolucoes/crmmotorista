object dmAcessos: TdmAcessos
  OldCreateOrder = False
  Height = 217
  Width = 269
  object dspAcessos: TDataSetProvider
    DataSet = sqlAcessos
    Left = 87
    Top = 10
  end
  object cdsAcessos: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'PCOLIG'
        ParamType = ptInput
      end>
    ProviderName = 'dspAcessos'
    Left = 152
    Top = 10
    object cdsAcessosCODPERFIL: TWideStringField
      Tag = 1
      DisplayLabel = 'C'#243'digo'
      FieldName = 'CODPERFIL'
      Size = 10
    end
    object cdsAcessosNOMEUSU: TWideStringField
      Tag = 1
      DisplayLabel = 'Nome Usu'#225'rio'
      FieldName = 'NOMEUSU'
      Size = 60
    end
    object cdsAcessosNOME: TWideStringField
      Tag = 1
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Size = 50
    end
    object cdsAcessosCOLIG: TFMTBCDField
      FieldName = 'COLIG'
      Required = True
      Precision = 32
    end
    object cdsAcessosPERF: TFMTBCDField
      FieldName = 'PERF'
      Required = True
      Precision = 32
    end
    object cdsAcessosIDCRMUSUARIO: TFMTBCDField
      FieldName = 'IDCRMUSUARIO'
      Required = True
      Precision = 32
    end
    object cdsAcessosCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      Required = True
      Precision = 32
    end
    object cdsAcessosIDCRMPERFIL: TFMTBCDField
      FieldName = 'IDCRMPERFIL'
      Required = True
      Precision = 32
    end
  end
  object sqlAcessos: TSQLDataSet
    SchemaName = 'dbo'
    CommandText = 
      'SELECT P.CODCOLIGADA AS COLIG,'#13#10'       P.IDCRMPERFIL AS PERF,'#13#10' ' +
      '      P.CODPERFIL,'#13#10'       P.NOME,'#13#10'       A.IDCRMUSUARIO,'#13#10'    ' +
      '   A.CODCOLIGADA,'#13#10'       A.IDCRMPERFIL,'#13#10'       U.NOME AS NOMEU' +
      'SU'#13#10'  FROM CRMPERFIL P LEFT OUTER JOIN CRMACESSO A ON((P.CODCOLI' +
      'GADA = A.CODCOLIGADA OR P.CODCOLIGADA = 0) AND P.IDCRMPERFIL = A' +
      '.IDCRMPERFIL)'#13#10'       LEFT OUTER JOIN CRMUSUARIO U ON(A.IDCRMUSU' +
      'ARIO= U.IDCRMUSUARIO)'#13#10' WHERE (P.CODCOLIGADA = :PCOLIG OR P.CODC' +
      'OLIGADA = 0)'#13#10'   AND A.IDCRMPERFIL IS NOT NULL'#13#10' ORDER BY  P.COD' +
      'PERFIL'
    MaxBlobSize = 1
    Params = <
      item
        DataType = ftInteger
        Name = 'PCOLIG'
        ParamType = ptInput
      end>
    SQLConnection = dmPrincipal.Conn
    Left = 27
    Top = 10
    object sqlAcessosCOLIG: TFMTBCDField
      FieldName = 'COLIG'
      Required = True
      Precision = 32
    end
    object sqlAcessosPERF: TFMTBCDField
      FieldName = 'PERF'
      Required = True
      Precision = 32
    end
    object sqlAcessosCODPERFIL: TWideStringField
      FieldName = 'CODPERFIL'
      Size = 10
    end
    object sqlAcessosNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object sqlAcessosIDCRMUSUARIO: TFMTBCDField
      FieldName = 'IDCRMUSUARIO'
      Required = True
      Precision = 32
    end
    object sqlAcessosCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      Required = True
      Precision = 32
    end
    object sqlAcessosIDCRMPERFIL: TFMTBCDField
      FieldName = 'IDCRMPERFIL'
      Required = True
      Precision = 32
    end
    object sqlAcessosNOMEUSU: TWideStringField
      FieldName = 'NOMEUSU'
      Size = 60
    end
  end
  object dtsAcessos: TDataSource
    AutoEdit = False
    DataSet = cdsAcessos
    Left = 217
    Top = 10
  end
  object dspUsu: TDataSetProvider
    DataSet = sqlUsu
    UpdateMode = upWhereKeyOnly
    Left = 80
    Top = 100
  end
  object cdsUsu: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspUsu'
    AfterScroll = cdsUsuAfterScroll
    Left = 146
    Top = 100
    object cdsUsuATIVO: TWideStringField
      FieldName = 'ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsUsuIDCRMUSUARIO: TFMTBCDField
      FieldName = 'IDCRMUSUARIO'
      Required = True
      Precision = 32
    end
    object cdsUsuMASTERUSER: TWideStringField
      FieldName = 'MASTERUSER'
      FixedChar = True
      Size = 1
    end
    object cdsUsuNOME: TWideStringField
      FieldName = 'NOME'
      Size = 60
    end
    object cdsUsuSENHA: TWideStringField
      FieldName = 'SENHA'
    end
    object cdsUsuUSERNAME: TWideStringField
      FieldName = 'USERNAME'
      Size = 15
    end
  end
  object sqlUsu: TSQLDataSet
    SchemaName = 'dbo'
    CommandText = 
      'select ATIVO,'#13#10'IDCRMUSUARIO,'#13#10'MASTERUSER, '#13#10'NOME, '#13#10'SENHA,'#13#10'USER' +
      'NAME'#13#10'from CRMUSUARIO'#13#10'where masteruser = '#39'N'#39#13#10'and ativo = '#39'S'#39
    MaxBlobSize = -1
    Params = <>
    SQLConnection = dmPrincipal.Conn
    Left = 21
    Top = 100
    object sqlUsuATIVO: TWideStringField
      FieldName = 'ATIVO'
      FixedChar = True
      Size = 1
    end
    object sqlUsuIDCRMUSUARIO: TFMTBCDField
      FieldName = 'IDCRMUSUARIO'
      Required = True
      Precision = 32
    end
    object sqlUsuMASTERUSER: TWideStringField
      FieldName = 'MASTERUSER'
      FixedChar = True
      Size = 1
    end
    object sqlUsuNOME: TWideStringField
      FieldName = 'NOME'
      Size = 60
    end
    object sqlUsuSENHA: TWideStringField
      FieldName = 'SENHA'
    end
    object sqlUsuUSERNAME: TWideStringField
      FieldName = 'USERNAME'
      Size = 15
    end
  end
  object dtsUsu: TDataSource
    AutoEdit = False
    DataSet = cdsUsu
    Left = 210
    Top = 104
  end
  object dspAcess: TDataSetProvider
    DataSet = sqlAcess
    Left = 79
    Top = 150
  end
  object cdsAcess: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'PUSU'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'PCOLIG'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'PUSU'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'PCOLIG'
        ParamType = ptInput
      end>
    ProviderName = 'dspAcess'
    BeforePost = cdsAcessBeforePost
    OnNewRecord = cdsAcessNewRecord
    Left = 144
    Top = 150
    object cdsAcessCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      Precision = 32
    end
    object cdsAcessCODPERFIL: TWideStringField
      FieldName = 'CODPERFIL'
      Size = 10
    end
    object cdsAcessIDCRMPERFIL: TFMTBCDField
      FieldName = 'IDCRMPERFIL'
      Precision = 32
    end
    object cdsAcessNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object cdsAcessSELECIONADO: TWideStringField
      FieldName = 'SELECIONADO'
      FixedChar = True
      Size = 1
    end
  end
  object sqlAcess: TSQLDataSet
    SchemaName = 'dbo'
    CommandText = 
      'SELECT G.CODCOLIGADA,'#13#10'       G.CODPERFIL,'#13#10'       G.IDCRMPERFIL' +
      ','#13#10'       G.NOME,'#13#10'       '#39'S'#39' SELECIONADO'#13#10'FROM CRMPERFIL G'#13#10'WHE' +
      'RE G.IDCRMPERFIL IN(SELECT A.IDCRMPERFIL FROM CRMACESSO A WHERE ' +
      'A.IDCRMUSUARIO = :PUSU)'#13#10'     AND (G.CODCOLIGADA = :PCOLIG OR G.' +
      'CODCOLIGADA = 0)'#13#10#13#10'UNION ALL'#13#10#13#10'SELECT G.CODCOLIGADA,'#13#10'       G' +
      '.CODPERFIL,'#13#10'       G.IDCRMPERFIL,'#13#10'       G.NOME,'#13#10'       '#39'N'#39' S' +
      'ELECIONADO'#13#10'FROM CRMPERFIL G'#13#10'WHERE G.IDCRMPERFIL NOT IN(SELECT ' +
      'A.IDCRMPERFIL FROM CRMACESSO A WHERE A.IDCRMUSUARIO = :PUSU)'#13#10'  ' +
      '   AND ( G.CODCOLIGADA = :PCOLIG OR G.CODCOLIGADA = 0)'
    MaxBlobSize = 1
    Params = <
      item
        DataType = ftInteger
        Name = 'PUSU'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'PCOLIG'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'PUSU'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'PCOLIG'
        ParamType = ptInput
      end>
    SQLConnection = dmPrincipal.Conn
    Left = 19
    Top = 150
    object sqlAcessCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      Precision = 32
    end
    object sqlAcessCODPERFIL: TWideStringField
      FieldName = 'CODPERFIL'
      Size = 10
    end
    object sqlAcessIDCRMPERFIL: TFMTBCDField
      FieldName = 'IDCRMPERFIL'
      Precision = 32
    end
    object sqlAcessNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object sqlAcessSELECIONADO: TWideStringField
      FieldName = 'SELECIONADO'
      FixedChar = True
      Size = 1
    end
  end
  object dtsAcess: TDataSource
    DataSet = cdsAcess
    Left = 209
    Top = 150
  end
end
