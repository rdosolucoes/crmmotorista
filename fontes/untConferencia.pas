unit untConferencia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseVisao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinSilver,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  Data.DB, cxDBData, Vcl.StdCtrls, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ComCtrls, dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons, JvLabel,
  Vcl.ExtCtrls, cxCheckBox, cxImageComboBox,SQLExpr,StrUtils, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmConferencia = class(TfrmBaseVisao)
    btnAprovar: TJvXPButton;
    btnReprovar: TJvXPButton;
    cxGrid1DBTableView1NUMNOTA: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEPARC: TcxGridDBColumn;
    cxGrid1DBTableView1DATASEPARACAO: TcxGridDBColumn;
    cxGrid1DBTableView1ATRASO: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1SEL: TcxGridDBColumn;  procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAprovarClick(Sender: TObject);
    procedure btnReprovarClick(Sender: TObject);
    procedure cxGrid1DBTableView1ATRASOGetCellHint(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
      var AHintText: TCaption; var AIsHintMultiLine: Boolean;
      var AHintTextRect: TRect);
  private
    { Private declarations }
  public
    { Public declarations }
    bSetorA,bSetorB,bSetorC :Boolean;
    procedure pSetaDireitos; override;
    procedure pRefresh; override;
  end;

var
  frmConferencia: TfrmConferencia;

implementation

{$R *.dfm}

uses udmConferencia,untFuncoes,untPrincipal,untLogin, untReprovaConf,udmPrincipal;

procedure TfrmConferencia.btnAprovarClick(Sender: TObject);
var result :integer;
    sSql,sID :String;
begin
  inherited;
  //Application.CreateForm(TfrmLogin,frmLogin);
  //result := frmLogin.ShowModal;

  //if result = 6 then
  //begin
    With dmConferencia do
    begin
      cdsConferencia.DisableControls;
      cdsConferencia.First;

      while not cdsConferencia.Eof do
      begin
        if (cdsConferenciaSEL.AsInteger <> 1) or (cdsConferenciaDATASEPARACAO.IsNull) then
        begin
          cdsConferencia.Next;
          Continue;
        end;

        sID := cdsConferenciaNUNOTA.AsString;
        sSQL := 'UPDATE CRMSEPARACAO SET STATUS = ''A'', DATAAPROVACAO = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                ',''DD/MM/YYYY HH24:MI:SS'') WHERE NUNOTA = ' + sID +  ' AND CODEMP = ' + IntToStr(fGetColigada);
        SQLExecute(sSQL);
        AtualizaStatusPedido(StrToInt(sID),'Roteirização');
        cdsConferencia.Next;
      end;
      cdsConferencia.First;
      cdsConferencia.EnableControls;
    end;
    btnAtualizarClick(nil);
  //end;
end;

procedure TfrmConferencia.btnReprovarClick(Sender: TObject);
var result :integer;
    sSql,sID :String;
begin
  inherited;
  //Application.CreateForm(TfrmLogin,frmLogin);
  //result := frmLogin.ShowModal;

  //if result = 6 then
  //begin
    frmReprovaConf := TfrmReprovaConf.Create(frmConferencia,frmConferencia);
    result := frmReprovaConf.ShowModal;

    if result = 6 then
    begin
      With dmConferencia do
      begin
        cdsConferencia.DisableControls;
        cdsConferencia.First;

        while not cdsConferencia.Eof do
        begin
          if (cdsConferenciaSEL.AsInteger <> 1) then
          begin
            cdsConferencia.Next;
            Continue;
          end;

          sID := cdsConferenciaNUNOTA.AsString;
          sSQL := 'UPDATE CRMSEPARACAO SET STATUS = ''R'', DATAREPROVACAO = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                  ',''DD/MM/YYYY HH24:MI:SS'') WHERE NUNOTA = ' + sID + ' AND CODEMP = ' + IntToStr(fGetColigada);
          SQLExecute(sSQL);

          if bSetorA then
          begin
            sSQL := 'UPDATE CRMSEPARACAO SET SETORA = 2 WHERE NUNOTA = ' + sID + ' AND SETORA = 1  AND CODEMP = ' + IntToStr(fGetColigada);
            SQLExecute(sSQL);
          end;

          if bSetorB then
          begin
            sSQL := 'UPDATE CRMSEPARACAO SET SETORB = 2 WHERE NUNOTA = ' + sID + ' AND SETORB = 1 AND CODEMP = ' + IntToStr(fGetColigada);
            SQLExecute(sSQL);
          end;

          if bSetorC then
          begin
            sSQL := 'UPDATE CRMSEPARACAO SET SETORC = 2 WHERE NUNOTA = ' + sID + ' AND SETORC = 1 AND CODEMP = ' + IntToStr(fGetColigada);
            SQLExecute(sSQL);
          end;
          cdsConferencia.Next;
        end;
        cdsConferencia.First;
        cdsConferencia.EnableControls;
      end;
       btnAtualizarClick(nil);
    end;
  //end;
end;

procedure TfrmConferencia.cxGrid1DBTableView1ATRASOGetCellHint(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
  var AHintText: TCaption; var AIsHintMultiLine: Boolean;
  var AHintTextRect: TRect);
begin
  inherited;
  AHintText := VarToStr(ARecord.DisplayTexts[Sender.Index]);
  AIsHintMultiLine := True;
end;

procedure TfrmConferencia.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  frmConferencia := nil;
  FreeAndNil(dmConferencia);
end;

procedure TfrmConferencia.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmConferencia,dmConferencia);
  cdsPad := dmConferencia.cdsConferencia;
  sqlPad := dmConferencia.sqlConferencia;
  Tabela := 'TGFCAB';
end;

procedure TfrmConferencia.FormShow(Sender: TObject);
begin
  inherited;
  pRefresh;
  dmPrincipal.cdsParametros.Close;
  dmPrincipal.cdsParametros.Params[0].AsInteger := fGetColigada;
  dmPrincipal.cdsParametros.Open;
end;

procedure TfrmConferencia.pRefresh;
var
  chv :Integer;
begin
  inherited;
  chv := -1;
  with dmConferencia.cdsConferencia do
  begin
    if Active then
      chv := FieldByName('NUNOTA').AsInteger;

    Close;
   // dmConferencia.sqlConferencia.ParamByName('CODTIPOPER').AsString := '4,12';
   // dmConferencia.sqlConferencia.ParamByName('CODTIPOPERTRANSF').AsInteger := dmPrincipal.cdsParametros.FieldByName('CODTIPOPERTRANSF').AsInteger;
    dmConferencia.sqlConferencia.ParamByName('CODEMP').AsInteger := fGetColigada;
      Open;
    if (chv > -1) then
      Locate('NUNOTA', chv, []);
  end;
  pRegistros;
end;

procedure TfrmConferencia.pSetaDireitos;
begin
  inherited;
  btnAprovar.Enabled := frmprincipal.A01701.Enabled;
  btnReprovar.Enabled := frmprincipal.A01702.Enabled;
end;

end.
