unit untBaseConfirmacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untBase, ComCtrls, dxGDIPlusClasses, JvExControls, JvXPCore,
  JvXPButtons, StdCtrls, ExtCtrls;

type
  TfrmBaseConfirmacao = class(TfrmBase)
    pnlBotoes: TPanel;
    PageControl1: TPageControl;
    tbs2: TTabSheet;
    Panel2: TPanel;
    Label2: TLabel;
    btnConfirmar: TJvXPButton;
    btnCancelar: TJvXPButton;
    procedure FormShow(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    podefechar :boolean;
    Function fAcaoOk:boolean; virtual; abstract;
    Function fAcaoCancelar:boolean; virtual; abstract;
  end;

var
  frmBaseConfirmacao: TfrmBaseConfirmacao;

implementation

{$R *.dfm}

procedure TfrmBaseConfirmacao.btnCancelarClick(Sender: TObject);
begin
  inherited;
  if fAcaoCancelar then
  begin
    ModalResult := mrNo;
    podefechar := true;
  end;
end;

procedure TfrmBaseConfirmacao.btnConfirmarClick(Sender: TObject);
begin
  inherited;
  if fAcaoOk then
  begin
    ModalResult := mrYes;
    podefechar := true;
  end;
end;

procedure TfrmBaseConfirmacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if podefechar then
    Action := caFree
  else
    Action := caNone;
end;

procedure TfrmBaseConfirmacao.FormCreate(Sender: TObject);
begin
  inherited;
  podefechar := false;
end;

procedure TfrmBaseConfirmacao.FormShow(Sender: TObject);
begin
  inherited;
  StatusBar1.Panels.Items[0].Text := '';
end;

end.
