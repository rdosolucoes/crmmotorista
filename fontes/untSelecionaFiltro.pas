unit untSelecionaFiltro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBase, Vcl.ComCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, Data.DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, JvGIF,
  dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons, Vcl.ExtCtrls,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid,JvJVCLUtils,DBClient,SqlExpr,
  dxSkiniMaginary, dxSkinSilver, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, cxNavigator;

type
  TfrmSelecionaFiltro = class(TfrmBase)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Panel1: TPanel;
    btnAdicionar: TJvXPButton;
    btnEditar: TJvXPButton;
    btnExcluir: TJvXPButton;
    btnAtualizar: TJvXPButton;
    btnRenomear: TJvXPButton;
    btTodos: TJvXPButton;
    btnok: TJvXPButton;
    JvXPButton4: TJvXPButton;
    cxGrid1DBTableView1DESCRICAO: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    procedure btnAdicionarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure JvXPButton4Click(Sender: TObject);
    procedure btnokClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btTodosClick(Sender: TObject);
    procedure btnRenomearClick(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
    procedure pRegistros;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1UpdateEdit(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit);
    procedure VerificaTotalRegistros(Filtro :Boolean);
  private
    { Private declarations }
    procedure AbreFiltro;
  public
    cdsPad :TClientDataSet;
    SQLPad :TSQLDataSet;
    ScriptPad :String;
    Tabela :String;
  end;

var
  frmSelecionaFiltro: TfrmSelecionaFiltro;

implementation

{$R *.dfm}

uses udmFiltros,untFiltro,untBaseVisao,untFuncoes,udmPrincipal;

procedure TfrmSelecionaFiltro.AbreFiltro;
begin
  Application.CreateForm(TfrmFiltro,frmfiltro);
  frmFiltro.cxDBFilterControl1.DataSet := cdsPad;
  frmFiltro.Tabela := Tabela;
  frmFiltro.ShowModal;
end;

procedure TfrmSelecionaFiltro.btnAdicionarClick(Sender: TObject);
var
 Descricao :String;
begin
  inherited;
  Descricao := 'NOVO FILTRO';
  if InputQuery ('SANKHYA', 'Nome do Filtro',Descricao) then
  begin
    if dmfiltros.cdsfiltro.State in [dsEdit,dsInsert] then
      dmFiltros.cdsfiltro.Cancel;

    dmFiltros.cdsFiltro.Append;
    dmfiltros.cdsFiltroDESCRICAO.Value := Descricao;
    dmFiltros.cdsFiltroFORMULARIO.Value := dmFiltros.NomeForm;
    dmFiltros.cdsFiltro.Post;
    AbreFiltro;
  end;
end;

procedure TfrmSelecionaFiltro.btnAtualizarClick(Sender: TObject);
var
  Layout :TBytes;
  FiltroTxt,Descricao :String;
begin
  inherited;
  Descricao := 'NOVO FILTRO';
  if InputQuery ('SANKHYA', 'Nome do Filtro',Descricao) then
  begin
    if dmfiltros.cdsfiltro.State in [dsEdit,dsInsert] then
      dmFiltros.cdsfiltro.Cancel;

    Layout := dmFiltros.cdsFiltroLAYOUT.Value;
    FiltroTxt := dmFiltros.cdsFiltroFILTROTXT.Value;
    dmFiltros.cdsFiltro.Append;
    dmfiltros.cdsFiltroDESCRICAO.Value := Descricao;
    dmFiltros.cdsFiltroFORMULARIO.Value := dmFiltros.NomeForm;
    dmFiltros.cdsFiltroLAYOUT.Value := Layout;
    dmFiltros.cdsFiltroFILTROTXT.Value := FiltroTxt;
    dmFiltros.cdsFiltro.Post;
  end;
end;

procedure TfrmSelecionaFiltro.btnEditarClick(Sender: TObject);
begin
  AbreFiltro;
end;

procedure TfrmSelecionaFiltro.btnExcluirClick(Sender: TObject);
begin
  inherited;
  if Application.MessageBox('Voc� tem certeza que deseja excluir este filtro ?','SANKHYA',mb_yesno + mb_iconquestion) = idyes then
    dmFiltros.cdsFiltro.Delete;
end;

procedure TfrmSelecionaFiltro.btnRenomearClick(Sender: TObject);
var
  Descricao :String;
begin
  inherited;
  dmFiltros.cdsFiltro.Edit;
  Descricao := dmFiltros.cdsFiltroDESCRICAO.AsString;
  InputQuery ('SANKHYA', 'Nome do Filtro',Descricao);
  dmFiltros.cdsFiltroDESCRICAO.AsString := Descricao;
  dmFiltros.cdsFiltro.Post;
end;

procedure TfrmSelecionaFiltro.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
  btnokClick(nil);
end;

procedure TfrmSelecionaFiltro.cxGrid1DBTableView1UpdateEdit(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit);
begin
  inherited;
   cxGrid1DBTableView1DESCRICAO.Editing := False;
end;

procedure TfrmSelecionaFiltro.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  frmSelecionaFiltro := nil;
  dmFiltros.sqlFiltro.Close;
end;

procedure TfrmSelecionaFiltro.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmFiltros,dmfiltros);
end;


procedure TfrmSelecionaFiltro.btTodosClick(Sender: TObject);
begin
  inherited;
 // VerificaTotalRegistros(False);
  SQLPad.CommandText :=  ScriptPad;
  close;
end;

procedure TfrmSelecionaFiltro.btnokClick(Sender: TObject);
var
  AStream: TMemoryStream;
begin
  inherited;
  Application.CreateForm(TfrmFiltro,frmfiltro);
  frmFiltro.cxDBFilterControl1.DataSet := cdsPad;
  frmFiltro.Tabela := Tabela;
  if (dmFiltros.cdsFiltroLAYOUT.asstring <> '') then
  begin
   AStream := TMemoryStream.Create;
   try
     TBlobField(dmFiltros.cdsFiltroLAYOUT).SaveToStream(AStream);
     AStream.Position := 0;
     frmFiltro.cxDBFilterControl1.LoadFromStream(AStream);
   finally
     AStream.Free;
   end;
  end;
  frmFiltro.btnSalvarClick(nil);
  frmFiltro.Close;

  if dmFiltros.cdsFiltroFILTROTXT.AsString <> '' then
  begin
   // VerificaTotalRegistros(True);
    SQLPad.CommandText :=  'SELECT * FROM (' +  ScriptPad + ')X  WHERE ' +
    UpperCase(dmFiltros.cdsFiltroFILTROTXT.AsString);
    close;
  end
  else
    btTodosClick(nil);
end;

procedure TfrmSelecionaFiltro.JvXPButton4Click(Sender: TObject);
begin
  inherited;
  if cdsPad.IsEmpty then
    dmFiltros.Fechou := True;
  close;
end;

procedure TfrmSelecionaFiltro.pRegistros;
begin
  StatusBar1.Panels.Items[0].Text := 'Registros: '+
                                     IntToStr(cxGrid1DBTableView1.DataController.DataSet.RecNo)+'/'+
                                     IntToStr(cxGrid1DBTableView1.DataController.DataSet.RecordCount);
end;

procedure TfrmSelecionaFiltro.VerificaTotalRegistros(Filtro :Boolean);
var
 qMax :TSQLQuery;
 LimiteReg :Integer;
begin
    CriaQuery(qMax);
    if Filtro then
    begin
      qMax.SQL.Add(' SELECT COUNT(*) FROM ( SELECT * FROM (' +  ScriptPad + ')X )Y');
      qMax.SQL.Add(' WHERE ' + UpperCase(dmFiltros.cdsFiltroFILTROTXT.AsString));
      qMax.Params[0].DataType := ftInteger;
      qMax.Params[0].ParamType := ptInput;
      qMax.Params[0].AsInteger := fGetColigada;
      qMax.Open;
    end
    else
    begin
      qMax.SQL.Add(' SELECT COUNT(*) FROM ' + TABELA );
      qMax.Open;
    end;

    if dmPrincipal.cdsParametros.FieldByName('LIMITEREGISTROS').IsNull  then
      LimiteReg := 1000
    else
      LimiteReg := dmPrincipal.cdsParametros.FieldByName('LIMITEREGISTROS').AsInteger;

    if LimiteReg = 0 then
      LimiteReg := 1000;

    if qMax.Fields[0].Value > LimiteReg then
    begin
      Application.MessageBox(PChar('O Resultado da Pesquisa retornou mais de ' + IntToStr(LimiteReg) + ' registros.' + #13 +
      'Redefina os Filtros Utilizados !!'),'SANKHYA',mb_ok + MB_ICONWARNING);
      qMax.Close;
      FreeAndNil(qMax);
      Abort;
    end;
    qMax.Close;
    FreeAndNil(qMax);
end;

end.
