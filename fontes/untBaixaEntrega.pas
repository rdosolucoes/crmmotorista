
unit untBaixaEntrega;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBase, Vcl.ComCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinSilver, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxImageComboBox, cxContainer,
  Vcl.StdCtrls, dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, Vcl.Mask, JvExMask, JvToolEdit, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Vcl.ExtCtrls, cxCheckBox, Vcl.DBCtrls,
  cxCurrencyEdit, dxSkinOffice2013White, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmBaixaEntrega = class(TfrmBase)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Label1: TLabel;
    Label2: TLabel;
    dbData: TJvDateEdit;
    Label3: TLabel;
    Label4: TLabel;
    btnBaixaTodos: TJvXPButton;
    cxGrid1DBTableView1IDROMANEIO: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEPARC: TcxGridDBColumn;
    cxGrid1DBTableView1DATAFECHAMENTO: TcxGridDBColumn;
    btnLocalizar: TJvXPButton;
    cxGrid1DBTableView1SEL: TcxGridDBColumn;
    DBEdit1: TDBEdit;
    cxGrid1DBTableView1COMISSAO: TcxGridDBColumn;
    edtTotalSel: TcxCurrencyEdit;
    dblcMotorista: TcxLookupComboBox;
    cxGrid1DBTableView1CLIENTE: TcxGridDBColumn;
    btnBaixaSelecionados: TJvXPButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLocalizarClick(Sender: TObject);
    procedure CalculaSelecao;
    procedure cxGrid1DBTableView1SELPropertiesChange(Sender: TObject);
    procedure btnBaixaTodosClick(Sender: TObject);
    procedure btnBaixaSelecionadosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaixaEntrega: TfrmBaixaEntrega;

implementation

{$R *.dfm}

uses udmBaixaEntrega,untfuncoes;

procedure TfrmBaixaEntrega.btnLocalizarClick(Sender: TObject);
begin
  inherited;
  dmBaixaEntrega.cdsBaixa.Close;


  dmBaixaEntrega.cdsBaixa.Filtered := False;

  if (dblcMotorista.Text <> '') and (dbData.Date = 0) then
    dmBaixaEntrega.sqlBaixa.CommandText := dmBaixaEntrega.FsqlBaixa + ' WHERE X.CODPARC = ' + VarToStr(dblcMotorista.EditValue) + dmBaixaEntrega.FGroup
  else
  if (dbData.Date > 0) and (dblcMotorista.Text = '') then
   dmBaixaEntrega.sqlBaixa.CommandText := dmBaixaEntrega.FsqlBaixa + ' WHERE TO_DATE(X.DATAFECHAMENTO,''DD/MM/YY'') = ' +QuotedStr( FormatDateTime('DD/MM/YY',dbData.Date)) + dmBaixaEntrega.FGroup
  else
  if (dbData.Date > 0) and (dblcMotorista.Text <> '') then
   dmBaixaEntrega.sqlBaixa.CommandText := dmBaixaEntrega.FsqlBaixa + ' WHERE X.CODPARC = ' + VarToStr(dblcMotorista.EditValue) + ' AND TO_DATE(X.DATAFECHAMENTO,''DD/MM/YY'') = ' +QuotedStr( FormatDateTime('DD/MM/YY',dbData.Date)) + dmBaixaEntrega.FGroup
  else
    dmBaixaEntrega.sqlBaixa.CommandText :=  dmBaixaEntrega.FsqlBaixa + dmBaixaEntrega.FGroup;

  dmBaixaEntrega.cdsBaixa.Filtered := True;

  dmBaixaEntrega.cdsBaixa.Open;
 CalculaSelecao;

end;

procedure TfrmBaixaEntrega.CalculaSelecao;
var
  IdRomaneio,CodParc  :Integer;
begin
  With dmBaixaEntrega do
  begin
    IdRomaneio :=cdsBaixaIDROMANEIO.AsInteger;
    CodParc := cdsBaixaCODPARC.AsInteger;
    cdsBaixa.DisableControls;
    edtTotalSel.Value := 0;

    cdsBaixa.First;
    while not cdsBaixa.Eof do
    begin
      if cdsBaixaSEL.AsInteger = 1 then
        edtTotalSel.Text := FormatFloat('0.00',cdsBaixaCOMISSAO.AsFloat + edtTotalSel.Value);
      cdsBaixa.Next;
    end;
    cdsBaixa.Locate('IDROMANEIO;CODPARC',VarArrayOf([IdRomaneio,CodParc]),[]);
    cdsBaixa.EnableControls;
  end;
end;

procedure TfrmBaixaEntrega.cxGrid1DBTableView1SELPropertiesChange(
  Sender: TObject);
begin
  inherited;
  CalculaSelecao;
end;

procedure TfrmBaixaEntrega.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  frmBaixaEntrega := nil;
  FreeAndNil(dmBaixaEntrega);
end;

procedure TfrmBaixaEntrega.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmBaixaEntrega,dmBaixaEntrega);
  dmBaixaEntrega.sdsMotorista.Close;
  dmBaixaEntrega.sdsMotorista.Open;
end;

procedure TfrmBaixaEntrega.btnBaixaSelecionadosClick(Sender: TObject);
var
  sSQL :String;
begin
  inherited;
  if Application.MessageBox('Confirma a Baixa dos Romaneios Selecionados ?','SANKHYA',mb_yesno + MB_ICONQUESTION) = idyes then
  begin
    With dmBaixaEntrega do
    begin
      cdsBaixa.DisableControls;
      cdsBaixa.First;

      While not cdsBaixa.eof do
      begin
        if cdsBaixaSEL.AsInteger = 1 then
        begin
          sSQL := ' UPDATE CRMROMANEIO SET STATUS = ''B'' WHERE IDROMANEIO = ' + cdsBaixaIDROMANEIO.AsString +
                  ' AND CODEMP = ' + InttoStr(fGetColigada);
          SQLExecute(sSQL);
        end;
        cdsBaixa.Next;
      end;
      btnLocalizarClick(nil);
      cdsBaixa.EnableControls;
    end;
  end;
end;

procedure TfrmBaixaEntrega.btnBaixaTodosClick(Sender: TObject);
var
  sSQL :String;
begin
  inherited;
  if Application.MessageBox('Confirma a Baixa de Todos Romaneios ?','SANKHYA',mb_yesno + MB_ICONQUESTION) = idyes then
  begin
    With dmBaixaEntrega do
    begin
      cdsBaixa.DisableControls;
      cdsBaixa.First;

      While not cdsBaixa.eof do
      begin
        sSQL := ' UPDATE CRMROMANEIO SET STATUS = ''B'' WHERE IDROMANEIO = ' + cdsBaixaIDROMANEIO.AsString +
                  ' AND CODEMP = ' + InttoStr(fGetColigada);
        SQLExecute(sSQL);

        cdsBaixa.Next;
      end;

      btnLocalizarClick(nil);
      cdsBaixa.EnableControls;
    end;
  end;
end;

end.
