unit udmRastreaPedido;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.DB, Data.SqlExpr,
  Datasnap.Provider, Datasnap.DBClient;

type
  TdmRastreaPedido = class(TDataModule)
    dsPedido: TDataSource;
    cdsPedido: TClientDataSet;
    dspPedido: TDataSetProvider;
    sqlPedido: TSQLDataSet;
    sqlPedidoNUMNOTA: TFMTBCDField;
    sqlPedidoSITUACAO: TWideStringField;
    cdsPedidoNUMNOTA: TFMTBCDField;
    cdsPedidoSITUACAO: TWideStringField;
    sqlPedidoNUNOTA: TFMTBCDField;
    sqlPedidoNOMEPARC: TWideStringField;
    sqlPedidoDTMOV: TSQLTimeStampField;
    cdsPedidoNUNOTA: TFMTBCDField;
    cdsPedidoNOMEPARC: TWideStringField;
    cdsPedidoDTMOV: TSQLTimeStampField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    SQLPad :String;
  end;

var
  dmRastreaPedido: TdmRastreaPedido;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

uses udmPrincipal, untFuncoes;

procedure TdmRastreaPedido.DataModuleCreate(Sender: TObject);
begin
   With dmPrincipal do
   begin
     cdsParametros.Close;
     cdsParametros.Params[0].Value := fGetColigada;
     cdsParametros.Open;
   end;

  SQLPad := ' SELECT  C.NUNOTA,C.NUMNOTA, P.NOMEPARC, C.DTMOV,   ' +
            ' CASE WHEN  AD_STATUS_DISTRIBUICAO IS NULL THEN ''Separa��o'' ' +
            ' ELSE AD_STATUS_DISTRIBUICAO END SITUACAO  ' +
            ' FROM TGFCAB C      ' +
            ' INNER JOIN TGFPAR P ON C.CODPARC = P.CODPARC    ' +
            ' WHERE   ' +

            '  ( ' +
            '      (     C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPER').AsString + ' ) ' +
            '           AND C.PENDENTE = ''S'' ' +
            '      ) ' +
            '      OR ( C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPERTODOS').AsString + ' ) ) ' +
            '  ) ' +

            ' AND C.STATUSNOTA = ''L''  ' +
            ' AND C.NUMNOTA = :NUMNOTA  ' +
            ' AND C.CODEMP = :CODEMP    ' +
            ' AND C.AD_CODPARC IS NULL  ' +

            ' UNION               ' +

            ' SELECT  C.NUNOTA,C.NUMNOTA, P.NOMEPARC, C.DTMOV,   ' +
            ' CASE WHEN  AD_STATUS_DISTRIBUICAO IS NULL THEN ''Separa��o'' ELSE AD_STATUS_DISTRIBUICAO END SITUACAO  ' +
            ' FROM TGFCAB C  ' +
            ' INNER JOIN TGFPAR P ON C.AD_CODPARC = P.CODPARC ' +
            ' WHERE  ' +
            '  ( ' +
            '      (     C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPER').AsString + ' ) ' +
            '           AND C.PENDENTE = ''S'' ' +
            '      ) ' +
            '      OR ( C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPERTODOS').AsString + ' ) ) ' +
            '  ) ' +
            ' AND C.STATUSNOTA = ''L''  ' +
            ' AND C.NUMNOTA = :NUMNOTA   ' +
            ' AND C.CODEMP = :CODEMP  ' ;

   sqlPedido.CommandText := SQLPad;
end;

end.
