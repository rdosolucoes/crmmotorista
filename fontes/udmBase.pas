unit udmBase;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.DB, Datasnap.DBClient,
  Datasnap.Provider, Data.SqlExpr;

type
  TdmBase = class(TDataModule)
    SQLPad: TSQLDataSet;
    DspPad: TDataSetProvider;
    cdsPad: TClientDataSet;
    dsPad: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmBase: TdmBase;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
