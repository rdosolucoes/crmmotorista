unit udmPrincipal;

interface

uses
  SysUtils, Classes, WideStrings, DB, SqlExpr, DBXMsSQL, ImgList, Controls,
  cxGraphics, cxStyles, cxLookAndFeels, dxSkinsForm, cxLocalization, FMTBcd,
  Provider, DBClient, JvComponentBase, JvCipher, dxSkinsCore,dxSkinTheAsphaltWorld,
  cxClasses, System.Actions, Vcl.ActnList,Windows,
  Vcl.ActnMan, SimpleDS,dxSkiniMaginary, Data.DBXOracle, dxSkinSilver,
  dxSkinOffice2013White, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, System.ImageList, cxImageList;

type
  TdmPrincipal = class(TDataModule)
    imgGeral: TcxImageList;
    cxStyleGeral: TcxStyleRepository;
    cxGridLinhaImpar: TcxStyle;
    cxGridLinhaPar: TcxStyle;
    cxGridCabecalho: TcxStyle;
    cxGridRodape: TcxStyle;
    cxGridFundo: TcxStyle;
    BalloonHint1: TBalloonHint;
    imgHelp: TImageList;
    dxSkinController1: TdxSkinController;
    cxLocalizer1: TcxLocalizer;
    cdsLocalizar: TClientDataSet;
    dtsLocalizar: TDataSource;
    dspLocalizar: TDataSetProvider;
    sqlLocalizar: TSQLDataSet;
    sqlAux1: TSQLDataSet;
    cxGridGrupoColuna: TcxStyle;
    cxGridFiltro: TcxStyle;
    Cipher: TJvVigenereCipher;
    cdsEmpresa: TClientDataSet;
    dtsEmpresa: TDataSource;
    dspEmpresa: TDataSetProvider;
    sqlEmpresa: TSQLDataSet;
    cdsUsuario: TClientDataSet;
    dtsUsuario: TDataSource;
    dspUsuario: TDataSetProvider;
    sqlUsuario: TSQLDataSet;
    cdsVersao: TClientDataSet;
    dtsVersao: TDataSource;
    dspVersao: TDataSetProvider;
    sqlVersao: TSQLDataSet;
    SQLMonitor1: TSQLMonitor;
    cdsGrid: TClientDataSet;
    dtsGrid: TDataSource;
    dspGrid: TDataSetProvider;
    sqlGrid: TSQLDataSet;
    cdsDireitos: TClientDataSet;
    dtsDireitos: TDataSource;
    dspDireitos: TDataSetProvider;
    sqlDireitos: TSQLDataSet;
    Conn: TSQLConnection;
    dtsParametros: TDataSource;
    dspParametros: TDataSetProvider;
    cdsParametros: TClientDataSet;
    sqlParametros: TSQLDataSet;
    imgList: TcxImageList;
    sqlGridCODCOLIGADA: TFMTBCDField;
    sqlGridIDCRMUSUARIO: TFMTBCDField;
    sqlGridNOMEGRID: TWideStringField;
    sqlGridLAYOUT: TMemoField;
    sqlGridFILTRO: TMemoField;
    cdsGridCODCOLIGADA: TFMTBCDField;
    cdsGridIDCRMUSUARIO: TFMTBCDField;
    cdsGridNOMEGRID: TWideStringField;
    cdsGridLAYOUT: TMemoField;
    cdsGridFILTRO: TMemoField;
    sqlEmpresaCODEMP: TFMTBCDField;
    sqlEmpresaNOMEFANTASIA: TWideStringField;
    sqlEmpresaRAZAOSOCIAL: TWideStringField;
    sqlEmpresaCGC: TWideStringField;
    cdsEmpresaCODEMP: TFMTBCDField;
    cdsEmpresaNOMEFANTASIA: TWideStringField;
    cdsEmpresaRAZAOSOCIAL: TWideStringField;
    cdsEmpresaCGC: TWideStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsParametrosNewRecord(DataSet: TDataSet);
    procedure cdsParametrosAfterPost(DataSet: TDataSet);
    procedure dspGridBeforeApplyUpdates(Sender: TObject;
      var OwnerData: OleVariant);
    procedure ConnAfterConnect(Sender: TObject);
    procedure upCeramicaNeedVersion(Sender: TObject; var DeployVersion: string);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure pAbreFormAtrasados;
    procedure pAbreFormPedProg;
  end;

var
  dmPrincipal: TdmPrincipal;

implementation

uses
  Dialogs, Forms, untPrincipal, untLogin, untSetaEmpresa, untFuncoes;

{$R *.dfm}

procedure TdmPrincipal.cdsParametrosAfterPost(DataSet: TDataSet);
begin
  cdsParametros.ApplyUpdates(0);
end;

procedure TdmPrincipal.cdsParametrosNewRecord(DataSet: TDataSet);
begin
  cdsParametros.fieldbyname('CODCOLIGADA').Value := fgetColigada;
  cdsParametros.fieldbyname('ENTERCOMOTAB').Value := 0;
  cdsParametros.fieldbyname('USARFILTROANTES').Value := 0;
end;

procedure TdmPrincipal.ConnAfterConnect(Sender: TObject);
const
  SQL = 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS='',.''';
  SQL1 = 'ALTER SESSION SET NLS_DATE_FORMAT= ''DD/MM/YY''';

begin
  Conn.ExecuteDirect(SQL);
  Conn.ExecuteDirect(SQL1);
  //Conn.ExecuteDirect('alter session set nls_numeric_characters = '+ QuotedStr('.,'));
end;

procedure TdmPrincipal.DataModuleCreate(Sender: TObject);
var
  result :integer;
  qAux :TSQLQuery;
  codEmp :integer;
begin
  cdsEmpresa.close;
  cdsUsuario.Close;
  //

  cxLocalizer1.Active := false;
  cxLocalizer1.FileName := ExtractFilePath(ParamStr(0))+'DevGrid_PTBR.ini';
  cxLocalizer1.Active := true;
  cxLocalizer1.LanguageIndex := 1;
  //
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  frmLogin := TfrmLogin.Create(Self);
  result := frmLogin.ShowModal;

  if result = 7 then
    try
      frmLogin.Close;
      Application.Terminate;
    except
    end
  else
  begin
    frmLogin.close;
    frmPrincipal.Show;

    frmPrincipal.trocandoEmpresa := false;
  {  if dmPrincipal.cdsUsuario.FieldByName('SKIN').AsString <> '' then
    begin
      dmPrincipal.dxSkinController1.SkinName := dmPrincipal.cdsUsuario.FieldByName('SKIN').AsString;
      frmPrincipal.ribMenu.ColorSchemeName := dmPrincipal.dxSkinController1.SkinName;
    end;      }

    if (dmprincipal.cdsUsuario.FieldByName('CODCOLIGADA').Value > 0) then
      codemp := cdsUsuario.FieldByName('CODCOLIGADA').AsInteger
    else
      codemp := 0;

    cdsEmpresa.Close;
    sqlEmpresa.ParamByName('CODEMP').AsInteger := codemp;
    cdsEmpresa.Open;

    Application.CreateForm(TfrmSetaEmpresa, frmSetaEmpresa);
    frmSetaEmpresa.ShowModal;

    cdsParametros.Close;
    cdsParametros.Params[0].Value := fGetColigada;
    cdsParametros.Open;

    frmPrincipal.pSetaAmbiente;
//  frmPrincipal.dxBarLookupCombo3.KeyValue := cdsUsuario.FieldByName('SKIN').AsString;
  end;
end;

procedure TdmPrincipal.dspGridBeforeApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  Conn.CloseDataSets;
end;

procedure TdmPrincipal.pAbreFormAtrasados;
begin

end;

procedure TdmPrincipal.pAbreFormPedProg;
begin


end;

procedure TdmPrincipal.upCeramicaNeedVersion(Sender: TObject;
  var DeployVersion: string);
var
  qAux :TSQLQuery;
begin
  CriaQuery(qAux);
  qAux.Close;
  qAux.SQL.Clear;
  qAux.SQL.Add( ' SELECT VERSAOSIS FROM CRMVERSAO ' );
  qAux.Open;

  IF NOT qAux.IsEmpty THEN
     DeployVersion := qAux.FieldByName( 'VERSAOSIS' ).AsString
  ELSE
     DeployVersion := '0.0.0.0' ;
  FreeAndNil(qAux);
end;
end.
