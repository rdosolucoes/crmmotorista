unit udmAcessos;

interface

uses
  SysUtils, Classes, FMTBcd, DB, SqlExpr, DBClient, Provider;

type
  TdmAcessos = class(TDataModule)
    dspAcessos: TDataSetProvider;
    cdsAcessos: TClientDataSet;
    sqlAcessos: TSQLDataSet;
    dtsAcessos: TDataSource;
    dspUsu: TDataSetProvider;
    cdsUsu: TClientDataSet;
    sqlUsu: TSQLDataSet;
    dtsUsu: TDataSource;
    dspAcess: TDataSetProvider;
    cdsAcess: TClientDataSet;
    sqlAcess: TSQLDataSet;
    dtsAcess: TDataSource;
    sqlAcessosCOLIG: TFMTBCDField;
    sqlAcessosPERF: TFMTBCDField;
    sqlAcessosCODPERFIL: TWideStringField;
    sqlAcessosNOME: TWideStringField;
    sqlAcessosIDCRMUSUARIO: TFMTBCDField;
    sqlAcessosCODCOLIGADA: TFMTBCDField;
    sqlAcessosIDCRMPERFIL: TFMTBCDField;
    sqlAcessosNOMEUSU: TWideStringField;
    cdsAcessosCOLIG: TFMTBCDField;
    cdsAcessosPERF: TFMTBCDField;
    cdsAcessosCODPERFIL: TWideStringField;
    cdsAcessosNOME: TWideStringField;
    cdsAcessosIDCRMUSUARIO: TFMTBCDField;
    cdsAcessosCODCOLIGADA: TFMTBCDField;
    cdsAcessosIDCRMPERFIL: TFMTBCDField;
    cdsAcessosNOMEUSU: TWideStringField;
    sqlUsuATIVO: TWideStringField;
    sqlUsuIDCRMUSUARIO: TFMTBCDField;
    sqlUsuMASTERUSER: TWideStringField;
    sqlUsuNOME: TWideStringField;
    sqlUsuSENHA: TWideStringField;
    sqlUsuUSERNAME: TWideStringField;
    cdsUsuATIVO: TWideStringField;
    cdsUsuIDCRMUSUARIO: TFMTBCDField;
    cdsUsuMASTERUSER: TWideStringField;
    cdsUsuNOME: TWideStringField;
    cdsUsuSENHA: TWideStringField;
    cdsUsuUSERNAME: TWideStringField;
    sqlAcessCODCOLIGADA: TFMTBCDField;
    sqlAcessCODPERFIL: TWideStringField;
    sqlAcessIDCRMPERFIL: TFMTBCDField;
    sqlAcessNOME: TWideStringField;
    sqlAcessSELECIONADO: TWideStringField;
    cdsAcessCODCOLIGADA: TFMTBCDField;
    cdsAcessCODPERFIL: TWideStringField;
    cdsAcessIDCRMPERFIL: TFMTBCDField;
    cdsAcessNOME: TWideStringField;
    cdsAcessSELECIONADO: TWideStringField;
    procedure cdsAcessNewRecord(DataSet: TDataSet);
    procedure cdsUsuAfterScroll(DataSet: TDataSet);
    procedure cdsAcessBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmAcessos: TdmAcessos;

implementation

uses udmPrincipal, untFuncoes,untCadAcessos;

{$R *.dfm}

procedure TdmAcessos.cdsAcessBeforePost(DataSet: TDataSet);
var
  txt :string;
begin
  Try
    if cdsAcessSELECIONADO.AsString = 'S' then
      txt := 'insert into CRMACESSO(CODCOLIGADA, '+
                                  'IDCRMUSUARIO, '+
                                  'IDCRMPERFIL) '+
                         'values(0,'+
                                   dmAcessos.cdsUsu.FieldByName('IDCRMUSUARIO').AsString+','+
                                   dmAcessos.cdsAcessIDCRMPERFIL.AsString+')'
    else
      txt := 'delete '+
               'from CRMACESSO '+
              'where IDCRMUSUARIO = '+dmAcessos.cdsUsu.FieldByName('IDCRMUSUARIO').AsString+' '+
                'and IDCRMPERFIL = '+dmAcessos.cdsAcessIDCRMPERFIL.AsString;
    //
    pExec(txt);
  Except
  End;
end;

procedure TdmAcessos.cdsAcessNewRecord(DataSet: TDataSet);
begin
  cdsAcess.FieldByName('CODCOLIGADA').AsInteger := 0;
  cdsAcess.FieldByName('IDCRMUSUARIO').AsInteger := fGetUsuario;
end;

procedure TdmAcessos.cdsUsuAfterScroll(DataSet: TDataSet);
begin
  cdsAcess.Close;
  cdsAcess.Params.ParamByName('PCOLIG').AsInteger := fGetColigada;
  cdsAcess.Params.ParamByName('PUSU').AsInteger := dmAcessos.cdsUsu.FieldByName('IDCRMUSUARIO').AsInteger;
  cdsAcess.Open;
end;

end.
