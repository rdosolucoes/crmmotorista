inherited frmVisaoAcessos: TfrmVisaoAcessos
  Caption = 'CRM - Vis'#227'o de Acessos'
  ClientHeight = 400
  ClientWidth = 711
  ExplicitWidth = 727
  ExplicitHeight = 439
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar1: TStatusBar
    Top = 377
    Width = 711
    ExplicitTop = 377
    ExplicitWidth = 711
  end
  inherited pnlBotoes: TPanel
    Height = 377
    ExplicitHeight = 377
    inherited pnlMovimento: TPanel
      Top = 348
      ExplicitTop = 348
    end
    inherited Panel1: TPanel
      Top = 319
      ExplicitTop = 319
      inherited JvLabel1: TJvLabel
        Height = 27
      end
    end
    inherited PageControl1: TPageControl
      Height = 319
      ExplicitHeight = 319
      inherited tbs1: TTabSheet
        ExplicitHeight = 291
        inherited Panel2: TPanel
          Height = 291
          ExplicitHeight = 291
        end
      end
    end
  end
  inherited pnlTrabalho: TPanel
    Width = 595
    Height = 377
    ExplicitWidth = 595
    ExplicitHeight = 377
    inherited cxGrid1: TcxGrid
      Width = 595
      Height = 346
      ExplicitWidth = 595
      ExplicitHeight = 346
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnCellDblClick = cxGrid1DBTableView1CellDblClick
        DataController.DataSource = dmAcessos.dtsAcessos
        object cxGrid1DBTableView1CODIGO: TcxGridDBColumn
          DataBinding.FieldName = 'CODPERFIL'
        end
        object cxGrid1DBTableView1NOMEUSU: TcxGridDBColumn
          DataBinding.FieldName = 'NOMEUSU'
        end
        object cxGrid1DBTableView1NOME: TcxGridDBColumn
          DataBinding.FieldName = 'NOME'
        end
      end
    end
    inherited Panel3: TPanel
      Width = 595
      ExplicitWidth = 595
    end
  end
end
