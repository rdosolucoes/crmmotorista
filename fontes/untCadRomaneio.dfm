inherited frmCadRomaneio: TfrmCadRomaneio
  Caption = 'Libera'#231#227'o de Entrega'
  ClientHeight = 538
  ClientWidth = 1046
  ExplicitWidth = 1052
  ExplicitHeight = 567
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar1: TStatusBar
    Top = 519
    Width = 1046
    ExplicitTop = 519
    ExplicitWidth = 1046
  end
  inherited pnlBotoes: TPanel
    Width = 114
    Height = 519
    ExplicitWidth = 114
    ExplicitHeight = 519
    inherited PageControl1: TPageControl
      Width = 114
      Height = 490
      ExplicitWidth = 114
      ExplicitHeight = 490
      inherited tbs1: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 24
        ExplicitWidth = 106
        ExplicitHeight = 462
        inherited Panel1: TPanel
          Width = 106
          Height = 462
          ExplicitWidth = 106
          ExplicitHeight = 462
          inherited Label1: TLabel
            Width = 106
            ExplicitWidth = 106
          end
          object btnRoteirizar: TJvXPButton
            Left = 6
            Top = 230
            Width = 94
            Height = 36
            Hint = 'CRMi|OK|0'
            CustomHint = dmPrincipal.BalloonHint1
            Caption = 'Roteirizar'
            TabOrder = 5
            Glyph.Data = {
              0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
              000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
              1974455874536F6674776172650041646F626520496D616765526561647971C9
              653C000008AB494441545847C594797094E51DC7B708880AD6412B4AABD5A040
              1541902249B80248380C479820850041502C959B240442383BD3E94C6D6B3B53
              676A3B8E9D324E3177C845E4480224D96CEE6CEEE05ED9EC6E367BDF49BEFD3E
              EF86E59A525AFF90E193E77D9FE7F7FB7EBFCFF3EEFBCA007CAF487F0ED538EF
              C1253B5CEB922592A45AB72C7998A41A690C4BA9F7AC3ED6E0494C6DF47C72BC
              D1F37B8EC9A90D9EB59C7BF558BD47C67B19E765C79B8669F6C8D2941C95EEBB
              78E800C2F8489D7B456A93F7CB93ADBE9B673A0283673B0338DB75071D81A133
              6D7EF549A5EF1C4DD730C088EF1E8063729D7BD1F1666FF9E90E3F4E770FE014
              4923C7BB06904A923A82A398136B67880874AAD5274F6BF22CFFFF03D4B846A4
              34B83F3DD9E6C709EEF61839D211A0610089ED011C2649E473DD000EB6057050
              DC732D9988DA130C71BAD38F932DDEBF31C0E8FF35C0B894464F7E1A779D42B1
              C3ED7E1C609003AD7EEC27FBC8474A3F7EDD1D603B50691D92E63E6E09D6885A
              D193C23027448856DF250618FF700114CE478E34B80B8E51E070BB8FC23EEC69
              F151DC875F2A7DD8D1ECC3AF789DC4F53C23030C0D5162080DF641EC65805DAC
              11B5A247F41E6AF321951B39D1EA2DA3E998070750386589F5AE4F8FB0713FD9
              ADF4E2A3E620098D5E1CE25CB66100375D8370F887E01B18823B10646870081D
              CE419E800FDB9B6EF7090DA195C220692D9E2F8F4B8F224828C0419A0B18625E
              92D283BD6CFA90223B69BA83C4D77BF017951F06CF20060606E1F633806F10F6
              3BB0115F60105D4EFE306918DFE0C1CE610DA1B5879A47DABCA0F1CAFB4E60BF
              C2211871A8DE55B9A7998D8D6E2434B8B1ADDE8D8D756E9CD3FAE0A681D53B88
              7E8670896B8E66CFC05D88350FD74C1C4FB57BB189BDDBA893409D1DD4DCC3CD
              1D6DF1B4A42ADDA3C9ED00FB44801A47F43E16ED60437C9D0B9B6B5D8855B8F0
              499717368A1B5C03E823623CAFF3A1A0D7CF1034750F48A3A087BB6FB206506A
              F2E3379D1EC4D5B8B0893A02A1F93EB5F72BDD38AA746F38766780BD0AC70FF6
              D63AFFF9618328766283C289F5D54E6CAF77A1C316408F234817AF935BDC78A7
              D2817CBD0FDDBCBF6AF4E39CC68BB3ED6EEC62FF861A2756CB9D58CB7EA113CB
              318EA3B816DAC22349E9CE658891A1001F2BEC61BBEB1C9A2D2C585FEDC05AB9
              03CB69F2F94D0F7A69DCCD5DDDA4D9C12617A22BEDD27A7C8D03BF5038B0AACA
              2ECDAD2031550EACE19A5817F7CB49429D08119C13DAF10CB8B7C9654E56BAA7
              8702EC56D8D7ECAC750C6E60410C9B569255A4DCE043A7C58F6FAD7EFC43E541
              D4759B342F58516193B8757F2742E388D28532730076BE2109B50E445704E7E3
              E8F101431D68746E0D05D8A5B0256F55D8B1BACA86E81B562CBE6EC546B91D0D
              261F9A49BBD90F9E10165EB36219D71F84E88D93DBE0E12B7AEB5F62B3130BCA
              AD92764CA50D5B6AEC42EF349782017656DBFEB0914DCB591075CD82C8320BB6
              296C68A479BDD1876A9EC4BA4A2BE6975BB088EB0F62216B965EB740C51F6B40
              84E0FFB41627DEBEDA2FAD4733E07BD536ECACB1FD391460BBDCFAC7F53458C2
              E679A5FD98C3E2CD722BEA0C5E54EBBDA820EFDEB0602EE723B9FE20C2591355
              DE0F9533207D2F4488C42607665D314BDAC223965EDBE4D6DB01B6CA2D47D756
              307D99196F5FE9C3ACCB7D5875BD1F55346E10217ABD88E5FACC4B7D98C3F507
              F1266BE22A2DD237C342C4474A6C6606E785F6825233D6DCE0062BFACF8602C4
              CB2DB19C1C9A7755989B30E31B136633C4B96E177EAB7460E53533854DF721EA
              5E2F31E1353293F782B022233EEB72C1417333BF11AD7C8384F174D6CEE27A24
              3D62A8B7F19A797B28C0E62ACB2B6B6FF4EB22AF0A512345C94523A6739C5464
              C094E2E0FD548EE2FEA542038D0C143522BADC8CF515FD52CF0B05066CAAB240
              E7188096AFAF95013EEB7449F5625D684730CCBB657D96B852D3CC50800D37CC
              23D65D377F35EF8A09D3683485E293C9ABC3635861AF44F86523B654F5E374B3
              1D5F7DEB4285D10BAD3D804B7C5413F37BB15D6E411B5F5B35E7D4FC6EB4F4FB
              11C11315BD4273DA450322E91173C554B0EE92717428C0BA7213E98B59C41310
              45AFB0E1E58220132FE8C18028E3EFA0B1CF27096B8918BB687693479CA7F1E0
              8B2E27549C13DF8D0E1AEB18E283EA7E3C9BD78330EA084DA1BD809B882931C4
              13C95BFA13536A92AD2E358D5C76D55437B3C48049057ABC78A1073F21132930
              B5A817E7B9630DCD9A18E25E5AF99DE8A2A9B86EE7D8CD10876BAD783A476804
              B5C2A83983DA4B4B0C5D2B8BF58FAD28EEBD1D60C5254390CB86A511DFF09917
              EA25F3E77375782E4F47211D26F0FA774A3B3A6950C76F43ED30E25A7C2F5AFB
              82218AB41EAC29EFC3B8AC60CF7344684DA6E6DC8BBD5856A88F5D56D0231384
              02BC53D22BB18C2C29E9FDFB9BC57AFC944D42E0996CADC453595A3C91A9456A
              BD054A1A2AF848C4EB295ED5628D1B7F6D7720A1A24F327C3C4383A787FB2630
              BCD09A51A44754414FFAD20B5AD9926142011617E9432C29D23F3ABF587FF9B5
              C21EFC98BB0F9A6BF043322E538391E96ABC5F69E6F7C10725779F586361300D
              C6A46B30EA6BB554236A458FE8151A3F2BE84164BE4EBE38573B362A572BBB45
              28405461CF5D2C2EEC191F51A82B158D1373B5184FB127293C36438D2788ECBC
              0AEBCA8CA8E4EEC34B7A31E27C707E2C772E6A44ADE899C8D3989ADF83B979DA
              AA4539DAE789EC4EFE6300C1C20BDA47232EE8BE9896AFC38B795AFC289B3B1B
              0EF1384F41F62F155EE2BC301EC37B3127AE45CD33AC7D81C15FE7EEC3B335E7
              1764A9C7DE6BFE5F032CE0339ACF638ACCD5C4BC95ABAD9F4233213A81E26277
              4F65064D9FA4A9B81673CF0AE31C2D26B36E56B6A635225315373F53259B9FA5
              BECFFCE103E4686491599A51E159EAF76667ABB3DEC8D118A7E4683089BC3C8C
              B816736F646BFADECA52E78567A8B644A6AB1E8BC850C9E665AABF7B80882C86
              A01077F408C5A7CFCD50C7CFC9549F9C9DA9FE93E0E719EA5373D3D5DBC2BF56
              CD8C48578DA2B98CA3ECA1037C7F40F66F18C684D84A47F0630000000049454E
              44AE426082}
            ParentShowHint = False
            ShowHint = True
            OnClick = btnRoteirizarClick
          end
          object btnImprimir: TJvXPButton
            Left = 6
            Top = 272
            Width = 94
            Height = 36
            Hint = 'CRMi|Filtrar Registros|0'
            CustomHint = dmPrincipal.BalloonHint1
            Caption = 'Imprimir'
            TabOrder = 6
            Glyph.Data = {
              0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
              000000200806000000737A7AF40000000467414D410000AFC837058AE9000000
              1974455874536F6674776172650041646F626520496D616765526561647971C9
              653C000009C849444154584795576950945716FD444B4A0DA31147ABA61214E3
              9258D1A99AC98F710908821B89B132C6512831AE688C51E384881A5414318C7B
              749C38201A3560322E084A101565A7D9B76669A4E90D9ADEE98D66913BE7BD06
              05A3C6BCAA535F77F3BE7BCE5DDEBD0F2134F4C40BF1D55727841D3BBEF3BD78
              F1B63C2929437EE3C603D9F39098F81437131FCA929232D5616147E35D5D87FC
              49F8ADB571E3811722242442D8B2257A954CA6A6DFBB22234F3509C2808F4031
              D0C9F4821512B2EF8558B76E8FB0797354B044A2EC31DB7F75035D8FBBC9D1F1
              986C8E4E32DB3BC804B0DFF7EC39DA02F381C0204EF4A2B56E5D38C7DAB50CBB
              FB61F5EA9DC2679F4504D7D5C93961DFD50D96CEAE6E6AEFE822BBA38B2C2036
              5ADBC9004013857F73580DF3CB81970B080AFA02D82CAC58B14D58B3E61B61D5
              AA306007C7CA95A1884478706DADAC87D6B9FA7ACEC8CDF64E4EAE33B7931660
              0276EF8E7E350173E7CE05FC8079C2C71F0741C83F85E0E050FE0C0AFA12A276
              06D7D4489DCC588C9C79EE80E7B61ECF4DD60ED29B1DD4626AA366631BC411ED
              DA19F56A02E6CF5FC0E1EBEB2BF8F9F90801018B85C0C0AD20DF262C5BB659F8
              F4D3D0E0EAEA06273B5617276739777A6EE29E3BC99B0C3652E9ACD843141616
              F92B0183063D474BAF803973E608F3E6CDC5D357F0F75F00F24DC2D2A59B108D
              6D2BC4E27A4EDED7735670461BC82DEDA4697580DC4E0AAD85141A33DFF7F5D7
              11FD04B8B9B90953A64C11060C18C0BE3E5DCF0AF0F7F717BCBC662125F38505
              0B0220226469AF0047E753CF7B73AE81E72A78AE04B9ACD9448D2A2317B07D7B
              3813F0776080ABABAB3073E64C61EAD4A99CB3DF7A56809F9F1F17E0EBEB83DF
              7CDE415DA489AB9C026C6D3DE43D9EB7189F7A2E579B48AAD453835C4BEDED5D
              14B12FBACDC3636CAC8B8B8B1B009B5E3C02BF5ABD0266CF9E2D787B7B8DF6F6
              F6FE00E407F0DBA5D5AB574BFF157D924A4BC55C80D9E63C6AACD279CE8DB67E
              E492460DD535A8C962B1534CCC65D4C12E9A3DDB2777FAF4BF9D4504B64D9A34
              F13D179701C37AA89D8B91FBFBCF1D1310F0C1F75BB76E6F3973E6BF949A9A4E
              22513949A52ADABF3F9A8A8ACAB8004BFB6332210A3A6B27A959DEF576526A10
              FA2623352874542F6D21494333391CEDE8844728232387C462296567175242C2
              55DAB72FB23B3030A8C0C3C32318B5E0EC90F3E6CD1FB868D147D7EEDDCBA096
              96563220A47ABD9574A8E6A6260DBC08A73B77B24952AFA2D2AA462AAD68A4C2
              D247403D15144B4854584BB92231E5E45551766E256564955375B50C9D301A8E
              DC851D3B35A336743A1B19F0592A6D82CD9D5DEEEEEE6B40EF829CFBFA1D3972
              BCD36281576A2336EB41ACC30B66AAACACA18B17E349A331419C9EB45A03E938
              8C106922839E093693D180273EEB7420E27F335366A6887EFEF93A6CC8A81022
              CB4A1E517EB198EAEB5BE8E14311CD9831A31CE9785758B870615456561E99CD
              6D203182CC80A7815A5BED78B118DE3FE039D5C230032701D867B55A4F2A9586
              E4F2666A6C6CE2297BF44849325933882574E34632D5D52AA9AAAC91CEA5A750
              68721C175253ADA0356B426C83070F5E8BF3FE8FA4FA7A2904B0D09B80560E93
              C9463939F9949F5F06836A186724CDC4065379395200AFF2F3AB280FA1EF0BE7
              6F62CA453A121353B898DA1A05A51514D183E2729240901C27E5E0C1E8EE2143
              86EC17366CD850A1D1E8E1651B482D64345A781A542A2DC2988B62ACA2DA5A05
              27CD427EEFDC29A05F7E1171A4A616BC0445F4D34FC910AEE0E215B2165234AA
              A9A14105FB469E5A37B7D7BEC3A523B4C96A6D43E5A2A74300CBBF42D182A796
              EEDE7D084F2AE8FEFD62BA72E53E5DBE9C46F1F177817BAF80748A8DFD1FC44B
              4068804D353C579352A9E1E9BD7D3B95468C181E2B6CDAB45553545483DC1B21
              C0FC240D1A8D0E394CA1B8B85B74F8703C1D3D1A4FC78F5FF94D1C3B9680BD09
              74E448027DFBED39AAA8A802213B55285A142BAB1B89A489CE9D4BA0912347FE
              8079BF4573F56A062525E5D28307A53CD472790B44E8E1F52D9CDD582086C2C3
              CFD2CE9D6768C78ED3E8F3A79F3C7B3F338485FD1B63F83FB4776F0C4544C4D2
              A143E7D107EA40DE4A353532D48798525244E012D1C993E769D4287726E00B59
              72720EDDBA958FA2C9A6EBD7B32839390FF9AF40118A79DE5971B1A2CACC2C43
              3A0A91FF5CECCFC4BE4C18CB4438B3902E118E57099A4E05DFCF9E39399578AF
              1AF55088686601D974F3660EA5A595D0A953E7118111E731EFD715A6A6E6537A
              7A19DDBB570C94E0857CBA7AF51EC43C004106274A49C94601E6E2EF22201F42
              44FDC07E4B4BCBC3BBCC19E644261C7AC8EDDCBC99C16DDFBDEB4466A618693A
              43C387BB7D8FB91F94C80CE7E5D5C2DB4A2A28A8A74B97AE919F9F3FA1459397
              D76C7AFF7DEF1E78D1AC592FC7D3BDDEE4EBEB473E3EBEB465CB97087F2DA252
              C5398A8AEA91A6838F870D1B7A5458B264C9C9949487289646FCA18EAAAA1428
              904B303693162E5C80DC87631EEC474EF70111F8FC6A888A8AA255AB56110611
              85846CE0B68B8BEBF194702E886A73751DBC0783C86FFDB56BB7705635E8E172
              B4CA66BA70E1122D5FBE1C86F6535D1D1325C64B951CE5E5155456C6508E29C9
              5046252565305EDA831244B118C5578DF425C14E208A330CFB6A604B856254F0
              825CB66CB91EAD78A33066CC98A9C78E9D34B221A452E9704E75BCB5DEBF9F07
              439598EDED389EACE7B3E3C9DAB1B355ABD55A3EAC94CA167EBEFBB6628944CE
              1B8E56AB47EFB88E5ED0807D5A0EBDDE867497D0B469D32A318C0204B4C34148
              C395B2B21ABC60C1F9774E427632587B66D76FD6A4ECF676C0C13B26FBDD64B2
              A26B5A61D08CF75AF91C61C3ACA98975511D5AB0863A70774C4BCB44D86BB1B7
              0DB62DD863A20307A2BA870E1D7A01029C37147C198F4B69C2DAB5EB1B76EFDE
              6B3C74E8983D3DBD88DF0118A9CDE620D62D9F125B38313BDF6C52AAD58627C4
              0A8596F711D67EB55A3645756848A73B7117307FFEF916ED871F2E928C1E3D3A
              11B44B81D7B8809E350A980104CC9CB9E047368E1FE382CF88AD563BC86D9CDC
              6060E4AD3DC6D9F836F421D620154EF27ADC1FEAEA94B0817F52C2A3ACB07B1A
              58C1EC037F054600FD978BCB4061F1E2F5436362AEDFE7EE637523074C48272E
              A49D9D5DFCBEE7C0BF616D6D2C2D1D10D78EB43820B08DF77836458DB8AA190C
              564487394110D3489E9E93CF82E275E0996B71CF72731B81A6B44B888CFCF12F
              AC25F77AD55B3C0A858683151C1BB10CBD8527952A51740A8E478F642094A110
              1B0129A2D0802294D2279FACAC03CD7B4EB6E7AC61C3FE204C9AF46761DCB8B7
              5F9F3C79DAE68913DF8D993061CA79208EE1ADB7DEE1183FFE6D60729CA7E7A4
              B871E31826C68D1D3B01788BC3C3637CDC9B6F8E8F7BE30D4F601C3016DF3D7F
              7077FFE309D04C07FA444010FE0F291844A8CE6D552F0000000049454E44AE42
              6082}
            ParentShowHint = False
            ShowHint = True
            OnClick = btnImprimirClick
          end
        end
      end
      inherited tbs2: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 24
        ExplicitWidth = 106
        ExplicitHeight = 462
        inherited Panel2: TPanel
          Width = 106
          Height = 462
          ExplicitWidth = 106
          ExplicitHeight = 462
          inherited Label2: TLabel
            Width = 106
            ExplicitWidth = 106
          end
        end
      end
    end
    inherited pnlMovimento: TPanel
      Top = 490
      Width = 114
      ExplicitTop = 490
      ExplicitWidth = 114
    end
  end
  inherited pnlTrabalho: TPanel
    Left = 114
    Width = 932
    Height = 519
    ExplicitLeft = 114
    ExplicitWidth = 932
    ExplicitHeight = 519
    object GroupBox2: TGroupBox
      Left = 505
      Top = 48
      Width = 427
      Height = 471
      Align = alClient
      Caption = 'PEDIDOS SELECIONADOS'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object pnlSel: TPanel
        Left = 2
        Top = 15
        Width = 423
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
      end
      object cxGrid2: TcxGrid
        Left = 2
        Top = 56
        Width = 423
        Height = 413
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object cxGridDBTableView1: TcxGridDBTableView
          OnMouseDown = cxGridDBTableView1MouseDown
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.PriorPage.Enabled = False
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.Prior.Visible = True
          Navigator.Buttons.NextPage.Enabled = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Insert.Enabled = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Append.Enabled = False
          Navigator.Buttons.Delete.Enabled = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Enabled = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Enabled = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Enabled = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Enabled = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Enabled = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Enabled = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Enabled = False
          Navigator.Buttons.Filter.Visible = False
          FilterBox.CustomizeDialog = False
          OnCustomDrawCell = cxGridDBTableView1CustomDrawCell
          DataController.DataSource = dmRomaneio.dsItensRomaneio
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          DateTimeHandling.IgnoreTimeForFiltering = True
          DateTimeHandling.DateFormat = 'DD/MM/YYYY'
          DateTimeHandling.Grouping = dtgByDate
          DateTimeHandling.HourFormat = 'HH:NN:SS'
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.InvertSelect = False
          OptionsView.NoDataToDisplayInfoText = '<Sem dados para exibi'#231#227'o>'
          OptionsView.GroupByBox = False
          OptionsView.GroupByHeaderLayout = ghlHorizontal
          OptionsView.GroupRowStyle = grsOffice11
          OptionsView.Indicator = True
          Styles.Group = cxStyle1
          Styles.Header = dmPrincipal.cxGridCabecalho
          object cxGridDBTableView1NUMNOTA: TcxGridDBColumn
            DataBinding.FieldName = 'NUMNOTA'
            Width = 310
          end
          object cxGridDBTableView1NOMEPARC: TcxGridDBColumn
            DataBinding.FieldName = 'NOMEPARC'
            Visible = False
            GroupIndex = 1
          end
          object cxGridDBTableView1NOMEREG: TcxGridDBColumn
            DataBinding.FieldName = 'NOMEREG'
            Visible = False
            GroupIndex = 0
          end
          object cxGridDBTableView1TEMPO: TcxGridDBColumn
            DataBinding.FieldName = 'TEMPO'
            Width = 83
          end
          object cxGridDBTableView1OBSERVACOES: TcxGridDBColumn
            DataBinding.FieldName = 'OBSERVACOES'
          end
          object cxGridDBTableView1NUNOTA: TcxGridDBColumn
            DataBinding.FieldName = 'NUNOTA'
            Visible = False
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 932
      Height = 48
      Align = alTop
      TabOrder = 1
      object Label5: TLabel
        Left = 6
        Top = 17
        Width = 68
        Height = 16
        Caption = 'Romaneio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBText1: TDBText
        Left = 77
        Top = 17
        Width = 65
        Height = 17
        DataField = 'IDROMANEIO'
        DataSource = dmRomaneio.dsRomaneio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DBText2: TDBText
        Left = 228
        Top = 17
        Width = 53
        Height = 16
        AutoSize = True
        DataField = 'STATUS'
        DataSource = dmRomaneio.dsRomaneio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 174
        Top = 17
        Width = 48
        Height = 16
        Caption = 'Status:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblMotorista: TLabel
        Left = 500
        Top = 19
        Width = 57
        Height = 16
        Caption = 'Motoboy'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBRGTipo1: TDBRadioGroup
        Left = 356
        Top = 49
        Width = 143
        Height = 36
        Caption = 'Tipo'
        Columns = 2
        DataField = 'TIPO'
        DataSource = dmRomaneio.dsRomaneio
        Items.Strings = (
          'Entrega'
          'Retirada')
        TabOrder = 0
        Values.Strings = (
          'ENTREGA'
          'RETIRADA')
        Visible = False
      end
      object dblcMotorista: TcxDBLookupComboBox
        Left = 569
        Top = 18
        DataBinding.DataField = 'CODPARC'
        DataBinding.DataSource = dmRomaneio.dsRomaneio
        Properties.DropDownAutoSize = True
        Properties.KeyFieldNames = 'CODPARC'
        Properties.ListColumns = <
          item
            Caption = 'Nome'
            FieldName = 'NOMEPARC'
          end>
        Properties.ListSource = dmRomaneio.dsMotorista
        TabOrder = 1
        Width = 197
      end
      object dblcbMotoboy: TcxDBLookupComboBox
        Left = 570
        Top = 19
        DataBinding.DataField = 'CODPARC'
        DataBinding.DataSource = dmRomaneio.dsRomaneio
        Properties.DropDownAutoSize = True
        Properties.KeyFieldNames = 'CODPARC'
        Properties.ListColumns = <
          item
            Caption = 'Nome'
            FieldName = 'NOMEPARC'
          end>
        Properties.ListSource = dmRomaneio.dsMotoboy
        Properties.ReadOnly = True
        TabOrder = 2
        Width = 197
      end
      object rgMotorista2: TDBRadioGroup
        Left = 372
        Top = 49
        Width = 154
        Height = 36
        Columns = 2
        DataField = 'TIPOMOTORISTA'
        DataSource = dmRomaneio.dsRomaneio
        Items.Strings = (
          'Motoboy'
          'Motorista')
        TabOrder = 3
        Values.Strings = (
          '0'
          '1')
        Visible = False
      end
      object rgMotorista: TcxDBRadioGroup
        Left = 297
        Top = 1
        DataBinding.DataField = 'TIPOMOTORISTA'
        DataBinding.DataSource = dmRomaneio.dsRomaneio
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = 'Motoboy'
            Value = '0'
          end
          item
            Caption = 'Motorista'
            Value = '1'
          end>
        TabOrder = 4
        OnClick = rgMotoristaClick
        Height = 42
        Width = 152
      end
      object DBRGTipo: TcxDBRadioGroup
        Left = 773
        Top = 4
        Caption = 'Tipo'
        DataBinding.DataField = 'TIPO'
        DataBinding.DataSource = dmRomaneio.dsRomaneio
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = 'Entrega'
            Value = 'ENTREGA'
          end
          item
            Caption = 'Retirada'
            Value = 'RETIRADA'
          end>
        Properties.OnChange = DBRGTipoPropertiesChange
        TabOrder = 5
        Height = 38
        Width = 148
      end
    end
    object pnlPedReal: TPanel
      Left = 0
      Top = 48
      Width = 505
      Height = 471
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 449
        Height = 471
        Align = alLeft
        Caption = 'PEDIDOS REALIZADOS'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 2
          Top = 56
          Width = 445
          Height = 373
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnMouseDown = cxGrid1DBTableView1MouseDown
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.PriorPage.Enabled = False
            Navigator.Buttons.PriorPage.Visible = False
            Navigator.Buttons.Prior.Visible = True
            Navigator.Buttons.NextPage.Enabled = False
            Navigator.Buttons.NextPage.Visible = False
            Navigator.Buttons.Insert.Enabled = False
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Append.Enabled = False
            Navigator.Buttons.Delete.Enabled = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Enabled = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Enabled = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Enabled = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Enabled = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Enabled = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Enabled = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Buttons.Filter.Enabled = False
            Navigator.Buttons.Filter.Visible = False
            FilterBox.CustomizeDialog = False
            OnCustomDrawCell = cxGrid1DBTableView1CustomDrawCell
            DataController.DataSource = dmRomaneio.dsPedidos
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting, dcoGroupsAlwaysExpanded]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            DateTimeHandling.IgnoreTimeForFiltering = True
            DateTimeHandling.DateFormat = 'DD/MM/YYYY'
            DateTimeHandling.Grouping = dtgByDate
            DateTimeHandling.HourFormat = 'HH:NN:SS'
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.InvertSelect = False
            OptionsView.NoDataToDisplayInfoText = '<Sem dados para exibi'#231#227'o>'
            OptionsView.GroupByBox = False
            OptionsView.GroupByHeaderLayout = ghlHorizontal
            OptionsView.GroupRowStyle = grsOffice11
            OptionsView.Indicator = True
            Styles.Group = cxStyle1
            Styles.Header = dmPrincipal.cxGridCabecalho
            object cxGrid1DBTableView1STATUS: TcxGridDBColumn
              DataBinding.FieldName = 'STATUS'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRomaneio.imgList
              Properties.Items = <
                item
                  ImageIndex = 6
                  Value = 'A'
                end
                item
                  ImageIndex = 7
                end
                item
                  ImageIndex = 12
                  Value = 'R'
                end>
              Properties.ShowDescriptions = False
              Width = 45
            end
            object cxGrid1DBTableView1NUMNOTA: TcxGridDBColumn
              DataBinding.FieldName = 'NUMNOTA'
              PropertiesClassName = 'TcxMaskEditProperties'
              FooterAlignmentHorz = taCenter
              GroupSummaryAlignment = taCenter
              Width = 254
            end
            object cxGrid1DBTableView1NOMEPARC: TcxGridDBColumn
              DataBinding.FieldName = 'NOMEPARC'
              Visible = False
              GroupIndex = 1
            end
            object cxGrid1DBTableView1NOMEREG: TcxGridDBColumn
              DataBinding.FieldName = 'NOMEREG'
              Visible = False
              GroupIndex = 0
            end
            object cxGrid1DBTableView1TEMPO: TcxGridDBColumn
              DataBinding.FieldName = 'TEMPO'
              Width = 92
            end
            object NUNOTA: TcxGridDBColumn
              DataBinding.FieldName = 'NUNOTA'
              Visible = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 445
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label3: TLabel
            Left = 6
            Top = 14
            Width = 41
            Height = 14
            Caption = 'Pedido:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object edtPesq: TEdit
            Left = 53
            Top = 11
            Width = 97
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnChange = edtPesqChange
          end
          object rgCliLojaold: TDBRadioGroup
            Left = 264
            Top = 4
            Width = 41
            Height = 31
            Columns = 3
            Items.Strings = (
              'Todos'
              'Cliente'
              'Loja')
            TabOrder = 1
            Values.Strings = (
              'A'
              'C'
              'L'
              '')
            Visible = False
            OnClick = rgCliLojaoldClick
          end
          object rgCliLoja: TcxRadioGroup
            Left = 216
            Top = -10
            Properties.Columns = 3
            Properties.Items = <
              item
                Caption = 'Todos'
                Value = 'A'
              end
              item
                Caption = 'Cliente'
                Value = 'C'
              end
              item
                Caption = 'Loja'
                Value = 'L'
              end>
            ItemIndex = 0
            Style.BorderStyle = ebsNone
            TabOrder = 2
            OnClick = rgCliLojaClick
            Height = 47
            Width = 224
          end
        end
        object Panel4: TPanel
          Left = 2
          Top = 429
          Width = 445
          Height = 40
          Align = alBottom
          TabOrder = 2
          object btnIncluiPedidos: TJvXPButton
            Left = 295
            Top = 6
            Width = 69
            Height = 28
            Hint = 'CRMi|Cancelar Opera'#231#227'o|0'
            CustomHint = dmPrincipal.BalloonHint1
            Caption = 'Incluir'
            TabOrder = 0
            TabStop = False
            Glyph.Data = {
              0B544A76474946496D6167657A01000047494638396110001000D52700E2E9F0
              49556B434B5C88AFC895CAFC3E455589BBECDEE7EF2931439AC0D6CBCCD184BC
              3B80B83895C94889C03F97CB4B7EB6378FC44492C74790C545D5DBE496CA4AD5
              E7F07AB3338BC0409ECEFB52617B577091ECF1F672AC2DE5EDF57697B4A4D1FB
              B1D7FCF2F6FAC0DFFDBBDCFDABD4FBB6DAFCFFFFFFFFFFFF0000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000000000000000000000000000000000000000000021F904
              01000028002C0000000010001000000697C09370481CA28E27CBA9D339250683
              8FF473420A1F8DA167EB316CAACB8A24E2688ECE228357D899601610338924AA
              6B888CCB704432994222774E4B667C7E212581420344737F8889018B22677D7F
              252520201E92271F1C95979920191E02429F8E909A191900A79E1C9688AC0404
              0705A8227DABADB61CB9271B14221C5B00C800071C0A08A81BD01A1A01010202
              0505088245DC4441003B}
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            OnClick = btnIncluiPedidosClick
          end
          object btnReprovaPedidos: TJvXPButton
            Left = 370
            Top = 6
            Width = 71
            Height = 28
            Hint = 'CRMi|Cancelar Opera'#231#227'o|0'
            CustomHint = dmPrincipal.BalloonHint1
            Caption = 'Reprovar'
            TabOrder = 1
            TabStop = False
            Glyph.Data = {
              0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000010
              0000001008060000001FF3FF610000000467414D410000B18F0BFC6105000002
              5D49444154384FA551CF4BDB6018AE25ADD57C2996DA58BA5041CA687178706B
              6995CD8378D38B670F4345BDD882280A1E77554105F5AA7F8230A80863960D44
              98B6CCC32C8834BA83DBD236AD756AA9AFEF1BA3B5ABB7051EF2E4F9F585C400
              00FF8567C5EFA3A335DB56EB9B6DC6DE6B404EDA73D92AE113637D074E6732D5
              D707CAF838289108C8FDFD70E07225C9FB375FF110E3F989E340E0565D5D85DC
              C64605D4B535380E066F29F3B4F3483E33D6F5C3E72BA5171721BBB202D9A525
              B8D8DCD4409C34F22843D9AA813DC676CE2727415D5E86CB68148A6767A0CECF
              83BAB0A0F1CBAD2DCD3B9F9A823D41D8A918F8DAD0D078E8F596727373A0CECE
              42F1F41465D08A84079E418F3287ADAD25EAA0AC0F30E63FE9EE860B0C10B213
              1370A38FD0455C191880DCD010E4A7A7E1A4A707A883D6FDC02E636DA960100A
              23231AD2636370A39F4CD7B52CC36F2CE5DADB21D7D505A98E0EA00E5AF703FB
              6E776DB2A9492D747682D2DB0BD77AF90A4FBEC2B2C6F1FE2B108082CF074951
              54A98372F9237E636C352349906F6E860CFE362AFCC1B722FC459E5E5F87BCDB
              0D94A1EC43EF71206EB335ED33F653696C84822882822715F14E50BCDE7B0D3D
              CA1C60B66A8090B05ADB76793E95600CCE0401D23A8893A6799879DA29971309
              C3CCCC4C4D54146DF8813E7CA9AF3F42DCEA3822ED237A9148C4188BC5CA0354
              9224C9220882CD62B1BC309BCD1E93C9F48AE3B8D70E8E7B8B7887DC8F6843EF
              256624C698DDE974D60D0E0ED6682BB22C1BC2E1B031140A711E8FC7EC72B9EA
              4451E4ED763B23381C0E9EB49696965ABFDFCF0D0F0F1BE3F13876C1700770C1
              60DEE22C3AA70000000049454E44AE426082}
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            OnClick = btnReprovaPedidosClick
          end
        end
      end
      object Panel6: TPanel
        Left = 455
        Top = -3
        Width = 56
        Height = 471
        BevelOuter = bvNone
        TabOrder = 1
        object btnCarrega: TJvXPButton
          Left = 1
          Top = 183
          Width = 43
          Height = 36
          Hint = 'CRMi|Cancelar Opera'#231#227'o|0'
          CustomHint = dmPrincipal.BalloonHint1
          TabOrder = 0
          TabStop = False
          Glyph.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000018
            000000180806000000E0773DF80000000467414D410000B18F0BFC6105000005
            E049444154484B9D560B4C536718FDF73006A90222352A221A17DD60D12D3EC1
            071BA8144426302C62781750140A883CE45190F763E5DD96D71505FB4028BE60
            94698B14321F0B2AB0E99C62A289C66576C99C3A93EDECBFA5A273731A4F7272
            9B7BF39FF3DDF37FDF7F4B5E8B5CB294E41111C9276A5240B4A488B298FE2EA1
            F74AE9B3B7460E09E11472C6D634AE4344471492BBF6A3BA4F86AA3E2992BA52
            10AA0E8753B333CC2BCDC78898849856BD01B269553964686BAB2F2467EBD1A8
            3F84F2D3B5C8EF2E455AA70829EA2CE47415A2482346B5AE0E65DA4A782ABD40
            2AC810A97CDD1B51716E09D770405380A681C328EA1163EFD1FD8853EEC36EE5
            5EEC5224205A2E44943C0E02F91E4429E3B0FF640EC4BA5A24F7A4C35A36C340
            AA5E6542C5EDC5F30DD2FE0648FB9A904A2B15AA92B147998418937028B313C1
            4C14C2E4310895EF42B03C1A418A6844B4ED41FE9932E4EB4A615D474D6A5E36
            C926969C02CE185B392BBEAF3DC3243E5E75646B2CFC4A0311D79084D886BDD8
            52E68F40B900814A01025411E0ABC211D82640B6B608F1BDC998229D32466A89
            A5499D228788D8CC9BF48727C4D948762AE211298FC5765918CA4FD4E0193457
            4E8327F6819F2A187E4783E1DBCE3208FEEA50E40D94C1ADC31DD44064522784
            5BCA35340F1E416E57F1BFC4C31431F09704A1B55F61921FC7D7C3DFC0AD6A0B
            BCD581F03EBE035B8E051A19A68945DEB9AF605667662012F62D7249C8C66677
            D4F5338857A5186379513C9866FC656310763242FCF6E4A1497E1C5DA3BDF854
            BC0E1F1F5E8B95CA0D706EE31999A4CFC2EAF6B5A006B47DF30893D2958E624D
            3962E9861A3357C421B03EC258B9AF6C073CA57E70CC5E09974A4F181EFD6A92
            1FC7A18B729865CD0447620B8E6C2EA52D96AB5CE1A2E681480943E8846A99C1
            16641CCFA5D124225A2104BF3A04B18D7B8DB1D46919884EE643D45D00116D82
            CE915326E9E7681D52627A9E1DB88D0B61C32CC447F255D8AE11B0065A42C77F
            A871E010523BB38CD10814B1E0E57E81878FFF19C7EB507AB602732B16C1FE88
            2396B4392354BB8B351822A490181AF4CD48EECC44148DC64DE28D49A156A665
            6F8EF6914E2CAA5D02C78E15703AEE8AE87E21888C18083DB8B4F5FA834852A7
            D38C3D40122D4022A722848986F67A3FB43FF54377438FB33707D03F3688E1BB
            A3F88B0ABEC8AB3F5FC3DAFACFB1BA733D5677B96043CF6608CFED630D684425
            449DD1930D3E130A229C0692440DD22CC7AF299678377D3A2667D98093310BF6
            050EB8787B188F9FFE39C1E17BD7B0A199874D3D1EE0E9BCB049EB8990C10884
            E923590335A147AE707D13DD75DA2124818A2653665183C2E978A7DC1A93AA67
            C2BC620EE6E67D08FDCDCBF8E5E1D3095EBCFD033C5BBCE0A3F305FF7C00022E
            04C0FFDC36A48F6482A7A169C888909032626F516E05EFFA00CCC89C0F924AC5
            B3AD404AA841E50C4CAE9D85A92573904687F0CE8327131CB8398ACD47BCC1D7
            F3117E3914D12302EC1C8D44FCF77128BF21C6ECD6D9AC81FDF8288B09E32C73
            8573CD2690FDD4E0003528B3C67B55363093CC8665991D623A3270FDDE23234F
            5F1B068F56CED76F4314154EB8BE07A9630948BB95889A3BE548BC14CF8AD319
            788672626F5EC9317830BE70AC7202C9A306626BBC5FC585B9D4969E900BF041
            E952249C388094EE42B832EED8AAF33156CE8A8BEEA4A0F8BE08CC0309987B32
            705B6C0CCFAB7F864A1262279D0FEFD6ED582C5D316E50CD354E263B3CF30E39
            6071C32758D6E2848DBD1ED8769E6FAC3E85565E743F0BAADF1968FE3806D75E
            17B6FA577CE1AA086357BF00DE6D8158A3F2C034E9BC090376808C3DDEFD19DC
            759BE9A6F28DB967DE4A46ABA1119A279D703FE3F65234FF851A22B4A8B3824B
            BB277C4E05C1E9A83B1C14ABB058B90C0EEDCBB1EAD47A78F56D85E0BB48E4FC
            4863B95B8FACAB6998AFB23775CD9BA0967E916A8976D6C1B970EE70857F4F08
            C275BB11339080C40BA948BF2442C6954C847D1B86A59D4B58612DE55BFCC390
            D04512DA63F44CE1344EC3ECC3B6B06DB583C541DA69327ACEC8E8B3FF1526E4
            6F829478EFB5A37B0F0000000049454E44AE426082}
          ParentShowHint = False
          ShowHint = True
          OnClick = btnCarregaClick
        end
        object btnVolta: TJvXPButton
          Left = 2
          Top = 225
          Width = 43
          Height = 36
          Hint = 'CRMi|Cancelar Opera'#231#227'o|0'
          CustomHint = dmPrincipal.BalloonHint1
          TabOrder = 1
          TabStop = False
          Glyph.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000018
            000000180806000000E0773DF80000000467414D410000B18F0BFC6105000005
            B949444154484B95557950D46518FEC64247200101B5118FD0D28C0C34110413
            4D54406595885B39964B91FB5814810541C14556EEDD051710D96595233CC2B0
            E126B59C742AFDA394D1996C4C47700683F2787ABF0532358F9E997766677FBB
            CFF3FE9EF7622F452633647B983FCB624A96CDDAD85ED6C7722872E9F37EFA4E
            42CFF2E837FF1B62FA5306539A484CE15DE707D1A914287B6B70A8E7302ABAAB
            A0E8AE44FCA9247C7ED40BC6C52660F924267D5D213113E865EBF507D787A1AA
            B716F22E25F6B51CC0AE2631929BD290D4988AC486DD483FB917D2B61294742A
            E0D71800DD22DD7E56C004A32C2F8098F9CFCC9F85B2CE7228BA2A91DA9C8518
            8D0891750988A88BC336750CC254D108514542A8DA81205504221B1221692B44
            4EDB014C2F37032B24DBFE139439272F2572E9996224D4A7205A93841D75F144
            1CAB25F65786C1AB2400DE8A20F8ABC2B1551D063F7528FC35E148399D89AC76
            09A6579048F1B36F429E735BC6C8638F26234A93A8CD3A9CB2E619FBCA851016
            6F436DA706A29ADD101479517D84F0D404C14313481180A4D6746474E4606219
            D955F2EF9A5041B9E7DC169EF9B3E43E947164453C0687063106DF322136D5FA
            C2AD7E2B36376CA1F0837BA33FC45D39703FE10D12508E90532BF26EE1054D6D
            DEA3B5859387A9A3114C3EFF173947707504D6AB3DE1DAEC8B8DCD3EDAD8D0EC
            0D9F9650649DCB8361C564B052FE16D4E7BC1579B78C79CE330FFE27F3380C0E
            3F4D7EF462132C726DF0FE611EB65850F32416D62E475467325636AD012BE305
            A721E27DBEB7254FDB2DBCA04F6C799EBCF2DB5A8C4B3082CE4E634C489F8289
            E2A9D0CB7E1BFA52334C92CFC45BB219B052AF8043931317209B6842F910EDA4
            3EDF4ED684AAA3E0AB108ED8F20C3947FBD52E745CED46C7B51E746AA31747BE
            D7C0327F19A694CCC1D4CA7731AFF663F8B486708136C6C7BFA2A71A221A22AD
            35EA1D70103BE1D6C0EFA394AFC6A3C7C0C92B5F6146FE3C98AB3E84E5317B04
            B66FE7027D8CEF96721AFF449A509EBD8DF453307F7DF4DDBE3EFAF717E33111
            3F24F6E1078F48E00CE6175B6261E352D81F774478770C988C0BD0E2E2BB25A1
            3105EFED5904166700166D00CBCC65E8FF6360946A040343F790DB21C581AE02
            48BB8B51D0538AC2DE320A1996CB57C1B67E05ECBE5C85B5AD1B117B2E890B90
            45B415E368713916B912F124B04412483104DB69084B89FD73227597EA619EBB
            0016F2C5B03E62077BCD4AACAC77C4BA1617ACEF748553C706049E0D866FE756
            2E4045A6956B2DB38363B1002C96C845146924B08FFA387B32164B5750E62322
            DC8E3F1F3E2691262C2AB1C6DA3617087A36C3E39C077CBEF3D186E7792F882F
            67C0B9D5850B509BD23ED795EAC155E10593D477B499B30C23B0FD9331AEC004
            13F64F856DD16ADCB93F80E1BF1EE1FEF043DC1B7A80D0A6086CFC5A00BF0BBE
            08F92108DB2F8722E24A18E2AEC4E0E035298CAA884336B62E689F5B95D9C051
            2E804EAA2958263DCC33C61B85A6D02D9D0EA383B3E02073C6CD81BB18B8FF00
            37EEDE853B4DB1578F27C27F0A41FC2F5148B91E8FB41B2294FF568A80B363F6
            8C818E854EC1F87E67A51BAC4A3E01CB22817C63BC5934057A65663039340766
            B2F9F8E8E052A49FC9856B8D1B9C1B5D20BC1488B89F2321FE3519923B19A819
            50A0EA961C7A87F4FA9F643F063A1606C54670AE7187558503740AA66905F465
            663055CEC5EC5A0B7CA05982252A3BAC39ED445E7B22ECC76088FA6221B99D81
            86A16A7C31A882B96636CFFE0587878E8541298968DCE1D8E08669E5F39F12B0
            68B0C6B2532BB1AE7D3D3C4820940432AEEF465D7F35EAEFD560EE31F3D1C2BE
            0C742C744AC7F72F52D962D3C92DD870C20B36D4867C3AED8EAFD6F6F8675D9E
            88BC188DBCAB1254DC9423FC4208F4955A5B5E7132C7C08F05ED735D853E2CEB
            96C2B1997A9B467F5B4F0CE2CE8BB0EB621A84DF0443D0268071B5F148419FF3
            FC75C0F7395FB97C2BF2C5C5770B1F7F3EA123A4FE2F2766EC6FF2B37DD6B983
            88160000000049454E44AE426082}
          ParentShowHint = False
          ShowHint = True
          OnClick = btnVoltaClick
        end
      end
    end
  end
  object pnlPedido: TJvCaptionPanel
    Left = 620
    Top = 172
    Width = 289
    Height = 228
    Buttons = []
    Caption = ''
    CaptionPosition = dpTop
    CaptionFont.Charset = DEFAULT_CHARSET
    CaptionFont.Color = clWhite
    CaptionFont.Height = -13
    CaptionFont.Name = 'Tahoma'
    CaptionFont.Style = [fsBold]
    OutlookLook = False
    TabOrder = 3
    Visible = False
    object lblCliLoja: TLabel
      Left = 10
      Top = 81
      Width = 44
      Height = 16
      Caption = 'Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 111
      Width = 58
      Height = 13
      Caption = 'Observa'#231#227'o'
    end
    object JvXPButton3: TJvXPButton
      Left = 138
      Top = 191
      Width = 64
      Height = 22
      Hint = 'CRMi|Cancelar Opera'#231#227'o|0'
      CustomHint = dmPrincipal.BalloonHint1
      Caption = 'OK'
      TabOrder = 0
      TabStop = False
      Glyph.Data = {
        0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000010
        0000001008060000001FF3FF610000000467414D410000B18F0BFC6105000000
        097048597300000B0C00000B0C013F4022C80000002074455874536F66747761
        7265004D6163726F6D656469612046697265776F726B73204D58BB912A240000
        022749444154384F8D524B681351147DD348896093A25956F08322D299CCA413
        632B354141BAA87651088DB6A408BA13172E1574EFA2DDB87665B7A3927E924A
        531334D64C3F9449A01BB39854A98B06032EB48BE3DC379938132CF8E0BC79EF
        CE3DF7F3EE61003CB8608C4B526542538CDBA66A4C42D94EE1E2D6B87976E396
        D6A78F489DFE9E4BB892D222D53B086D5E874FBF04A60F70F8F42882FA559C29
        8FE2E4DA88E6E6F0ED9139E357AB53E6A9ED9B6DD26138B19640DFC71B666C79
        C2DF0E4099FF87ECA0B7348C5031C12B616A75324A65FFCBB113D3B56788EFDC
        E3E7D0FB388EE787A3ACDF48E6A8E74E6737123BF751FBB58B74ED29584106FB
        A0C05F8AA1677528C7A295A9BAFBC1A815B99A6ADF67F7E6AC4A81875F9E8365
        443B806517CA2A02CB837546A3729C09F9A68ED96F73E82DC7A135F29CAC7D5F
        017BDD0FB66A931D1C5B8A81D19C1D036576D6FEC10FFE6D1C34119CBF0C9695
        3C64C2D1C52898B895B45AB00E966166EF1527B9577AFD31D85B2BFB672F5928
        0FA07B3E5267A73746732412320637E378F9F54D8B0AD47EEEDAA5B7FA76A3AB
        68092C2BE7D8797D4C2585B57F5A99D2C613347E3731F6E981FD702EA283230B
        32848CA87221913C49616E87F07AD22EBDA878EC84AE1505C282640B89B66B85
        BB7E922729CCE35C8A78EF1638392399ECC5B9BF527640F22485914868CE0E89
        1E8C7AE665B7323BF00420040A57C4407E48EB793768D29C6954DD8B11D3B724
        6B42362C7AFDC1FE007221495CC104923F0000000049454E44AE426082}
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = JvXPButton3Click
    end
    object JvXPButton4: TJvXPButton
      Left = 208
      Top = 191
      Width = 64
      Height = 22
      Hint = 'CRMi|Cancelar Opera'#231#227'o|0'
      CustomHint = dmPrincipal.BalloonHint1
      Caption = 'Sair'
      TabOrder = 1
      TabStop = False
      Glyph.Data = {
        0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000010
        0000001008060000001FF3FF610000000467414D410000B18F0BFC6105000001
        FD49444154384FA592DD4B53611CC71F08C16EBCE8FDC2B4394B96BAD9329D29
        2669BB9095A306F38DCD6CAD987A9C3A77105F1843C5C81B4104416A82687813
        D64089E5A603990B7520062148DD75957FC2B7DFB3897AE61104BFF0390FFC9E
        E7FBE1709EC3CE9D1FA21667E5A0220DDFD81A3520E47DB2977281E93934D611
        6A229BB8415C3C551075DF43C8751F7FBF88087FE8089E26308C5E029B642725
        EFDD762C3B35F835F208FF82EF30D1AC09D2582A186733F1B29C20E22A404050
        E3A757873FBE97785374851F3A120CB2765E1C8E0D2604C9AC756AF0AD351FDB
        9E62FCF6D9A4825E26F243DE2D0F5CD10E3823ADE8DB10E18D0D40587F0BD645
        82B0331F8B8E5CC4FA0BB13BD9281550D9B33900FDD26394FA8B50B5540E4340
        0FF38A1142D49E10ACB4E5C16F5761A3478B9D3193EC1B3484CCA80954C312AE
        857DCD1A2FF7C7BA138265C75D7C7E9583884B8309773D44EB33D96FE05C6F49
        149219E9B261DE928DD5F63CCC091568AE2E910A8EDF021528FC71085BB4DDC6
        6C9D02DF1D2AAC8AA578FDE0F24901FD07554369F282AF4D4AF84C19E0A280A0
        854397748D07027E8E5649392EE0F9F83C7D7BC1A280DFA6425BC9553EE56515
        A12432082E4921E40553C6F4AC4FB599FB0B56253ACBAEF16925514E3C24EE10
        A9443CB2029EE917378DF375B7D053719D4FCDC45342C1F78E472A00FB0F8CAD
        6313AC60C0270000000049454E44AE426082}
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = JvXPButton4Click
    end
    object lcCliente: TcxDBLookupComboBox
      Left = 60
      Top = 80
      DataBinding.DataField = 'CODPARC'
      DataBinding.DataSource = dmRomaneio.dsItensRomaneio
      Properties.DropDownAutoSize = True
      Properties.KeyFieldNames = 'CODPARC'
      Properties.ListColumns = <
        item
          Caption = 'Regi'#227'o'
          FieldName = 'NOMEREG'
        end
        item
          Caption = 'Cliente'
          FieldName = 'NOMEPARC'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmRomaneio.dsCliente
      TabOrder = 2
      Width = 213
    end
    object lcLoja: TcxDBLookupComboBox
      Left = 60
      Top = 80
      DataBinding.DataField = 'CODPARC'
      DataBinding.DataSource = dmRomaneio.dsItensRomaneio
      Properties.DropDownAutoSize = True
      Properties.KeyFieldNames = 'CODPARC'
      Properties.ListColumns = <
        item
          Caption = 'Regi'#227'o'
          FieldName = 'NOMEREG'
        end
        item
          Caption = 'Cliente'
          FieldName = 'NOMEPARC'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmRomaneio.dsLoja
      TabOrder = 3
      Visible = False
      Width = 213
    end
    object rgCliLoja2: TDBRadioGroup
      Left = 10
      Top = 35
      Width = 263
      Height = 31
      Color = clBtnFace
      Columns = 2
      DataField = 'CLILOJA'
      DataSource = dmRomaneio.dsRomaneio
      Items.Strings = (
        'Cliente'
        'Loja')
      ParentBackground = False
      ParentColor = False
      TabOrder = 4
      Values.Strings = (
        'C'
        'L')
      OnClick = rgCliLoja2Click
    end
    object cxDBMemo1: TcxDBMemo
      Left = 8
      Top = 126
      DataBinding.DataField = 'OBSERVACOES'
      DataBinding.DataSource = dmRomaneio.dsItensRomaneio
      TabOrder = 5
      Height = 62
      Width = 265
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 1056
    Top = 56
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
  end
  object Timer1: TTimer
    Interval = 60000
    OnTimer = Timer1Timer
    Left = 1000
    Top = 208
  end
end
