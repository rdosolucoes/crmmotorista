object dmControlePercurso: TdmControlePercurso
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 238
  Width = 386
  object dsPercurso: TDataSource
    DataSet = cdsPercurso
    Left = 112
    Top = 80
  end
  object cdsPercurso: TClientDataSet
    Aggregates = <>
    AutoCalcFields = False
    FetchOnDemand = False
    Params = <>
    ProviderName = 'dspPercurso'
    AfterPost = cdsPercursoAfterPost
    Left = 40
    Top = 16
    object cdsPercursoCODEMP: TFMTBCDField
      FieldName = 'CODEMP'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 10
      Size = 0
    end
    object cdsPercursoIDCONTROLE: TFMTBCDField
      FieldName = 'IDCONTROLE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 10
      Size = 0
    end
    object cdsPercursoIDPERCURSO: TFMTBCDField
      FieldName = 'IDPERCURSO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 10
      Size = 0
    end
    object cdsPercursoDATA: TSQLTimeStampField
      DisplayLabel = 'Data'
      FieldName = 'DATA'
    end
    object cdsPercursoHORASAIDA: TWideStringField
      DisplayLabel = 'Hora Sa'#237'da'
      FieldName = 'HORASAIDA'
      Size = 8
    end
    object cdsPercursoKMSAIDA: TFMTBCDField
      DisplayLabel = 'Km Sa'#237'da'
      FieldName = 'KMSAIDA'
      Precision = 15
      Size = 0
    end
    object cdsPercursoHORACHEGADA: TWideStringField
      DisplayLabel = 'Hora Chegada'
      FieldName = 'HORACHEGADA'
      Size = 8
    end
    object cdsPercursoKMCHEGADA: TFMTBCDField
      DisplayLabel = 'Km Chegada'
      FieldName = 'KMCHEGADA'
      Precision = 15
      Size = 0
    end
    object cdsPercursoCODPARC: TFMTBCDField
      DisplayLabel = 'Cod.Parc'
      FieldName = 'CODPARC'
      Precision = 10
      Size = 0
    end
    object cdsPercursoNOMEPARC: TWideStringField
      DisplayLabel = 'Motoboy'
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
    object cdsPercursoPERCURSO: TFMTBCDField
      DisplayLabel = 'Total Km'
      FieldName = 'PERCURSO'
      Precision = 32
    end
  end
  object dspPercurso: TDataSetProvider
    DataSet = sqlPercurso
    UpdateMode = upWhereKeyOnly
    Left = 32
    Top = 80
  end
  object dsMotorista: TDataSource
    DataSet = sdsMotorista
    Left = 224
    Top = 80
  end
  object sdsMotorista: TSimpleDataSet
    Aggregates = <>
    Connection = dmPrincipal.Conn
    DataSet.CommandText = 
      'SELECT CODPARC,NOMEPARC  FROM TGFPAR'#13#10'WHERE (MOTORISTA = '#39'S'#39')  O' +
      'R'#13#10'    (AD_MOTOBOY = '#39'S'#39') '#13#10'AND ATIVO = '#39'S'#39#13#10'ORDER BY NOMEPARC'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 216
    Top = 24
    object sdsMotoristaCODPARC: TFMTBCDField
      FieldName = 'CODPARC'
      Required = True
      Precision = 10
      Size = 0
    end
    object sdsMotoristaNOMEPARC: TWideStringField
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
  end
  object sqlPercurso: TSQLDataSet
    SchemaName = 'teste'
    CommandText = 
      'SELECT PC.CODEMP,PC.IDCONTROLE,PC.IDPERCURSO,C.DATA,TO_CHAR(HORA' +
      'SAIDA,'#39'HH24:MI:SS'#39') HORASAIDA,KMSAIDA,'#13#10'TO_CHAR(HORACHEGADA,'#39'HH2' +
      '4:MI:SS'#39') HORACHEGADA,KMCHEGADA,KMCHEGADA - KMSAIDA PERCURSO,C.C' +
      'ODPARC,p.nomeparc'#13#10'FROM CRMPERCURSOMOTOBOY PC'#13#10'INNER JOIN CRMCON' +
      'TROLEMOTOBOY C ON C.IDCONTROLE = PC.IDCONTROLE'#13#10'INNER JOIN TGFPA' +
      'R P ON P.CODPARC = C.CODPARC'
    MaxBlobSize = -1
    ParamCheck = False
    Params = <>
    SQLConnection = dmPrincipal.Conn
    Left = 120
    Top = 16
    object sqlPercursoCODEMP: TFMTBCDField
      FieldName = 'CODEMP'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 10
      Size = 0
    end
    object sqlPercursoIDCONTROLE: TFMTBCDField
      FieldName = 'IDCONTROLE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 10
      Size = 0
    end
    object sqlPercursoIDPERCURSO: TFMTBCDField
      FieldName = 'IDPERCURSO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 10
      Size = 0
    end
    object sqlPercursoDATA: TSQLTimeStampField
      FieldName = 'DATA'
    end
    object sqlPercursoHORASAIDA: TWideStringField
      FieldName = 'HORASAIDA'
      Size = 8
    end
    object sqlPercursoKMSAIDA: TFMTBCDField
      FieldName = 'KMSAIDA'
      Precision = 15
      Size = 0
    end
    object sqlPercursoHORACHEGADA: TWideStringField
      FieldName = 'HORACHEGADA'
      Size = 8
    end
    object sqlPercursoKMCHEGADA: TFMTBCDField
      FieldName = 'KMCHEGADA'
      Precision = 15
      Size = 0
    end
    object sqlPercursoCODPARC: TFMTBCDField
      FieldName = 'CODPARC'
      Precision = 10
      Size = 0
    end
    object sqlPercursoNOMEPARC: TWideStringField
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
    object sqlPercursoPERCURSO: TFMTBCDField
      FieldName = 'PERCURSO'
      Precision = 32
    end
  end
end
