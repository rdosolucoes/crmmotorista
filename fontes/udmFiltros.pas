unit udmFiltros;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.DB, Datasnap.Provider,
  Datasnap.DBClient, Data.SqlExpr,Dialogs;

type
  TdmFiltros = class(TDataModule)
    sqlFiltro: TSQLDataSet;
    dsFiltro: TDataSource;
    cdsFiltro: TClientDataSet;
    dspFiltro: TDataSetProvider;
    sqlFiltroCODCOLIGADA: TFMTBCDField;
    sqlFiltroIDFILTRO: TFMTBCDField;
    sqlFiltroID: TFMTBCDField;
    sqlFiltroDESCRICAO: TWideStringField;
    sqlFiltroIDUSUARIO: TFMTBCDField;
    sqlFiltroFORMULARIO: TWideStringField;
    sqlFiltroFILTROTXT: TMemoField;
    sqlFiltroLAYOUT: TBlobField;
    cdsFiltroCODCOLIGADA: TFMTBCDField;
    cdsFiltroIDFILTRO: TFMTBCDField;
    cdsFiltroID: TFMTBCDField;
    cdsFiltroDESCRICAO: TWideStringField;
    cdsFiltroIDUSUARIO: TFMTBCDField;
    cdsFiltroFORMULARIO: TWideStringField;
    cdsFiltroFILTROTXT: TMemoField;
    cdsFiltroLAYOUT: TBlobField;
    sqlFiltroULTIMOID2: TFMTBCDField;
    cdsFiltroULTIMOID: TFMTBCDField;
    procedure cdsFiltroNewRecord(DataSet: TDataSet);
    procedure cdsFiltroBeforePost(DataSet: TDataSet);
    procedure cdsFiltroReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsFiltroAfterPost(DataSet: TDataSet);
    procedure cdsFiltroAfterScroll(DataSet: TDataSet);
    procedure dspFiltroBeforeApplyUpdates(Sender: TObject;
      var OwnerData: OleVariant);
  private
    { Private declarations }
    function GeraID(NomeForm :String):Integer;
    procedure AtualizaUltimoID;
  public
     NomeForm :String;
     Fechou :Boolean;
  end;

var
  dmFiltros: TdmFiltros;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses udmPrincipal,untFuncoes,untFiltro,untSelecionaFiltro;

{$R *.dfm}

procedure TdmFiltros.AtualizaUltimoID;
var
  qAux :TSQLQuery;
begin
  CriaQuery(qAux);
  qAux.SQL.Add('UPDATE CRMFILTROS SET ULTIMOID = :ID ');
  qAux.SQL.Add(' WHERE FORMULARIO = :FORM AND CODCOLIGADA = :COLIGADA ');
  qAux.SQL.Add(' AND IDUSUARIO = :IDUSUARIO ');
  qAux.Params[0].AsInteger := cdsFiltroID.AsInteger;
  qAux.Params[1].AsString := cdsFiltroFORMULARIO.AsString;
  qAux.Params[2].AsInteger := fGetColigada;
  qAux.Params[3].AsInteger := fGetUsuario;
  qAux.ExecSQL;
  FreeAndNil(qAux);
end;

procedure TdmFiltros.cdsFiltroAfterPost(DataSet: TDataSet);
begin
  cdsFiltro.ApplyUpdates(0);
end;

procedure TdmFiltros.cdsFiltroAfterScroll(DataSet: TDataSet);
begin
  if Assigned(frmSelecionaFiltro) then
    frmSelecionaFiltro.pRegistros;
end;

procedure TdmFiltros.cdsFiltroBeforePost(DataSet: TDataSet);
begin
  if cdsFiltro.State = dsInsert then
    cdsFiltroID.Value := GeraID(cdsFiltroFORMULARIO.AsString);
  AtualizaUltimoID;
end;

procedure TdmFiltros.cdsFiltroNewRecord(DataSet: TDataSet);
begin
  cdsFiltroCODCOLIGADA.Value := fgetcoligada;
  cdsFiltroIDFILTRO.Value := GeraCodigo('IDFILTRO');
  cdsFiltroIDUSUARIO.Value := fGetUsuario;
end;

procedure TdmFiltros.cdsFiltroReconcileError(DataSet: TCustomClientDataSet;
  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
  showmessage(E.Message);
end;

procedure TdmFiltros.dspFiltroBeforeApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  dmPrincipal.Conn.CloseDataSets;
end;

function TdmFiltros.GeraID(NomeForm: String): Integer;
var
  qAux :TSQLQuery;
begin
  CriaQuery(qAux);
  qAux.SQL.Add('SELECT NVL(MAX(ID),0) + 1 FROM CRMFILTROS');
  qAux.SQL.Add(' WHERE CODCOLIGADA = :CODCOLIGADA AND FORMULARIO = :FORM ');
  qAux.Params[0].Value := fGetColigada;
  qAux.Params[1].AsString := NomeForm;
  qAux.Open;

  Result := qAux.Fields[0].Value;
  qAux.Close;
  FreeAndNil(qAux);
end;

end.
