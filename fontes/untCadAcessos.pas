unit untCadAcessos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untBaseCad, JvExControls, JvLabel, dxGDIPlusClasses, JvXPCore,
  JvXPButtons, StdCtrls, ComCtrls, ExtCtrls, DBCtrls, JvExComCtrls,
  JvDBTreeView,
  Grids, DBGrids, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCheckBox, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, dxSkinsCore, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinscxPCPainter;

type
  TfrmCadAcessos = class(TfrmBaseCad)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1ATIVO: TcxGridDBColumn;
    cxGrid1DBTableView1NOME: TcxGridDBColumn;
    Label3: TLabel;
    Label4: TLabel;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1CODIGO: TcxGridDBColumn;
    cxGridDBTableView1NOME: TcxGridDBColumn;
    cxGridDBTableView1SELECIONADO: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGridDBTableView1SELECIONADOPropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure pSetaDireitos; override;
    //
    procedure pNovo; override;
    procedure pEditar; override;
    procedure pExcluir; override;
    procedure pRefresh; override;
    // --
    Function fValidaCampos: boolean; override;
    Function pGravar: boolean; override;
    procedure pCancelar; override;
  end;

var
  frmCadAcessos: TfrmCadAcessos;

implementation

uses udmAcessos, untFuncoes, udmPrincipal, untPrincipal;

{$R *.dfm}
{ TfrmCadAcessos }

procedure TfrmCadAcessos.cxGridDBTableView1SELECIONADOPropertiesChange
  (Sender: TObject);
begin
  inherited;
  dmAcessos.cdsAcess.Post;
  PageControl1.ActivePage := tbs1;
end;

procedure TfrmCadAcessos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  frmCadAcessos := nil;
end;

procedure TfrmCadAcessos.FormCreate(Sender: TObject);
begin
  inherited;
  cdsPad := dmAcessos.cdsUsu;
end;

procedure TfrmCadAcessos.FormShow(Sender: TObject);
begin
  inherited;
  pRegistros;
  dmAcessos.cdsUsu.Open;
  dmAcessos.cdsUsu.Locate('IDCRMUSUARIO',
    dmAcessos.cdsAcessos.FieldByName('IDCRMUSUARIO').AsInteger, []);
  btnAdicionar.Visible := false;
  btnEditar.Visible := false;
  btnConfirmar.Visible := false;
  btnCancelar.Visible := false;
end;

function TfrmCadAcessos.fValidaCampos: boolean;
begin
  result := true;
end;

procedure TfrmCadAcessos.pCancelar;
begin
  inherited;
  with dmAcessos.cdsAcess do
    if (State in [dsInsert, dsEdit]) then
    begin
      Cancel;
      RevertRecord;
    end;
end;

procedure TfrmCadAcessos.pEditar;
begin
  inherited;
  with dmAcessos.cdsAcess do
    if (FieldByName('IDCRMPERFIL').AsInteger > 0) then
      Edit;
end;

procedure TfrmCadAcessos.pExcluir;
var
  txt: string;
begin
  inherited;
  with dmAcessos.cdsAcess do
    if (FieldByName('IDCRMPERFIL').AsInteger > 0) then
    begin
      Try
        txt := 'delete ' + 'from CRMACESSO ' + 'where CODCOLIGADA = ' +
          inttostr(fGetColigada) + ' ' + 'and IDCRMUSUARIO = ' +
          dmAcessos.cdsUsu.FieldByName('IDCRMUSUARIO').AsString;
        //
        dmPrincipal.sqlAux1.Close;
        dmPrincipal.sqlAux1.CommandText := txt;
        dmPrincipal.sqlAux1.ExecSQL(true);
      Except
      End;
      // Delete;
      // if (ApplyUpdates(0) > 0) then
      // raise Exception.Create('Erro ao excluir registro');
    end;
end;

function TfrmCadAcessos.pGravar: boolean;
begin
  result := false;
  with dmAcessos.cdsAcess do
  begin
    // pGrardarDireitos;
    Post;
    if (ApplyUpdates(0) > 0) then
      raise Exception.Create('Erro ao incluir registro');
    result := true;
  end;
end;

procedure TfrmCadAcessos.pNovo;
begin
  inherited;
  with dmAcessos.cdsAcess do
    if (State in [dsBrowse]) then
      // Append
      Edit
    else if (State in [dsInsert, dsEdit]) then
      raise Exception.Create('Erro salve o registro antes')
    else // if State in [dsInactive] then
      raise Exception.Create('Erro ao criar registro');
end;

procedure TfrmCadAcessos.pRefresh;
var
  chv: Integer;
begin
  inherited;
  with dmAcessos.cdsUsu do
  begin
    chv := FieldByName('IDCRMUSUARIO').AsInteger;
    Close;
    Open;
    Locate('IDCRMUSUARIO', chv, []);
  end;
end;

procedure TfrmCadAcessos.pSetaDireitos;
begin
  inherited;
  btnAdicionar.Enabled := frmprincipal.A00305.Enabled;
  btnEditar.Enabled := frmprincipal.A00305.Enabled;
  dmAcessos.dtsAcess.AutoEdit := frmprincipal.A00305.Enabled;
  btnExcluir.Enabled := frmprincipal.A00305.Enabled;
  btnConfirmar.Enabled := frmprincipal.A00305.Enabled;
end;

end.
