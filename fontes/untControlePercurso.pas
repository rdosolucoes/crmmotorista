
unit untControlePercurso;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBase, Vcl.ComCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinSilver, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxImageComboBox, cxContainer,
  Vcl.StdCtrls, dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, Vcl.Mask, JvExMask, JvToolEdit, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Vcl.ExtCtrls, cxCheckBox, Vcl.DBCtrls,
  cxCurrencyEdit, dxSkinOffice2013White, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmControlePercurso = class(TfrmBase)
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Label1: TLabel;
    Label2: TLabel;
    dbData: TJvDateEdit;
    btnLocalizar: TJvXPButton;
    dblcMotorista: TcxLookupComboBox;
    cxGrid1DBTableView1DATA: TcxGridDBColumn;
    cxGrid1DBTableView1HORASAIDA: TcxGridDBColumn;
    cxGrid1DBTableView1KMSAIDA: TcxGridDBColumn;
    cxGrid1DBTableView1HORACHEGADA: TcxGridDBColumn;
    cxGrid1DBTableView1KMCHEGADA: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEPARC: TcxGridDBColumn;
    Label3: TLabel;
    dbDataFinal: TJvDateEdit;
    cxGrid1DBTableView1PERCURSO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLocalizarClick(Sender: TObject);
    procedure CalculaSelecao;
    procedure cxGrid1DBTableView1SELPropertiesChange(Sender: TObject);
    procedure btnBaixaTodosClick(Sender: TObject);
    procedure btnBaixaSelecionadosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmControlePercurso: TfrmControlePercurso;

implementation

{$R *.dfm}

uses udmControlePercurso,untfuncoes;

procedure TfrmControlePercurso.btnLocalizarClick(Sender: TObject);
var sWhere :string;
begin
  inherited;
  dmControlePercurso.cdsPercurso.Close;

  sWhere := ' WHERE 1=1 ';

  if (dbData.Date > 0) then
    sWhere :=  sWhere + ' AND TO_DATE(C.DATA,''DD/MM/YY'') >= ' + QuotedStr( FormatDateTime('DD/MM/YY',dbData.Date));

  if (dbDataFinal.Date > 0) then
    sWhere := sWhere + ' AND TO_DATE(C.DATA,''DD/MM/YY'') <= ' + QuotedStr( FormatDateTime('DD/MM/YY',dbDataFinal.Date));

  if (dblcMotorista.Text <> '') then
     sWhere := sWhere + ' AND C.CODPARC = ' + VarToStr(dblcMotorista.EditValue);

  dmControlePercurso.sqlPercurso.CommandText := dmControlePercurso.FsqlPercurso + sWhere + dmControlePercurso.FOrder;

  dmControlePercurso.cdsPercurso.Open;
 //CalculaSelecao;

end;

procedure TfrmControlePercurso.CalculaSelecao;
var
  IdRomaneio,CodParc  :Integer;
begin
  With dmControlePercurso do
  begin
    //IdRomaneio :=cdsPercursoIDROMANEIO.AsInteger;
    CodParc := cdsPercursoCODPARC.AsInteger;
    cdsPercurso.DisableControls;

    cdsPercurso.First;
    while not cdsPercurso.Eof do
    begin
      //if cdsPercursoSEL.AsInteger = 1 then
      //  edtTotalSel.Text := FormatFloat('0.00',cdsPercursoCOMISSAO.AsFloat + edtTotalSel.Value);
      cdsPercurso.Next;
    end;
    //cdsPercurso.Locate('IDROMANEIO;CODPARC',VarArrayOf([IdRomaneio,CodParc]),[]);
    cdsPercurso.EnableControls;
  end;
end;

procedure TfrmControlePercurso.cxGrid1DBTableView1SELPropertiesChange(
  Sender: TObject);
begin
  inherited;
  CalculaSelecao;
end;

procedure TfrmControlePercurso.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  frmControlePercurso := nil;
  FreeAndNil(dmControlePercurso);
end;

procedure TfrmControlePercurso.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmControlePercurso,dmControlePercurso);
  dmControlePercurso.sdsMotorista.Close;
  dmControlePercurso.sdsMotorista.Open;
end;

procedure TfrmControlePercurso.btnBaixaSelecionadosClick(Sender: TObject);
var
  sSQL :String;
begin
  inherited;
  if Application.MessageBox('Confirma a Baixa dos Romaneios Selecionados ?','SANKHYA',mb_yesno + MB_ICONQUESTION) = idyes then
  begin
    With dmControlePercurso do
    begin
      cdsPercurso.DisableControls;
      cdsPercurso.First;

      While not cdsPercurso.eof do
      begin
        //if cdsPercursoSEL.AsInteger = 1 then
        //begin
       //   sSQL := ' UPDATE CRMROMANEIO SET STATUS = ''B'' WHERE IDROMANEIO = ' + cdsPercursoIDROMANEIO.AsString +
       //           ' AND CODEMP = ' + InttoStr(fGetColigada);
       //   SQLExecute(sSQL);
       // end;
        cdsPercurso.Next;
      end;
      btnLocalizarClick(nil);
      cdsPercurso.EnableControls;
    end;
  end;
end;

procedure TfrmControlePercurso.btnBaixaTodosClick(Sender: TObject);
var
  sSQL :String;
begin
  inherited;
  if Application.MessageBox('Confirma a Baixa de Todos Romaneios ?','SANKHYA',mb_yesno + MB_ICONQUESTION) = idyes then
  begin
    With dmControlePercurso do
    begin
      cdsPercurso.DisableControls;
      cdsPercurso.First;

      While not cdsPercurso.eof do
      begin
       // sSQL := ' UPDATE CRMROMANEIO SET STATUS = ''B'' WHERE IDROMANEIO = ' + cdsPercursoIDROMANEIO.AsString +
        //          ' AND CODEMP = ' + InttoStr(fGetColigada);
        SQLExecute(sSQL);

        cdsPercurso.Next;
      end;

      btnLocalizarClick(nil);
      cdsPercurso.EnableControls;
    end;
  end;
end;

end.
