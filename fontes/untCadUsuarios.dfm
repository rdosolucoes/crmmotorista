inherited frmCadUsuarios: TfrmCadUsuarios
  Caption = 'CRM - Cadastro de Usu'#225'rios'
  ClientHeight = 419
  ClientWidth = 644
  ExplicitWidth = 650
  ExplicitHeight = 448
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar1: TStatusBar
    Top = 400
    Width = 644
    ExplicitTop = 400
    ExplicitWidth = 644
  end
  inherited pnlBotoes: TPanel
    Height = 400
    ExplicitHeight = 400
    inherited PageControl1: TPageControl
      Height = 371
      ExplicitHeight = 371
      inherited tbs1: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 24
        ExplicitWidth = 108
        ExplicitHeight = 343
        inherited Panel1: TPanel
          Height = 343
          ExplicitHeight = 343
          inherited btnAdicionar: TJvXPButton
            Top = 23
            ExplicitTop = 23
          end
          inherited btSair: TJvXPButton
            Top = 229
            TabOrder = 5
            ExplicitTop = 229
          end
          object btnRedifinirSenha: TJvXPButton
            Left = 6
            Top = 188
            Width = 94
            Height = 39
            Hint = 'CRMi|Redefinir a Senha do Usu'#225'rio|0'
            CustomHint = dmPrincipal.BalloonHint1
            Caption = 'Senha       '
            TabOrder = 4
            Glyph.Data = {
              0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000018
              000000180806000000E0773DF80000000467414D410000B18F0BFC6105000004
              4649444154484BCD947B4C567518C7DF6CAE78419776D5566956B3B58A2DB584
              002F208A49DE206329391544145868615C4234052F8088D3392EE28B8B711113
              1A020982184282042F2A86E81A954DABAD56BDEFE19CF3E9118E6E6CD65EE89F
              BEDB6F67E7F73BCFF33DDFDFF7791ED370F051F42773A22362DB7626A7B13521
              99F8982D6CDE94C0BAD028667BF816F9CE9EEF677C3A7448F2A5A52565FC132E
              5FEE226465184232C708711CDBB7A406971DAFE84F74F3E64DBEE9B0D2D17989
              766B27CDE75BA8A9ADA7AABA86ABD7AEDD210934421D43DDE9B3FDC96FFDFC0B
              97BAAE70E8402E616B227B0317AE6859F1CEFBE4E659A8ACAEA5ACE2247BD30F
              E03ED5E3B811EA18CE35B6F4135CE9EEE670CE513646C60DBAEB596EDED6C6C6
              260A8A4A282CF9FC36C129E3C831DC21B8D0D64E626C4AAFB17D177E3EFEE6A4
              84ED349C6DC2525088DB148F72E3C8319CA93FD74FD074BE95B898AD1DC6F620
              AC5D1321E72D1CC8CA11059E4323B8ED81A66B9C6D6C242E36B9D3D81E84B5AB
              D78B82AFD8B337138F695E4327D045814180B13D08A1ABC2F9B2A686945DA978
              BC3EA3DC143FC6B42EDF7F3C16CFFBB04C3391F79A89C2E926CA67C99A6FA6C2
              7F14D58B4751B7C4858600179A02CDB40699697FCFCCC565F7D31138C2D5C8DD
              8FF090086AEA1B48CDC8C46BFACC72D3C1198FCB3FD5CA4A07DB0EF8351E7A37
              4AC78442CB7238B3182A7DA0D41D2CAFC2C14990F134DA6E896B8DA07BFDB841
              4A02172E233E2189B0900DCCF4F0693508AC68B7CAD07EDC85DABD09D51A8AD6
              1C8C763A10F58B79A885EE28B9AE28FB2661DF310E5BD218D4432F43D55BF484
              8CBAE755DDC50081DCEDD52CF40BC9E8F59BD04EACA02FDF93BE236FA01679A3
              552C40AD7B17A53514A53312E5EB75E84DE1D0FE213DEB9F7084E05BF4DFABD1
              7F3A86FE5D9EBCEE97E09D528FB1689561684501A8595E28A92FA2243E8A2D66
              24F64F4743D644BA578D7480E02F49DEB907BD211AAD7C397D166F6C9993B165
              3C8B5AE086766A216AEB1AB4EF13B0FF912ED7998AFE4386CC8C6C7AA2260C57
              418A28F85814AC15054B4581E77F50C019F1E09078B04D3C88140F82C4036FFA
              F2DE442DF6453BB9583C081AF0C01A31E041F306A98DCDF46C18EF08C115F4DF
              2AD16F14A35FCF45EFDA076DDBA131461484888225A8393350F6BE84B24DAA28
              CE097BCA58C87D81ABAB1FF85F28B88707D65DD01C8F5E158E562CFD902D0AD2
              86EB814D925F4C473F276559152AB5BF087BBA349574AB72C415B5622E5ACB4A
              B41B49287F664815A50DB58A7AD1C403AD2D16B5DC1FFBD1A928F95350B25FC1
              BEEF396C3B9F94EE1D8B2DF121EC89B79FA385FC31F1E07947ABE81834CC152B
              16C8589A272340664F9927144F837C57C8990CFB2742E6848195F10CFA7E9949
              ED1F703DFAA97F27887FD81465F17D84C35E4EE4CF74E1336F178A7C9C299DEB
              CC093F672A163853FDB699BA45661A9698690A30A669901397831FA463E98860
              23D53D6032FD0D4825E3323D4CD3F00000000049454E44AE426082}
            ParentShowHint = False
            ShowHint = True
            OnClick = btnAtualizarClick
          end
        end
      end
      inherited tbs2: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 24
        ExplicitWidth = 108
        ExplicitHeight = 352
        inherited Panel2: TPanel
          Height = 343
          ExplicitHeight = 352
        end
      end
    end
    inherited pnlMovimento: TPanel
      Top = 371
      ExplicitTop = 371
    end
  end
  inherited pnlTrabalho: TPanel
    Width = 528
    Height = 400
    ExplicitWidth = 528
    ExplicitHeight = 400
    object PageControl2: TPageControl
      Left = 0
      Top = 0
      Width = 528
      Height = 400
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Identifica'#231#227'o'
        object grbLogin: TGroupBox
          Left = 0
          Top = 60
          Width = 520
          Height = 77
          Align = alTop
          Caption = '[ Login ]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          object Label7: TLabel
            Left = 9
            Top = 20
            Width = 36
            Height = 13
            Caption = 'Usu'#225'rio'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 137
            Top = 20
            Width = 30
            Height = 13
            Caption = 'Senha'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label5: TLabel
            Left = 285
            Top = 20
            Width = 68
            Height = 13
            Caption = 'Repetir Senha'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object edtUsuario: TDBEdit
            Left = 9
            Top = 34
            Width = 121
            Height = 21
            DataField = 'USERNAME'
            DataSource = dmUsuarios.dtsUsu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object edtSenha: TDBEdit
            Left = 137
            Top = 34
            Width = 121
            Height = 21
            DataField = 'SENHA'
            DataSource = dmUsuarios.dtsUsu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            PasswordChar = '*'
            TabOrder = 1
          end
          object edtSenha2: TEdit
            Left = 285
            Top = 34
            Width = 121
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            PasswordChar = '*'
            TabOrder = 2
            Text = 'edtSenha2'
          end
        end
        object GroupBox3: TGroupBox
          Left = 0
          Top = 193
          Width = 520
          Height = 80
          Align = alTop
          Caption = '[ Contatos ]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          object Label8: TLabel
            Left = 295
            Top = 20
            Width = 24
            Height = 13
            Caption = 'Email'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label9: TLabel
            Left = 7
            Top = 22
            Width = 42
            Height = 13
            Caption = 'Telefone'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label10: TLabel
            Left = 144
            Top = 22
            Width = 33
            Height = 13
            Caption = 'Celular'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object DBEdit2: TDBEdit
            Left = 5
            Top = 38
            Width = 121
            Height = 21
            DataField = 'TELEFONE'
            DataSource = dmUsuarios.dtsUsu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 142
            Top = 38
            Width = 137
            Height = 21
            DataField = 'CELULAR'
            DataSource = dmUsuarios.dtsUsu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object DBEdit1: TDBEdit
            Left = 293
            Top = 38
            Width = 188
            Height = 21
            DataField = 'EMAIL'
            DataSource = dmUsuarios.dtsUsu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 520
          Height = 60
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 4
            Top = 11
            Width = 61
            Height = 13
            Caption = 'Identificador'
          end
          object Label4: TLabel
            Left = 76
            Top = 11
            Width = 27
            Height = 13
            Caption = 'Nome'
          end
          object edtIdentificador: TDBEdit
            Left = 4
            Top = 27
            Width = 61
            Height = 21
            TabStop = False
            Color = clSilver
            DataField = 'IDCRMUSUARIO'
            DataSource = dmUsuarios.dtsUsu
            ReadOnly = True
            TabOrder = 0
          end
          object edtNome: TDBEdit
            Left = 76
            Top = 27
            Width = 229
            Height = 21
            DataField = 'NOME'
            DataSource = dmUsuarios.dtsUsu
            TabOrder = 1
          end
          object chkAtivo: TDBCheckBox
            Left = 317
            Top = 29
            Width = 65
            Height = 17
            Caption = 'Ativo ?'
            DataField = 'ATIVO'
            DataSource = dmUsuarios.dtsUsu
            TabOrder = 2
            ValueChecked = 'S'
            ValueUnchecked = 'N'
          end
          object chkMaster: TDBCheckBox
            Left = 379
            Top = 29
            Width = 65
            Height = 17
            Caption = 'Master ?'
            DataField = 'MASTERUSER'
            DataSource = dmUsuarios.dtsUsu
            TabOrder = 3
            ValueChecked = 'S'
            ValueUnchecked = 'N'
          end
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 137
          Width = 520
          Height = 56
          Align = alTop
          Caption = '[ Empresa ]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          object dblcEmpresa: TcxDBLookupComboBox
            Left = 9
            Top = 23
            DataBinding.DataField = 'CODCOLIGADA'
            DataBinding.DataSource = dmUsuarios.dtsUsu
            ParentFont = False
            Properties.DropDownAutoSize = True
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'CODEMP'
            Properties.ListColumns = <
              item
                Caption = 'Cod.'
                FieldName = 'CODEMP'
              end
              item
                Caption = 'Nome Fantasia'
                FieldName = 'NOMEFANTASIA'
              end>
            Properties.ListSource = dmUsuarios.dtsEmp
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 0
            Width = 56
          end
          object cxDBLookupComboBox2: TcxDBLookupComboBox
            Left = 68
            Top = 23
            DataBinding.DataField = 'CODCOLIGADA'
            DataBinding.DataSource = dmUsuarios.dtsUsu
            ParentFont = False
            Properties.DropDownAutoSize = True
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'CODEMP'
            Properties.ListColumns = <
              item
                Caption = 'Cod.'
                FieldName = 'CODEMP'
              end
              item
                Caption = 'Nome Fantasia'
                FieldName = 'NOMEFANTASIA'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dmUsuarios.dtsEmp
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 1
            Width = 187
          end
        end
        object GroupBox2: TGroupBox
          Left = 0
          Top = 273
          Width = 520
          Height = 56
          Align = alTop
          Caption = '[ Motorista ]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          object dblcMotorista: TcxDBLookupComboBox
            Left = 9
            Top = 25
            DataBinding.DataField = 'CODPARC'
            DataBinding.DataSource = dmUsuarios.dtsUsu
            ParentFont = False
            Properties.KeyFieldNames = 'CODPARC'
            Properties.ListColumns = <
              item
                Caption = 'Nome'
                FieldName = 'NOMEPARC'
              end>
            Properties.ListSource = dmUsuarios.dsMotorista
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 0
            Width = 197
          end
        end
        object DBCheckBox1: TDBCheckBox
          Left = 4
          Top = 345
          Width = 245
          Height = 17
          Caption = 'Permite Incluir Pedido na Tela de Libera'#231#227'o'
          DataField = 'PERMITEINCPEDIDO'
          DataSource = dmUsuarios.dtsUsu
          TabOrder = 5
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
    end
  end
end
