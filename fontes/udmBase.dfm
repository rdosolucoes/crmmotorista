object dmBase: TdmBase
  OldCreateOrder = False
  Height = 124
  Width = 297
  object SQLPad: TSQLDataSet
    MaxBlobSize = -1
    Params = <>
    SQLConnection = dmPrincipal.Conn
    Left = 32
    Top = 16
  end
  object DspPad: TDataSetProvider
    DataSet = SQLPad
    Left = 96
    Top = 16
  end
  object cdsPad: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DspPad'
    Left = 168
    Top = 16
  end
  object dsPad: TDataSource
    DataSet = cdsPad
    Left = 232
    Top = 16
  end
end
