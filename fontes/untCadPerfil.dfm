inherited frmCadPerfil: TfrmCadPerfil
  Caption = 'CRM - Cadastro de Perfil'
  ClientHeight = 553
  ClientWidth = 527
  ExplicitWidth = 533
  ExplicitHeight = 582
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar1: TStatusBar
    Top = 534
    Width = 527
    ExplicitTop = 534
    ExplicitWidth = 527
  end
  inherited pnlBotoes: TPanel
    Height = 534
    ExplicitHeight = 534
    inherited PageControl1: TPageControl
      Height = 505
      ExplicitHeight = 505
      inherited tbs1: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 24
        ExplicitWidth = 108
        ExplicitHeight = 477
        inherited Panel1: TPanel
          Height = 477
          ExplicitHeight = 477
        end
      end
      inherited tbs2: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 24
        ExplicitWidth = 108
        ExplicitHeight = 477
        inherited Panel2: TPanel
          Height = 477
          ExplicitHeight = 477
        end
      end
    end
    inherited pnlMovimento: TPanel
      Top = 505
      ExplicitTop = 505
    end
  end
  inherited pnlTrabalho: TPanel
    Width = 411
    Height = 534
    ExplicitWidth = 411
    ExplicitHeight = 534
    object Label3: TLabel
      Left = 28
      Top = 7
      Width = 61
      Height = 13
      Caption = 'Identificador'
    end
    object Label4: TLabel
      Left = 144
      Top = 46
      Width = 46
      Height = 13
      Caption = 'Descri'#231#227'o'
    end
    object Label5: TLabel
      Left = 28
      Top = 46
      Width = 33
      Height = 13
      Caption = 'C'#243'digo'
    end
    object Label6: TLabel
      Left = 28
      Top = 88
      Width = 84
      Height = 13
      Caption = 'A'#231#245'es Habilitadas'
    end
    object edtIdentificador: TDBEdit
      Left = 28
      Top = 21
      Width = 61
      Height = 21
      TabStop = False
      Color = clSilver
      DataField = 'IDCRMPERFIL'
      DataSource = dmPerfil.dtsPerfil
      ReadOnly = True
      TabOrder = 0
    end
    object edtNome: TDBEdit
      Left = 144
      Top = 60
      Width = 245
      Height = 21
      DataField = 'NOME'
      DataSource = dmPerfil.dtsPerfil
      TabOrder = 2
    end
    object edtCodigo: TDBEdit
      Left = 28
      Top = 60
      Width = 97
      Height = 21
      DataField = 'CODPERFIL'
      DataSource = dmPerfil.dtsPerfil
      TabOrder = 1
    end
    object treeAcoes: TJvTreeView
      Left = 28
      Top = 104
      Width = 361
      Height = 397
      Indent = 19
      TabOrder = 3
      OnClick = treeAcoesClick
      Items.NodeData = {
        03020000004600000000000000010000000A000000FFFFFFFF00000000000000
        0004000000011443006100640061007300740072006F00200064006500200055
        0073007500E100720069006F0073002A000000000000006600000014000000FF
        FFFFFF01000000000000000000000001064700720061007600610072002C0000
        0000000000650000001E000000FFFFFFFF020000000000000000000000010749
        006E00730065007200690072002C000000000000006400000028000000FFFFFF
        FF030000000000000000000000010741006C00740065007200610072002C0000
        00000000000D00000032000000FFFFFFFF040000000000000000000000010745
        00780063006C007500690072004600000000000000000000003C000000FFFFFF
        FF050000000000000004000000011443006100640061007300740072006F0020
        0064006500200043006C00690065006E007400650073002A0000000000000000
        00000046000000FFFFFFFF060000000000000000000000010647007200610076
        00610072002C000000000000000000000050000000FFFFFFFF63870B00000000
        0000000000010749006E00730065007200690072002C00000000000000000000
        005A000000FFFFFFFF080000000000000000000000010741006C007400650072
        00610072002C00000000000000000000005B000000FFFFFFFF09000000000000
        000000000001074500780063006C00750069007200}
      LineColor = clScrollBar
      Checkboxes = True
    end
    object btnExpand: TJvXPButton
      Left = 28
      Top = 500
      Width = 23
      Hint = 'CRMi|Expandir '#193'rvore|0'
      CustomHint = dmPrincipal.BalloonHint1
      Caption = '[+]'
      TabOrder = 4
      Layout = blGlyphTop
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = btnExpandClick
    end
    object btnColapse: TJvXPButton
      Left = 51
      Top = 500
      Width = 22
      Hint = 'CRMi|Retrair '#193'rvore|0'
      CustomHint = dmPrincipal.BalloonHint1
      Caption = '[-]'
      TabOrder = 5
      Layout = blGlyphTop
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = btnColapseClick
    end
    object chkExpandido: TCheckBox
      Left = 77
      Top = 503
      Width = 97
      Height = 17
      Caption = 'Auto Expandir?'
      TabOrder = 6
    end
  end
end
