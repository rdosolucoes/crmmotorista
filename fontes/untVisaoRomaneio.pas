unit untVisaoRomaneio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseVisao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinSilver,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  Data.DB, cxDBData, Vcl.StdCtrls, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ComCtrls, dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons, JvLabel,
  Vcl.ExtCtrls, cxImageComboBox, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, cxNavigator,
  dxDateRanges, cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmVisaoRomaneio = class(TfrmBaseVisao)
    cxGrid1DBTableView1IDROMANEIO: TcxGridDBColumn;
    cxGrid1DBTableView1TIPO: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEPARC: TcxGridDBColumn;
    cxGrid1DBTableView1DATACRIACAO: TcxGridDBColumn;
    cxGrid1DBTableView1DATAROTEIRIZACAO: TcxGridDBColumn;
    cxGrid1DBTableView1DATAFECHAMENTO: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    JvXPButton2: TJvXPButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1STATUSGetCellHint(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
      var AHintText: TCaption; var AIsHintMultiLine: Boolean;
      var AHintTextRect: TRect);
    procedure JvXPButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure pSetaDireitos; override;
    //
    procedure pNovo; override;
    procedure pEditar; override;
    procedure pExcluir; override;
    procedure pRefresh; override;
    //--
    Procedure pAbrirTela;
  end;

var
  frmVisaoRomaneio: TfrmVisaoRomaneio;

implementation

uses untCadRomaneio, udmRomaneio, untFuncoes, untPrincipal;

{$R *.dfm}

{ TfrmVisaoRomaneio }

procedure TfrmVisaoRomaneio.cxGrid1DBTableView1STATUSGetCellHint(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
  var AHintText: TCaption; var AIsHintMultiLine: Boolean;
  var AHintTextRect: TRect);
begin
  inherited;
  AHintText := VarToStr(ARecord.DisplayTexts[Sender.Index]);
  AIsHintMultiLine := True;
end;

procedure TfrmVisaoRomaneio.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if frmCadRomaneio <> nil then
    frmCadRomaneio.Close;
  frmVisaoRomaneio := nil;
  dmRomaneio.Free;
end;

procedure TfrmVisaoRomaneio.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmRomaneio, dmRomaneio);
  cdsPad := dmRomaneio.cdsRomaneio;
  sqlPad := dmRomaneio.sqlRomaneio;
  Tabela := 'CRMROMANEIO';
end;

procedure TfrmVisaoRomaneio.FormShow(Sender: TObject);
begin
  inherited;
  pRefresh;
  btnFiltrar.Visible := false;
end;

procedure TfrmVisaoRomaneio.JvXPButton2Click(Sender: TObject);
begin
  inherited;
  CarregaRelatorio('Relatório de Romaneio',['CODEMP','IDROMANEIO'],[dmRomaneio.cdsRomaneioCODEMP.AsString,dmRomaneio.cdsRomaneioIDROMANEIO.AsString]);
end;

procedure TfrmVisaoRomaneio.pAbrirTela;
begin
  pAbreCadastro(TfrmCadRomaneio,frmCadRomaneio);
end;

procedure TfrmVisaoRomaneio.pEditar;
begin
  inherited;
  if (dmRomaneio.cdsRomaneio.RecNo <= 0) then
    Exit;

  pAbrirTela;
end;

procedure TfrmVisaoRomaneio.pExcluir;
var
  sSQl,sID :String;
begin
  inherited;
  with dmRomaneio.cdsRomaneio do
  begin
    sID := FieldByName('IDROMANEIO').AsString;
    Delete;

    sSQL := 'DELETE FROM CRMITENSROMANEIO WHERE IDROMANEIO = ' + sID +
            ' AND CODEMP = ' + IntToStr(fGetColigada);

    SQLExecute(sSQL);

    if ApplyUpdates(0) > 0 then
      raise Exception.Create('Erro ao Excluir Registro.');
  end;
end;

procedure TfrmVisaoRomaneio.pNovo;
begin
  inherited;
  pAbrirTela;
end;

procedure TfrmVisaoRomaneio.pRefresh;
var
  chv :Integer;
begin
  inherited;
  chv := -1;
  with dmRomaneio.cdsRomaneio do
  begin
    if Active then
      chv := FieldByName('IDRomaneio').AsInteger;

    EnableControls;
    Close;
    Params[0].AsInteger := fGetColigada;
    Open;
    ControlsDisabled;



    if (chv > -1) then
      Locate('IDRomaneio', chv, []);
  end;
  pRegistros;
end;

procedure TfrmVisaoRomaneio.pSetaDireitos;
begin
  inherited;
  btnAdicionar.Enabled := frmprincipal.A01802.Enabled;
  btnEditar.Enabled := frmprincipal.A01803.Enabled;
  dmRomaneio.dsRomaneio.AutoEdit := frmprincipal.A01803.Enabled;
  btnExcluir.Enabled := frmprincipal.A01804.Enabled;
  panel1.Enabled := frmprincipal.A01805.Enabled;
end;

end.
