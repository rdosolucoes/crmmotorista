unit udmSeparacao;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.DB, Datasnap.Provider,
  Datasnap.DBClient, Data.SqlExpr, Vcl.ImgList, Vcl.Controls, cxGraphics,
  System.ImageList, cxImageList;

type
  TdmSeparacao = class(TDataModule)
    sqlSeparacao: TSQLDataSet;
    cdsSeparacao: TClientDataSet;
    dspSeparacao: TDataSetProvider;
    dsSeparacao: TDataSource;
    sqlSeparacaoNUMNOTA: TFMTBCDField;
    sqlSeparacaoNOMEPARC: TWideStringField;
    sqlSeparacaoAPELIDO: TWideStringField;
    sqlSeparacaoNOMEREG: TWideStringField;
    cdsSeparacaoNUMNOTA: TFMTBCDField;
    cdsSeparacaoNOMEPARC: TWideStringField;
    cdsSeparacaoAPELIDO: TWideStringField;
    cdsSeparacaoNOMEREG: TWideStringField;
    cdsSeparacaoSEL: TIntegerField;
    sqlSeparacaoSETORA: TFMTBCDField;
    sqlSeparacaoSETORB: TFMTBCDField;
    sqlSeparacaoSETORC: TFMTBCDField;
    cdsSeparacaoSETORA: TFMTBCDField;
    cdsSeparacaoSETORB: TFMTBCDField;
    cdsSeparacaoSETORC: TFMTBCDField;
    sqlSeparacaoATRASO: TWideStringField;
    cdsSeparacaoATRASO: TWideStringField;
    imgList: TcxImageList;
    sqlSeparacaoNUNOTA: TFMTBCDField;
    cdsSeparacaoNUNOTA: TFMTBCDField;
    sqlSeparacaoDTMOV: TSQLTimeStampField;
    sqlSeparacaoHRENTSAI: TWideStringField;
    cdsSeparacaoDTMOV: TSQLTimeStampField;
    cdsSeparacaoHRENTSAI: TWideStringField;
    sqlSeparacaoSTATUS: TWideStringField;
    cdsSeparacaoSTATUS: TWideStringField;
    sqlSeparacaoHRMOV: TWideStringField;
    cdsSeparacaoHRMOV: TWideStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    SQLPad :String;
  end;

var
  dmSeparacao: TdmSeparacao;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses udmPrincipal,untFuncoes;

{$R *.dfm}

procedure TdmSeparacao.DataModuleCreate(Sender: TObject);
begin
  With dmPrincipal do
  begin
    cdsParametros.Close;
    cdsParametros.Params[0].Value := fGetColigada;
    cdsParametros.Open;
  end;


  SQLPad := ' SELECT  C.NUNOTA,C.NUMNOTA, P.NOMEPARC, C.DTMOV, ' +
            ' LPAD(C.HRMOV,6,0) HRMOV,DTMOV || '' '' || Substr(to_char(LPAD(C.HRMOV,6,0)),1,2) || '':'' ||                    ' +
            ' Substr(to_char(LPAD(C.HRMOV,6,0)),3,2) || '':'' || Substr(to_char(LPAD(C.HRMOV,6,0)),5,2)HRENTSAI,              ' +
            ' CASE WHEN  (TRUNC(1440 * (SYSDATE - to_date((C.DTMOV || LPAD(C.HRMOV,6,0)),''DD/MM/YYHH24MISS'' ))) >           ' +
            ' (SELECT TEMPOSEPARA FROM CRMPARAMETROS PR WHERE (PR.CODCOLIGADA = 0 ))) THEN ''A'' ELSE '''' END ATRASO, ' +
            ' S.SETORA,S.SETORB,S.SETORC,V.APELIDO, R.NOMEREG,S.STATUS  ' +
            ' FROM TGFCAB C  ' +
            ' INNER JOIN TGFPAR P ON C.CODPARC = P.CODPARC  ' +
            ' INNER JOIN TGFVEN V ON C.CODVEND = V.CODVEND  ' +
            ' INNER JOIN TSIREG R ON R.CODREG  = P.CODREG   ' +
            ' LEFT OUTER JOIN CRMSEPARACAO S ON S.NUNOTA = C.NUNOTA AND S.CODEMP = C.CODEMP  ' +
            ' WHERE  ' +
            '  ( ' +
            '      (     C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPER').AsString + ' ) ' +
            '           AND C.PENDENTE = ''S'' ' +
            '      ) ' +
            '      OR ( C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPERTODOS').AsString + ' ) ) ' +
            '  ) ' +
            ' AND C.STATUSNOTA = ''L''  ' +
            ' AND C.CODEMP = :CODEMP  ' +
            ' AND (S.STATUS IS NULL OR S.STATUS = ''R'')  ' +
            ' AND C.AD_CODPARC IS NULL ' +

            ' UNION  ' +

            ' SELECT  C.NUNOTA,C.NUMNOTA, P.NOMEPARC, C.DTMOV, LPAD(C.HRMOV,6,0) HRMOV,DTMOV || '' '' ||   ' +
            ' Substr(to_char(LPAD(C.HRMOV,6,0)),1,2) || '':'' || Substr(to_char(LPAD(C.HRMOV,6,0)),3,2) || '':'' ||  ' +
            ' Substr(to_char(LPAD(C.HRMOV,6,0)),5,2)HRENTSAI, ' +
            ' CASE WHEN  (TRUNC(1440 * (SYSDATE - to_date((C.DTMOV || LPAD(C.HRMOV,6,0)),''DD/MM/YYHH24MISS'' ))) >  ' +
            ' (SELECT TEMPOSEPARA FROM CRMPARAMETROS PR WHERE (PR.CODCOLIGADA = 0 )))   THEN ''A'' ELSE '''' END ATRASO, ' +
            ' S.SETORA,S.SETORB,S.SETORC,V.APELIDO, R.NOMEREG,S.STATUS ' +
            ' FROM TGFCAB C  ' +
            ' INNER JOIN TGFPAR P ON C.AD_CODPARC = P.CODPARC ' +
            ' INNER JOIN TGFVEN V ON C.CODVEND = V.CODVEND  ' +
            ' INNER JOIN TSIREG R ON R.CODREG  = P.CODREG   ' +
            ' LEFT OUTER JOIN CRMSEPARACAO S ON S.NUNOTA = C.NUNOTA AND S.CODEMP = C.CODEMP  ' +
            ' WHERE ' +
            '  ( ' +
            '      (     C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPER').AsString + ' ) ' +
            '           AND C.PENDENTE = ''S'' ' +
            '      ) ' +
            '      OR ( C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPERTODOS').AsString + ' ) ) ' +
            '  ) ' +
            ' AND C.STATUSNOTA = ''L''   ' +
            ' AND C.CODEMP = :CODEMP     ' +
            ' AND (S.STATUS IS NULL OR S.STATUS = ''R'')   ';

    sqlSeparacao.CommandText := SQLPad;
end;

end.
