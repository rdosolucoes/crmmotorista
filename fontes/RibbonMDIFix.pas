unit RibbonMDIFix;

interface

uses
  Windows, Messages, Classes, Controls, Forms, Ribbon;

type
  TCustomRibbonMDIFix = class(TComponent)
  private
    FRibbon: TRibbon;
    procedure SetRibbon(const Value: TRibbon);
  protected
    procedure InstallWindowHook;
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure RemoveWindowHook;
  public
    destructor Destroy; override;
    property Ribbon: TRibbon read FRibbon write SetRibbon;
  end;

  TRibbonMDIFix = class(TCustomRibbonMDIFix)
  published
    property Ribbon;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Ribbon Controls', [TRibbonMDIFix]);
end;

var
  _MenuCallWndHook: HHook;
  _Ribbon: TRibbon;

function CallWindowHook(Code: Integer; wparam: WPARAM; Msg: PCWPStruct): Longint; stdcall;

  procedure UpdateMDIMenuButtons;
  var
    I: Integer;
    Parent: THandle;
  begin
    if _Ribbon = nil then
      Exit;
    Parent := GetParent(Msg.hwnd);
    if Parent = 0 then
      exit;
    if _Ribbon.ApplicationMenu.Menu.ControlCount > 1 then
    begin
      for I := 1 to _Ribbon.ApplicationMenu.Menu.ControlCount - 1 do
        _Ribbon.ApplicationMenu.Menu.Controls[I].Visible := False;
    end;
  end;

begin
  if Code = HC_ACTION then
    case Msg.message of
      // Use of WM_SIZE is required for any Windows version except Themed XP
      {WM_MDIGETACTIVE, } // causes processor usage to skyrocket
      WM_MDIRESTORE, WM_MDIMAXIMIZE, WM_SIZE, WM_MDIDESTROY: UpdateMDIMenuButtons;
    end;
  Result := CallNextHookEx(_MenuCallWndHook, Code, WParam, Longint(Msg));
end;

{ TCustomRibbonMDIFix }

destructor TCustomRibbonMDIFix.Destroy;
begin
  if not (csDesigning in ComponentState) then
    RemoveWindowHook;
  inherited;
end;

procedure TCustomRibbonMDIFix.InstallWindowHook;
begin
  if not (csDesigning in ComponentState) then
  begin
    if _MenuCallWndHook <> 0 then
      RemoveWindowHook;
    _MenuCallWndHook := SetWindowsHookEx(WH_CALLWNDPROC, @CallWindowHook, 0, GetCurrentThreadID);
  end;
end;

procedure TCustomRibbonMDIFix.Loaded;
begin
  inherited;
  InstallWindowHook;
end;

procedure TCustomRibbonMDIFix.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if Operation = opRemove then
  begin
    if AComponent = FRibbon then
    begin
      FRibbon := nil;
      _Ribbon := nil
    end;
  end;
end;

procedure TCustomRibbonMDIFix.RemoveWindowHook;
begin
  UnHookWindowsHookEx(_MenuCallWndHook);
  _MenuCallWndHook := 0;
end;

procedure TCustomRibbonMDIFix.SetRibbon(const Value: TRibbon);
begin
  if FRibbon <> Value then
  begin
    FRibbon := Value;
    _Ribbon := FRibbon;
    if FRibbon <> nil then
      InstallWindowHook
    else
      RemoveWindowHook;
  end;
end;

initialization
  _MenuCallWndHook := 0;

end.
