unit udmBaixaEntrega;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.DB, Datasnap.Provider,
  Datasnap.DBClient, Data.SqlExpr, SimpleDS, Data.DBXOracle;

type
  TdmBaixaEntrega = class(TDataModule)
    dsBaixa: TDataSource;
    cdsBaixa: TClientDataSet;
    dspBaixa: TDataSetProvider;
    cdsBaixaIDROMANEIO: TFMTBCDField;
    cdsBaixaNOMEPARC: TWideStringField;
    cdsBaixaDATAFECHAMENTO: TSQLTimeStampField;
    cdsBaixaSEL: TIntegerField;
    cdsBaixaCOMISSAO: TFMTBCDField;
    cdsBaixaTOTALCOMISSAO: TAggregateField;
    cdsBaixaCODPARC: TFMTBCDField;
    dsMotorista: TDataSource;
    sdsMotorista: TSimpleDataSet;
    sdsMotoristaCODPARC: TFMTBCDField;
    sdsMotoristaNOMEPARC: TWideStringField;
    sqlBaixa: TSQLDataSet;
    sqlBaixaIDROMANEIO: TFMTBCDField;
    sqlBaixaCODPARC: TFMTBCDField;
    sqlBaixaNOMEPARC: TWideStringField;
    sqlBaixaDATAFECHAMENTO: TSQLTimeStampField;
    sqlBaixaCOMISSAO: TFMTBCDField;
    sqlBaixaCLIENTE: TWideStringField;
    cdsBaixaCLIENTE: TWideStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FsqlBaixa,FGroup :String;
  end;

var
  dmBaixaEntrega: TdmBaixaEntrega;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmBaixaEntrega.DataModuleCreate(Sender: TObject);
begin
  FsqlBaixa := ' SELECT * FROM (  ' +
               ' SELECT R.IDROMANEIO,I.CODPARC CODCLI,PI.NOMEPARC CLIENTE,R.CODPARC,P.NOMEPARC,DATAFECHAMENTO, ' +
               ' CASE WHEN P.AD_VEICULO = 1 THEN P.AD_REPASSE_ENTREGA ELSE 0 END COMISSAO ' +
               ' FROM CRMROMANEIO R  ' +
               ' INNER JOIN CRMITENSROMANEIO I ON I.IDROMANEIO = R.IDROMANEIO AND I.CODEMP = R.CODEMP ' +
               ' LEFT OUTER JOIN TGFCAB C ON C.NUNOTA = I.NUNOTA  INNER JOIN TGFPAR P ON P.CODPARC = R.CODPARC ' +
               ' INNER JOIN TGFPAR PI ON PI.CODPARC = I.CODPARC AND C.AD_CODPARC IS NULL ' +
               ' WHERE R.STATUS = ''F''' +

               ' UNION ALL ' +

               ' SELECT R.IDROMANEIO, C.AD_CODPARC CODCLI,PI.NOMEPARC CLIENTE,R.CODPARC,P.NOMEPARC,DATAFECHAMENTO,  ' +
               ' CASE WHEN P.AD_VEICULO = 1 THEN P.AD_REPASSE_ENTREGA ELSE 0 END COMISSAO ' +
               ' FROM CRMROMANEIO R ' +
               ' INNER JOIN CRMITENSROMANEIO I ON I.IDROMANEIO = R.IDROMANEIO AND I.CODEMP = R.CODEMP ' +
               ' LEFT OUTER JOIN TGFCAB C ON C.NUNOTA = I.NUNOTA  INNER JOIN TGFPAR P ON P.CODPARC = R.CODPARC ' +
               ' INNER JOIN TGFPAR PI ON PI.CODPARC = C.AD_CODPARC  ' +
               ' WHERE R.STATUS = ''F'' ' +
               ' )X ';

  FGroup :=  ' GROUP BY X.IDROMANEIO,X.CODCLI,X.CLIENTE,X.CODPARC,X.NOMEPARC,X.DATAFECHAMENTO,X.COMISSAO ';
end;

end.
