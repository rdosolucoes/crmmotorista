unit udmGeradorRelatorios;

interface

uses
  SysUtils, Classes, frxClass, Data.FMTBcd, Data.DB, Datasnap.DBClient,
  Data.SqlExpr, Datasnap.Provider, frxDBXComponents, frxDCtrl, frxDesgn, frxRich,
  frxOLE, frxBarcode, frxExportMail, frxExportPDF, Data.DBXOracle, frxDBSet,
  SimpleDS, frxExportBaseDialog;
type
  TdmGeradorRelatorios = class(TDataModule)
    DsPasta: TDataSource;
    dspPasta: TDataSetProvider;
    DsRel: TDataSource;
    sqlRel: TSQLDataSet;
    cdsRel: TClientDataSet;
    dspRel: TDataSetProvider;
    cdsPasta: TClientDataSet;
    sqlPasta: TSQLDataSet;
    frxReport1: TfrxReport;
    frxDesigner1: TfrxDesigner;
    frxDialogControls1: TfrxDialogControls;
    frxDBXComponents1: TfrxDBXComponents;
    frxRichObject1: TfrxRichObject;
    frxBarCodeObject1: TfrxBarCodeObject;
    frxOLEObject1: TfrxOLEObject;
    frxPDFExport1: TfrxPDFExport;
    cdsPastaIDPASTA: TFMTBCDField;
    cdsPastaNOME: TWideStringField;
    cdsRelIDREL: TFMTBCDField;
    cdsRelIDPASTA: TFMTBCDField;
    cdsRelNOME: TWideStringField;
    cdsRelLAYOUT: TMemoField;
    frxMailExport1: TfrxMailExport;
    sdsCliente: TSimpleDataSet;
    sdsClienteCODPARC: TFMTBCDField;
    sdsClienteNOMEPARC: TWideStringField;
    frxDBCliente: TfrxDBDataset;
    sdsUsuario: TSimpleDataSet;
    sdsUsuarioUSERNAME: TWideStringField;
    frxDBUsuario: TfrxDBDataset;
    sdsEntregador: TSimpleDataSet;
    FMTBCDField1: TFMTBCDField;
    WideStringField1: TWideStringField;
    frxDBEntregador: TfrxDBDataset;
    sdsMotorista: TSimpleDataSet;
    FMTBCDField2: TFMTBCDField;
    WideStringField2: TWideStringField;
    frxDBMotorista: TfrxDBDataset;
    sdsEmpresa: TSimpleDataSet;
    sdsEmpresaCODEMP: TFMTBCDField;
    sdsEmpresaNOMEFANTASIA: TWideStringField;
    sdsEmpresaRAZAOSOCIAL: TWideStringField;
    frxDBEmpresa: TfrxDBDataset;
    procedure cdsPastaNewRecord(DataSet: TDataSet);
    procedure cdsRelNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsPastaAfterPost(DataSet: TDataSet);
    procedure cdsRelAfterPost(DataSet: TDataSet);
    procedure dspPastaBeforeApplyUpdates(Sender: TObject;
      var OwnerData: OleVariant);
    procedure dspRelBeforeApplyUpdates(Sender: TObject;
      var OwnerData: OleVariant);
  private
    { Private declarations }
    function GeraCodigoPasta :Integer;
    function GeraCodigoRel :Integer;
  public
    { Public declarations }
  end;

var
  dmGeradorRelatorios: TdmGeradorRelatorios;

implementation

{$R *.dfm}

uses udmPrincipal,untFuncoes;

{ TdmGeradorRelatorios }

procedure TdmGeradorRelatorios.cdsPastaAfterPost(DataSet: TDataSet);
begin
  cdsPasta.ApplyUpdates(0);
end;

procedure TdmGeradorRelatorios.cdsPastaNewRecord(DataSet: TDataSet);
begin
  cdsPastaIDPASTA.Value := GeraCodigoPasta;
end;

procedure TdmGeradorRelatorios.cdsRelAfterPost(DataSet: TDataSet);
begin
  cdsRel.ApplyUpdates(0);
end;

procedure TdmGeradorRelatorios.cdsRelNewRecord(DataSet: TDataSet);
begin
  cdsRelIDREL.Value := GeraCodigoRel;
end;

procedure TdmGeradorRelatorios.DataModuleCreate(Sender: TObject);
begin
  cdsPasta.Close;
  cdsPasta.Open;

  cdsRel.Close;
  cdsRel.Open;
end;

procedure TdmGeradorRelatorios.dspPastaBeforeApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  dmPrincipal.Conn.CloseDataSets;
end;

procedure TdmGeradorRelatorios.dspRelBeforeApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  dmPrincipal.Conn.CloseDataSets;
end;

function TdmGeradorRelatorios.GeraCodigoPasta: Integer;
var
  qMax :TSQLQuery;
begin
  CriaQuery(qMax);
  qMax.SQL.Add('SELECT NVL(MAX(IDPASTA),0) + 1 FROM CRMPASTA');
  qMax.Open;

  Result := qMax.Fields[0].Value;
  qMax.Close;
  FreeAndNil(qMAx);
end;

function TdmGeradorRelatorios.GeraCodigoRel: Integer;
var
  qMax :TSQLQuery;
begin
  CriaQuery(qMax);
  qMax.SQL.Add('SELECT NVL(MAX(IDREL),0) + 1 FROM CRMREL');
  qMax.Open;

  Result := qMax.Fields[0].Value;
  qMax.Close;
  FreeAndNil(qMAx);
end;

end.
