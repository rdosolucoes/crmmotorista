unit untBaseVisao;

interface

uses Forms, untBase, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  StdCtrls, Classes, ActnList, PlatformDefaultStyleActnCtrls, ActnMan,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, ComCtrls, Graphics, dxGDIPlusClasses,
  JvExControls, JvXPCore, JvXPButtons, Controls, JvLabel, ExtCtrls, Dialogs,
  Messages,dxSkinTheAsphaltWorld,Windows,DBClient,Provider, SqlExpr,
  dxSkinsCore, dxSkinscxPCPainter,strUtils, dxSkiniMaginary, dxSkinSilver,
  cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmBaseVisao = class(TfrmBase)
    pnlBotoes: TPanel;
    pnlMovimento: TPanel;
    btnPrimeiro: TJvXPButton;
    btnAnterior: TJvXPButton;
    btnProximo: TJvXPButton;
    btnUltimo: TJvXPButton;
    pnlTrabalho: TPanel;
    Panel1: TPanel;
    btnSalvarGrid: TJvXPButton;
    btnCarregarGrid: TJvXPButton;
    btnGridPadrao: TJvXPButton;
    PageControl1: TPageControl;
    tbs1: TTabSheet;
    Panel2: TPanel;
    Label1: TLabel;
    btnAdicionar: TJvXPButton;
    btnEditar: TJvXPButton;
    btnExcluir: TJvXPButton;
    btnAtualizar: TJvXPButton;
    btnFiltrar: TJvXPButton;
    JvLabel1: TJvLabel;
    btnColunas: TJvXPButton;
    JvLabel2: TJvLabel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel3: TPanel;
    edtPesquisa: TEdit;
    cbPesquisa: TComboBox;
    btnLocalizar: TJvXPButton;
    JvXPButton1: TJvXPButton;
    cbFiltro: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure btnAdicionarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnColunasClick(Sender: TObject);
    procedure btnSalvarGridClick(Sender: TObject);
    procedure btnCarregarGridClick(Sender: TObject);
    procedure btnGridPadraoClick(Sender: TObject);
    procedure btnPrimeiroClick(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnProximoClick(Sender: TObject);
    procedure btnUltimoClick(Sender: TObject);
    procedure btnLocalizarClick(Sender: TObject);
    procedure JvXPButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormPaint(Sender: TObject);
    procedure edtPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure edtPesquisaChange(Sender: TObject);
    procedure cbFiltroClick(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
    msStream: TStringStream;
    Procedure ControleBarras(ativa :TTabSheet);
  public
    { Public declarations }
    cdsPad: TClientDataSet;
    SQLPad: TSQLDataSet;
    ScriptPad,ScriptFiltro :String;
    Tabela,NomeForm,FiltroPadrao : String;
    Fechar :Boolean;
    procedure pSetaDireitos; virtual; abstract;
    procedure pRegistros;
    //
    procedure pNovo; virtual; abstract;
    procedure pEditar; virtual; abstract;
    procedure pExcluir; virtual; abstract;
    procedure pRefresh; virtual; abstract;
    //--
    procedure pFiltrar;
    function UsafiltroAntes :Boolean;
  end;

var
  frmBaseVisao: TfrmBaseVisao;



implementation

uses untFuncoes, untColunasGrid, SysUtils, udmPrincipal,untFiltro,DBCommon,untPrincipal,
     untSelecionaFiltro,udmFiltros;

{$R *.dfm}

{ TfrmBaseVisao }

procedure TfrmBaseVisao.btnAdicionarClick(Sender: TObject);
begin
  inherited;
   if  cxGrid1DBTableView1.DataController.DataSet.State <> dsBrowse then
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
   cxGrid1DBTableView1.DataController.DataSet.Append;
  pNovo;
end;

procedure TfrmBaseVisao.btnAnteriorClick(Sender: TObject);
begin
  inherited;
  cxGrid1DBTableView1.DataController.DataSet.Prior;
  pRegistros;
end;

procedure TfrmBaseVisao.btnAtualizarClick(Sender: TObject);
begin
  inherited;
  screen.Cursor := crHourGlass;
  pRefresh;
  pRegistros;
  screen.Cursor := crDefault;
end;

procedure TfrmBaseVisao.btnCarregarGridClick(Sender: TObject);
begin
  inherited;
  CarregarGrid(cxGrid1DBTableView1,self.Name);
end;

procedure TfrmBaseVisao.btnColunasClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TfrmColunasGrid, frmColunasGrid);
  frmColunasGrid.grid := cxGrid1DBTableView1;
  frmColunasGrid.ShowModal;
end;

procedure TfrmBaseVisao.btnEditarClick(Sender: TObject);
begin
  inherited;
  if not(cxGrid1DBTableView1.DataController.DataSet.IsEmpty) then
  begin
    if  cxGrid1DBTableView1.DataController.DataSet.State <> dsBrowse then
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
    pEditar;
  end;
end;

procedure TfrmBaseVisao.btnExcluirClick(Sender: TObject);
begin
  inherited;
  if not(cxGrid1DBTableView1.DataController.DataSet.IsEmpty) then
    if Application.MessageBox('Deseja Realmente Excluir Registro(s) Selecionado(s) ?','SANKHYA',MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      pExcluir;
      pRegistros;
    end;
end;

procedure TfrmBaseVisao.btnFiltrarClick(Sender: TObject);
begin
  inherited;
  pFiltrar;
  pRegistros;
end;

procedure TfrmBaseVisao.btnGridPadraoClick(Sender: TObject);
begin
  inherited;
  CarregarGridPadrao(cxGrid1DBTableView1);
end;

procedure TfrmBaseVisao.btnLocalizarClick(Sender: TObject);
var
 i :integer;
begin
  inherited;
  try
    If (cbPesquisa.Text <> '') and (edtPesquisa.Text <> '') then
    begin
      screen.Cursor := crHourGlass;
      if not cdsPad.IsEmpty then
        cxGrid1DBTableView1.Controller.FocusedRecord.Selected := false;

     //acha o campo selecionado nos campos dispon�veis da Query
      For I:= 0 to cdsPad.FieldCount -1 do
      begin
        If cdsPad.Fields[I].DisplayLabel = cbPesquisa.Text then
        begin
          //testa qual � o tipo exigido pelo campo selecionado
          If (cdsPad.Fields[I].DataType = ftinteger)  or
             (cdsPad.Fields[I].DataType = ftsmallint) or
             (cdsPad.Fields[I].DataType = ftTimeStamp)or
             (cdsPad.Fields[I].DataType = ftfloat) or
             (cdsPad.Fields[I].DataType = ftFMTBcd) then
          begin
            //testa se o mesmo � inteiro conforme exigido para
            //n�o ter problema de erro no tipo do dado informado
            if cbFiltro.Checked then
            begin
              if FiltroPadrao = '' then
                cdspad.Filter := FiltroPadrao + cdsPad.Fields[I].FieldName + ' = '+ QuotedStr(edtPesquisa.Text)
              else
                cdspad.Filter := FiltroPadrao + ' AND ' + cdsPad.Fields[I].FieldName + ' = ' + QuotedStr(edtPesquisa.Text);

              cdspad.Filtered := True;
            end
            else
            begin
              cdsPad.Locate(cdsPad.Fields[I].FieldName,edtPesquisa.Text,[ loPartialKey, loCaseInsensitive	]);
              if not cdsPad.IsEmpty then
              begin
                cxGrid1DBTableView1.Controller.FocusedRecord.Selected := true;
                cxGrid1.SetFocus;
              end;
            end;
          end
          else
          begin
            if cbFiltro.Checked then
            begin
              if FiltroPadrao = '' then
                cdspad.Filter := FiltroPadrao + UPPERCASE( cdsPad.Fields[I].FieldName + ' like ('+ QuotedStr(edtPesquisa.Text) + ')')
              else
                cdspad.Filter := FiltroPadrao + ' AND ' + UPPERCASE( cdsPad.Fields[I].FieldName + ' like ('+ QuotedStr (edtPesquisa.Text) + ')');

              cdspad.Filtered := True;
            end
            else
            begin
              cdsPad.Locate(cdsPad.Fields[I].FieldName,edtPesquisa.Text,[ loPartialKey, loCaseInsensitive	]);
              if not cdsPad.IsEmpty then
              begin
                cxGrid1DBTableView1.Controller.FocusedRecord.Selected := true;
                cxGrid1.SetFocus;
              end;
            end;
          end;
        end;
      end;
      screen.Cursor := crDefault;
    end
    else
      if cbFiltro.Checked then
      begin
        cdspad.Filter := FiltroPadrao;
        cdspad.Filtered := True;
      end;
  Except
     Application.MessageBox('Valor informado � incompat�vel com o tipo do Campo.','SANKHYA',mb_ok + MB_ICONERROR);
     screen.Cursor := crDefault;
  end;
end;

procedure TfrmBaseVisao.btnPrimeiroClick(Sender: TObject);
begin
  inherited;
  cxGrid1DBTableView1.DataController.DataSet.First;
  pRegistros;
end;

procedure TfrmBaseVisao.btnProximoClick(Sender: TObject);
begin
  inherited;
  cxGrid1DBTableView1.DataController.DataSet.Next;
  pRegistros;
end;

procedure TfrmBaseVisao.btnSalvarGridClick(Sender: TObject);
begin
  inherited;
  SalvarGrid(cxGrid1DBTableView1,self.Name);
end;

procedure TfrmBaseVisao.btnUltimoClick(Sender: TObject);
begin
  inherited;
  cxGrid1DBTableView1.DataController.DataSet.Last;
  pRegistros;
end;

procedure TfrmBaseVisao.cbFiltroClick(Sender: TObject);
begin
  inherited;
  if not cbFiltro.Checked then
  begin
    cdspad.Filter := FiltroPadrao;
    cdspad.Filtered := True;
  end;

end;

procedure TfrmBaseVisao.ControleBarras(ativa: TTabSheet);
var
  Component: TComponent;
begin
  for Component in Self do
    if Component is TTabSheet then
      TTabSheet(Component).TabVisible := false;

  TPageControl(ativa.Parent).ActivePage := ativa;
end;

procedure TfrmBaseVisao.cxGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  pRegistros;
end;

procedure TfrmBaseVisao.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
  pEditar;
end;

procedure TfrmBaseVisao.edtPesquisaChange(Sender: TObject);
begin
  inherited;
  btnLocalizarClick(nil);
end;

procedure TfrmBaseVisao.edtPesquisaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = VK_RETURN then
    btnLocalizarClick(nil);
end;

procedure TfrmBaseVisao.FormCreate(Sender: TObject);
begin
  inherited;
  //
end;

procedure TfrmBaseVisao.FormPaint(Sender: TObject);
begin
 // inherited;
end;

procedure TfrmBaseVisao.FormShow(Sender: TObject);
var
  i :Integer;
begin
  inherited;
  ScriptPad := SQLPad.CommandText;
  if  UsafiltroAntes and  (Tabela <> '') and not(AnsiMatchText(TForm(Sender).Name,['frmVisaoUsuarios','frmVisaoPerfil','frmVisaoAcessos'])) then
    pFiltrar;

  ControleBarras(tbs1);
  pSetaDireitos;
  btnCarregarGridClick(nil);

  cbPesquisa.Items.Clear;
  For I:= 0 to cdsPad.FieldCount -1 do
  begin
      If cdsPad.Fields[I].Tag = 1 then
      begin
         cbPesquisa.Items.Add(cdsPad.Fields[I].DisplayLabel);
         cbPesquisa.Text := cdsPad.Fields[I].DisplayLabel;
      end;
  end;
  edtPesquisa.SetFocus;

  cbPesquisa.Text := cdsPad.Fields[0].DisplayName;
  FiltroPadrao := cdsPad.Filter;
end;

procedure TfrmBaseVisao.JvXPButton1Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmBaseVisao.pFiltrar;
begin
    if frmSelecionaFiltro = nil then
      Application.CreateForm(TfrmSelecionaFiltro,frmSelecionaFiltro);
    NomeForm :=  TForm(Self).Name;
    frmSelecionaFiltro.cdsPad := cdsPad;
    frmSelecionaFiltro.Tabela := Tabela;
    frmSelecionaFiltro.SQLPad := SQLPad;
    frmSelecionaFiltro.ScriptPad := ScriptPad;
    dmFiltros.cdsFiltro.Close;
    dmFiltros.sqlFiltro.Params[0].Value := fGetColigada;
    dmFiltros.sqlFiltro.Params[1].Value := NomeForm;
    dmFiltros.sqlFiltro.Params[2].Value := fGetUsuario;
    dmFiltros.cdsFiltro.Open;
    if not dmFiltros.cdsFiltro.IsEmpty then
      dmFiltros.cdsFiltro.Locate('ID',dmFiltros.cdsFiltroULTIMOID.AsInteger,[]);
    dmFiltros.NomeForm := NomeForm;
    frmSelecionaFiltro.ShowModal;
    if dmFiltros.Fechou then
    begin
      FreeAndNil(dmFiltros);
      PostMessage(TForm(Self).Handle, WM_CLOSE, 0, 0);
      Abort;
    end;
    ScriptFiltro := SQLPad.CommandText;
    FreeAndNil(dmFiltros);
    btnAtualizarClick(nil);
end;

procedure TfrmBaseVisao.pRegistros;
begin
  StatusBar1.Panels.Items[0].Text := 'Registros: '+
                                     IntToStr(cdsPad.RecNo)+'/'+
                                     IntToStr(cdsPad.RecordCount);
end;

function TfrmBaseVisao.UsafiltroAntes: Boolean;
var
 qAux :TSQLQuery;
begin
 CriaQuery(qAux);
 qAux.SQL.Add(' SELECT USARFILTROANTES FROM CRMPARAMETROS ');
 qAux.SQL.Add(' WHERE (CODCOLIGADA = :COLIGADA OR CODCOLIGADA = 0)');
 qAux.Params[0].Value := fGetColigada;
 qAux.Open;

 if qAux.Fields[0].Value = 1 then
  Result := True
 else
  Result := False;

 FreeAndNil(qAux);
end;

end.
