unit udmRel;

interface

uses
   SysUtils, Classes, frxClass, Data.FMTBcd, Data.DB, Datasnap.DBClient,
  Data.SqlExpr, Datasnap.Provider, frxDBXComponents, frxDCtrl, frxDesgn, frxRich,
  frxOLE, frxBarcode, frxExportMail, frxExportPDF, frxDBSet;

type
  TdmRel = class(TDataModule)
    sqlMov: TSQLDataSet;
    sqlMovCODCOLIGADA: TSmallintField;
    sqlMovIDMOV: TIntegerField;
    sqlMovCODCFO: TStringField;
    sqlMovCODCPG: TStringField;
    sqlMovCODRPR: TStringField;
    sqlMovCODLOC: TStringField;
    sqlMovCODTRA: TStringField;
    sqlMovDATAENTREGA: TSQLTimeStampField;
    sqlMovNORDEM: TStringField;
    sqlMovNOMEFANTASIA: TStringField;
    sqlMovNOME: TStringField;
    sqlMovNOME_2: TStringField;
    sqlMovCODIGOPRD: TStringField;
    sqlMovNOMEFANTASIA_2: TStringField;
    sqlMovCODUND: TStringField;
    sqlMovQUANTIDADE: TFMTBCDField;
    sqlMovPRECOUNITARIO: TFMTBCDField;
    sqlMovNOME_3: TStringField;
    sqlMovDESCRICAO: TStringField;
    sqlMovDATAEMISSAO: TSQLTimeStampField;
    sqlMovCGC: TStringField;
    sqlMovINSCRICAOESTADUAL: TStringField;
    sqlMovNOME_4: TStringField;
    sqlMovNOME_5: TStringField;
    sqlMovNOME_6: TStringField;
    sqlMovCLASSECLIENTE: TStringField;
    sqlMovALIQUOTA: TFMTBCDField;
    sqlMovNUMEROMOVGERADO: TStringField;
    sqlMovVALORDESC: TFMTBCDField;
    sqlMovVALORIPI: TFMTBCDField;
    sqlMovCLASSECLIENTE_2: TStringField;
    sqlMovFCFO_RUA_FCFO_NUMERO_FCF: TStringField;
    sqlMovFCFO_CIDADE_FCFO_CODETD: TStringField;
    sqlMovFCFO_CGCCFO_FCFO_INSCRES: TStringField;
    sqlMovCRMITMMOVIMENTO_PRECOUNIT: TFMTBCDField;
    sqlMovGCOLIGADA_RUA_GCOLIGADA_N: TStringField;
    sqlMovGCOLIGADA_TELEFONE_GCOLIG: TStringField;
    sqlMovCase_when_CRMMOVIMENTO_cl: TStringField;
    sqlMovSELECT_LEFT_TPRD_CODIGOP: TStringField;
    sqlMovSELECT_SUM_Y_TOTAL_SUM_Y: TFMTBCDField;
    DbMov: TfrxDBDataset;
    rptOrcamento: TfrxReport;
    sqlTotais: TSQLDataSet;
    DbTotais: TfrxDBDataset;
    sqlTotaisVALORBRUTO: TFMTBCDField;
    sqlTotaisVALORLIQUIDO: TFMTBCDField;
    sqlTotaisVALORTOTALIPI: TFMTBCDField;
    sqlTotaisVALORDESC: TFMTBCDField;
    sqlTotaisHISTORICOLONGO: TMemoField;
    sqlTotaisCODCOLIGADA: TSmallintField;
    sqlMovGNRE: TSQLDataSet;
    sqlMovGNREIDMOV: TIntegerField;
    sqlMovGNREDATAEMISSAO: TSQLTimeStampField;
    sqlMovGNRECODCOLIGADA: TSmallintField;
    sqlMovGNRECODTMV: TStringField;
    sqlMovGNRECODETD: TStringField;
    sqlMovGNRESERIE: TStringField;
    sqlMovGNRECODCFO: TStringField;
    sqlMovGNRENUMEROMOV: TStringField;
    sqlMovGNRERAZAOSOCIAL: TStringField;
    sqlMovGNRENOMEFANTASIA: TStringField;
    sqlMovGNREENDERECO: TStringField;
    sqlMovGNREMUNICIPIO: TStringField;
    sqlMovGNRECEP: TStringField;
    sqlMovGNRETELEFONE: TStringField;
    sqlMovGNRECGC: TStringField;
    sqlMovGNREIE: TStringField;
    sqlMovGNREDESCRICAO: TStringField;
    sqlMovGNREIDNAT: TIntegerField;
    sqlMovGNREVALORLIQUIDO: TFMTBCDField;
    sqlMovGNREVALORBRUTO: TFMTBCDField;
    sqlMovGNREPERIODO: TStringField;
    sqlMovGNREVALORIPI: TFMTBCDField;
    sqlMovGNREcodrpr: TStringField;
    sqlMovGNRETAXA: TFMTBCDField;
    sqlMovGNREMARGEM: TFMTBCDField;
    sqlMovGNRECODRECEITA: TStringField;
    sqlMovGNREFUNDOPOBREZA: TFMTBCDField;
    sqlMovGNREALIQICMS: TFMTBCDField;
    sqlMovGNRECODNAT: TStringField;
    sqlMovGNREBASEICMSST: TFMTBCDField;
    sqlMovGNREICMSST: TFMTBCDField;
    sqlMovGNREICMS: TFMTBCDField;
    sqlMovGNRERECOLHER: TFMTBCDField;
    sqlMovGNREFUNDODEPOBREZA: TFMTBCDField;
    sqlMovGNRECodRepres: TStringField;
    DBMovGNRE: TfrxDBDataset;
    rptMovGNRE: TfrxReport;
    rptPobreza: TfrxReport;
    sqlMovHISTORICOLONGO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmRel: TdmRel;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses udmPrincipal;

{$R *.dfm}

end.
