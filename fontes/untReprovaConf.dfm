inherited frmReprovaConf: TfrmReprovaConf
  Caption = 'Reprova'#231#227'o de Confer'#234'ncia'
  ClientHeight = 153
  ClientWidth = 364
  ExplicitWidth = 370
  ExplicitHeight = 182
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar1: TStatusBar
    Top = 134
    Width = 364
    ExplicitTop = 134
    ExplicitWidth = 364
  end
  inherited pnlBotoes: TPanel
    Height = 134
    ExplicitHeight = 134
    inherited PageControl1: TPageControl
      Height = 134
      ExplicitHeight = 134
      inherited tbs2: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 6
        ExplicitWidth = 108
        ExplicitHeight = 124
        inherited Panel2: TPanel
          Height = 124
          ExplicitHeight = 124
        end
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 116
    Top = 0
    Width = 248
    Height = 134
    Align = alClient
    Caption = 'Setores'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object cbSetorA: TCheckBox
      Left = 29
      Top = 55
      Width = 41
      Height = 17
      Caption = 'A'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
    end
    object cbSetorB: TCheckBox
      Left = 108
      Top = 55
      Width = 34
      Height = 17
      Caption = 'B'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
    end
    object cbSetorC: TCheckBox
      Left = 181
      Top = 55
      Width = 33
      Height = 17
      Caption = 'C'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 2
    end
  end
end
