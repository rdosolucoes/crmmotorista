unit untColunasGrid;

interface

uses untBase, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCheckBox, DBClient, Classes, ActnList, PlatformDefaultStyleActnCtrls,
  ActnMan, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, dxGDIPlusClasses,
  JvExControls, JvXPCore, JvXPButtons, Controls, StdCtrls, ExtCtrls, ComCtrls,
  dxSkinsCore,dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,dxSkinscxPCPainter, System.Actions;

type
  TfrmColunasGrid = class(TfrmBase)
    acmBotoes: TActionManager;
    actConfirmar: TAction;
    actCancelar: TAction;
    Panel2: TPanel;
    Label2: TLabel;
    btnConfirmar: TJvXPButton;
    btnCancelar: TJvXPButton;
    pnlTrabalho: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cdsCampos: TClientDataSet;
    dtsCampos: TDataSource;
    cdsCamposCampo: TStringField;
    cdsCamposExibir: TStringField;
    cxGrid1DBTableView1Exibir: TcxGridDBColumn;
    cxGrid1DBTableView1Campo: TcxGridDBColumn;
    cdsCamposNome: TStringField;
    cdsCamposagrupado: TStringField;
    procedure FormShow(Sender: TObject);
    procedure actConfirmarExecute(Sender: TObject);
    procedure actCancelarExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    grid :TcxGridDBTableView;
  end;

var
  frmColunasGrid: TfrmColunasGrid;

implementation

{$R *.dfm}

procedure TfrmColunasGrid.actCancelarExecute(Sender: TObject);
begin
  inherited;
  self.Close;
end;

procedure TfrmColunasGrid.actConfirmarExecute(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  if cdsCampos.State <> dsBrowse then
    cdsCampos.Post;

  cdsCampos.First;
  while not(cdsCampos.Eof) do
  begin
    i := grid.FindItemByName(cdsCamposNome.AsString).Index;

    if (cdsCamposExibir.AsString = 'S') and (cdsCamposagrupado.AsString = 'N') then
      grid.Columns[i].Visible := true
    else
      grid.Columns[i].Visible := false;
    //
    cdsCampos.Next;
  end;

  self.Close;
end;

procedure TfrmColunasGrid.FormShow(Sender: TObject);
var
  I: integer;
begin
  inherited;
  StatusBar1.Panels[0].Text := '';
  with cdsCampos do
  begin
    CreateDataSet;
    if grid = nil then
      Exit;

    for I := 0 to grid.ColumnCount - 1 do
    begin
      Append;
      cdsCamposCampo.AsString := grid.Columns[i].Caption;
      cdsCamposNome.AsString := grid.Columns[i].Name;
      //
      if (grid.Columns[i].GroupIndex >= 0) then
        cdsCamposagrupado.AsString := 'S'
      else
        cdsCamposagrupado.AsString := 'N';
      //
      If ((grid.Columns[i].Visible) or (grid.Columns[i].GroupIndex >= 0)) then
        cdsCamposExibir.AsString := 'S'
      else
        cdsCamposExibir.AsString := 'N';

      Post;
    end;//for
  end;//with
end;

end.
