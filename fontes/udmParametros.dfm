object dmParametros: TdmParametros
  OldCreateOrder = False
  Height = 80
  Width = 313
  object dsParametros: TDataSource
    DataSet = cdsParametros
    Left = 231
    Top = 15
  end
  object dspParametros: TDataSetProvider
    DataSet = sqlParametros
    Left = 96
    Top = 16
  end
  object cdsParametros: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'PCOLIG'
        ParamType = ptInput
        Value = 0
      end>
    ProviderName = 'dspParametros'
    AfterPost = cdsParametrosAfterPost
    OnNewRecord = cdsParametrosNewRecord
    OnReconcileError = cdsParametrosReconcileError
    Left = 168
    Top = 16
    object cdsParametrosCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 32
    end
    object cdsParametrosENTERCOMOTAB: TFMTBCDField
      FieldName = 'ENTERCOMOTAB'
      Precision = 32
    end
    object cdsParametrosUSARFILTROANTES: TFMTBCDField
      FieldName = 'USARFILTROANTES'
      Precision = 32
    end
    object cdsParametrosTEMPOSEPARA: TFMTBCDField
      FieldName = 'TEMPOSEPARA'
      Precision = 32
    end
    object cdsParametrosTEMPOCONFERE: TFMTBCDField
      FieldName = 'TEMPOCONFERE'
      Precision = 32
    end
    object cdsParametrosTEMPOLIBERA: TFMTBCDField
      FieldName = 'TEMPOLIBERA'
      Precision = 32
    end
    object cdsParametrosTEMPOENTREGA: TFMTBCDField
      FieldName = 'TEMPOENTREGA'
      Precision = 32
    end
    object cdsParametrosDIASIMPORT: TFMTBCDField
      FieldName = 'DIASIMPORT'
      Precision = 32
    end
    object cdsParametrosLIMITEREGISTROS: TFMTBCDField
      FieldName = 'LIMITEREGISTROS'
      Precision = 32
    end
    object cdsParametrosCODTIPOPER: TFMTBCDField
      FieldName = 'CODTIPOPER'
      Precision = 32
    end
    object cdsParametrosCODTIPOPERTRANSF: TFMTBCDField
      FieldName = 'CODTIPOPERTRANSF'
      Precision = 32
    end
    object cdsParametrosCODTIPOSPER: TWideStringField
      FieldName = 'CODTIPOSPER'
      Size = 500
    end
    object cdsParametrosCODTIPOSPERTODOS: TWideStringField
      FieldName = 'CODTIPOSPERTODOS'
      Size = 500
    end
  end
  object sqlParametros: TSQLDataSet
    CommandText = 
      'SELECT *'#13#10'FROM CRMPARAMETROS P'#13#10'WHERE (P.CODCOLIGADA = :PCOLIG O' +
      'R P.CODCOLIGADA = 0)'
    MaxBlobSize = 1
    Params = <
      item
        DataType = ftUnknown
        Name = 'PCOLIG'
        ParamType = ptInput
      end>
    SQLConnection = dmPrincipal.Conn
    Left = 24
    Top = 16
    object sqlParametrosCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 32
    end
    object sqlParametrosENTERCOMOTAB: TFMTBCDField
      FieldName = 'ENTERCOMOTAB'
      Precision = 32
    end
    object sqlParametrosUSARFILTROANTES: TFMTBCDField
      FieldName = 'USARFILTROANTES'
      Precision = 32
    end
    object sqlParametrosTEMPOSEPARA: TFMTBCDField
      FieldName = 'TEMPOSEPARA'
      Precision = 32
    end
    object sqlParametrosTEMPOCONFERE: TFMTBCDField
      FieldName = 'TEMPOCONFERE'
      Precision = 32
    end
    object sqlParametrosTEMPOLIBERA: TFMTBCDField
      FieldName = 'TEMPOLIBERA'
      Precision = 32
    end
    object sqlParametrosTEMPOENTREGA: TFMTBCDField
      FieldName = 'TEMPOENTREGA'
      Precision = 32
    end
    object sqlParametrosDIASIMPORT: TFMTBCDField
      FieldName = 'DIASIMPORT'
      Precision = 32
    end
    object sqlParametrosLIMITEREGISTROS: TFMTBCDField
      FieldName = 'LIMITEREGISTROS'
      Precision = 32
    end
    object sqlParametrosCODTIPOPER: TFMTBCDField
      FieldName = 'CODTIPOPER'
      Precision = 32
    end
    object sqlParametrosCODTIPOPERTRANSF: TFMTBCDField
      FieldName = 'CODTIPOPERTRANSF'
      Precision = 32
    end
    object sqlParametrosCODTIPOSPER: TWideStringField
      FieldName = 'CODTIPOSPER'
      Size = 500
    end
    object sqlParametrosCODTIPOSPERTODOS: TWideStringField
      FieldName = 'CODTIPOSPERTODOS'
      Size = 500
    end
  end
end
