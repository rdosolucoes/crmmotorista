unit untLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, jpeg, Menus, dxGDIPlusClasses,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, dxSkinsCore,dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  JvExControls, JvMarkupLabel, cxLabel,cxTextEdit, JvExStdCtrls, JvEdit, cxImage,
  JvXPCore, JvXPButtons;

type
  TParamConn = record
    Nome :String;
    SchemaOverride :String;
    DriverName :String;
    HostName :String;
    Database :String;
    Mars_Connection :String;
    ConnectionString :String;
    UserName :String;
    Password :String;
end;

type
  TfrmLogin = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    btnSair: TSpeedButton;
    btnSenha: TSpeedButton;
    edtUser: TEdit;
    edtPass: TEdit;
    ListBox1: TListBox;
    ListBox2: TListBox;
    cmbConexao: TComboBox;
    Pop: TPopupMenu;
    btnConn: TSpeedButton;
    lblConn: TLabel;
    lbluser: TLabel;
    lblPass: TLabel;
    btLimpar: TSpeedButton;
    mSettings: TMemo;
    Label1: TLabel;
    Label2: TLabel;
    btnLogin: TJvXPButton;
    Label3: TLabel;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    procedure btnLoginClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cmbConexaoChange(Sender: TObject);
    procedure menuClick(Sender: TObject);
    procedure btnConnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtPassExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure edtPassChange(Sender: TObject);
    procedure edtUserChange(Sender: TObject);
    procedure edtUserKeyPress(Sender: TObject; var Key: Char);
    procedure edtPassKeyPress(Sender: TObject; var Key: Char);
    procedure btLimparClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    PConn :array of TParamConn;
    Setado :integer;
    podefechar :boolean;
    erros :integer;

    Procedure pConexoes;
    Function fChecaVersoes:boolean;
  public
    { Public declarations }
  end;
  Const
    ConexoesBase   :String = 'Corpore_';
    //Params conexao fixo
    UserName       :String = 'rm';
    password       :String = 'rm';
    PrepareSql     :String = 'True';
    ConnectTimeOut :String = '60';
    //-----------------------------

var
  frmLogin: TfrmLogin;


implementation

{$R *.dfm}

uses IniFiles, udmPrincipal, untPrincipal, untFuncoes;

procedure TfrmLogin.btnLoginClick(Sender: TObject);
begin
  if (edtUser.Text = '') then
  begin
    MessageDlg('Informe o usu�rio',mtInformation,[mbOK],0);
    edtUser.SetFocus;
    exit;
  end;
  if (edtPass.Text = '') then
  begin
    MessageDlg('Informe a senha',mtInformation,[mbOK],0);
    edtPass.SetFocus;
    exit;
  end;
  //
  with dmPrincipal do
  begin
    cdsUsuario.Close;
    cdsUsuario.Params.ParamByName('puser').AsString := edtUser.Text;
    cdsUsuario.Params.ParamByName('ppass').AsString := Cripto(edtPass.Text);
    cdsUsuario.Open;


    if (cdsUsuario.FieldByName('ativo').AsString = 'N') then
      raise Exception.Create('Usu�rio Inativo!');


    if (cdsUsuario.IsEmpty) then
    begin
      inc(erros);
      if (erros = 1) then
        MessageDlg('Usu�rio/Senha inv�lidos.'+#13+#13+
                   'Tentativa 1/3',mtInformation,[mbOK],0)
      else if (erros = 2) then
        MessageDlg('Usu�rio/Senha inv�lidos.'+#13+
                   'Lembre-se teclas mai�sculas e minusculas N�O fazem diferen�a.'+#13+#13+
                   'Tentativa 2/3',mtInformation,[mbOK],0)
      else
        try
          podefechar := true;
          ModalResult := mrNo;
        except
        end;
      //
      edtUser.Clear;
      edtPass.Clear;
    end
    else
    begin
        If fChecaVersoes then
        begin
        podefechar := true;
        ModalResult := mrYes;

         try
          mSettings.Clear;
          mSettings.Lines.Add( edtUser.Text );
          mSettings.Lines.SaveToFile( ExtractFilePath(Application.ExeName) + 'CRMCONFIG.TXT' );
         except
         end;
        end;
    end;
  end;//with
end;

procedure TfrmLogin.btnSairClick(Sender: TObject);
begin
  podefechar := true;
  dmPrincipal.Conn.close;
  ModalResult := mrNo;
end;

procedure TfrmLogin.Button1Click(Sender: TObject);
begin
  showmessage(CarregaSetor);
end;

procedure TfrmLogin.cmbConexaoChange(Sender: TObject);
var
  i :integer;
  x: integer;
begin
  StatusBar1.Panels[0].Text := 'Servidor: ';
  StatusBar1.Panels[1].Text := 'BD: ';

  for I := 0 to length(PConn)-1 do
  begin
     If (PConn[i].Nome = cmbConexao.Text) then
     begin
       StatusBar1.Panels[0].Text := 'Servidor: '+PConn[i].HostName;
       StatusBar1.Panels[1].Text := 'BD: '+PConn[i].Database;
       Setado := i;

       With dmPrincipal do
       begin
         Conn.close;
         conn.ConnectionName := cmbConexao.Text;
         //nao funfa assim
      {   Conn.Params.Add('SchemaOverride='+PConn[i].SchemaOverride);
         Conn.Params.Add('DriverName='+PConn[i].DriverName);
         Conn.Params.Add('HostName='+PConn[i].HostName);
         Conn.Params.Add('Database='+PConn[i].Database);
         Conn.Params.Add('PrepareSql='+PrepareSql);
         Conn.Params.Add('ConnectTimeOut='+ConnectTimeOut);
         Conn.Params.Add('Mars_Connection='+PConn[i].Mars_Connection);
         Conn.Params.Add('ConnectionString='+PConn[i].ConnectionString);   }

         if True then


         Conn.Params.Values['SchemaOverride']:= PConn[i].SchemaOverride;
         Conn.Params.Values['DriverName'] := PConn[i].DriverName;
         Conn.Params.Values['HostName'] := PConn[i].HostName;
         Conn.Params.Values['User_Name'] := PConn[i].UserName;
         Conn.Params.Values['Password'] := PConn[i].Password;
         Conn.Params.Values['PrepareSql'] := PrepareSql;
         Conn.Params.Values['ConnectTimeOut'] := ConnectTimeOut;
         Conn.Params.Values['Database']:= PConn[i].Database;

   {      Conn.Params.Values['Mars_Connection']:= PConn[i].Mars_Connection;
         Conn.Params.Values['ConnectionString']:= PConn[i].ConnectionString; }


      {  Conn.Params.Values['Database']:='Corpore_CRM';
         Conn.Params.Values['UserName'] := 'rm';
         Conn.Params.Values['password'] := 'rm';
         Conn.Params.Values['PrepareSql'] := PrepareSql;
         Conn.Params.Values['ConnectTimeOut'] := ConnectTimeOut;     }

         Conn.Open;
       end;//with

       Break;
     end;//if
  end;//for

  edtUser.Enabled := (dmPrincipal.Conn.Connected);
  edtPass.Enabled := (dmPrincipal.Conn.Connected);
end;

procedure TfrmLogin.edtPassChange(Sender: TObject);
var Aux, i : Integer;
    Aux2 : String;
begin
  Aux2 := '';

  Aux := Length(edtPass.Text);

  for i := 0 to Pred(Aux) do
    Aux2 := Aux2 + '*';

  lblPass.Caption := Aux2;
end;

procedure TfrmLogin.edtPassExit(Sender: TObject);
begin
  if (edtUser.Text <> '')  and (edtPass.Text <> '') then
    btnLoginClick(self);
end;


procedure TfrmLogin.edtPassKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnLoginClick(nil);
end;

procedure TfrmLogin.edtUserChange(Sender: TObject);
begin
  lbluser.Caption := edtUser.Text;
end;

procedure TfrmLogin.edtUserKeyPress(Sender: TObject; var Key: Char);
begin
    if (Key = #13) then
  begin
    Key := #0;
    Perform(Wm_NextDlgCtl,0,0);
  end
end;

Function TfrmLogin.fChecaVersoes: boolean;
var
  vApl, vBD :String;
begin
  result := true;
  with dmPrincipal do
  begin
    cdsVersao.Close;
    cdsVersao.Open;

    vApl := cdsVersao.FieldByName('VERSAOSIS').AsString;
    vBD  := cdsVersao.FieldByName('VERSAOBD').AsString;

  {  If (vApl <> frmPrincipal.fGetVersao('A')) then
    begin
      result := false;
      MessageDlg('Vers�es incompat�veis !'+#13+#13+
                 'Vers�o Atual do Sistema: '+VersaoExe+#13+
                 'Vers�o de Controle: '+vApl+#13+#13+
                 'Atualize o sistema e tente novamente.',mtInformation,[mbok],0);
    end
    else if (vBD <> frmPrincipal.fGetVersao('B')) then
    begin
      result := false;
      MessageDlg('Vers�es incompat�veis !'+#13+#13+
                 'Vers�o Atual do B.D.: '+frmPrincipal.fGetVersao('B')+#13+
                 'Vers�o de Controle: '+vBD+#13+#13+
                 'Atualize o sistema e tente novamente.',mtInformation,[mbok],0);
    end;    }
  end;//with
end;

procedure TfrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if podefechar then
    Action := caFree
  else
    Action := caNone;
end;

procedure TfrmLogin.FormCreate(Sender: TObject);
var
  Arq :String;
  f :TIniFile;
  i: Integer;

  Procedure pCopyConexao(Nome :String);
  var
    i, x, posi :integer;
    temp1 :string;
  begin
    i := Length(PConn)+1;
    SetLength(PConn, i);
    i := i - 1;

    PConn[i].Nome := Nome;
    cmbConexao.Items.Add(Nome);

    for x := 0 to ListBox2.Items.Count - 1 do
    begin
      temp1 := ListBox2.Items[x];

      if pos('ConnectionString',temp1) > 0 then
      begin
        posi := Pos('=',temp1);
        PConn[i].ConnectionString := Copy(temp1, posi+1, length(temp1) - posi);
      end
      else if pos('SchemaOverride',temp1) > 0 then
      begin
        posi := Pos('=',temp1);
        PConn[i].SchemaOverride := Copy(temp1, posi+1, length(temp1) - posi);
      end
      else if pos('DriverName',temp1) > 0 then
      begin
        posi := Pos('=',temp1);
        PConn[i].DriverName := Copy(temp1, posi+1, length(temp1) - posi);
      end
      else if pos('HostName',temp1) > 0 then
      begin
        posi := Pos('=',temp1);
        PConn[i].HostName := Copy(temp1, posi+1, length(temp1) - posi);
      end
      else if pos('Database',temp1) > 0 then
      begin
        posi := Pos('=',temp1);
        PConn[i].Database := Copy(temp1, posi+1, length(temp1) - posi);
      end
      else if pos('Mars_Connection',temp1) > 0 then
      begin
        posi := Pos('=',temp1);
        PConn[i].Mars_Connection := Copy(temp1, posi+1, length(temp1) - posi);
      end
      else if pos('User_Name',temp1) > 0 then
      begin
        posi := Pos('=',temp1);
        PConn[i].UserName := Copy(temp1, posi+1, length(temp1) - posi);
      end
      else if pos('Password',temp1) > 0 then
      begin
        posi := Pos('=',temp1);
        PConn[i].Password := Copy(temp1, posi+1, length(temp1) - posi);
      end;


    end;
  end;

begin
  erros := 0;
  //
  podefechar := false;
  cmbConexao.Items.Clear;
  Setado := -1;
  Arq := ExtractFilePath(ParamStr(0))+'Conexoes.ini';

  if not FileExists(Arq) then
    exit;

  f := TIniFile.Create(Arq);

  try
    f.ReadSections(ListBox1.Items);

    for i := 0 to ListBox1.Items.Count - 1 do
      if (Pos(ConexoesBase, ListBox1.Items.Strings[i]) > 0)  then
      begin
        f.ReadSectionValues(ListBox1.Items.Strings[i], ListBox2.Items);
        pCopyConexao(ListBox1.Items.Strings[i]);
      end;

  finally
    f.Free;
    cmbConexao.ItemIndex := 0;
    cmbConexao.Text := cmbConexao.Items.Strings[0];
    cmbConexaoChange(self);
  end;
  pConexoes;
end;

procedure TfrmLogin.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    btnLoginClick(nil)
  else
    if key = VK_ESCAPE then
      btnSairClick(nil);

end;

procedure TfrmLogin.FormShow(Sender: TObject);
begin
  try
     mSettings.Lines.LoadFromFile( ExtractFilePath(Application.ExeName) + 'CRMCONFIG.TXT');
  except
  end;
  if mSettings.Lines.Count > 0 then
  begin
    edtUser.Text      := mSettings.Lines.Strings[0];
    edtPass.SetFocus;
  end
  else
    edtUser.SetFocus;
end;

procedure TfrmLogin.pConexoes;
var
  Menu :TMenuItem;
  I :Integer;
begin
  try
    for i := 0 to cmbConexao.Items.Count - 1 do
    begin
      Menu := TMenuItem.Create(Self);
      pop.Items.Insert(i ,Menu);
      Menu.OnClick    := MenuClick;
      Menu.Caption    := cmbConexao.Items.Strings[I];
      Menu.RadioItem  := true;
      Menu.GroupIndex := 0;

      If i = 0 then
      begin
        Menu.Checked := true;
        lblConn.Caption := cmbConexao.Items.Strings[I]
      end;
    end;//for
  Finally
  end;
end;

procedure TfrmLogin.btLimparClick(Sender: TObject);
begin
  edtUser.Clear;
  edtPass.Clear;
end;

procedure TfrmLogin.btnConnClick(Sender: TObject);
var
  x, y :integer;
begin
  x := round(Screen.Width/2)+ 100;
  y := round(Screen.Height/2)-100;
  pop.Popup(x, y);
end;

procedure TfrmLogin.menuClick(Sender: TObject);
begin
  lblConn.Caption := (Sender as TMenuItem).Caption;
  (Sender as TMenuItem).Checked := true;

  cmbConexao.ItemIndex := (Sender as TMenuItem).MenuIndex;
  cmbConexao.Text := cmbConexao.Items.Strings[cmbConexao.ItemIndex];
  cmbConexaoChange(self);
end;

end.
