unit udmPerfil;

interface

uses
  SysUtils, Classes, FMTBcd, DB, SqlExpr, DBClient, Provider;

type
  TdmPerfil = class(TDataModule)
    dspPerfil: TDataSetProvider;
    cdsPerfil: TClientDataSet;
    sqlPerfil: TSQLDataSet;
    dtsPerfil: TDataSource;
    cdsAcoes: TClientDataSet;
    cdsAcoesIndex: TIntegerField;
    cdsAcoesTexto: TStringField;
    cdsAcoescodigo: TStringField;
    cdsPerfilIDCRMPERFIL: TFMTBCDField;
    cdsPerfilCODCOLIGADA: TFMTBCDField;
    cdsPerfilCODPERFIL: TWideStringField;
    cdsPerfilNOME: TWideStringField;
    cdsPerfilMENUS: TMemoField;
    sqlPerfilIDCRMPERFIL: TFMTBCDField;
    sqlPerfilCODCOLIGADA: TFMTBCDField;
    sqlPerfilCODPERFIL: TWideStringField;
    sqlPerfilNOME: TWideStringField;
    sqlPerfilMENUS: TMemoField;
    procedure dtsPerfilStateChange(Sender: TObject);
    procedure cdsPerfilBeforePost(DataSet: TDataSet);
    procedure cdsPerfilNewRecord(DataSet: TDataSet);
    procedure cdsPerfilAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmPerfil: TdmPerfil;

implementation

uses untCadPerfil, untFuncoes;

{$R *.dfm}

procedure TdmPerfil.cdsPerfilAfterScroll(DataSet: TDataSet);
begin
  if (frmCadPerfil <> nil) then
    frmCadPerfil.pPopulaTree;
end;

procedure TdmPerfil.cdsPerfilBeforePost(DataSet: TDataSet);
begin
  if (cdsPerfil.FieldByName('IDCRMPERFIL').IsNull) then
    cdsPerfil.FieldByName('IDCRMPERFIL').AsInteger := GeraCodigo('IDCRMPERFIL');
end;

procedure TdmPerfil.cdsPerfilNewRecord(DataSet: TDataSet);
begin
  cdsPerfil.FieldByName('CODCOLIGADA').AsInteger := 0;
end;

procedure TdmPerfil.dtsPerfilStateChange(Sender: TObject);
begin
  if dtsPerfil.State = dsEdit then
    if (frmCadPerfil <> nil) then
      frmCadPerfil.PageControl1.ActivePageIndex := 1;
end;

end.
