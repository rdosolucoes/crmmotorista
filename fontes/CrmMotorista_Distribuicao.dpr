program CrmMotorista_Distribuicao;




uses
  MidasLib,
  Vcl.Forms,
  udmAcessos in 'udmAcessos.pas' {dmAcessos: TDataModule},
  udmGeradorRelatorios in 'udmGeradorRelatorios.pas' {dmGeradorRelatorios: TDataModule},
  udmPerfil in 'udmPerfil.pas' {dmPerfil: TDataModule},
  udmPrincipal in 'udmPrincipal.pas' {dmPrincipal: TDataModule},
  udmUsuarios in 'udmUsuarios.pas' {dmUsuarios: TDataModule},
  untBase in 'untBase.pas' {frmBase},
  untBaseCad in 'untBaseCad.pas' {frmBaseCad},
  untBaseConfirmacao in 'untBaseConfirmacao.pas' {frmBaseConfirmacao},
  untBaseVisao in 'untBaseVisao.pas' {frmBaseVisao},
  untCadAcessos in 'untCadAcessos.pas' {frmCadAcessos},
  untCadPerfil in 'untCadPerfil.pas' {frmCadPerfil},
  untCadUsuarios in 'untCadUsuarios.pas' {frmCadUsuarios},
  untColunasGrid in 'untColunasGrid.pas' {frmColunasGrid},
  untFiltro in 'untFiltro.pas' {frmFiltro},
  untFuncoes in 'untFuncoes.pas',
  untLocalizar in 'untLocalizar.pas' {frmLocalizar},
  untLogin in 'untLogin.pas' {frmLogin},
  untParametros in 'untParametros.pas' {frmParametros},
  untPrincipal in 'untPrincipal.pas' {frmPrincipal},
  untSetaEmpresa in 'untSetaEmpresa.pas' {frmSetaEmpresa},
  untTrocaSenha in 'untTrocaSenha.pas' {frmTrocaSenha},
  untVisaoAcessos in 'untVisaoAcessos.pas' {frmVisaoAcessos},
  untVisaoPerfil in 'untVisaoPerfil.pas' {frmVisaoPerfil},
  untVisaoUsuarios in 'untVisaoUsuarios.pas' {frmVisaoUsuarios},
  untGeradorRel in 'untGeradorRel.pas' {frmGeradorRel},
  untSelecionaFiltro in 'untSelecionaFiltro.pas' {frmSelecionaFiltro},
  udmFiltros in 'udmFiltros.pas' {dmFiltros: TDataModule},
  udmPesquisa in 'udmPesquisa.pas' {dmPesquisa: TDataModule},
  untPesquisa in 'untPesquisa.pas' {frmPesquisa},
  udmParametros in 'udmParametros.pas' {dmParametros: TDataModule},
  udmRel in 'udmRel.pas' {dmRel: TDataModule},
  untSeparacao in 'untSeparacao.pas' {frmSeparacao},
  udmSeparacao in 'udmSeparacao.pas' {dmSeparacao: TDataModule},
  untConferencia in 'untConferencia.pas' {frmConferencia},
  udmConferencia in 'udmConferencia.pas' {dmConferencia: TDataModule},
  untReprovaConf in 'untReprovaConf.pas' {frmReprovaConf},
  udmRomaneio in 'udmRomaneio.pas' {dmRomaneio: TDataModule},
  untCadRomaneio in 'untCadRomaneio.pas' {frmCadRomaneio},
  untVisaoRomaneio in 'untVisaoRomaneio.pas' {frmVisaoRomaneio},
  Vcl.Themes,
  Vcl.Styles,
  untControleMotoboy in 'untControleMotoboy.pas' {frmControleMotoboy},
  udmControleMotoboy in 'udmControleMotoboy.pas' {dmControleMotoboy: TDataModule},
  untControlePercurso in 'untControlePercurso.pas' {frmControlePercurso},
  untRastreaPedidos in 'untRastreaPedidos.pas' {frmRastreaPedido},
  udmRastreaPedido in 'udmRastreaPedido.pas' {dmRastreaPedido: TDataModule},
  udmBaixaEntrega in 'udmBaixaEntrega.pas' {dmBaixaEntrega: TDataModule},
  untBaixaEntrega in 'untBaixaEntrega.pas' {frmBaixaEntrega},
  udmControlePercurso in 'udmControlePercurso.pas' {dmControlePercurso: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmPrincipal, dmPrincipal);
  Application.Run;
end.
