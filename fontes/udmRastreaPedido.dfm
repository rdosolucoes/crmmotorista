object dmRastreaPedido: TdmRastreaPedido
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 142
  Width = 187
  object dsPedido: TDataSource
    DataSet = cdsPedido
    Left = 112
    Top = 80
  end
  object cdsPedido: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspPedido'
    Left = 40
    Top = 16
    object cdsPedidoNUMNOTA: TFMTBCDField
      DisplayLabel = 'Num.Nota'
      FieldName = 'NUMNOTA'
      Required = True
      Precision = 10
      Size = 0
    end
    object cdsPedidoSITUACAO: TWideStringField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'SITUACAO'
      Size = 15
    end
    object cdsPedidoNUNOTA: TFMTBCDField
      FieldName = 'NUNOTA'
      Required = True
      Precision = 10
      Size = 0
    end
    object cdsPedidoNOMEPARC: TWideStringField
      DisplayLabel = 'Cliente'
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
    object cdsPedidoDTMOV: TSQLTimeStampField
      DisplayLabel = 'Data Pedido'
      FieldName = 'DTMOV'
    end
  end
  object dspPedido: TDataSetProvider
    DataSet = sqlPedido
    Left = 40
    Top = 80
  end
  object sqlPedido: TSQLDataSet
    SchemaName = 'democomercial'
    CommandText = 
      'SELECT  C.NUNOTA,C.NUMNOTA, P.NOMEPARC, C.DTMOV, '#13#10'CASE WHEN  AD' +
      '_STATUS_DISTRIBUICAO IS NULL THEN '#39'Separa'#231#227'o'#39' ELSE AD_STATUS_DIS' +
      'TRIBUICAO END SITUACAO'#13#10'FROM TGFCAB C'#13#10'INNER JOIN TGFPAR P ON C.' +
      'CODPARC = P.CODPARC'#13#10'WHERE '#13#10'C.CODTIPOPER = :CODTIPOPER'#13#10'AND C.S' +
      'TATUSNOTA = '#39'L'#39#13#10'AND C.PENDENTE   = '#39'S'#39#13#10'AND C.NUMNOTA = :NUMNOT' +
      'A'#13#10'AND C.CODEMP = :CODEMP'#13#10'AND C.AD_CODPARC IS NULL'#13#10#13#10'UNION'#13#10#13#10 +
      'SELECT  C.NUNOTA,C.NUMNOTA, P.NOMEPARC, C.DTMOV, '#13#10'CASE WHEN  AD' +
      '_STATUS_DISTRIBUICAO IS NULL THEN '#39'Separa'#231#227'o'#39' ELSE AD_STATUS_DIS' +
      'TRIBUICAO END SITUACAO'#13#10'FROM TGFCAB C'#13#10'INNER JOIN TGFPAR P ON C.' +
      'AD_CODPARC = P.CODPARC'#13#10'WHERE '#13#10'C.CODTIPOPER = :CODTIPOPER'#13#10'AND ' +
      'C.STATUSNOTA = '#39'L'#39#13#10'AND C.PENDENTE   = '#39'S'#39#13#10'AND C.NUMNOTA = :NUM' +
      'NOTA'#13#10'AND C.CODEMP = :CODEMP'
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Name = 'CODTIPOPER'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NUMNOTA'
        ParamType = ptInput
      end
      item
        DataType = ftSmallint
        Name = 'CODEMP'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'CODTIPOPER'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NUMNOTA'
        ParamType = ptInput
      end
      item
        DataType = ftSmallint
        Name = 'CODEMP'
        ParamType = ptInput
      end>
    SQLConnection = dmPrincipal.Conn
    Left = 112
    Top = 16
    object sqlPedidoNUMNOTA: TFMTBCDField
      FieldName = 'NUMNOTA'
      Required = True
      Precision = 10
      Size = 0
    end
    object sqlPedidoSITUACAO: TWideStringField
      DisplayWidth = 15
      FieldName = 'SITUACAO'
      Size = 15
    end
    object sqlPedidoNUNOTA: TFMTBCDField
      FieldName = 'NUNOTA'
      Required = True
      Precision = 10
      Size = 0
    end
    object sqlPedidoNOMEPARC: TWideStringField
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
    object sqlPedidoDTMOV: TSQLTimeStampField
      FieldName = 'DTMOV'
    end
  end
end
