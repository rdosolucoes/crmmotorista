object dmBaixaEntrega: TdmBaixaEntrega
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 212
  Width = 427
  object dsBaixa: TDataSource
    DataSet = cdsBaixa
    Left = 112
    Top = 80
  end
  object cdsBaixa: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    Params = <>
    ProviderName = 'dspBaixa'
    Left = 40
    Top = 16
    object cdsBaixaSEL: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'SEL'
      ProviderFlags = []
    end
    object cdsBaixaIDROMANEIO: TFMTBCDField
      DisplayLabel = 'Romaneio'
      FieldName = 'IDROMANEIO'
      Required = True
      Precision = 10
      Size = 0
    end
    object cdsBaixaCLIENTE: TWideStringField
      DisplayLabel = 'Cliente'
      FieldName = 'CLIENTE'
      Required = True
      Size = 40
    end
    object cdsBaixaCODPARC: TFMTBCDField
      FieldName = 'CODPARC'
      Precision = 10
      Size = 0
    end
    object cdsBaixaNOMEPARC: TWideStringField
      DisplayLabel = 'Entregador'
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
    object cdsBaixaDATAFECHAMENTO: TSQLTimeStampField
      DisplayLabel = 'Data'
      FieldName = 'DATAFECHAMENTO'
    end
    object cdsBaixaCOMISSAO: TFMTBCDField
      DisplayLabel = 'Comiss'#227'o'
      FieldName = 'COMISSAO'
      currency = True
      Precision = 32
    end
    object cdsBaixaTOTALCOMISSAO: TAggregateField
      FieldName = 'TOTALCOMISSAO'
      Active = True
      currency = True
      DisplayName = ''
      Expression = 'SUM(COMISSAO)'
    end
  end
  object dspBaixa: TDataSetProvider
    DataSet = sqlBaixa
    UpdateMode = upWhereKeyOnly
    Left = 32
    Top = 80
  end
  object dsMotorista: TDataSource
    DataSet = sdsMotorista
    Left = 176
    Top = 80
  end
  object sdsMotorista: TSimpleDataSet
    Aggregates = <>
    Connection = dmPrincipal.Conn
    DataSet.CommandText = 
      'SELECT CODPARC,NOMEPARC  FROM TGFPAR'#13#10'WHERE AD_MOTOBOY = '#39'S'#39' '#13#10'A' +
      'ND ATIVO = '#39'S'#39#13#10'ORDER BY NOMEPARC'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 216
    Top = 24
    object sdsMotoristaCODPARC: TFMTBCDField
      FieldName = 'CODPARC'
      Required = True
      Precision = 10
      Size = 0
    end
    object sdsMotoristaNOMEPARC: TWideStringField
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
  end
  object sqlBaixa: TSQLDataSet
    CommandText = 
      'SELECT R.IDROMANEIO,PI.NOMEPARC CLIENTE, R.CODPARC,P.NOMEPARC,DA' +
      'TAFECHAMENTO,'#13#10'CASE WHEN AD_VEICULO = 1 THEN P.AD_REPASSE_ENTREG' +
      'A  ELSE 0 END '#13#10'COMISSAO '#13#10'FROM CRMROMANEIO R '#13#10'INNER JOIN CRMIT' +
      'ENSROMANEIO I ON I.IDROMANEIO = R.IDROMANEIO AND I.CODEMP = R.CO' +
      'DEMP'#13#10'INNER JOIN TGFPAR P ON P.CODPARC = R.CODPARC '#13#10'INNER JOIN ' +
      'TGFPAR PI ON P.CODPARC = I.CODPARC'#13#10'WHERE R.STATUS = '#39'F'#39
    MaxBlobSize = -1
    Params = <>
    SQLConnection = dmPrincipal.Conn
    Left = 112
    Top = 24
    object sqlBaixaIDROMANEIO: TFMTBCDField
      FieldName = 'IDROMANEIO'
      Required = True
      Precision = 10
      Size = 0
    end
    object sqlBaixaCLIENTE: TWideStringField
      FieldName = 'CLIENTE'
      Required = True
      Size = 40
    end
    object sqlBaixaCODPARC: TFMTBCDField
      FieldName = 'CODPARC'
      Precision = 10
      Size = 0
    end
    object sqlBaixaNOMEPARC: TWideStringField
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
    object sqlBaixaDATAFECHAMENTO: TSQLTimeStampField
      FieldName = 'DATAFECHAMENTO'
    end
    object sqlBaixaCOMISSAO: TFMTBCDField
      FieldName = 'COMISSAO'
      Precision = 32
    end
  end
end
