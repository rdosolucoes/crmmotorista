unit untEditaPercurso;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseConfirmacao, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinOffice2013White, dxSkinSilver, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxTimeEdit, cxDBEdit, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls,
  dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons, Vcl.ComCtrls,
  Vcl.ExtCtrls,Data.DB;

type
  TfrmEditaPercurso = class(TfrmBaseConfirmacao)
    Label1: TLabel;
    Label3: TLabel;
    dbkmsaida: TDBEdit;
    dbhorasaida: TcxDBTimeEdit;
    Label5: TLabel;
    Label6: TLabel;
    dbkmchegada: TDBEdit;
    dbhorachegada: TcxDBTimeEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    function fAcaoOk:boolean; override;
    function fAcaoCancelar:boolean; override;
    Function fValidaCampos :boolean;
  end;



var
  frmEditaPercurso: TfrmEditaPercurso;

implementation

{$R *.dfm}

uses udmControleMotoboy;

{ TfrmEditaPercurso }

function TfrmEditaPercurso.fAcaoCancelar: boolean;
begin
  result := true;
end;

function TfrmEditaPercurso.fAcaoOk: boolean;
begin
   result := false;
   if fValidaCampos then
   begin
     if dmControleMotoboy.cdsPercurso.state = dsEdit then
     begin
        dmControleMotoboy.cdsPercurso.Post;
        dmControleMotoboy.cdsPercurso.ApplyUpdates(0);
     end;
     result := true;
   end;
end;

procedure TfrmEditaPercurso.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  frmEditaPercurso := nil;
end;

function TfrmEditaPercurso.fValidaCampos: boolean;
var
  erro :string;
begin
  result := true;
  erro := '';
  if dbKmSaida.Text = '' then
    erro := erro + 'Informar o km de saida;'+#13;

  if ((dmControleMotoboy.cdsPercursoSTATUS.Value = 'F') and (dbKmChegada.Text = '')) then
    erro := erro + 'Informar o km de chegada;'+#13;

  if not(dbHoraSaida.Time > 0) then
    erro := erro + 'Informe a Hora de Sa�da !;'+#13;

  if ((dmControleMotoboy.cdsPercursoSTATUS.Value = 'F') and ( not(dbHoraChegada.Time > 0))) then
    erro := erro + 'Informe a Hora de Chegada !;'+#13;

  if ((dmControleMotoboy.cdsPercursoSTATUS.Value = 'F') and (dbhorachegada.time < dbhorasaida.time)) then
     erro := erro + 'Hora de chegada n�o pode ser menor que hora de sa�da !;'+#13;

  if ((dmControleMotoboy.cdsPercursoSTATUS.Value = 'F') and
       (dmControleMotoboy.cdsPercursoKMCHEGADA.value < dmControleMotoboy.cdsPercursoKMSAIDA.value)) then
     erro := erro + 'Km de chegada n�o pode ser menor que Km de sa�da !;'+#13;

  if (erro <> '') then
  begin
    MessageDlg(Erro+'Corrija os erros acima e tente novamente.', mtInformation, [mbOK], 0);
    result := false;
  end;
end;

end.
