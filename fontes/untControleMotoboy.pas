unit untControleMotoboy;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseVisao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinSilver,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  Data.DB, cxDBData, Vcl.StdCtrls, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ComCtrls, dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons, JvLabel,
  Vcl.ExtCtrls, cxImageComboBox, Vcl.Mask, Vcl.DBCtrls, JvExExtCtrls,
  JvExtComponent, JvCaptionPanel, cxContainer, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxTimeEdit, cxDBEdit, SQLExpr, Vcl.Grids, Vcl.DBGrids,
  cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmControleMotoboy = class(TfrmBaseVisao)
    Panel4: TPanel;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    btnChegada: TJvXPButton;
    btnSaida: TJvXPButton;
    cxGrid1DBTableView1NOMEPARC: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    pnlSaida: TJvCaptionPanel;
    Label2: TLabel;
    edttKmSaida: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    btnOKSaida: TJvXPButton;
    btnSairSaida: TJvXPButton;
    pnlChegada: TJvCaptionPanel;
    Label5: TLabel;
    Label6: TLabel;
    edtKMChegada: TDBEdit;
    btnOKChegada: TJvXPButton;
    btnSairChegada: TJvXPButton;
    cxDBTimeEdit1: TcxDBTimeEdit;
    cxDBTimeEdit2: TcxDBTimeEdit;
    cxGridDBTableView1KMSAIDA: TcxGridDBColumn;
    cxGridDBTableView1KMCHEGADA: TcxGridDBColumn;
    cxGridDBTableView1IDROMANEIO: TcxGridDBColumn;
    cxGridDBTableView1TEMPO: TcxGridDBColumn;
    cxGridDBTableView1PERCURSO: TcxGridDBColumn;
    cxGridDBTableView1HORASAIDADESC: TcxGridDBColumn;
    cxGridDBTableView1HORACHEGADADESC: TcxGridDBColumn;
    Timer1: TTimer;
    JvXPButton2: TJvXPButton;
    Splitter1: TSplitter;
    pnlAusenciaChegada: TJvCaptionPanel;
    Label8: TLabel;
    edtKmChegadaAusencia: TDBEdit;
    JvXPButton3: TJvXPButton;
    JvXPButton4: TJvXPButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAdicionarClick(Sender: TObject);
    procedure cxGrid1DBTableView1STATUSGetCellHint(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
      var AHintText: TCaption; var AIsHintMultiLine: Boolean;
      var AHintTextRect: TRect);
    procedure btnSaidaClick(Sender: TObject);
    procedure btnSairSaidaClick(Sender: TObject);
    procedure btnOKSaidaClick(Sender: TObject);
    procedure btnChegadaClick(Sender: TObject);
    procedure btnSairChegadaClick(Sender: TObject);
    procedure btnOKChegadaClick(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxGridDBTableView1STATUSGetCellHint(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
      var AHintText: TCaption; var AIsHintMultiLine: Boolean;
      var AHintTextRect: TRect);
    procedure JvXPButton2Click(Sender: TObject);
    procedure JvXPButton3Click(Sender: TObject);
    procedure JvXPButton4Click(Sender: TObject);
  private
    { Private declarations }
    kmSaidaOld :Double;
    kmChegadaOld :Double;
  public
    { Public declarations }
    procedure pSetaDireitos; override;
    procedure pRefresh; override;
    function BuscaOrdem :Integer;
  end;

var
  frmControleMotoboy: TfrmControleMotoboy;

implementation

{$R *.dfm}

uses udmControleMotoboy,untFuncoes,untLogin,udmPrincipal;

{ TfrmControleMotoboy }

procedure TfrmControleMotoboy.btnAdicionarClick(Sender: TObject);
var result :integer;
    sCodParc :String;
begin
  Application.CreateForm(TfrmLogin,frmLogin);
  result := frmLogin.ShowModal;

  if result = 6 then
  begin
    sCodParc := dmPrincipal.cdsUsuario.FieldByName('CODPARC').AsString;

    if sCodParc = '' then
    begin
      Application.MessageBox('N�o Existe Motorista Relacionado a este Usu�rio.','SANKHYA',MB_OK + MB_ICONERROR);
      Abort;
    end
    else
    if dmControleMotoboy.cdsMotoboy.Locate('CODPARC',sCodParc,[]) then
    begin
      if  (dmControleMotoboy.cdsMotoboySTATUS.AsString = 'U') then
      begin

        if (dmControleMotoboy.cdsPercurso.IsEmpty) then
        begin
          dmControleMotoboy.cdsMotoboy.Edit;
          dmControleMotoboy.cdsMotoboySTATUS.AsString := 'A';
          dmControleMotoboy.cdsMotoboyORDEM.AsInteger := BuscaOrdem;
          dmControleMotoboy.cdsMotoboy.Post;
          dmControleMotoboy.cdsMotoboy.ApplyUpdates(0);
          btnAtualizarClick(nil);
        end
        else
        begin
          AlinharPanel(frmControleMotoboy,pnlAusenciaChegada,true);
          pnlAusenciaChegada.Visible := True;
          edtKmChegadaAusencia.Text := dmControleMotoboy.cdsPercursoKMCHEGADA.AsString;
        end;
      end
      else
        Application.MessageBox('Usu�rio j� Logado.','SANKHYA',MB_OK + MB_ICONWARNING);
    end
    else
    begin
      With dmControleMotoboy do
      begin
        cdsMotoboy.Append;
        cdsMotoboyCODEMP.AsInteger := fGetColigada;
        cdsMotoboyIDCONTROLE.AsInteger := GeraCodigo('IDCONTROLE');
        cdsMotoboyCODPARC.AsString := sCodParc;
        cdsMotoboyDATA.AsDateTime := Date;
        cdsMotoboySTATUS.AsString := 'A';
        cdsMotoboyORDEM.AsInteger := BuscaOrdem;
        cdsMotoboy.Post;
        cdsMotoboy.ApplyUpdates(0);
      end;
       btnAtualizarClick(nil);
    end;
  end;
end;

procedure TfrmControleMotoboy.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
 // inherited;

end;

procedure TfrmControleMotoboy.cxGrid1DBTableView1STATUSGetCellHint(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
  var AHintText: TCaption; var AIsHintMultiLine: Boolean;
  var AHintTextRect: TRect);
begin
  inherited;
  AHintText := VarToStr(ARecord.DisplayTexts[Sender.Index]);
  AIsHintMultiLine := True;
end;

procedure TfrmControleMotoboy.cxGridDBTableView1STATUSGetCellHint(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
  var AHintText: TCaption; var AIsHintMultiLine: Boolean;
  var AHintTextRect: TRect);
begin
  inherited;
  AHintText := VarToStr(ARecord.DisplayTexts[Sender.Index]);
  AIsHintMultiLine := True;
end;

procedure TfrmControleMotoboy.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  frmControleMotoboy := nil;
  FreeAndNil(dmControleMotoboy);
end;

procedure TfrmControleMotoboy.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmControleMotoboy,dmControleMotoboy);
  cdsPad := dmControleMotoboy.cdsMotoboy;
  sqlPad := dmControleMotoboy.sqlMotoboy;
  Tabela := 'CRMCONTROLEMOTOBOY';

end;

procedure TfrmControleMotoboy.FormShow(Sender: TObject);
begin
  inherited;
  pRefresh;
  Timer1.Enabled := True;
end;

procedure TfrmControleMotoboy.JvXPButton2Click(Sender: TObject);
var
  result :integer;
  sCodParc :String;
  qAux :TSQLQuery;
  KMChegada :Double;
begin
  inherited;
  Application.CreateForm(TfrmLogin,frmLogin);
  result := frmLogin.ShowModal;

  if result = 6 then
  begin
    sCodParc := dmPrincipal.cdsUsuario.FieldByName('CODPARC').AsString;
    if sCodParc = '' then
    begin
      Application.MessageBox('N�o Existe Motorista Relacionado a este Usu�rio.','SANKHYA',MB_OK + MB_ICONERROR);
      Abort;
    end;

    if not dmControleMotoboy.cdsMotoboy.Locate('CODPARC',sCodParc,[]) then
    begin
      Application.MessageBox('Usu�rio N�o Logado.','SANKHYA',MB_OK + MB_ICONWARNING);
      Abort;
    end;

    if dmControleMotoboy.cdsMotoboySTATUS.AsString <> 'A' then
    begin
      Application.MessageBox('Status Inv�lido para Ausentar-se.','SANKHYA',MB_OK + MB_ICONWARNING);
      Abort;
    end;

    if dmControleMotoboy.cdsMotoboy.State <> dsEdit then
      dmControleMotoboy.cdsMotoboy.Edit;

    dmControleMotoboy.cdsMotoboySTATUS.AsString := 'U';
    dmControleMotoboy.cdsMotoboy.Post;
    dmControleMotoboy.cdsMotoboy.ApplyUpdates(0);
    btnAtualizarClick(nil);
  end;

end;

procedure TfrmControleMotoboy.JvXPButton3Click(Sender: TObject);
begin
  inherited;
     dmControleMotoboy.cdsPercurso.Edit;
     dmControleMotoboy.cdsPercursoKMCHEGADAAUSENCIA.value := strToFloat(edtKmChegadaAusencia.text);
     dmControleMotoboy.cdsPercurso.Post;
     dmControleMotoboy.cdsPercurso.ApplyUpdates(0);

    dmControleMotoboy.cdsMotoboy.Edit;
    dmControleMotoboy.cdsMotoboySTATUS.AsString := 'A';
    dmControleMotoboy.cdsMotoboyORDEM.AsInteger := BuscaOrdem;
    dmControleMotoboy.cdsMotoboy.Post;
    dmControleMotoboy.cdsMotoboy.ApplyUpdates(0);
    btnAtualizarClick(nil);

    pnlAusenciaChegada.Visible := false;
end;

procedure TfrmControleMotoboy.JvXPButton4Click(Sender: TObject);
begin
  inherited;
  pnlAusenciaChegada.Visible := false;
end;

procedure TfrmControleMotoboy.btnSaidaClick(Sender: TObject);
var
  result :integer;
  sCodParc :String;
  qAux :TSQLQuery;
  KMChegada :Double;
begin
  inherited;
  Application.CreateForm(TfrmLogin,frmLogin);
  result := frmLogin.ShowModal;

  if result = 6 then
  begin
    sCodParc := dmPrincipal.cdsUsuario.FieldByName('CODPARC').AsString;
    if sCodParc = '' then
    begin
      Application.MessageBox('N�o Existe Motorista Relacionado a este Usu�rio.','SANKHYA',MB_OK + MB_ICONERROR);
      Abort;
    end;

    if not dmControleMotoboy.cdsMotoboy.Locate('CODPARC',sCodParc,[]) then
    begin
      Application.MessageBox('� necess�rio Logar no Sistema antes de Registrar uma Sa�da.','SANKHYA',MB_OK + MB_ICONWARNING);
      Abort;
    end;

    if dmControleMotoboy.cdsMotoboySTATUS.AsString = 'U' then
    begin
      Application.MessageBox('Motorista Ausente.','SANKHYA',MB_OK + MB_ICONWARNING);
      Abort;
    end;

    if dmControleMotoboy.cdsMotoboySTATUS.AsString = 'T' then
    begin
      Application.MessageBox('Motorista j� encontra-se em Tr�nsito','SANKHYA',MB_OK + MB_ICONWARNING);
      Abort;
    end;

    if not dmControleMotoboy.cdsPercurso.Locate('STATUS','A',[]) then
    begin
      Application.MessageBox('Aguarde a Libera��o do Romaneio para Sa�da.','SANKHYA',MB_OK + MB_ICONWARNING);
      Abort;
    end;

    AlinharPanel(frmControleMotoboy,pnlSaida,true);
    pnlSaida.Visible := True;
    dmControleMotoboy.cdsPercurso.Edit;

    if not dmControleMotoboy.cdsPercurso.IsEmpty then
    begin
      CriaQuery(qAux);
      qAux.SQL.Add('SELECT KMCHEGADA,KMCHEGADAAUSENCIA FROM CRMPERCURSOMOTOBOY WHERE IDCONTROLE = :IDCONTROLE ');
      qAux.SQL.Add(' AND IDPERCURSO = :IDPERCURSO AND CODEMP = :CODEMP ');
      qAux.Params[0].AsInteger := dmControleMotoboy.cdsMotoboyIDCONTROLE.AsInteger;
      qAux.Params[1].AsInteger := dmControleMotoboy.cdsPercursoIDPERCURSO.AsInteger - 1;
      qAux.Params[2].AsInteger := fGetColigada;
      qAux.Open;

      if (not(qAux.Fields[1].IsNull) and (qAux.Fields[1].AsFloat > 0)) then
        KMChegada := qAux.Fields[1].AsFloat
      else
        KMChegada := qAux.Fields[0].AsFloat;

      FreeAndNil(qAux);
    end;

    dmControleMotoboy.cdsPercursoHORASAIDA.AsDateTime := now;
    dmControleMotoboy.cdsPercursoKMSAIDA.AsFloat := KMChegada;
    dmControleMotoboy.cdsPercursoKMCHEGADAAUSENCIA.AsFloat:= 0;
    kmSaidaOld := kmChegada;
  end;
end;

procedure TfrmControleMotoboy.btnChegadaClick(Sender: TObject);
var
  result :Integer;
  sCodParc :String;
begin
  inherited;

   Application.CreateForm(TfrmLogin,frmLogin);
   result := frmLogin.ShowModal;

  if result = 6 then
  begin
    sCodParc := dmPrincipal.cdsUsuario.FieldByName('CODPARC').AsString;
    if sCodParc = '' then
    begin
      Application.MessageBox('N�o Existe Motorista Relacionado a este Usu�rio.','SANKHYA',MB_OK + MB_ICONERROR);
      Abort;
    end;

    if not dmControleMotoboy.cdsMotoboy.Locate('CODPARC',sCodParc,[]) then
    begin
      Application.MessageBox('� necess�rio Logar no Sistema antes de Registrar uma Entrada.','SANKHYA',MB_OK + MB_ICONWARNING);
      Abort;
    end;

    if dmControleMotoboy.cdsMotoboySTATUS.AsString = 'U' then
    begin
      Application.MessageBox('Motorista Ausente.','SANKHYA',MB_OK + MB_ICONWARNING);
      Abort;
    end;

    if dmControleMotoboy.cdsMotoboySTATUS.AsString <> 'T' then
    begin
      Application.MessageBox('Motorista ainda n�o se Encontra em Tr�nsito','SANKHYA',MB_OK + MB_ICONWARNING);
      Abort;
    end;

    dmControleMotoboy.cdsPercurso.First;
    dmControleMotoboy.cdsPercurso.Edit;
    dmControleMotoboy.cdsPercursoHORACHEGADA.AsDateTime := now;
    dmControleMotoboy.cdsPercursoKMCHEGADA.Value := dmControleMotoboy.cdsPercursoKMSAIDA.Value;
    kmChegadaOld :=  dmControleMotoboy.cdsPercursoKMSAIDA.AsFloat;

    AlinharPanel(frmControleMotoboy,pnlChegada,True);
    pnlChegada.Visible := True;
  end;
end;

procedure TfrmControleMotoboy.btnOKChegadaClick(Sender: TObject);
var
 sSQL :String;
 qAux :TSQLQuery;
 kmChegada :Double;
 IdRomaneio :Integer;
begin
  inherited;

  kmChegada := StrToFloat(edtKmChegada.Text);

  if edtKmChegada.Text = '' then
  begin
    Application.MessageBox('Informe o KM de Sa�da !','SANKYA',mb_ok + MB_ICONWARNING);
    Exit;
  end;

  if (kmchegada < dmControleMotoboy.cdsPercursoKMSAIDA.AsFloat) then
  begin
    Application.MessageBox('KM de Chegada n�o pode ser menor que o de Sa�da !','SANKYA',mb_ok + MB_ICONWARNING);
    Exit;
  end;

  sSQL := ' UPDATE CRMROMANEIO SET STATUS = ''F'', DATAFECHAMENTO = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
          ',''DD/MM/YYYY HH24:MI:SS'')  WHERE IDROMANEIO = ' + dmControleMotoboy.cdsPercursoIDROMANEIO.AsString +
          ' AND CODEMP = ' + IntToStr(fGetColigada);

  SQLExecute(sSQL);

  IdRomaneio := dmControleMotoboy.cdsPercursoIDROMANEIO.AsInteger;
  dmControleMotoboy.cdsPercursoSTATUS.AsString := 'F';
  dmControleMotoboy.cdsPercurso.Post;
  dmControleMotoboy.cdsPercurso.ApplyUpdates(0);
  dmControleMotoboy.cdsMotoboy.Edit;
  dmControleMotoboy.cdsMotoboySTATUS.AsString := 'A';
  dmControleMotoboy.cdsMotoboyORDEM.AsInteger := BuscaOrdem;
  dmControleMotoboy.cdsMotoboy.Post;
  dmControleMotoboy.cdsMotoboy.ApplyUpdates(0);

  CriaQuery(qAux);
  qAux.SQL.Clear;
  qAux.SQL.Add(' SELECT NUNOTA FROM CRMITENSROMANEIO WHERE IDROMANEIO = :IDROMANEIO');
  qAux.SQL.Add(' AND CODEMP = :CODEMP ');
  qAux.Params[0].AsInteger := IdRomaneio;
  qAux.Params[1].AsInteger := fGetColigada;
  qAux.Open;

  while not qAux.Eof do
  begin
    AtualizaStatusPedido(qAux.Fields[0].AsInteger,'Entregue');
    qAux.Next;
  end;

  FreeAndNil(qAux);

  kmChegadaOld := 0;
  pnlChegada.Visible := False;
  btnAtualizarClick(nil);
end;

procedure TfrmControleMotoboy.btnOKSaidaClick(Sender: TObject);
var
  qAux :TSQLQuery;
  IdRomaneio :Integer;
begin
  inherited;

  if edttKmSaida.Text = '' then
  begin
    Application.MessageBox('Informe o KM de Sa�da !','SANKYA',mb_ok + MB_ICONWARNING);
    Exit;
  end;

  if (dmControleMotoboy.cdsPercursoKMSAIDA.AsFloat < kmSaidaOld ) then
  begin
    Application.MessageBox('KM de Sa�da n�o pode ser menor que o �ltimo de Chegada !','SANKYA',mb_ok + MB_ICONWARNING);
    Exit;
  end;

  IdRomaneio := dmControleMotoboy.cdsPercursoIDROMANEIO.AsInteger;
  dmControleMotoboy.cdsPercurso.Post;
  dmControleMotoboy.cdsPercurso.ApplyUpdates(0);

  dmControleMotoboy.cdsMotoboy.Edit;
  dmControleMotoboy.cdsMotoboySTATUS.AsString := 'T';
  dmControleMotoboy.cdsMotoboy.Post;
  dmControleMotoboy.cdsMotoboy.ApplyUpdates(0);

  CriaQuery(qAux);
  qAux.SQL.Add(' SELECT NUNOTA FROM CRMITENSROMANEIO WHERE IDROMANEIO = :IDROMANEIO');
  qAux.SQL.Add(' AND CODEMP = :CODEMP ');
  qAux.Params[0].AsInteger := IdRomaneio;
  qAux.Params[1].AsInteger := fGetColigada;
  qAux.Open;

  while not qAux.Eof do
  begin
    AtualizaStatusPedido(qAux.Fields[0].AsInteger,'Tr�nsito');
    qAux.Next;
  end;

  FreeAndNil(qAux);

  kmSaidaOld := 0;
  pnlSaida.Visible := False;
  btnAtualizarClick(nil);

end;

procedure TfrmControleMotoboy.btnSairChegadaClick(Sender: TObject);
begin
  inherited;
  if dmControleMotoboy.cdsPercurso.State in [dsInsert,dsEdit] then
    dmControleMotoboy.cdsPercurso.Cancel;
  pnlChegada.Visible := False;
end;

procedure TfrmControleMotoboy.btnSairSaidaClick(Sender: TObject);
begin
  inherited;
  if dmControleMotoboy.cdsPercurso.State in [dsInsert,dsEdit] then
    dmControleMotoboy.cdsPercurso.Cancel;

  pnlSaida.Visible := False;
end;

function TfrmControleMotoboy.BuscaOrdem: Integer;
var
  qAux :TSQLQuery;
begin
  CriaQuery(qAux);
  qAux.SQL.Add('SELECT NVL(MAX(ORDEM),0) + 1  FROM CRMCONTROLEMOTOBOY ');
  qAux.SQL.Add(' WHERE DATA = TO_CHAR(SYSDATE,''DD/MM/YY'') ');
  qAux.Open;

  Result := qAux.Fields[0].AsInteger;
  FreeAndNil(qAux);
end;

procedure TfrmControleMotoboy.Button1Click(Sender: TObject);
begin
  inherited;
  dmControleMotoboy.cdsPercurso.Close;
  dmControleMotoboy.cdsPercurso.Open;
end;

procedure TfrmControleMotoboy.pRefresh;
var
  chv :Integer;
begin
  inherited;
  chv := -1;
  with dmControleMotoboy.cdsMotoboy do
  begin
    if Active then
      chv := FieldByName('IDCONTROLE').AsInteger;

    Close;
    Params[0].AsInteger := fGetColigada;
    Open;
    if (chv > -1) then
      Locate('IDCONTROLE', chv, []);
  end;
  pRegistros;
end;

procedure TfrmControleMotoboy.pSetaDireitos;
begin
  inherited;

end;

procedure TfrmControleMotoboy.Timer1Timer(Sender: TObject);
begin
  inherited;
  if not(pnlSaida.Visible) and not(pnlChegada.Visible) then
    btnAtualizarClick(nil);
end;

end.
