object dmRel: TdmRel
  OldCreateOrder = False
  Height = 218
  Width = 488
  object sqlMov: TSQLDataSet
    SchemaName = 'rm'
    CommandText = 
      'SELECT  CRMMOVIMENTO.CODCOLIGADA, '#13#10'       CRMMOVIMENTO.IDMOV, C' +
      'RMMOVIMENTO.CODCFO, '#13#10'       CRMMOVIMENTO.CODCPG, CRMMOVIMENTO.C' +
      'ODRPR, '#13#10'       CRMMOVIMENTO.CODLOC, CRMMOVIMENTO.CODTRA, '#13#10'    ' +
      '   CRMMOVIMENTO.DATAENTREGA, '#13#10'       CRMMOVIMENTO.NORDEM, FCFO.' +
      'NOMEFANTASIA, '#13#10'       TCPG.NOME, TTRA.NOME NOME_2, '#13#10'       TPR' +
      'D.CODIGOPRD, '#13#10'       TPRD.NOMEFANTASIA NOMEFANTASIA_2, '#13#10'      ' +
      ' CRMITMMOVIMENTO.CODUND, '#13#10'       CRMITMMOVIMENTO.QUANTIDADE, '#13#10 +
      '       CRMITMMOVIMENTO.PRECOUNITARIO, '#13#10'       GCOLIGADA.NOME NO' +
      'ME_3, FTB5.DESCRICAO, '#13#10'       CRMMOVIMENTO.DATAEMISSAO, GCOLIGA' +
      'DA.CGC, '#13#10'       GCOLIGADA.INSCRICAOESTADUAL, '#13#10'       TRPR.NOME' +
      ' NOME_4, '#13#10'       CAST(CRMMOVIMENTO.HISTORICOLONGO AS VARCHAR(80' +
      '00)) HISTORICOLONGO, '#13#10'       TLOC.NOME NOME_5, FCFO.NOME NOME_6' +
      ', '#13#10'       CRMMOVIMENTO.CLASSECLIENTE, '#13#10'       CRMITMMOVIMENTO.' +
      'ALIQUOTA, '#13#10'       CRMMOVIMENTO.NUMEROMOVGERADO, '#13#10'       CRMITM' +
      'MOVIMENTO.VALORDESC, '#13#10'       CRMITMMOVIMENTO.VALORIPI, '#13#10'      ' +
      ' CRMMOVIMENTO.CLASSECLIENTE CLASSECLIENTE_2, '#13#10'       '#39'END: '#39' + ' +
      'FCFO.RUA + '#39', '#39' + FCFO.NUMERO + '#39'                     BAIRRO: '#39' ' +
      '+ FCFO.BAIRRO + '#39'      CEP: '#39' + FCFO.CEP FCFO_RUA_FCFO_NUMERO_FC' +
      'F, '#13#10'       '#39'CIDADE: '#39' + FCFO.CIDADE + '#39' - U.F.: '#39' + FCFO.CODETD' +
      ' + '#39'            FONE: '#39' + ISNULL(FCFO.TELEFONE,'#39#39') FCFO_CIDADE_F' +
      'CFO_CODETD, '#13#10'       '#39'CGC: '#39' + FCFO.CGCCFO + '#39' / INSC.EST: '#39' + F' +
      'CFO.INSCRESTADUAL FCFO_CGCCFO_FCFO_INSCRES, '#13#10'       CRMITMMOVIM' +
      'ENTO.PRECOUNITARIO * CRMITMMOVIMENTO.QUANTIDADE CRMITMMOVIMENTO_' +
      'PRECOUNIT, '#13#10'       GCOLIGADA.RUA + '#39', '#39' + GCOLIGADA.NUMERO + GC' +
      'OLIGADA.CEP GCOLIGADA_RUA_GCOLIGADA_N, '#13#10'       GCOLIGADA.TELEFO' +
      'NE + '#39' - CIDADE: '#39' + GCOLIGADA.CIDADE + '#39' / '#39' + GCOLIGADA.ESTADO' +
      ' GCOLIGADA_TELEFONE_GCOLIG, '#13#10'       Case when CRMMOVIMENTO.clas' +
      'secliente = '#39'0'#39' then tprd.descricao else tprd.nomefantasia end  ' +
      'Case_when_CRMMOVIMENTO_cl, '#13#10'       ( SELECT LEFT(TPRD.CODIGOPRD' +
      ', 11) +'#39'.'#39'+ SUBSTRING( REPLACE(  PRECOPADRAO, '#39'.'#39','#39#39' ) , 0 , LEN' +
      '( PRECOPADRAO) -2 )  FROM CRMITMMOVIMENTO A WHERE A.CODCOLIGADA ' +
      '= CRMITMMOVIMENTO.CODCOLIGADA AND A.IDMOV = CRMITMMOVIMENTO.IDMO' +
      'V AND A.NSEQITMMOV = CRMITMMOVIMENTO.NSEQITMMOV AND A.VRCONCORRE' +
      'NTE > 0 ) SELECT_LEFT_TPRD_CODIGOP, '#13#10'       (SELECT SUM(Y.TOTAL' +
      ') / SUM(Y.QUANTIDADE) PRECOMEDIO FROM(SELECT X.PRECO * X.QUANTID' +
      'ADE TOTAL, X.QUANTIDADE FROM (SELECT SUBSTRING( REPLACE(  PRECOP' +
      'ADRAO, '#39'.'#39','#39#39' ) , 0 , LEN( PRECOPADRAO) -2 ) PRECO, A.QUANTIDADE' +
      ' FROM CRMITMMOVIMENTO A WHERE A.CODCOLIGADA = CRMITMMOVIMENTO.CO' +
      'DCOLIGADA AND A.IDMOV = CRMITMMOVIMENTO.IDMOV) X ) Y) SELECT_SUM' +
      '_Y_TOTAL_SUM_Y'#13#10'FROM CRMMOVIMENTO CRMMOVIMENTO'#13#10'      INNER JOIN' +
      ' CRMITMMOVIMENTO CRMITMMOVIMENTO ON '#13#10'     (CRMITMMOVIMENTO.CODC' +
      'OLIGADA = CRMMOVIMENTO.CODCOLIGADA)'#13#10'      AND (CRMITMMOVIMENTO.' +
      'IDMOV = CRMMOVIMENTO.IDMOV)'#13#10'      INNER JOIN TPRD TPRD ON '#13#10'   ' +
      '  (TPRD.CODCOLIGADA = CRMITMMOVIMENTO.CODCOLIGADA)'#13#10'      AND (T' +
      'PRD.IDPRD = CRMITMMOVIMENTO.IDPRD)'#13#10'      INNER JOIN FCFO FCFO O' +
      'N '#13#10'     (FCFO.CODCOLIGADA = CRMMOVIMENTO.CODCOLCFO)'#13#10'      AND ' +
      '(FCFO.CODCFO = CRMMOVIMENTO.CODCFO)'#13#10'      INNER JOIN TCPG TCPG ' +
      'ON '#13#10'     (TCPG.CODCOLIGADA = CRMMOVIMENTO.CODCOLIGADA)'#13#10'      A' +
      'ND (TCPG.CODCPG = CRMMOVIMENTO.CODCPG)'#13#10'      INNER JOIN TRPR TR' +
      'PR ON '#13#10'     (TRPR.CODCOLIGADA = CRMMOVIMENTO.CODCOLIGADA)'#13#10'    ' +
      '  AND (TRPR.CODRPR = CRMMOVIMENTO.CODRPR)'#13#10'      INNER JOIN TTRA' +
      ' TTRA ON '#13#10'     (TTRA.CODCOLIGADA = CRMMOVIMENTO.CODCOLIGADA)'#13#10' ' +
      '     AND (TTRA.CODTRA = CRMMOVIMENTO.CODTRA)'#13#10'      INNER JOIN G' +
      'COLIGADA GCOLIGADA ON '#13#10'     (GCOLIGADA.CODCOLIGADA = CRMMOVIMEN' +
      'TO.CODCOLIGADA)'#13#10'      INNER JOIN FTB5 FTB5 ON '#13#10'     (FTB5.CODC' +
      'OLIGADA = CRMMOVIMENTO.CODCOLIGADA)'#13#10'      AND (FTB5.CODTB5FLX =' +
      ' CRMMOVIMENTO.CODTB5FLX)'#13#10'      INNER JOIN TLOC TLOC ON '#13#10'     (' +
      'TLOC.CODCOLIGADA = CRMMOVIMENTO.CODCOLIGADA)'#13#10'      AND (TLOC.CO' +
      'DLOC = CRMMOVIMENTO.CODLOC)'#13#10'      AND (TLOC.CODFILIAL = CRMMOVI' +
      'MENTO.CODFILIAL)'#13#10'WHERE ( CRMMOVIMENTO.CODCOLIGADA = :CODCOLIGAD' +
      'A )'#13#10'       AND ( CRMMOVIMENTO.IDMOV IN (:IDMOV) )'#13#10'order by CRM' +
      'ITMMOVIMENTO.NSEQITMMOV'
    MaxBlobSize = 1
    Params = <
      item
        DataType = ftSmallint
        Name = 'CODCOLIGADA'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'IDMOV'
        ParamType = ptInput
      end>
    SQLConnection = dmPrincipal.Conn
    Left = 96
    Top = 16
    object sqlMovCODCOLIGADA: TSmallintField
      FieldName = 'CODCOLIGADA'
      Required = True
    end
    object sqlMovIDMOV: TIntegerField
      FieldName = 'IDMOV'
      Required = True
    end
    object sqlMovCODCFO: TStringField
      FieldName = 'CODCFO'
      Size = 25
    end
    object sqlMovCODCPG: TStringField
      FieldName = 'CODCPG'
      Size = 5
    end
    object sqlMovCODRPR: TStringField
      FieldName = 'CODRPR'
      Size = 25
    end
    object sqlMovCODLOC: TStringField
      FieldName = 'CODLOC'
      Size = 25
    end
    object sqlMovCODTRA: TStringField
      FieldName = 'CODTRA'
      Size = 5
    end
    object sqlMovDATAENTREGA: TSQLTimeStampField
      FieldName = 'DATAENTREGA'
    end
    object sqlMovNORDEM: TStringField
      FieldName = 'NORDEM'
      Size = 30
    end
    object sqlMovNOMEFANTASIA: TStringField
      FieldName = 'NOMEFANTASIA'
      Size = 60
    end
    object sqlMovNOME: TStringField
      FieldName = 'NOME'
      Size = 100
    end
    object sqlMovNOME_2: TStringField
      FieldName = 'NOME_2'
      Size = 40
    end
    object sqlMovCODIGOPRD: TStringField
      FieldName = 'CODIGOPRD'
      Size = 30
    end
    object sqlMovNOMEFANTASIA_2: TStringField
      FieldName = 'NOMEFANTASIA_2'
      Size = 100
    end
    object sqlMovCODUND: TStringField
      FieldName = 'CODUND'
      Size = 5
    end
    object sqlMovQUANTIDADE: TFMTBCDField
      FieldName = 'QUANTIDADE'
      Precision = 15
      Size = 4
    end
    object sqlMovPRECOUNITARIO: TFMTBCDField
      FieldName = 'PRECOUNITARIO'
      Precision = 15
      Size = 4
    end
    object sqlMovNOME_3: TStringField
      FieldName = 'NOME_3'
      Size = 60
    end
    object sqlMovDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 40
    end
    object sqlMovDATAEMISSAO: TSQLTimeStampField
      FieldName = 'DATAEMISSAO'
    end
    object sqlMovCGC: TStringField
      FieldName = 'CGC'
    end
    object sqlMovINSCRICAOESTADUAL: TStringField
      FieldName = 'INSCRICAOESTADUAL'
    end
    object sqlMovNOME_4: TStringField
      FieldName = 'NOME_4'
      Size = 40
    end
    object sqlMovNOME_5: TStringField
      FieldName = 'NOME_5'
      Size = 40
    end
    object sqlMovNOME_6: TStringField
      FieldName = 'NOME_6'
      Size = 60
    end
    object sqlMovCLASSECLIENTE: TStringField
      FieldName = 'CLASSECLIENTE'
      Size = 25
    end
    object sqlMovALIQUOTA: TFMTBCDField
      FieldName = 'ALIQUOTA'
      Precision = 15
      Size = 4
    end
    object sqlMovNUMEROMOVGERADO: TStringField
      FieldName = 'NUMEROMOVGERADO'
      Size = 6
    end
    object sqlMovVALORDESC: TFMTBCDField
      FieldName = 'VALORDESC'
      Precision = 15
      Size = 4
    end
    object sqlMovVALORIPI: TFMTBCDField
      FieldName = 'VALORIPI'
      Precision = 15
      Size = 4
    end
    object sqlMovCLASSECLIENTE_2: TStringField
      FieldName = 'CLASSECLIENTE_2'
      Size = 25
    end
    object sqlMovFCFO_RUA_FCFO_NUMERO_FCF: TStringField
      FieldName = 'FCFO_RUA_FCFO_NUMERO_FCF'
      Size = 244
    end
    object sqlMovFCFO_CIDADE_FCFO_CODETD: TStringField
      FieldName = 'FCFO_CIDADE_FCFO_CODETD'
      Size = 84
    end
    object sqlMovFCFO_CGCCFO_FCFO_INSCRES: TStringField
      FieldName = 'FCFO_CGCCFO_FCFO_INSCRES'
      Size = 58
    end
    object sqlMovCRMITMMOVIMENTO_PRECOUNIT: TFMTBCDField
      FieldName = 'CRMITMMOVIMENTO_PRECOUNIT'
      Precision = 31
    end
    object sqlMovGCOLIGADA_RUA_GCOLIGADA_N: TStringField
      FieldName = 'GCOLIGADA_RUA_GCOLIGADA_N'
      Size = 119
    end
    object sqlMovGCOLIGADA_TELEFONE_GCOLIG: TStringField
      FieldName = 'GCOLIGADA_TELEFONE_GCOLIG'
      Size = 63
    end
    object sqlMovCase_when_CRMMOVIMENTO_cl: TStringField
      FieldName = 'Case_when_CRMMOVIMENTO_cl'
      Size = 240
    end
    object sqlMovSELECT_LEFT_TPRD_CODIGOP: TStringField
      FieldName = 'SELECT_LEFT_TPRD_CODIGOP'
      Size = 8000
    end
    object sqlMovSELECT_SUM_Y_TOTAL_SUM_Y: TFMTBCDField
      FieldName = 'SELECT_SUM_Y_TOTAL_SUM_Y'
      Precision = 32
      Size = 6
    end
    object sqlMovHISTORICOLONGO: TStringField
      FieldName = 'HISTORICOLONGO'
      Size = 8000
    end
  end
  object DbMov: TfrxDBDataset
    UserName = 'DbMov'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CODCOLIGADA=CODCOLIGADA'
      'IDMOV=IDMOV'
      'CODCFO=CODCFO'
      'CODCPG=CODCPG'
      'CODRPR=CODRPR'
      'CODLOC=CODLOC'
      'CODTRA=CODTRA'
      'DATAENTREGA=DATAENTREGA'
      'NORDEM=NORDEM'
      'NOMEFANTASIA=NOMEFANTASIA'
      'NOME=NOME'
      'NOME_2=NOME_2'
      'CODIGOPRD=CODIGOPRD'
      'NOMEFANTASIA_2=NOMEFANTASIA_2'
      'CODUND=CODUND'
      'QUANTIDADE=QUANTIDADE'
      'PRECOUNITARIO=PRECOUNITARIO'
      'NOME_3=NOME_3'
      'DESCRICAO=DESCRICAO'
      'DATAEMISSAO=DATAEMISSAO'
      'CGC=CGC'
      'INSCRICAOESTADUAL=INSCRICAOESTADUAL'
      'NOME_4=NOME_4'
      'HISTORICOLONGO=HISTORICOLONGO'
      'NOME_5=NOME_5'
      'NOME_6=NOME_6'
      'CLASSECLIENTE=CLASSECLIENTE'
      'ALIQUOTA=ALIQUOTA'
      'NUMEROMOVGERADO=NUMEROMOVGERADO'
      'VALORDESC=VALORDESC'
      'VALORIPI=VALORIPI'
      'CLASSECLIENTE_2=CLASSECLIENTE_2'
      'FCFO_RUA_FCFO_NUMERO_FCF=FCFO_RUA_FCFO_NUMERO_FCF'
      'FCFO_CIDADE_FCFO_CODETD=FCFO_CIDADE_FCFO_CODETD'
      'FCFO_CGCCFO_FCFO_INSCRES=FCFO_CGCCFO_FCFO_INSCRES'
      'CRMITMMOVIMENTO_PRECOUNIT=CRMITMMOVIMENTO_PRECOUNIT'
      'GCOLIGADA_RUA_GCOLIGADA_N=GCOLIGADA_RUA_GCOLIGADA_N'
      'GCOLIGADA_TELEFONE_GCOLIG=GCOLIGADA_TELEFONE_GCOLIG'
      'Case_when_CRMMOVIMENTO_cl=Case_when_CRMMOVIMENTO_cl'
      'SELECT_LEFT_TPRD_CODIGOP=SELECT_LEFT_TPRD_CODIGOP'
      'SELECT_SUM_Y_TOTAL_SUM_Y=SELECT_SUM_Y_TOTAL_SUM_Y')
    DataSet = sqlMov
    BCDToCurrency = False
    Left = 208
    Top = 16
  end
  object rptOrcamento: TfrxReport
    Version = '6.3.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41439.716350671300000000
    ReportOptions.LastChange = 41442.529687291710000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '          '
      '            '
      ''
      ''
      ''
      'procedure rptOrcamentoOnReportPrint(Sender: TfrxComponent);'
      'begin'
      '  '
      'end;'
      ''
      'procedure Memo2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      ' if (<DbTotais."CODCOLIGADA">) = 1 then'
      '  MEMO2.text := '#39'TRIUNFO'#39
      ' else'
      '  MEMO2.text := '#39'ROCHA;'#39'      '
      'end;'
      ''
      'begin'
      ''
      'end.                                                          ')
    OnReportPrint = 'rptOrcamentoOnReportPrint'
    Left = 32
    Top = 16
    Datasets = <
      item
        DataSet = DbMov
        DataSetName = 'DbMov'
      end
      item
        DataSet = DbTotais
        DataSetName = 'DbTotais'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      OnAfterPrint = 'Page1OnAfterPrint'
      OnBeforePrint = 'Page1OnBeforePrint'
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 68.031540000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'PageHeader1OnBeforePrint'
        object Memo1: TfrxMemoView
          Align = baCenter
          AllowVectorExport = True
          Left = 308.031694999999900000
          Top = 4.000000000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'OR'#199'AMENTO')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Align = baWidth
          AllowVectorExport = True
          Top = 26.677180000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 34.015770000000010000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'Memo2OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'OR'#199'AMENTO')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 63.370130000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 192.756030000000000000
        Top = 215.433210000000000000
        Width = 718.110700000000000000
        DataSet = DbMov
        DataSetName = 'DbMov'
        KeepHeader = True
        RowCount = 0
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 282.480520000000000000
          Top = 7.559059999999931000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'REPRESENTANTE:')
          ParentFont = False
        end
        object frxDBDataset1CODRPR: TfrxMemoView
          AllowVectorExport = True
          Left = 406.984540000000000000
          Top = 7.559059999999931000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataField = 'CODRPR'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."CODRPR"]')
          ParentFont = False
        end
        object frxDBDataset1NOME_4: TfrxMemoView
          AllowVectorExport = True
          Left = 452.897960000000000000
          Top = 7.559059999999931000
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          DataField = 'NOME_4'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."NOME_4"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 8.338590000000000000
          Top = 30.354359999999790000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'DATA ENTREGA:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 247.448980000000000000
          Top = 30.354359999999790000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'PED. CLIENTE:')
          ParentFont = False
        end
        object frxDBDataset1DATAENTREGA: TfrxMemoView
          AllowVectorExport = True
          Left = 123.283550000000000000
          Top = 30.354359999999790000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DataField = 'DATAENTREGA'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."DATAENTREGA"]')
          ParentFont = False
        end
        object frxDBDataset1NORDEM: TfrxMemoView
          AllowVectorExport = True
          Left = 349.834880000000000000
          Top = 30.354359999999790000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DataField = 'NORDEM'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."NORDEM"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 8.338590000000000000
          Top = 53.370129999999820000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CLASSE:')
          ParentFont = False
        end
        object frxDBDataset1CLASSECLIENTE_2: TfrxMemoView
          AllowVectorExport = True
          Left = 71.370130000000000000
          Top = 53.370129999999820000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          DataField = 'CLASSECLIENTE_2'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."CLASSECLIENTE_2"]')
          ParentFont = False
        end
        object frxDBDataset1CODCFO: TfrxMemoView
          AllowVectorExport = True
          Left = 79.149660000000000000
          Top = 76.606369999999880000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DataField = 'CODCFO'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."CODCFO"]')
          ParentFont = False
        end
        object frxDBDataset1NOME_6: TfrxMemoView
          AllowVectorExport = True
          Left = 179.196970000000000000
          Top = 76.606370000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          DataField = 'NOME_6'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."NOME_6"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 8.338590000000000000
          Top = 76.606369999999880000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CLIENTE:')
          ParentFont = False
        end
        object frxDBDataset1FCFO_RUA_FCFO_NUMERO_FCF: TfrxMemoView
          AllowVectorExport = True
          Left = 8.338590000000000000
          Top = 99.063079999999990000
          Width = 691.653990000000000000
          Height = 18.897650000000000000
          DataField = 'FCFO_RUA_FCFO_NUMERO_FCF'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."FCFO_RUA_FCFO_NUMERO_FCF"]')
          ParentFont = False
        end
        object frxDBDataset1FCFO_CGCCFO_FCFO_INSCRES: TfrxMemoView
          AllowVectorExport = True
          Left = 8.338590000000000000
          Top = 144.196970000000000000
          Width = 362.834880000000000000
          Height = 18.897650000000000000
          DataField = 'FCFO_CGCCFO_FCFO_INSCRES'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."FCFO_CGCCFO_FCFO_INSCRES"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 388.968770000000000000
          Top = 144.196970000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'TRANSP:')
          ParentFont = False
        end
        object frxDBDataset1CODTRA: TfrxMemoView
          AllowVectorExport = True
          Left = 463.559370000000000000
          Top = 144.196970000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataField = 'CODTRA'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."CODTRA"]')
          ParentFont = False
        end
        object frxDBDataset1NOME_2: TfrxMemoView
          AllowVectorExport = True
          Left = 508.693260000000000000
          Top = 144.196970000000000000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          DataField = 'NOME_2'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."NOME_2"]')
          ParentFont = False
        end
        object frxDBDataset1FCFO_CIDADE_FCFO_CODETD: TfrxMemoView
          AllowVectorExport = True
          Left = 8.338590000000000000
          Top = 122.063080000000000000
          Width = 691.653990000000000000
          Height = 18.897650000000000000
          DataField = 'FCFO_CIDADE_FCFO_CODETD'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."FCFO_CIDADE_FCFO_CODETD"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 8.338590000000000000
          Top = 166.756030000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'COND PAGTO:')
          ParentFont = False
        end
        object frxDBDataset1CODCPG: TfrxMemoView
          AllowVectorExport = True
          Left = 122.504020000000000000
          Top = 166.756030000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataField = 'CODCPG'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."CODCPG"]')
          ParentFont = False
        end
        object frxDBDataset1NOME: TfrxMemoView
          AllowVectorExport = True
          Left = 166.078850000000000000
          Top = 166.756030000000000000
          Width = 487.559370000000000000
          Height = 18.897650000000000000
          DataField = 'NOME'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."NOME"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 8.338590000000000000
          Top = 7.559059999999931000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'EMISS'#195'O:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 169.196970000000000000
          Top = 7.559059999999931000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'PEDIDO:')
          ParentFont = False
        end
        object frxDBDataset1DATAEMISSAO: TfrxMemoView
          AllowVectorExport = True
          Left = 82.488250000000000000
          Top = 7.559059999999931000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'DATAEMISSAO'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."DATAEMISSAO"]')
          ParentFont = False
        end
        object DbMovSELECT_SUM_Y_TOTAL_SUM_Y: TfrxMemoView
          AllowVectorExport = True
          Left = 603.661720000000000000
          Top = 76.606370000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DataField = 'SELECT_SUM_Y_TOTAL_SUM_Y'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."SELECT_SUM_Y_TOTAL_SUM_Y"]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Frame.Typ = []
        Height = 17.763777090000000000
        Top = 472.441250000000000000
        Width = 718.110700000000000000
        DataSet = DbMov
        DataSetName = 'DbMov'
        RowCount = 0
        object Shape8: TfrxShapeView
          AllowVectorExport = True
          Top = 0.779530000000022500
          Width = 328.818890310000000000
          Height = 18.897650000000000000
          Frame.Typ = []
        end
        object frxDBDataset1SELECT_LEFT_TPRD_CODIGOP: TfrxMemoView
          AllowVectorExport = True
          Left = 217.992270000000000000
          Top = 2.000000000000000000
          Width = 105.826840000000000000
          Height = 15.118120000000000000
          DataField = 'SELECT_LEFT_TPRD_CODIGOP'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."SELECT_LEFT_TPRD_CODIGOP"]')
          ParentFont = False
        end
        object frxDBDataset1CODUND: TfrxMemoView
          AllowVectorExport = True
          Left = 331.157700000000000000
          Top = 1.779530000000079000
          Width = 22.677180000000000000
          Height = 15.118120000000000000
          DataField = 'CODUND'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."CODUND"]')
          ParentFont = False
        end
        object frxDBDataset1QUANTIDADE: TfrxMemoView
          AllowVectorExport = True
          Left = 354.055350000000000000
          Top = 1.779530000000079000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataField = 'QUANTIDADE'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DbMov."QUANTIDADE"]')
          ParentFont = False
        end
        object frxDBDataset1PRECOUNITARIO: TfrxMemoView
          AllowVectorExport = True
          Left = 433.425480000000000000
          Top = 1.779530000000079000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          DataField = 'PRECOUNITARIO'
          DataSet = DbMov
          DataSetName = 'DbMov'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DbMov."PRECOUNITARIO"]')
          ParentFont = False
        end
        object frxDBDataset1CRMITMMOVIMENTO_PRECOUNIT: TfrxMemoView
          AllowVectorExport = True
          Left = 505.236550000000000000
          Top = 1.779530000000079000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'CRMITMMOVIMENTO_PRECOUNIT'
          DataSet = DbMov
          DataSetName = 'DbMov'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DbMov."CRMITMMOVIMENTO_PRECOUNIT"]')
          ParentFont = False
        end
        object frxDBDataset1ALIQUOTA: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 1.779530000000079000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          DataField = 'ALIQUOTA'
          DataSet = DbMov
          DataSetName = 'DbMov'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DbMov."ALIQUOTA"]')
          ParentFont = False
        end
        object frxDBDataset1VALORIPI: TfrxMemoView
          AllowVectorExport = True
          Left = 641.299630000000000000
          Top = 1.779530000000079000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataField = 'VALORIPI'
          DataSet = DbMov
          DataSetName = 'DbMov'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DbMov."VALORIPI"]')
          ParentFont = False
        end
        object Shape9: TfrxShapeView
          AllowVectorExport = True
          Left = 328.818897637795000000
          Top = 0.779529999999908700
          Width = 26.456692910000000000
          Height = 18.897650000000000000
          Frame.Typ = []
        end
        object Shape10: TfrxShapeView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 0.779530000000022500
          Width = 75.590548740000000000
          Height = 18.897650000000000000
          Frame.Typ = []
        end
        object Shape11: TfrxShapeView
          AllowVectorExport = True
          Left = 430.866141732284000000
          Top = 0.779529999999908700
          Width = 71.811018740000000000
          Height = 18.897650000000000000
          Frame.Typ = []
        end
        object Shape12: TfrxShapeView
          AllowVectorExport = True
          Left = 502.457020000000000000
          Top = 0.779529999999908700
          Width = 86.929126540000000000
          Height = 18.897650000000000000
          Frame.Typ = []
        end
        object Shape13: TfrxShapeView
          AllowVectorExport = True
          Left = 638.362204720000000000
          Top = 0.779529999999908700
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Frame.Typ = []
        end
        object Shape14: TfrxShapeView
          AllowVectorExport = True
          Left = 589.606299210000000000
          Top = 0.779529999999908700
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          Frame.Typ = []
        end
        object DbMovCase_when_CRMMOVIMENTO_cl: TfrxMemoView
          AllowVectorExport = True
          Left = 3.000000000000000000
          Top = 2.000000000000000000
          Width = 211.653680000000000000
          Height = 15.118120000000000000
          DataField = 'Case_when_CRMMOVIMENTO_cl'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."Case_when_CRMMOVIMENTO_cl"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 19.118120000000000000
        Top = 430.866420000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          ShiftMode = smDontShift
          Top = 0.220469999999977500
          Width = 328.818890310000000000
          Height = 18.897650000000000000
          Fill.BackColor = clSilver
          Frame.Typ = []
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Left = 328.598640000000000000
          Top = 0.220469999999977500
          Width = 26.456692910000000000
          Height = 18.897650000000000000
          Fill.BackColor = clSilver
          Frame.Typ = []
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Left = 354.897637800000000000
          Top = 0.220469999999977500
          Width = 75.590548740000000000
          Height = 18.897650000000000000
          Fill.BackColor = clSilver
          Frame.Typ = []
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Left = 430.645950000000000000
          Top = 0.220469999999977500
          Width = 71.811018740000000000
          Height = 18.897650000000000000
          Fill.BackColor = clSilver
          Frame.Typ = []
        end
        object Shape5: TfrxShapeView
          AllowVectorExport = True
          Left = 502.457020000000000000
          Top = 0.220469999999977500
          Width = 86.929126540000000000
          Height = 18.897650000000000000
          Fill.BackColor = clSilver
          Frame.Typ = []
        end
        object Shape6: TfrxShapeView
          AllowVectorExport = True
          Left = 588.827150000000000000
          Top = 0.220469999999977500
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          Fill.BackColor = clSilver
          Frame.Typ = []
        end
        object Shape7: TfrxShapeView
          AllowVectorExport = True
          Left = 637.961040000000000000
          Top = 0.220469999999977500
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          Fill.BackColor = clSilver
          Frame.Typ = []
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 130.165430000000000000
          Width = 56.692950000000010000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 269.346630000000000000
          Width = 56.692950000000010000
          Height = 18.897650000000000000
          Visible = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Ton/Bit')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 329.260050000000000000
          Width = 22.677180000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'UN')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 366.614410000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Unit'#225'rio')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 650.079160000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Vl IPI')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 102.047310000000000000
        Top = 771.024120000000000000
        Width = 718.110700000000000000
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 34.015770000000000000
          Top = 49.133889999999950000
          Width = 188.976500000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 49.133889999999950000
          Width = 188.976500000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Top = 52.913419999999740000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Ass. Vendedor')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 52.913419999999740000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Ass. Supervisor')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 147.401670000000000000
        Width = 718.110700000000000000
        Condition = 'DbMov."IDMOV"'
        KeepTogether = True
        object frxDBDataset1NOME_5: TfrxMemoView
          AllowVectorExport = True
          Left = 560.268090000000000000
          Top = 15.118120000000010000
          Width = 147.401670000000000000
          Height = 18.897650000000000000
          DataField = 'NOME_5'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."NOME_5"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 15.897650000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'ORC - Or'#231'amentos')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 148.401670000000000000
          Top = 15.118120000000010000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Faturamento:')
          ParentFont = False
        end
        object frxDBDataset1DESCRICAO: TfrxMemoView
          AllowVectorExport = True
          Left = 308.921460000000000000
          Top = 16.118120000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          DataField = 'DESCRICAO'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."DESCRICAO"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 446.102660000000000000
          Top = 16.118120000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Estoque: ')
          ParentFont = False
        end
        object frxDBDataset1CODLOC: TfrxMemoView
          AllowVectorExport = True
          Left = 520.693260000000000000
          Top = 16.118120000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          DataField = 'CODLOC'
          DataSet = DbMov
          DataSetName = 'DbMov'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbMov."CODLOC"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 196.315090000000000000
        Top = 514.016080000000000000
        Width = 718.110700000000000000
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 465.102660000000000000
          Top = 35.015769999999970000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Valor Bruto:')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 438.645950000000000000
          Top = 59.472480000000010000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Valor Desconto:')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 484.000310000000000000
          Top = 82.149660000000040000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Valor IPI:')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 452.630280080000000000
          Top = 104.826840000000000000
          Width = 106.960629920000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Valor L'#237'quido:')
          ParentFont = False
        end
        object frxDBDataset1VALORTOTALIPI: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 81.929189999999950000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DataField = 'VALORTOTALIPI'
          DataSet = DbTotais
          DataSetName = 'DbTotais'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbTotais."VALORTOTALIPI"]')
          ParentFont = False
        end
        object frxDBDataset1VALORLIQUIDO: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 104.606370000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DataField = 'VALORLIQUIDO'
          DataSet = DbTotais
          DataSetName = 'DbTotais'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbTotais."VALORLIQUIDO"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 53.913419999999970000
          Width = 200.315090000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#195'O VALE COMO RECIBO')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 93.488250000000000000
          Width = 404.409710000000000000
          Height = 94.488250000000000000
          DataField = 'HISTORICOLONGO'
          DataSet = DbTotais
          DataSetName = 'DbTotais'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbTotais."HISTORICOLONGO"]')
          ParentFont = False
        end
        object frxDBDataset1VALORBRUTO: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 35.015769999999970000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DataField = 'VALORBRUTO'
          DataSet = DbTotais
          DataSetName = 'DbTotais'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbTotais."VALORBRUTO"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 272.448980000000000000
          Top = 8.779530000000136000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Qtde Total:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 361.055350000000000000
          Top = 8.779530000000023000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DataSet = DbMov
          DataSetName = 'DbMov'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<DbMov."QUANTIDADE">,DetailData1)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 59.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DataField = 'VALORDESC'
          DataSet = DbTotais
          DataSetName = 'DbTotais'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[DbTotais."VALORDESC"]')
          ParentFont = False
        end
      end
    end
  end
  object sqlTotais: TSQLDataSet
    SchemaName = 'rm'
    CommandText = 
      'SELECT CODCOLIGADA,VALORBRUTO, VALORLIQUIDO, VALORTOTALIPI, '#13#10'  ' +
      '          ISNULL(VALORDESC,0) VALORDESC,HISTORICOLONGO FROM CRMM' +
      'OVIMENTO'#13#10'            WHERE CODCOLIGADA = :CODCOLIGADA'#13#10'        ' +
      '   AND IDMOV = :IDMOV'
    MaxBlobSize = 1
    Params = <
      item
        DataType = ftSmallint
        Name = 'CODCOLIGADA'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'IDMOV'
        ParamType = ptInput
      end>
    SQLConnection = dmPrincipal.Conn
    Left = 152
    Top = 16
    object sqlTotaisVALORBRUTO: TFMTBCDField
      FieldName = 'VALORBRUTO'
      Precision = 15
      Size = 4
    end
    object sqlTotaisVALORLIQUIDO: TFMTBCDField
      FieldName = 'VALORLIQUIDO'
      Precision = 15
      Size = 4
    end
    object sqlTotaisVALORTOTALIPI: TFMTBCDField
      FieldName = 'VALORTOTALIPI'
      Precision = 15
      Size = 4
    end
    object sqlTotaisVALORDESC: TFMTBCDField
      FieldName = 'VALORDESC'
      Precision = 15
      Size = 4
    end
    object sqlTotaisHISTORICOLONGO: TMemoField
      FieldName = 'HISTORICOLONGO'
      BlobType = ftMemo
      Size = 1
    end
    object sqlTotaisCODCOLIGADA: TSmallintField
      FieldName = 'CODCOLIGADA'
      Required = True
    end
  end
  object DbTotais: TfrxDBDataset
    UserName = 'DbTotais'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VALORBRUTO=VALORBRUTO'
      'VALORLIQUIDO=VALORLIQUIDO'
      'VALORTOTALIPI=VALORTOTALIPI'
      'VALORDESC=VALORDESC'
      'HISTORICOLONGO=HISTORICOLONGO'
      'CODCOLIGADA=CODCOLIGADA')
    DataSet = sqlTotais
    BCDToCurrency = False
    Left = 256
    Top = 16
  end
  object sqlMovGNRE: TSQLDataSet
    SchemaName = 'rm'
    CommandText = 
      'SELECT *, ISNULL( ( ( J.VALORBRUTO + J.VALORIPI ) + ( ( J.VALORB' +
      'RUTO + J.VALORIPI ) * ( J.MARGEM / 100 ) ) ), 0 ) BASEICMSST, '#13#10 +
      'ISNULL( ( ( ( J.VALORBRUTO + J.VALORIPI ) + ( ( J.VALORBRUTO + J' +
      '.VALORIPI ) * ( J.MARGEM / 100 ) ) ) * ( J.TAXA / 100 ) ) ,0 ) I' +
      'CMSST,'#13#10'ISNULL( ( ( J.VALORBRUTO + J.VALORIPI ) * ( J.ALIQICMS /' +
      ' 100 ) ), 0 ) ICMS,'#13#10'ISNULL( ( ( ( ( J.VALORBRUTO + J.VALORIPI )' +
      ' + ( ( J.VALORBRUTO + J.VALORIPI ) * ( J.MARGEM / 100 ) ) ) * ( ' +
      'J.TAXA / 100 ) ) - ( ( J.VALORBRUTO + J.VALORIPI ) * ( J.ALIQICM' +
      'S / 100 ) ) ), 0 )  RECOLHER ,'#13#10'ISNULL( ( ( J.VALORBRUTO + J.VAL' +
      'ORIPI ) + ( ( J.VALORBRUTO + J.VALORIPI ) * ( J.MARGEM / 100 ) )' +
      ' ) *  ( J.FUNDOPOBREZA / 100 ), 0 ) FUNDODEPOBREZA'#13#10',j.codrpr as' +
      ' CodRepres'#13#10'FROM('#13#10'SELECT * , '#13#10'( SELECT TAXA FROM CRMCALCULOICM' +
      'S WHERE CODETD = X.CODETD AND IDNAT = X.IDNAT AND CODCOLIGADA = ' +
      'X.CODCOLIGADA  ) TAXA, '#13#10'( SELECT MARGEM FROM CRMCALCULOICMS WHE' +
      'RE CODETD = X.CODETD AND IDNAT = X.IDNAT AND CODCOLIGADA = X.COD' +
      'COLIGADA  ) MARGEM,'#13#10'( SELECT CODRECEITA FROM CRMCALCULOICMS WHE' +
      'RE CODETD = X.CODETD AND IDNAT = X.IDNAT AND CODCOLIGADA = X.COD' +
      'COLIGADA  ) CODRECEITA,'#13#10'( SELECT FUNDOPOBREZA FROM CRMCALCULOIC' +
      'MS WHERE CODETD = X.CODETD AND IDNAT = X.IDNAT AND CODCOLIGADA =' +
      ' X.CODCOLIGADA  ) FUNDOPOBREZA, '#13#10'( SELECT ALIQICMS FROM DNATURE' +
      'ZA WHERE IDNAT = X.IDNAT AND CODCOLIGADA = X.CODCOLIGADA ) ALIQI' +
      'CMS ,'#13#10'( SELECT CODNAT FROM DNATUREZA WHERE IDNAT = X.IDNAT AND ' +
      'CODCOLIGADA = X.CODCOLIGADA ) CODNAT '#13#10'FROM ( '#13#10'SELECT IDMOV, DA' +
      'TAEMISSAO, CODCOLIGADA, CODTMV,'#13#10'( SELECT CODETD FROM FCFO WHERE' +
      ' CODCFO = TMOV.CODCFO AND CODCOLIGADA = TMOV.CODCOLCFO ) CODETD,' +
      #13#10'SERIE, CODCFO, NUMEROMOV, '#13#10'(SELECT NOME FROM FCFO WHERE CODCF' +
      'O = TMOV.CODCFO AND CODCOLIGADA = TMOV.CODCOLCFO ) RAZAOSOCIAL,'#13 +
      #10'(SELECT NOMEFANTASIA FROM FCFO WHERE CODCFO = TMOV.CODCFO AND C' +
      'ODCOLIGADA = TMOV.CODCOLCFO ) NOMEFANTASIA,'#13#10'(SELECT RUA +'#39', '#39'+ ' +
      'NUMERO FROM FCFO WHERE CODCFO = TMOV.CODCFO AND CODCOLIGADA = TM' +
      'OV.CODCOLCFO ) ENDERECO,'#13#10'(SELECT CIDADE FROM FCFO WHERE CODCFO ' +
      '= TMOV.CODCFO AND CODCOLIGADA = TMOV.CODCOLCFO ) MUNICIPIO,'#13#10'(SE' +
      'LECT CEP FROM FCFO WHERE CODCFO = TMOV.CODCFO AND CODCOLIGADA = ' +
      'TMOV.CODCOLCFO ) CEP,'#13#10'(SELECT TELEFONE FROM FCFO WHERE CODCFO =' +
      ' TMOV.CODCFO AND CODCOLIGADA = TMOV.CODCOLCFO ) TELEFONE,'#13#10'(SELE' +
      'CT CGCCFO FROM FCFO WHERE CODCFO = TMOV.CODCFO AND CODCOLIGADA =' +
      ' TMOV.CODCOLCFO ) CGC,'#13#10'(SELECT INSCRESTADUAL FROM FCFO WHERE CO' +
      'DCFO = TMOV.CODCFO AND CODCOLIGADA = TMOV.CODCOLCFO ) IE,'#13#10'(SELE' +
      'CT NOME FROM GETD WHERE CODETD = ( SELECT CODETD FROM FCFO WHERE' +
      ' CODCFO = TMOV.CODCFO AND CODCOLIGADA = TMOV.CODCOLCFO ) ) DESCR' +
      'ICAO,'#13#10'IDNAT, '#13#10'VALORLIQUIDO, VALORBRUTO, '#13#10'( CAST( DATEPART(MON' +
      'TH,TMOV.DATAEMISSAO) AS VARCHAR(2) )+ '#39'/'#39' + CAST( DATEPART(YEAR,' +
      'TMOV.DATAEMISSAO) AS VARCHAR(4) ) ) PERIODO,'#13#10'( SELECT SUM( VALO' +
      'R ) FROM TTRBMOV WHERE IDMOV = TMOV.IDMOV AND CODCOLIGADA = TMOV' +
      '.CODCOLIGADA  AND TTRBMOV.CODTRB = '#39'IPI'#39' ) VALORIPI'#13#10',tmov.codrp' +
      'r'#13#10'FROM TMOV '#13#10'WHERE CODCOLIGADA = :CODCOLIGADA '#13#10'  AND CODTMV I' +
      'N ( '#39'2.2.10'#39','#39'2.2.11'#39','#39'2.2.13'#39','#39'2.2.14'#39','#39'2.2.15'#39' )'#13#10'  AND STATUS' +
      ' <> '#39'C'#39#13#10'  AND IDMOV = :IDMOV )X'#13#10'  )J'
    MaxBlobSize = 1
    Params = <
      item
        DataType = ftSmallint
        Name = 'CODCOLIGADA'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'IDMOV'
        ParamType = ptInput
      end>
    SQLConnection = dmPrincipal.Conn
    Left = 96
    Top = 72
    object sqlMovGNREIDMOV: TIntegerField
      FieldName = 'IDMOV'
      Required = True
    end
    object sqlMovGNREDATAEMISSAO: TSQLTimeStampField
      FieldName = 'DATAEMISSAO'
    end
    object sqlMovGNRECODCOLIGADA: TSmallintField
      FieldName = 'CODCOLIGADA'
      Required = True
    end
    object sqlMovGNRECODTMV: TStringField
      FieldName = 'CODTMV'
      Size = 10
    end
    object sqlMovGNRECODETD: TStringField
      FieldName = 'CODETD'
      Size = 2
    end
    object sqlMovGNRESERIE: TStringField
      FieldName = 'SERIE'
      Size = 8
    end
    object sqlMovGNRECODCFO: TStringField
      FieldName = 'CODCFO'
      Size = 25
    end
    object sqlMovGNRENUMEROMOV: TStringField
      FieldName = 'NUMEROMOV'
      Size = 35
    end
    object sqlMovGNRERAZAOSOCIAL: TStringField
      FieldName = 'RAZAOSOCIAL'
      Size = 60
    end
    object sqlMovGNRENOMEFANTASIA: TStringField
      FieldName = 'NOMEFANTASIA'
      Size = 60
    end
    object sqlMovGNREENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 110
    end
    object sqlMovGNREMUNICIPIO: TStringField
      FieldName = 'MUNICIPIO'
      Size = 32
    end
    object sqlMovGNRECEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object sqlMovGNRETELEFONE: TStringField
      FieldName = 'TELEFONE'
      Size = 15
    end
    object sqlMovGNRECGC: TStringField
      FieldName = 'CGC'
    end
    object sqlMovGNREIE: TStringField
      FieldName = 'IE'
    end
    object sqlMovGNREDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 40
    end
    object sqlMovGNREIDNAT: TIntegerField
      FieldName = 'IDNAT'
    end
    object sqlMovGNREVALORLIQUIDO: TFMTBCDField
      FieldName = 'VALORLIQUIDO'
      Precision = 15
      Size = 4
    end
    object sqlMovGNREVALORBRUTO: TFMTBCDField
      FieldName = 'VALORBRUTO'
      Precision = 15
      Size = 4
    end
    object sqlMovGNREPERIODO: TStringField
      FieldName = 'PERIODO'
      Size = 7
    end
    object sqlMovGNREVALORIPI: TFMTBCDField
      FieldName = 'VALORIPI'
      Precision = 32
      Size = 4
    end
    object sqlMovGNREcodrpr: TStringField
      FieldName = 'codrpr'
      Size = 15
    end
    object sqlMovGNRETAXA: TFMTBCDField
      FieldName = 'TAXA'
      Precision = 15
      Size = 4
    end
    object sqlMovGNREMARGEM: TFMTBCDField
      FieldName = 'MARGEM'
      Precision = 15
      Size = 4
    end
    object sqlMovGNRECODRECEITA: TStringField
      FieldName = 'CODRECEITA'
      Size = 25
    end
    object sqlMovGNREFUNDOPOBREZA: TFMTBCDField
      FieldName = 'FUNDOPOBREZA'
      Precision = 15
      Size = 4
    end
    object sqlMovGNREALIQICMS: TFMTBCDField
      FieldName = 'ALIQICMS'
      Precision = 15
      Size = 4
    end
    object sqlMovGNRECODNAT: TStringField
      FieldName = 'CODNAT'
    end
    object sqlMovGNREBASEICMSST: TFMTBCDField
      FieldName = 'BASEICMSST'
      Required = True
      Precision = 32
      Size = 4
    end
    object sqlMovGNREICMSST: TFMTBCDField
      FieldName = 'ICMSST'
      Required = True
      Precision = 32
      Size = 6
    end
    object sqlMovGNREICMS: TFMTBCDField
      FieldName = 'ICMS'
      Required = True
      Precision = 32
      Size = 6
    end
    object sqlMovGNRERECOLHER: TFMTBCDField
      FieldName = 'RECOLHER'
      Required = True
      Precision = 32
      Size = 6
    end
    object sqlMovGNREFUNDODEPOBREZA: TFMTBCDField
      FieldName = 'FUNDODEPOBREZA'
      Required = True
      Precision = 32
      Size = 6
    end
    object sqlMovGNRECodRepres: TStringField
      FieldName = 'CodRepres'
      Size = 15
    end
  end
  object DBMovGNRE: TfrxDBDataset
    UserName = 'DBMovGNRE'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IDMOV=IDMOV'
      'DATAEMISSAO=DATAEMISSAO'
      'CODCOLIGADA=CODCOLIGADA'
      'CODTMV=CODTMV'
      'CODETD=CODETD'
      'SERIE=SERIE'
      'CODCFO=CODCFO'
      'NUMEROMOV=NUMEROMOV'
      'RAZAOSOCIAL=RAZAOSOCIAL'
      'NOMEFANTASIA=NOMEFANTASIA'
      'ENDERECO=ENDERECO'
      'MUNICIPIO=MUNICIPIO'
      'CEP=CEP'
      'TELEFONE=TELEFONE'
      'CGC=CGC'
      'IE=IE'
      'DESCRICAO=DESCRICAO'
      'IDNAT=IDNAT'
      'VALORLIQUIDO=VALORLIQUIDO'
      'VALORBRUTO=VALORBRUTO'
      'PERIODO=PERIODO'
      'VALORIPI=VALORIPI'
      'codrpr=codrpr'
      'TAXA=TAXA'
      'MARGEM=MARGEM'
      'CODRECEITA=CODRECEITA'
      'FUNDOPOBREZA=FUNDOPOBREZA'
      'ALIQICMS=ALIQICMS'
      'CODNAT=CODNAT'
      'BASEICMSST=BASEICMSST'
      'ICMSST=ICMSST'
      'ICMS=ICMS'
      'RECOLHER=RECOLHER'
      'FUNDODEPOBREZA=FUNDODEPOBREZA'
      'CodRepres=CodRepres')
    DataSet = sqlMovGNRE
    BCDToCurrency = False
    Left = 168
    Top = 72
  end
  object rptMovGNRE: TfrxReport
    Version = '6.3.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41442.576768206020000000
    ReportOptions.LastChange = 41442.720424803240000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 24
    Top = 72
    Datasets = <
      item
        DataSet = DBMovGNRE
        DataSetName = 'DBMovGNRE'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 6.000000000000000000
      RightMargin = 6.000000000000000000
      TopMargin = 6.000000000000000000
      BottomMargin = 6.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 336.378170000000000000
        Top = 98.267780000000000000
        Width = 748.346940000000000000
        DataSet = DBMovGNRE
        DataSetName = 'DBMovGNRE'
        RowCount = 0
        object Shape22: TfrxShapeView
          Align = baClient
          AllowVectorExport = True
          Width = 748.346940000000000000
          Height = 336.378170000000000000
          Frame.Typ = []
        end
        object Shape11: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 165.921259840000000000
          Width = 533.669291340000000000
          Height = 157.984251970000000000
          Frame.Typ = []
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 6.047241650000004000
          Width = 363.968503940000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Left = 377.196850390000000000
          Top = 6.047241650000004000
          Width = 169.322834650000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 37.795300000000010000
          Width = 533.669291340000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 69.811069999999990000
          Width = 324.661417320000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape5: TfrxShapeView
          AllowVectorExport = True
          Left = 337.889763780000000000
          Top = 69.811069999999990000
          Width = 209.007874020000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape6: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 102.047244090000000000
          Width = 533.291338580000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape7: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 134.063080000000000000
          Width = 226.771653540000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape8: TfrxShapeView
          AllowVectorExport = True
          Left = 240.244094490000000000
          Top = 134.063080000000000000
          Width = 48.377952755905510000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape9: TfrxShapeView
          AllowVectorExport = True
          Left = 288.755905510000000000
          Top = 134.063080000000000000
          Width = 124.724409450000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape10: TfrxShapeView
          AllowVectorExport = True
          Left = 413.480314960000000000
          Top = 134.063080000000000000
          Width = 133.039370078740200000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape12: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 6.047241650000004000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape13: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 37.795299999999980000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape14: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 69.811070000000040000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape15: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 102.047310000000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape16: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 134.063080000000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape17: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 165.543307090000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape18: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 197.291338580000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape19: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 228.771800000000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape20: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 260.409448820000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape21: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 292.244280000000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 6.047244094488192000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '01')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 37.795299999999980000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '02')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 69.811070000000040000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '03')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 102.047310000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '04')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 134.063080000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '05')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 165.543307086614200000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '06')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 196.913385826771700000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '07')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 229.039370078740100000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '08')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 260.409448820000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '09')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 292.535433070000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '10')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 13.228346456692910000
          Top = 6.047244090000006000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '13')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 377.196850390000000000
          Top = 6.047244094488192000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '14'
            '')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 13.228346456692910000
          Top = 37.795300000000010000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '15'
            '')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 13.228346456692910000
          Top = 69.811069999999990000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '16'
            '')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 338.267716535433100000
          Top = 69.921259840000010000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '17'
            '')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 101.669291338582700000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '18')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 13.338590000000000000
          Top = 134.063080000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '19')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 240.000000000000000000
          Top = 134.063080000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '20')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 413.480314960000000000
          Top = 134.063080000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '22')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 288.755905510000000000
          Top = 134.063080000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '21')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 13.472440940000000000
          Top = 165.921259840000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '23')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 5.559060000000003000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data do Vencimento')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 37.795299999999980000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'C'#243'digo da Receita')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 69.811070000000040000
          Width = 124.724490000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CNPJ / CPF do Contribuinte')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 102.047310000000000000
          Width = 128.504020000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#176' do Documento de Origem')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 589.165740000000000000
          Top = 134.063080000000000000
          Width = 147.401670000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo de Refer'#234'ncia / N'#176' Parcela')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 165.299320000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Valor Principal')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 197.315090000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Atualiza'#231#227'o Monet'#225'ria')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 229.330860000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 260.567100000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Multa'
            '')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 292.582870000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total a Recolher')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 5.559060000000003000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'UF Favorecido')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 37.574830000000010000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#176' do Conv'#234'nio ou Protocolo / Especifica'#231#227'o da Mercadoria')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 69.811069999999990000
          Width = 158.740260000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Nome da Firma ou Raz'#227'o Social')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 101.826840000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Endere'#231'o Completo')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 37.574830000000000000
          Top = 134.063080000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Munic'#237'pio')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 166.078850000000000000
          Width = 147.401670000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Informa'#231#245'es Complementares')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 401.630180000000000000
          Top = 5.559060000000003000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data do Vencimento')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 361.834880000000000000
          Top = 69.811070000000040000
          Width = 162.519790000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Inscri'#231#227'o Estadual ou UF Favorecido')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 438.850650000000000000
          Top = 134.063080000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'DDD / Telefone')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 312.700990000000000000
          Top = 134.063080000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 263.567100000000000000
          Top = 134.063080000000000000
          Width = 18.897650000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object DBMovGNRECODETD: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataField = 'CODETD'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."CODETD"]')
          ParentFont = False
        end
        object DBMovGNREDESCRICAO: TfrxMemoView
          AllowVectorExport = True
          Left = 99.267780000000000000
          Top = 18.897650000000000000
          Width = 249.448980000000000000
          Height = 15.118120000000000000
          DataField = 'DESCRICAO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."DESCRICAO"]')
        end
        object DBMovGNREDATAEMISSAO: TfrxMemoView
          AllowVectorExport = True
          Left = 401.630180000000000000
          Top = 18.897650000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          DataField = 'DATAEMISSAO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."DATAEMISSAO"]')
        end
        object DBMovGNRERAZAOSOCIAL: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 82.149660000000000000
          Width = 291.023810000000000000
          Height = 15.118120000000000000
          DataField = 'RAZAOSOCIAL'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."RAZAOSOCIAL"]')
        end
        object DBMovGNREIE: TfrxMemoView
          AllowVectorExport = True
          Left = 361.614410000000000000
          Top = 81.929189999999990000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DataField = 'IE'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."IE"]')
        end
        object DBMovGNREENDERECO: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 113.385900000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          DataField = 'ENDERECO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."ENDERECO"]')
        end
        object DBMovGNREMUNICIPIO: TfrxMemoView
          AllowVectorExport = True
          Left = 37.574830000000000000
          Top = 146.181200000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          DataField = 'MUNICIPIO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."MUNICIPIO"]')
        end
        object DBMovGNRECODETD1: TfrxMemoView
          AllowVectorExport = True
          Left = 263.567100000000000000
          Top = 147.401670000000000000
          Width = 22.677180000000000000
          Height = 18.897650000000000000
          DataField = 'CODETD'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."CODETD"]')
        end
        object DBMovGNRECEP: TfrxMemoView
          AllowVectorExport = True
          Left = 312.700990000000000000
          Top = 147.401670000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DataField = 'CEP'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."CEP"]')
        end
        object DBMovGNRETELEFONE: TfrxMemoView
          AllowVectorExport = True
          Left = 438.850650000000000000
          Top = 147.401670000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          DataField = 'TELEFONE'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."TELEFONE"]')
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 188.976500000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'NF')
        end
        object DBMovGNRENUMEROMOV: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590599999999990000
          Top = 188.976500000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          DataField = 'NUMEROMOV'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."NUMEROMOV"]')
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 215.433210000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Margem')
        end
        object DBMovGNREMARGEM: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 215.433210000000000000
          Width = 147.401670000000000000
          Height = 18.897650000000000000
          DataField = 'MARGEM'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."MARGEM"]')
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 32.126005000000000000
          Top = 268.346630000000000000
          Width = 495.118430000000000000
          Height = 45.354360000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              '** Documento apenas informativo, n'#227'o deve ser usado para pagamen' +
              'to, nem como '
            'comprovante de pagamento.**')
          ParentFont = False
        end
        object DBMovGNREDATAEMISSAO1: TfrxMemoView
          AllowVectorExport = True
          Left = 592.386210000000000000
          Top = 17.897650000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          DataField = 'DATAEMISSAO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."DATAEMISSAO"]')
        end
        object DBMovGNRECODRECEITA: TfrxMemoView
          AllowVectorExport = True
          Left = 596.165740000000000000
          Top = 49.133889999999990000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DataField = 'CODRECEITA'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."CODRECEITA"]')
        end
        object DBMovGNRECGC: TfrxMemoView
          AllowVectorExport = True
          Left = 596.165740000000000000
          Top = 80.370130000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DataField = 'CGC'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."CGC"]')
        end
        object DBMovGNRENUMEROMOV1: TfrxMemoView
          AllowVectorExport = True
          Left = 599.945270000000100000
          Top = 113.385900000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          DataField = 'NUMEROMOV'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."NUMEROMOV"]')
        end
        object DBMovGNREPERIODO: TfrxMemoView
          AllowVectorExport = True
          Left = 675.535870000000000000
          Top = 147.401670000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataField = 'PERIODO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."PERIODO"]')
        end
        object DBMovGNRERECOLHER: TfrxMemoView
          AllowVectorExport = True
          Left = 596.165740000000000000
          Top = 177.637910000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DataField = 'RECOLHER'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."RECOLHER"]')
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 596.165740000000000000
          Top = 305.141930000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DataField = 'RECOLHER'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."RECOLHER"]')
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 637.740570000000000000
          Top = 207.874150000000000000
          Width = 94.488249999999990000
          Height = 18.897650000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '0,00')
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 637.740570000000000000
          Top = 238.110390000000000000
          Width = 94.488249999999990000
          Height = 18.897650000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '0,00')
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 637.740570000000000000
          Top = 272.567100000000000000
          Width = 94.488249999999990000
          Height = 18.897650000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '0,00')
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 748.346940000000000000
        object Memo50: TfrxMemoView
          Align = baClient
          AllowVectorExport = True
          Width = 748.346940000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Para Preenchimento da Guia Nacional de Recolhimento de Tributos ' +
              'Estaduais - GNRE')
          ParentFont = False
        end
      end
    end
  end
  object rptPobreza: TfrxReport
    Version = '6.3.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41442.576768206020000000
    ReportOptions.LastChange = 41442.719733229170000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 24
    Top = 136
    Datasets = <
      item
        DataSet = DBMovGNRE
        DataSetName = 'DBMovGNRE'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 6.000000000000000000
      RightMargin = 6.000000000000000000
      TopMargin = 6.000000000000000000
      BottomMargin = 6.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 336.378170000000000000
        Top = 98.267780000000000000
        Width = 748.346940000000000000
        DataSet = DBMovGNRE
        DataSetName = 'DBMovGNRE'
        RowCount = 0
        object Shape22: TfrxShapeView
          Align = baClient
          AllowVectorExport = True
          Width = 748.346940000000000000
          Height = 336.378170000000000000
          Frame.Typ = []
        end
        object Shape11: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 165.921259840000000000
          Width = 533.669291340000000000
          Height = 157.984251970000000000
          Frame.Typ = []
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 6.047241650000004000
          Width = 363.968503940000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Left = 377.196850390000000000
          Top = 6.047241650000004000
          Width = 169.322834650000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 37.795300000000010000
          Width = 533.669291340000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 69.811069999999990000
          Width = 324.661417320000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape5: TfrxShapeView
          AllowVectorExport = True
          Left = 337.889763780000000000
          Top = 69.811069999999990000
          Width = 209.007874020000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape6: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 102.047244090000000000
          Width = 533.291338580000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape7: TfrxShapeView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 134.063080000000000000
          Width = 226.771653540000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape8: TfrxShapeView
          AllowVectorExport = True
          Left = 240.244094490000000000
          Top = 134.063080000000000000
          Width = 48.377952755905510000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape9: TfrxShapeView
          AllowVectorExport = True
          Left = 288.755905510000000000
          Top = 134.063080000000000000
          Width = 124.724409450000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape10: TfrxShapeView
          AllowVectorExport = True
          Left = 413.480314960000000000
          Top = 134.063080000000000000
          Width = 133.039370078740200000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape12: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 6.047241650000004000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape13: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 37.795299999999980000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape14: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 69.811070000000040000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape15: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 102.047310000000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape16: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 134.063080000000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape17: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 165.543307090000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape18: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 197.291338580000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape19: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 228.771800000000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape20: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 260.409448820000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Shape21: TfrxShapeView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 292.244280000000000000
          Width = 168.944881890000000000
          Height = 31.748031500000000000
          Frame.Typ = []
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 6.047244094488192000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '01')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 37.795299999999980000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '02')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 69.811070000000040000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '03')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 102.047310000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '04')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 134.063080000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '05')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 165.543307086614200000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '06')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 196.913385826771700000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '07')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 229.039370078740100000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '08')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 260.409448820000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '09')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 567.709030000000000000
          Top = 292.535433070000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '10')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 13.228346456692910000
          Top = 6.047244090000006000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '13')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 377.196850390000000000
          Top = 6.047244094488192000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '14'
            '')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 13.228346456692910000
          Top = 37.795300000000010000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '15'
            '')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 13.228346456692910000
          Top = 69.811069999999990000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '16'
            '')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 338.267716535433100000
          Top = 69.921259840000010000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '17'
            '')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 13.228346460000000000
          Top = 101.669291338582700000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '18')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 13.338590000000000000
          Top = 134.063080000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '19')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 240.000000000000000000
          Top = 134.063080000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '20')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 413.480314960000000000
          Top = 134.063080000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '22')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 288.755905510000000000
          Top = 134.063080000000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '21')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 13.472440940000000000
          Top = 165.921259840000000000
          Width = 18.897637800000000000
          Height = 16.629921260000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '23')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 5.559060000000003000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data do Vencimento')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 37.795299999999980000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'C'#243'digo da Receita')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 69.811070000000040000
          Width = 124.724490000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CNPJ / CPF do Contribuinte')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 102.047310000000000000
          Width = 128.504020000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#176' do Documento de Origem')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 589.165740000000000000
          Top = 134.063080000000000000
          Width = 147.401670000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Per'#237'odo de Refer'#234'ncia / N'#176' Parcela')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 165.299320000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Valor Principal')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 197.315090000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Atualiza'#231#227'o Monet'#225'ria')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 229.330860000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 260.567100000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Multa'
            '')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 592.165740000000000000
          Top = 292.582870000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total a Recolher')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 5.559060000000003000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'UF Favorecido')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 37.574830000000010000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#176' do Conv'#234'nio ou Protocolo / Especifica'#231#227'o da Mercadoria')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 69.811069999999990000
          Width = 158.740260000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Nome da Firma ou Raz'#227'o Social')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 101.826840000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Endere'#231'o Completo')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 37.574830000000000000
          Top = 134.063080000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Munic'#237'pio')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 166.078850000000000000
          Width = 147.401670000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Informa'#231#245'es Complementares')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 401.630180000000000000
          Top = 5.559060000000003000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data do Vencimento')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 361.834880000000000000
          Top = 69.811070000000040000
          Width = 162.519790000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Inscri'#231#227'o Estadual ou UF Favorecido')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 438.850650000000000000
          Top = 134.063080000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'DDD / Telefone')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 312.700990000000000000
          Top = 134.063080000000000000
          Width = 94.488249999999990000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 263.567100000000000000
          Top = 134.063080000000000000
          Width = 18.897650000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object DBMovGNRECODETD: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataField = 'CODETD'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."CODETD"]')
          ParentFont = False
        end
        object DBMovGNREDESCRICAO: TfrxMemoView
          AllowVectorExport = True
          Left = 99.267780000000000000
          Top = 18.897650000000000000
          Width = 249.448980000000000000
          Height = 15.118120000000000000
          DataField = 'DESCRICAO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."DESCRICAO"]')
        end
        object DBMovGNREDATAEMISSAO: TfrxMemoView
          AllowVectorExport = True
          Left = 401.630180000000000000
          Top = 18.897650000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          DataField = 'DATAEMISSAO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."DATAEMISSAO"]')
        end
        object DBMovGNRERAZAOSOCIAL: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 82.149660000000000000
          Width = 291.023810000000000000
          Height = 15.118120000000000000
          DataField = 'RAZAOSOCIAL'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."RAZAOSOCIAL"]')
        end
        object DBMovGNREIE: TfrxMemoView
          AllowVectorExport = True
          Left = 361.614410000000000000
          Top = 81.929189999999990000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DataField = 'IE'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."IE"]')
        end
        object DBMovGNREENDERECO: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 113.385900000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          DataField = 'ENDERECO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."ENDERECO"]')
        end
        object DBMovGNREMUNICIPIO: TfrxMemoView
          AllowVectorExport = True
          Left = 37.574830000000000000
          Top = 146.181200000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          DataField = 'MUNICIPIO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."MUNICIPIO"]')
        end
        object DBMovGNRECODETD1: TfrxMemoView
          AllowVectorExport = True
          Left = 263.567100000000000000
          Top = 147.401670000000000000
          Width = 22.677180000000000000
          Height = 18.897650000000000000
          DataField = 'CODETD'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."CODETD"]')
        end
        object DBMovGNRECEP: TfrxMemoView
          AllowVectorExport = True
          Left = 312.700990000000000000
          Top = 147.401670000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DataField = 'CEP'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."CEP"]')
        end
        object DBMovGNRETELEFONE: TfrxMemoView
          AllowVectorExport = True
          Left = 438.850650000000000000
          Top = 147.401670000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          DataField = 'TELEFONE'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."TELEFONE"]')
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 188.976500000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'NF')
        end
        object DBMovGNRENUMEROMOV: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590599999999990000
          Top = 188.976500000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          DataField = 'NUMEROMOV'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."NUMEROMOV"]')
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 215.433210000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Margem')
        end
        object DBMovGNREMARGEM: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 215.433210000000000000
          Width = 147.401670000000000000
          Height = 18.897650000000000000
          DataField = 'MARGEM'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBMovGNRE."MARGEM"]')
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 32.126005000000000000
          Top = 268.346630000000000000
          Width = 495.118430000000000000
          Height = 45.354360000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              '** Documento apenas informativo, n'#227'o deve ser usado para pagamen' +
              'to, nem como '
            'comprovante de pagamento.**')
          ParentFont = False
        end
        object DBMovGNREDATAEMISSAO1: TfrxMemoView
          AllowVectorExport = True
          Left = 592.386210000000000000
          Top = 17.897650000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          DataField = 'DATAEMISSAO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."DATAEMISSAO"]')
        end
        object DBMovGNRECGC: TfrxMemoView
          AllowVectorExport = True
          Left = 596.165740000000000000
          Top = 80.370130000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DataField = 'CGC'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."CGC"]')
        end
        object DBMovGNRENUMEROMOV1: TfrxMemoView
          AllowVectorExport = True
          Left = 599.945270000000100000
          Top = 113.385900000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          DataField = 'NUMEROMOV'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."NUMEROMOV"]')
        end
        object DBMovGNREPERIODO: TfrxMemoView
          AllowVectorExport = True
          Left = 675.535870000000000000
          Top = 147.401670000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataField = 'PERIODO'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."PERIODO"]')
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 637.740570000000000000
          Top = 207.874150000000000000
          Width = 94.488249999999990000
          Height = 18.897650000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '0,00')
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 637.740570000000000000
          Top = 238.110390000000000000
          Width = 94.488249999999990000
          Height = 18.897650000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '0,00')
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 637.740570000000000000
          Top = 272.567100000000000000
          Width = 94.488249999999990000
          Height = 18.897650000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '0,00')
        end
        object DBMovGNREFUNDOPOBREZA: TfrxMemoView
          AllowVectorExport = True
          Left = 603.724800000000000000
          Top = 177.637910000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          DataField = 'FUNDOPOBREZA'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."FUNDOPOBREZA"]')
        end
        object DBMovGNREFUNDOPOBREZA1: TfrxMemoView
          AllowVectorExport = True
          Left = 603.724800000000000000
          Top = 302.362400000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          DataField = 'FUNDOPOBREZA'
          DataSet = DBMovGNRE
          DataSetName = 'DBMovGNRE'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBMovGNRE."FUNDOPOBREZA"]')
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 637.740570000000000000
          Top = 49.133889999999990000
          Width = 94.488249999999990000
          Height = 18.897650000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '750-1')
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 245.669450000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '******** FUNDO DE POBREZA*********')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 748.346940000000000000
        object Memo50: TfrxMemoView
          Align = baClient
          AllowVectorExport = True
          Width = 748.346940000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Para Preenchimento da Guia Nacional de Recolhimento de Tributos ' +
              'Estaduais - GNRE')
          ParentFont = False
        end
      end
    end
  end
end
