unit untVisaoPerfil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untBaseVisao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, StdCtrls, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ComCtrls, dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons,
  JvLabel, ExtCtrls, JvExStdCtrls, JvMemo, dxSkinsCore, dxSkinTheAsphaltWorld,
  dxSkinscxPCPainter, dxSkiniMaginary;

type
  TfrmVisaoPerfil = class(TfrmBaseVisao)
    cxGrid1DBTableView1NOME: TcxGridDBColumn;
    cxGrid1DBTableView1IDCRMPERFIL: TcxGridDBColumn;
    cxGrid1DBTableView1CODPERFIL: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure pSetaDireitos; override;
    //
    procedure pNovo; override;
    procedure pEditar; override;
    procedure pExcluir; override;
    procedure pRefresh; override;
    //--
    Procedure pAbrirTela;
  end;

var
  frmVisaoPerfil: TfrmVisaoPerfil;

implementation

uses untCadPerfil, udmPerfil, untFuncoes, untPrincipal;

{$R *.dfm}

{ TfrmVisaoPerfil }

procedure TfrmVisaoPerfil.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if frmCadPerfil <> nil then
    frmCadPerfil.Close;
  frmVisaoPerfil := nil;
  dmPerfil.Free;
end;

procedure TfrmVisaoPerfil.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmPerfil, dmPerfil);
  cdsPad := dmPerfil.cdsPerfil;
  sqlPad := dmPerfil.sqlPerfil;
  Tabela := 'CRMPERFIL';
end;

procedure TfrmVisaoPerfil.FormShow(Sender: TObject);
begin
  inherited;
  pRefresh;
  btnFiltrar.Visible := false;
end;

procedure TfrmVisaoPerfil.pAbrirTela;
begin
  pAbreCadastro(TfrmCadPerfil,frmCadPerfil);
end;

procedure TfrmVisaoPerfil.pEditar;
begin
  inherited;
  if (dmPerfil.cdsPerfil.RecNo <= 0) then
    Exit;

  pAbrirTela;
end;

procedure TfrmVisaoPerfil.pExcluir;
begin
  inherited;
  with dmPerfil.cdsPerfil do
  begin
    Delete;
    if ApplyUpdates(0) > 0 then
      raise Exception.Create('Erro ao Excluir Registro.');
  end;
end;

procedure TfrmVisaoPerfil.pNovo;
begin
  inherited;
  pAbrirTela;
end;

procedure TfrmVisaoPerfil.pRefresh;
var
  chv :Integer;
begin
  inherited;
  chv := -1;
  with dmPerfil.cdsPerfil do
  begin
    if Active then
      chv := FieldByName('IDCRMPERFIL').AsInteger;

    Close;
    Params[0].Value := fGetColigada;
    Open;
    if (chv > -1) then
      Locate('IDCRMPERFIL', chv, []);
  end;
  pRegistros;
end;

procedure TfrmVisaoPerfil.pSetaDireitos;
begin
  inherited;
  btnAdicionar.Enabled := frmprincipal.A00202.Enabled;
  btnEditar.Enabled := frmprincipal.A00203.Enabled;
  dmPerfil.dtsPerfil.AutoEdit := frmprincipal.A00203.Enabled;
  btnExcluir.Enabled := frmprincipal.A00204.Enabled;
  panel1.Enabled := frmprincipal.A00205.Enabled;
end;

end.
