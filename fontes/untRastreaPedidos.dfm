inherited frmRastreaPedido: TfrmRastreaPedido
  Caption = 'Rastrear Pedidos'
  ClientHeight = 451
  ClientWidth = 812
  FormStyle = fsNormal
  Visible = False
  ExplicitWidth = 818
  ExplicitHeight = 480
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar1: TStatusBar
    Top = 432
    Width = 812
    Visible = False
    ExplicitTop = 432
    ExplicitWidth = 812
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 33
    Align = alTop
    TabOrder = 1
    object Label2: TLabel
      Left = 5
      Top = 11
      Width = 48
      Height = 16
      Caption = 'Pedido:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtPedido: TEdit
      Left = 57
      Top = 9
      Width = 121
      Height = 21
      TabOrder = 0
      OnKeyPress = edtPedidoKeyPress
    end
    object btnLocalizar: TJvXPButton
      Left = 184
      Top = 5
      Width = 31
      Height = 26
      Hint = 'CRMi|Localizar'
      CustomHint = dmPrincipal.BalloonHint1
      TabOrder = 1
      Glyph.Data = {
        0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000018
        000000180806000000E0773DF80000000467414D410000B18F0BFC6105000004
        0449444154484BB5555D4F226718E51FD4B49B742F9AD60F4070154140164161
        F870586694814151119C5110441761BBD266DD7611D0A46E536AAC64E39AA076
        F7A6A9AD4D9A348D4DBCE9D526ADD7BD68F809FC84D39759D2D69426DD969EE4
        84E4BC99F33ECF799E616400FE57B614DBC996623BD9526C275B8AEDE43F02C7
        711D84C2EAEA6A35168BD5777676F0DE46AEDC3CFE77383F3FD7E6097C3EDF15
        CBB2A0691A1445A1B7B717D3C129F4297BAF95179A0A75C41663F64AA5222412
        89AF773FDABDDC2B7F2A348FAFE3F8F8B8F3F0F010A7A7A7C8E572309BCD3018
        0CD06AB5B08E8E617D3D8342A18862A1580F7001ED92B8547DBCFBB8FEFCD933
        14F25BA0EC14561249DC3618EB4DCB97502A955A526539954AD5676767118FC7
        717070205DA052A960B55A110E879149AF63511091CD6450FEA48C86F1079B0F
        31C9B018D20C424DBA9B098520EFEC826C6E6EAEC3EFE7F30ECA53CB64B2B8B8
        B8C0E9C909E2B118DC6E374451C4DADA1A42E48148248246FE2CC360636303A5
        5209240A78690F066FF54347CC299B1DB3A119880B0286F5862B19312EBF7943
        85D75F53A0F1BB9EBE87FC878FE0226DEA7543B0D96C48A7D352F51A8D0646A3
        11D16814C96412D96C169B9B9BF0731CDC4E37A29128F28FF228158B482E2F23
        C0F93B656AE53022D132F62ABF62413C42CFDB0E724916FA412D943D72297786
        54ECF7FB2197CBA5B81A431704017992F7FEFE3ED277D3286C15B04D3A5A5D59
        C1548087D36E87496F28CBE4DD43B8FFE005E2AB9710123F10F15DF858112ECA
        0179579794BDC56291B257ABD564804E986F3B212CA470F6E557F8E6FC1C3BDB
        DB584BA530CDF352E7C6213DFA556A28BB7B6A3245B701B4F709ACE612ACC325
        9806EF63DC2162DCE944AF5C21ADA5C964422010C0B0D182373A5478EBA605F2
        775868D441940A1F23115F960A1AFEC3581AB0A2AB1BB2C1017BAD5F198159F7
        3E265D9F83713E0543A740BBDC522563D651B81CCECB4976426814E3A173D87F
        5243BEF802664316269D4822BA8B7ED2DD9F8D550A25347DB76A32DACD0BBA3E
        11D4C80E62E19F100E7E0F1F930473C70BD6CB5C723ECEDE5861B26DF69B3774
        882D9F61EDDE8F52A401FE18437D2904FD31E8063424D2A631D928B241249591
        BCB4FFAC375AA62C39F8DCCFC1B325CC87E7AB413ED8291D36412EE854C969B0
        9347F0302764889FC135BA8711DD03785C715063B6DF8D6DE465743B5DD7FF4A
        8881B65165C3A829FD0526FD4C4D3F90964CBDB623F09E3378EC1578DD2B0D43
        C9D8334E5F4D30ACD4F52B83B9332F180612B091659899FC0EA9855FC04F7C21
        45CA326C9DC4F93292FF8271C76295326F6186FB16F1F0CFE0E80A42D391CBE9
        E9D0DF76FECA98E297B8A06FA31A0A3CAC36626DCAADD1EA23D14EB614DBC996
        62FB08D96FEB3A50771C930DAE0000000049454E44AE426082}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnLocalizarClick
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 33
    Width = 812
    Height = 399
    Align = alClient
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.PriorPage.Enabled = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.NextPage.Enabled = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Insert.Enabled = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Enabled = False
      Navigator.Buttons.Delete.Enabled = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Enabled = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Enabled = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Enabled = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Enabled = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Enabled = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Enabled = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Enabled = False
      Navigator.Buttons.Filter.Visible = False
      FilterBox.CustomizeDialog = False
      DataController.DataSource = dmRastreaPedido.dsPedido
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DateTimeHandling.IgnoreTimeForFiltering = True
      DateTimeHandling.DateFormat = 'DD/MM/YYYY'
      DateTimeHandling.Grouping = dtgByDate
      DateTimeHandling.HourFormat = 'HH:NN:SS'
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.InvertSelect = False
      OptionsView.NoDataToDisplayInfoText = '<Sem dados para exibi'#231#227'o>'
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1NUMNOTA: TcxGridDBColumn
        DataBinding.FieldName = 'NUMNOTA'
      end
      object cxGrid1DBTableView1NOMEPARC: TcxGridDBColumn
        DataBinding.FieldName = 'NOMEPARC'
      end
      object cxGrid1DBTableView1DTMOV: TcxGridDBColumn
        DataBinding.FieldName = 'DTMOV'
      end
      object cxGrid1DBTableView1SITUACAO: TcxGridDBColumn
        DataBinding.FieldName = 'SITUACAO'
        Width = 150
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
end
