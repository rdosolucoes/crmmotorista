unit untPrincipal;

interface

uses Windows, Forms, Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer,
  cxEdit, ImgList, Controls, Classes, ActnList, ActnMan, ComCtrls, StdCtrls,
  ActnCtrls, ToolWin, ActnMenus,
  Datasnap.DBClient,DBXcommon,DBCtrls, frxClass,dxSkinTheAsphaltWorld,dxBar, cxClasses, dxRibbon,
  dxBarApplicationMenu, Vcl.Menus, dxGDIPlusClasses,dxRibbonForm,ShellApi,
  dxSkinsdxRibbonPainter, dxBarExtDBItems, dxBarExtItems,Variants,
  dxRibbonSkins, dxSkinsCore, dxSkinsdxBarPainter, dxSkiniMaginary, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, cxPC, dxSkinscxPCPainter,
  dxTabbedMDI, dxSkinOffice2013White, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, Vcl.StdActns, System.Actions,
  Vcl.PlatformDefaultStyleActnCtrls, System.ImageList, Vcl.ExtCtrls, cxImageList;

type
  TfrmPrincipal = class(TdxRibbonForm)
    StatusBar1: TStatusBar;
    imgMenu: TcxImageList;
    actPrincipal: TActionManager;
    A001: TAction;
    A00101: TAction;
    A00102: TAction;
    A00103: TAction;
    A00104: TAction;
    A00105: TAction;
    A00106: TAction;
    A002: TAction;
    A00201: TAction;
    A00202: TAction;
    A00203: TAction;
    A00204: TAction;
    A00205: TAction;
    A003: TAction;
    A00305: TAction;
    actTrocaUsu: TAction;
    actTrocaSenha: TAction;
    actSair: TAction;
    A004: TAction;
    A015: TAction;
    A01501: TAction;
    A01502: TAction;
    A01503: TAction;
    A01504: TAction;
    A01505: TAction;
    A01506: TAction;
    A01507: TAction;
    A01508: TAction;
    A01509: TAction;
    actRibbon: TActionManager;
    WindowClose1: TWindowClose;
    WindowCascade1: TWindowCascade;
    WindowTileHorizontal1: TWindowTileHorizontal;
    WindowTileVertical1: TWindowTileVertical;
    WindowMinimizeAll1: TWindowMinimizeAll;
    WindowArrange1: TWindowArrange;
    SmallImages: TImageList;
    SmallDisabledImages: TImageList;
    LargeDisabledImages: TImageList;
    LargeImages: TImageList;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarManager1Bar5: TdxBar;
    dxBarManager1Bar6: TdxBar;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarApplicationMenu1: TdxBarApplicationMenu;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarManager1Bar8: TdxBar;
    Image24: TcxImageList;
    mImport: TMemo;
    memDetalhes: TMemo;
    imgCRM: TImage;
    ribMenu: TdxRibbon;
    dxRibbon1Tab3: TdxRibbonTab;
    dxRibbon1Tab4: TdxRibbonTab;
    ribMenuTab1: TdxRibbonTab;
    dxBarManager1Bar2: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    A016: TAction;
    A017: TAction;
    A018: TAction;
    dxBarLargeButton5: TdxBarLargeButton;
    A01601: TAction;
    A01701: TAction;
    A01702: TAction;
    A01801: TAction;
    A01802: TAction;
    A01803: TAction;
    A01804: TAction;
    A01805: TAction;
    A019: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    A01806: TAction;
    A01807: TAction;
    dxtab: TdxTabbedMDIManager;
    dxBarButton1: TdxBarButton;
    WindowsTab: TAction;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarManager1Bar3: TdxBar;
    A020: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarManager1Bar4: TdxBar;
    A021: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    A01901: TAction;
    actTrocaEmpresa: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    A022: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure A001Execute(Sender: TObject);
    procedure A002Execute(Sender: TObject);
    procedure A003Execute(Sender: TObject);
    procedure imgCRMClick(Sender: TObject);
    procedure actSairExecute(Sender: TObject);
    procedure actTrocaEmpresaExecute(Sender: TObject);
    procedure actTrocaSenhaExecute(Sender: TObject);
    procedure actTrocaUsuExecute(Sender: TObject);
    procedure A004Execute(Sender: TObject);
    procedure A015Execute(Sender: TObject);
    procedure dxBarLookupCombo3KeyValueChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure dxBarButton35Click(Sender: TObject);
    procedure dxBarButton30Click(Sender: TObject);
    procedure dxBarButton8Click(Sender: TObject);
    procedure A016Execute(Sender: TObject);
    procedure A017Execute(Sender: TObject);
    procedure A018Execute(Sender: TObject);
    procedure A019Execute(Sender: TObject);
    procedure actPrincipalChange(Sender: TObject);
    procedure WindowsTabExecute(Sender: TObject);
    procedure actRibbonExecute(Action: TBasicAction; var Handled: Boolean);
    procedure A020Execute(Sender: TObject);
    procedure A021Execute(Sender: TObject);
    procedure A022Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    trocandoEmpresa,flag_comp_Enter,flag_comp,flag_cancela:boolean;
    TipoMov,Motivo :String;
    IdMotivo,Filial :Integer;

    Procedure pSetaAmbiente;
    Procedure pSetaDireitosMenus;
    Function fGetVersao(Par :String):String;

  end;

var
  frmPrincipal: TfrmPrincipal;

{*******************************************************************************
   MUDAR SEMPRE AQUI PARA CONTROLE DE VERS�O DO APLICATIVO E SCRIPT PARA
   ATUALIZAR O B.D. E CONTROLAR VERS�O DO BANCO.
*******************************************************************************}
//Const
//  VersaoAplicativo :String = '2.0.0.0';
//  VersaoBDAbilitado :String = '2.0.0.0';
{******************************************************************************}

implementation

uses udmPrincipal, untLogin, untVisaoUsuarios, untSetaEmpresa, SysUtils,
  untVisaoPerfil, untVisaoAcessos, untFuncoes, untTrocaSenha,untParametros,
  untGeradorRel,udmParametros, untSeparacao, untConferencia, untVisaoRomaneio,
  untControleMotoboy, untBaixaEntrega, untRastreaPedidos, Data.SqlExpr,
  untControlePercurso;

{$R *.dfm}

procedure TfrmPrincipal.A003Execute(Sender: TObject);
begin
  if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreVisao(TfrmVisaoAcessos,frmVisaoAcessos);
end;

procedure TfrmPrincipal.A004Execute(Sender: TObject);
begin
  if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreVisao(TfrmParametros,frmParametros);
end;

procedure TfrmPrincipal.A015Execute(Sender: TObject);
begin
  pAbreCadastro(TfrmGeradorRel,frmGeradorRel);
end;

procedure TfrmPrincipal.A016Execute(Sender: TObject);
begin
  if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreVisao(TfrmSeparacao,frmSeparacao);
end;

procedure TfrmPrincipal.A017Execute(Sender: TObject);
begin
  if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreVisao(TfrmConferencia,frmConferencia);
end;

procedure TfrmPrincipal.A018Execute(Sender: TObject);
begin
   if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreVisao(TfrmVisaoRomaneio,frmVisaoRomaneio);
end;

procedure TfrmPrincipal.A019Execute(Sender: TObject);
begin
   if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreVisao(TfrmControleMotoboy,frmControleMotoboy);
end;

procedure TfrmPrincipal.A020Execute(Sender: TObject);
begin
  if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreCadastro(TfrmBaixaEntrega,frmBaixaEntrega);
end;

procedure TfrmPrincipal.A021Execute(Sender: TObject);
begin
 if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreCadastro(TfrmRastreaPedido,frmRastreaPedido);
end;

procedure TfrmPrincipal.A022Execute(Sender: TObject);
begin
  if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreCadastro(TfrmControlePercurso,frmControlePercurso);
end;

procedure TfrmPrincipal.actPrincipalChange(Sender: TObject);
begin
   WindowsTab.Enabled := MDIChildCount > 0;
end;

procedure TfrmPrincipal.actRibbonExecute(Action: TBasicAction;
  var Handled: Boolean);
begin
  if not (Action.Name = 'WindowClose1') then
    dxtab.Active := False;
end;

procedure TfrmPrincipal.actSairExecute(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmPrincipal.actTrocaEmpresaExecute(Sender: TObject);
begin
  trocandoEmpresa := True;

  if frmSetaEmpresa = nil then
    Application.CreateForm(TfrmSetaEmpresa, frmSetaEmpresa);
  frmSetaEmpresa.ShowModal;
end;

procedure TfrmPrincipal.actTrocaSenhaExecute(Sender: TObject);
begin
  Application.CreateForm(TfrmTrocaSenha, frmTrocaSenha);
  frmTrocaSenha.userName := dmprincipal.cdsUsuario.FieldByName('NOME').AsString;
  frmTrocaSenha.userPass := dmprincipal.cdsUsuario.FieldByName('USERNAME').AsString;
  frmTrocaSenha.userID   := dmPrincipal.cdsUsuario.FieldByName('IDCRMUSUARIO').AsString;
  frmTrocaSenha.Caption := 'Trocar Senha';
  frmTrocaSenha.ShowModal;
end;

procedure TfrmPrincipal.actTrocaUsuExecute(Sender: TObject);
var
  i, result :Integer;
  codEmp :integer;
begin
  for i := 0 to Screen.FormCount-1 do
  begin
    if Screen.Forms[ i ] <> self then
      Screen.Forms[ i ].Close;
  end;

  frmLogin := TfrmLogin.Create(Self);
  result := frmLogin.ShowModal;

  if result = 7 then
    try
      frmLogin.Close;
      Application.Terminate;
    except
    end
  else
  begin
    frmLogin.close;
    //frmPrincipal.Show;

    frmPrincipal.trocandoEmpresa := true;
{    if dmPrincipal.cdsUsuario.FieldByName('SKIN').AsString <> '' then
    begin
      dmPrincipal.dxSkinController1.SkinName := dmPrincipal.cdsUsuario.FieldByName('SKIN').AsString;
      frmPrincipal.ribMenu.ColorSchemeName := dmPrincipal.dxSkinController1.SkinName;
    end;    }

    if (dmprincipal.cdsUsuario.FieldByName('CODCOLIGADA').Value > 0) then
      codemp := dmPrincipal.cdsUsuario.FieldByName('CODCOLIGADA').AsInteger
    else
      codemp := 0;

    dmPrincipal.cdsEmpresa.Close;
    dmPrincipal.sqlEmpresa.ParamByName('CODEMP').AsInteger := codemp;
    dmPrincipal.cdsEmpresa.Open;

    Application.CreateForm(TfrmSetaEmpresa, frmSetaEmpresa);
    frmSetaEmpresa.ShowModal;
    dmPrincipal.pAbreFormAtrasados;
    dmPrincipal.pAbreFormPedProg
  end;
  pSetaAmbiente;
  //Release;// hide the active form
end;



procedure TfrmPrincipal.dxBarButton30Click(Sender: TObject);
begin
  if ribMenu.ShowTabGroups  then
    ribMenu.ShowTabGroups := False
  else
    ribMenu.ShowTabGroups := True;
end;

procedure TfrmPrincipal.dxBarButton35Click(Sender: TObject);
begin
  ShellExecute(Handle, 'OPEN', 'calc.exe', nil, nil, sw_shownormal);
end;

procedure TfrmPrincipal.dxBarButton8Click(Sender: TObject);
begin
  if ribMenu.ShowTabGroups  then
    ribMenu.ShowTabGroups := False
  else
    ribMenu.ShowTabGroups := True;
end;

procedure TfrmPrincipal.dxBarLookupCombo3KeyValueChange(Sender: TObject);
begin
 { dmPrincipal.dxSkinController1.SkinName := dxBarLookupCombo3.Text;
  ribMenu.ColorSchemeName := dxBarLookupCombo3.Text;
  SQLExecute('UPDATE CRMUSUARIO SET SKIN = ' + QuotedStr(dxBarLookupCombo3.Text) +
             ' WHERE IDCRMUSUARIO = ' + IntToStr(fGetUsuario));      }
end;

function TfrmPrincipal.fGetVersao(Par: String): String;
var
 qAux :TSQLQuery;
begin
  CriaQuery(qAux);
  qAux.SQL.Add(' SELECT VERSAOBD FROM CRMVERSAO ');
  qAux.Open;

  if (Par = 'A') then
    Result := VersaoExe
  else if (Par = 'B') then
    Result := qAux.Fields[0].AsString
  else
  begin
    Result := '';
    raise Exception.Create('Par�metro inv�lido.');
  end;
  qAux.Close;
  FreeAndNil(qAux);
end;

procedure TfrmPrincipal.A002Execute(Sender: TObject);
begin
   if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreVisao(TfrmVisaoPerfil,frmVisaoPerfil);
end;

procedure TfrmPrincipal.A001Execute(Sender: TObject);
begin
  if MDIChildCount > 0 then
    ExecuteAction(WindowMinimizeAll1);
  pAbreVisao(TfrmVisaoUsuarios,frmVisaoUsuarios);
end;

procedure TfrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmPrincipal.Conn.close;
  Application.Terminate;
end;

procedure TfrmPrincipal.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
 I :Integer;
begin
   if (Abs(GetKeyState(VK_SHIFT)) <> 127) and (Abs(GetKeyState(VK_SHIFT)) <> 128) then
    IF Application.MessageBox('Deseja Realmente Sair do Sistema ?','SANKHYA', mb_YesNo + MB_ICONQUESTION )= idYes then
    begin
      for  I := 0 to Screen.FormCount-1 do
      if Screen.Forms[I].Name <> 'frmPrincipal' then
        Screen.Forms[I].Close;
      Release;
    end
    else
      CanClose := false;
end;

procedure TfrmPrincipal.FormResize(Sender: TObject);
begin
  imgCRM.Top := round(((self.Height - ribMenu.Height - StatusBar1.Height)/2) - (imgCRM.Height / 2)) + ribMenu.Height;
  imgCRM.Left := round((self.Width/2) - (imgCRM.Width/2));
end;

procedure TfrmPrincipal.imgCRMClick(Sender: TObject);
begin
  imgCRM.SendToBack;
end;

procedure TfrmPrincipal.pSetaAmbiente;
var
  qAux :TSQLQuery;
begin
  frmPrincipal.Caption := 'SANKHYA -'{Fixo}+
                 //    'BAS'+{nome sistema}
                     '   (Empresa: '+dmPrincipal.cdsEmpresa.FieldByName('NOMEFANTASIA').AsString+')';
  StatusBar1.Panels.Items[0].Text := 'SANKHYA';//Fixo
  StatusBar1.Panels.Items[1].Text := 'Vers�o: '+ VersaoExe;
  StatusBar1.Panels.Items[2].Text := 'BD: '+dmPrincipal.cdsVersao.FieldByName('VERSAOBD').AsString;
  StatusBar1.Panels.Items[3].Text := 'Base: '+dmPrincipal.Conn.ConnectionName;
  StatusBar1.Panels.Items[4].Text := 'Usu�rio: '+dmPrincipal.cdsUsuario.FieldByName('NOME').AsString;

  pSetaDireitosMenus;


  dmPrincipal.cdsParametros.Close;
  dmPrincipal.cdsParametros.Params[0].AsInteger := fGetColigada;
  dmPrincipal.cdsParametros.Open;

end;

procedure TfrmPrincipal.pSetaDireitosMenus;
var
  Component: TComponent;
  I: Integer;
  act :string;
  status :boolean;
begin
  if (dmPrincipal.cdsUsuario.FieldByName('MASTERUSER').AsString = 'S') then
    status := true
  else
    status := false;

  Component := frmprincipal.actPrincipal;
  for I := 0 to TActionManager(Component).ActionCount-1 do
    if TActionManager(Component).Actions[i] is TAction then
      If (TAction(TActionManager(Component).Actions[i]).Category <> 'Basico') then
        TAction(TActionManager(Component).Actions[i]).Enabled := status;

  if (dmPrincipal.cdsUsuario.FieldByName('MASTERUSER').AsString = 'S') then
    exit;

  with dmPrincipal.cdsDireitos do
  begin
    close;
    //Params.ParamByName('PCOLIG').AsInteger := fGetColigada;
    Params.ParamByName('PUSU').AsInteger := fGetUsuario;
    Open;

    First;
    while not(Eof) do
    begin
      for I := 0 to TActionManager(Component).ActionCount-1 do
      begin
        if TActionManager(Component).Actions[i] is TAction then
        begin
          act := TAction(TActionManager(Component).Actions[i]).Name;
          If (Pos(act, FieldByName('MENUS').AsString) > 0) then
            TAction(TActionManager(Component).Actions[i]).Enabled := true;
        end;
      end;//for

      next;
    end;//while

  end;//with
end;

procedure TfrmPrincipal.WindowsTabExecute(Sender: TObject);
var
  i:Integer;
begin
   if dxtab.Active then
     dxtab.Active := False
   else
     dxtab.Active := True;
end;

end.
