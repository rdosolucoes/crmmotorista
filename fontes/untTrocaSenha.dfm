inherited frmTrocaSenha: TfrmTrocaSenha
  Caption = 'Trocar Senha'
  ClientHeight = 246
  ClientWidth = 483
  ExplicitWidth = 489
  ExplicitHeight = 275
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 140
    Top = 12
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  inherited StatusBar1: TStatusBar
    Top = 227
    Width = 483
    ExplicitTop = 227
    ExplicitWidth = 483
  end
  inherited pnlBotoes: TPanel
    Height = 227
    ExplicitHeight = 227
    inherited PageControl1: TPageControl
      Height = 227
      ExplicitHeight = 227
      inherited tbs2: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 6
        ExplicitWidth = 108
        ExplicitHeight = 217
        inherited Panel2: TPanel
          Height = 217
          ExplicitHeight = 217
        end
      end
    end
  end
  object edtNome: TEdit
    Left = 140
    Top = 26
    Width = 313
    Height = 21
    TabStop = False
    Color = clSilver
    ReadOnly = True
    TabOrder = 2
    Text = 'edtNome'
  end
  object grbLogin: TGroupBox
    Left = 140
    Top = 61
    Width = 313
    Height = 125
    Caption = '[ Login ]'
    TabOrder = 3
    object Label7: TLabel
      Left = 16
      Top = 25
      Width = 36
      Height = 13
      Caption = 'Usu'#225'rio'
    end
    object Label6: TLabel
      Left = 16
      Top = 75
      Width = 30
      Height = 13
      Caption = 'Senha'
    end
    object Label5: TLabel
      Left = 164
      Top = 75
      Width = 68
      Height = 13
      Caption = 'Repetir Senha'
    end
    object edtSenha2: TEdit
      Left = 164
      Top = 89
      Width = 121
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object edtUsuario: TEdit
      Left = 16
      Top = 38
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object edtSenha: TEdit
      Left = 16
      Top = 90
      Width = 121
      Height = 21
      PasswordChar = '*'
      TabOrder = 1
    end
  end
  object btCript: TJvXPButton
    Left = 425
    Top = 200
    Width = 52
    Height = 22
    Hint = 'CRMi|Confirmar Opera'#231#227'o|0'
    CustomHint = dmPrincipal.BalloonHint1
    Caption = 'Cript'
    TabOrder = 4
    Glyph.Data = {
      0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000010
      0000001008060000001FF3FF610000000467414D410000B18F0BFC6105000001
      4649444154384F8593B14AC4401445030161611797D5F53B8485ADAC044110D2
      E503D26CBB7FB05F90CA2244BB606B2958097E804DAA5459100282294260FBE7
      BD8FCC904C5CB63873DFBB73E731C58C27224779BB9A6DFEF3FBD8E27539F3C1
      27B8EE79105BAFC107F08D476C415E2ECF03F003740814A2BA06BFE0DE640D83
      863C5FCC03B00713F004A6A002A3C3646490C7C57CE1F4BCCA2063D025CBB204
      8843726A4F07A4699A00391C0ED2B6ADC29A1E03C7F6784E07C4712C4DD308D5
      410354C7B7791DB0DBEDA42C4BA1D238859BF7B6DBADE4792E5407BD01D5F16D
      5E0744519400298A42AAAA52EABA167A0C50D99B3DE6E8F19C0EE012866102C4
      410354C7B77B76800BFEC099D32FFB7D9F9181B77E0BF6600236600ABE41E066
      C9A0C15BBFEBDEFC4DD7435457807FE4C1640DB6C05BF7C117D0C39D07B1F50A
      BC83DE6F14EF0FE7389003DF767A770000000049454E44AE426082}
    ParentShowHint = False
    ShowHint = True
    OnClick = btCriptClick
  end
end
