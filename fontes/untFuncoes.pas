unit untFuncoes;

interface

uses cxGridDBTableView, udmPrincipal, JvComCtrls,JvDBLookUp, cxGridCustomView,Vcl.ExtCtrls,Vcl.StdCtrls,
     SqlExpr,Forms,JvDBControls,JvToolEdit,Windows,Classes,SysUtils,Variants,StrUtils,Data.DB,Controls,
     untPesquisa,udmPesquisa,DBClient,Vcl.dbctrls,cxFilter,udmGeradorRelatorios,frxDBXComponents,dxCore;
{$REGION 'DeveloperX'}
//DeveloperX
Procedure SalvarGrid(var grid :TcxGridDBTableView; frm :String);
Procedure CarregarGridPadrao(var grid :TcxGridDBTableView);
Procedure CarregarGrid(var grid :TcxGridDBTableView; frm :String);
Procedure SalvarGridM(var grid :TcxGridDBTableView);
Procedure CarregarGridM(var grid :TcxGridDBTableView);
procedure SalvarGridFiltro(grid :TcxCustomGridView);
procedure CarregarGridFiltro(grid :TcxCustomGridView);
{$ENDREGION DeveloperX}

{$REGION 'Uteis'}
//Uteis
Function GeraCodigo(campo :string; CodEmp :Integer = 0) : integer;
Procedure pMonitor(Status :Boolean);
function fAjustaTop(TamTela :integer):Integer;
procedure AjustaTela(Form :TForm);
procedure AjustaForm(Form :TForm);
Procedure pExec(txt :String);
procedure AchaComponente (Form : TForm);
function IsInteger(TestaString: String) : boolean;
function SQLExecute (SQL :String):Integer;
procedure pAbreVisao(oForm: TComponentClass; var oReferencia);
procedure pAbreCadastro(oForm: TComponentClass; var oReferencia);
procedure  CriaQuery (var Query:TSQLQuery);
function ZerosEsq( S:String;tam:Integer ) : String;
procedure AlinharPanel(AForm: TForm; APanel: TCustomPanel; ACentro: Boolean);
Function DataProximoMes(xData: TDateTime): Integer;
function CopiaRegistro(oDataSetOrigem: TDataSet; oDataSetDestino: TDataSet; cCamposExecoes: string): Boolean;
function EstaContido(const cVerificar, cString: string): Boolean;
function PesquisaCodDescricao(edt1,edt2 :TEdit;Tabela,Chave,Condicao:String;Sender:TObject):Variant ;
function Pesquisa(DSPrincipal :TClientDataSet;Tabela,Chave:String;Campos,CamposPesq,AliasCampos :array of string;
         CondicaoWhere :String;Sender:TObject):Variant;
function AllTrim( S: String ) : String;
function RTrim( S: String ) : String;
function LTrim( S: String ) : String;
procedure AddFiltroCxGrid(Grid: TcxGridDBTableView;Coluna1,Coluna2 : TcxGridDBColumn;Operador1,Operador2 :String;Valor1,Valor2 :String;Condicao :String = '');
function Decimal( Valor: Double ): String;
function cpf(num: string): boolean;
function Cnpj(xCNPJ: String):Boolean;
function EspacosDir( S:String;tam:Integer ) : String;
function SoCaracteresPermitidos(s:string):string;
function VersaoExe: String;
procedure CarregaRelatorio(NomeRel:String;Parametros,Valores:Array of String;SoImprime :Boolean = False );
function CarregaSetor :String;
procedure AtualizaStatusPedido(NuNota :Integer;Status :String);
{$ENDREGION Uteis}

{$REGION 'Seguranca'}
//Seguranca
Function Cripto(Texto :String):String;
Function DeCripto(Texto :String):String;
{$ENDREGION Seguranca}

{$REGION 'Gets'}
//Gets
Function fGetColigada:Integer;
Function fGetUsuario:Integer;
Function fGetNomeUsuario :String;
Function fGetNomeColigada:String;
{$ENDREGION 'Gets'}

{$REGION 'JvTreeView Manipulacao'}
//JvTreeView Manipulacao
Procedure pExpandTree(var tree :TJvTreeView);
Procedure pColapseTree(var tree :TJvTreeView);
{$ENDREGION 'JvTreeView Manipulacao'}


{$REGION 'Arrays'}
function ArrayItens(cString, cSeparador: string; iIndice: Integer): string;
function ArrayCont(cString, cSeparador: string): Integer;
function ArrayLocateItem(cString, cItem, cSeparador: string): Integer;


{$ENDREGION 'Arrays'}



procedure InsertMov(Tabela : string; sCampos : String; sValores : Variant );



type
  TFieldDesc = record
  FName, FValue : String;
  end;


implementation

uses cxCustomData, untPrincipal,Dialogs;

Const
  NomeTempGrid :String = 'GRIDTEMP.TXT';
  CiPherKey :String = 'CRMi';

{$REGION 'DeveloperX'}
Procedure SalvarGrid(var grid :TcxGridDBTableView; frm :String);
var
  arq :String;
begin
  arq := ExtractFilePath(application.ExeName)+NomeTempGrid;
  grid.StoreToIniFile(arq,true,[],'');

  with dmPrincipal do
  begin
    cdsGrid.Close;
    cdsGrid.Params.ParamByName('PEMPRESA').AsInteger := fgetColigada;
    cdsGrid.Params.ParamByName('PUSU').AsInteger := cdsUsuario.FieldByName('IDCRMUSUARIO').AsInteger;
    cdsGrid.Params.ParamByName('PGRID').AsString := frm+'.'+grid.Name;
    cdsGrid.Open;

    if cdsGrid.IsEmpty then
    begin
      cdsGrid.Append;
      cdsGridCODCOLIGADA.AsInteger := fgetColigada;
      cdsGridIDCRMUSUARIO.AsInteger := cdsUsuario.FieldByName('IDCRMUSUARIO').AsInteger;
    end
    else
      cdsGrid.Edit;

    cdsGridLAYOUT.Asstring := '';
    cdsGridNOMEGRID.AsString := frm+'.'+grid.Name;
    cdsGridLAYOUT.LoadFromFile(arq);
    cdsGrid.Post;
    cdsGrid.ApplyUpdates(0);

    deletefile(arq);
  end;
end;

Procedure CarregarGridPadrao(var grid :TcxGridDBTableView);
var
  I: Integer;
  captions: array of string;
begin
  SetLength(captions, grid.ColumnCount);

  for I := 0 to grid.ColumnCount - 1 do
  begin
    grid.Columns[i].GroupIndex := -1;
    grid.Columns[i].Visible := true;
    grid.Columns[i].SortIndex := -1;
    grid.Columns[i].SortOrder := sonone;
    //
    captions[i] := grid.Columns[i].Caption;
  end;

  grid.RestoreDefaults;

  for I := 0 to grid.ColumnCount - 1 do
    grid.Columns[i].Caption := captions[i];
end;

Procedure CarregarGrid(var grid :TcxGridDBTableView; frm :String);
var
  arq :String;
begin
  arq := ExtractFilePath(application.ExeName)+NomeTempGrid;

  with dmPrincipal do
  begin
    cdsGrid.Close;
    cdsGrid.Params.ParamByName('PEMPRESA').Value := fGetColigada;
    cdsGrid.Params.ParamByName('PUSU').AsInteger := cdsUsuario.FieldByName('IDCRMUSUARIO').AsInteger;
    cdsGrid.Params.ParamByName('PGRID').AsString := frm+'.'+grid.Name;
    cdsGrid.Open;

    if not(cdsGrid.IsEmpty) then
    begin
      cdsGridLAYOUT.SaveToFile(arq);
      grid.RestoreFromIniFile(ExtractFilePath(application.ExeName)+NomeTempGrid,true,true,[],'');
      deletefile(arq);
    end;
  end;
end;

Procedure SalvarGridM(var grid :TcxGridDBTableView);
var
  msStream :TMemoryStream;
  arq :string;
begin
  if Assigned(grid) then
  begin
    msStream := TMemoryStream.Create;
    try
      grid.StoreToStream(msStream);
      msStream.Position := 0;
      arq := ExtractFilePath(application.ExeName)+NomeTempGrid;
      //msStream.SaveToFile(arq);

      with dmPrincipal do
      begin
        cdsGrid.Close;
        cdsGrid.Params.ParamByName('PEMPRESA').AsInteger := cdsEmpresa.FieldByName('CODCOLIGADA').AsInteger;
        cdsGrid.Params.ParamByName('PUSU').AsInteger := cdsUsuario.FieldByName('IDCRMUSUARIO').AsInteger;
        cdsGrid.Params.ParamByName('PGRID').AsString := grid.Name;
        cdsGrid.Open;

        if cdsGrid.IsEmpty then
        begin
          cdsGrid.Append;
          cdsGridCODCOLIGADA.AsInteger := cdsEmpresa.FieldByName('CODCOLIGADA').AsInteger;
          cdsGridIDCRMUSUARIO.AsInteger := cdsUsuario.FieldByName('IDCRMUSUARIO').AsInteger;
        end
        else
          cdsGrid.Edit;

        cdsGridNOMEGRID.AsString := grid.Name;
        cdsGridLAYOUT.LoadFromFile(arq);
        cdsGrid.Post;
        cdsGrid.ApplyUpdates(0);

       // deletefile(arq);
      end;

    finally
      msStream.Free;
    end;
  end;
end;

Procedure CarregarGridM(var grid :TcxGridDBTableView);
var
  msStream :TStringStream;
  arq :string;
begin
  if Assigned(grid) then
  begin
    with dmPrincipal do
    begin
      cdsGrid.Close;
      cdsGrid.Params.ParamByName('PEMPRESA').AsInteger := cdsEmpresa.FieldByName('CODCOLIGADA').AsInteger;
      cdsGrid.Params.ParamByName('PUSU').AsInteger := cdsUsuario.FieldByName('IDCRMUSUARIO').AsInteger;
      cdsGrid.Params.ParamByName('PGRID').AsString := grid.Name;
      cdsGrid.Open;

      if not(cdsGrid.IsEmpty) then
      begin
        arq := ExtractFilePath(application.ExeName)+NomeTempGrid;
        cdsGridLAYOUT.SaveToFile(arq);

        msStream := TStringStream.Create(arq);
        try
          //msStream.Seek(0,sofrombeginning);
          msStream.Position := 0;
          grid.RestoreFromStream(msStream);

          deletefile(arq);
        finally
          msStream.Free;
        end;
      end;
    end;//with
  end;
end;

procedure SalvarGridFiltro(grid :TcxCustomGridView);
var
  msStream :TStringStream;

  AnArray    : array[1..5000] of single; // Just a comment on the expected format of this parameter
  bf         : TBlobField;
StreamSize : integer;
  ArraySize  : integer;
  Stream     : TMemoryStream;
begin
  if Assigned(grid) then
  begin
    msStream := TStringStream.Create(EmptyStr);
    try
      grid.DataController.Filter.SaveToStream(msStream);

      ShowMessage(inttostr(msStream.Size));
      msStream.Seek(0,sofrombeginning);

      with dmPrincipal do
      begin
        cdsGrid.Close;
        cdsGrid.Params.ParamByName('PEMPRESA').AsInteger := cdsEmpresa.FieldByName('CODCOLIGADA').AsInteger;
        cdsGrid.Params.ParamByName('PUSU').AsInteger := cdsUsuario.FieldByName('IDCRMUSUARIO').AsInteger;
        cdsGrid.Params.ParamByName('PGRID').AsString := grid.Name;
        cdsGrid.Open;

        if cdsGrid.IsEmpty then
        begin
          cdsGrid.Append;
          cdsGridCODCOLIGADA.AsInteger := cdsEmpresa.FieldByName('CODCOLIGADA').AsInteger;
          cdsGridIDCRMUSUARIO.AsInteger := cdsUsuario.FieldByName('IDCRMUSUARIO').AsInteger;
          cdsGridNOMEGRID.AsString := grid.Name;
          //cdsGridFILTRO.AsVariant := msStream.DataString;
          //cdsGrid.Post;
          //cdsGrid.ApplyUpdates(0);
        end
        else
        begin
          cdsGrid.Edit;
          //cdsGridFILTRO.AsVariant := msStream.DataString;
          //cdsGrid.Post;
          //cdsGrid.ApplyUpdates(0);
        end;

        cdsGridFILTRO.LoadFromStream(msStream);


//  bf := cdsGridFILTRO as TBlobField;

//  Stream := TMemoryStream.Create;
  try
  //  bf.LoadFromStream(msStream);
//  StreamSize := Stream.Size; // just for debug
  //  ArraySize  := high(AnArray) * SizeOf(single);  // compute the maximum number of bytes to copy

//    Stream.Position :=0; // after loading stream, *position* is the last byte
  //  Stream.Read(AnArray, ArraySize); // copy up to "ArraySize" bytes to array
    //      grid.DataController.Filter.LoadFromStream(msStream);
      //    grid.DataController.Filter.Active := True;
  finally
    Stream.Free;
  end;
          cdsGrid.Post;
          cdsGrid.ApplyUpdates(0);

      end;
    finally
      msStream.Free;
    end;
  end;
end;

   {
var

begin
  bf := cdsGridFILTRO.FieldByName(Field) as TBlobField;

  Stream := TMemoryStream.Create;
  try
    bf.SaveToStream(Stream);
//  StreamSize := Stream.Size; // just for debug
    ArraySize  := high(AnArray) * SizeOf(single);  // compute the maximum number of bytes to copy

    Stream.Position :=0; // after loading stream, *position* is the last byte
    Stream.Read(AnArray, ArraySize); // copy up to "ArraySize" bytes to array
  finally
    Stream.Free;
  end;

}

procedure CarregarGridFiltro(grid :TcxCustomGridView);
var
  msStream :TStringStream;

  AnArray    : array[1..5000] of single; // Just a comment on the expected format of this parameter
  bf         : TBlobField;
StreamSize : integer;
  ArraySize  : integer;
  Stream     : TMemoryStream;
begin
  if Assigned(grid) then
  begin
    with dmPrincipal do
    begin
      cdsGrid.Close;
      cdsGrid.Params.ParamByName('PEMPRESA').AsInteger := cdsEmpresa.FieldByName('CODCOLIGADA').AsInteger;
      cdsGrid.Params.ParamByName('PUSU').AsInteger := cdsUsuario.FieldByName('IDCRMUSUARIO').AsInteger;
      cdsGrid.Params.ParamByName('PGRID').AsString := grid.Name;
      cdsGrid.Open;

      if not(cdsGrid.IsEmpty) then
      begin
        Stream := TMemoryStream.Create();
        cdsGridFILTRO.SaveToStream(Stream);
        ShowMessage(inttostr(Stream.Size));

        grid.DataController.Filter.LoadFromStream(Stream);

 { bf := cdsGridFILTRO as TBlobField;

  Stream := TMemoryStream.Create;
  try
    bf.SaveToStream(Stream);
  StreamSize := Stream.Size; // just for debug
    ArraySize  := high(AnArray) * SizeOf(single);  // compute the maximum number of bytes to copy

    Stream.Position :=0; // after loading stream, *position* is the last byte
    Stream.Read(AnArray, ArraySize); // copy up to "ArraySize" bytes to array
          grid.DataController.Filter.LoadFromStream(msStream);
          grid.DataController.Filter.Active := True;
  finally
    Stream.Free;
        {msStream := TStringStream.Create(cdsGridFILTRO.AsString);
        ShowMessage(inttostr(msStream.Size));
        try
          msStream.Seek(0,sofrombeginning);
          grid.DataController.Filter.LoadFromStream(msStream);
          grid.DataController.Filter.Active := True;
        finally
          msStream.Free;}
       // end;
      end;
    end;//with

  end;
end;
{$ENDREGION DeveloperX}

{$REGION 'Uteis'}
Function GeraCodigo(campo :string;CodEmp :Integer = 0) : integer;
var qUpdate, qUltReg : TSQLQuery;
begin
  qUpdate := TSQLQuery.Create( nil );
  qUpdate.SQLConnection := dmPrincipal.Conn;

  qUltReg := TSQLQuery.Create( nil );
  qUltReg.SQLConnection := dmPrincipal.Conn;

  qUltReg.Close;
  qUltReg.SQL.Clear;
  qUltReg.SQL.Add( ' SELECT VALOR, CODEMP FROM CRMAUTOINC ');
  qUltReg.SQL.Add( ' WHERE CAMPO = ' + QuotedStr( CAMPO ) + ' AND CODEMP = 0' );
  qUltReg.Open;

  if qUltReg.RecordCount > 0 then
  begin
    qUpdate.Close;
    qUpdate.SQL.Clear;
    qUpdate.SQL.Add( ' UPDATE CRMAUTOINC SET VALOR = ' + IntToStr ( qUltReg.FieldByName ('VALOR').AsInteger + 1 ) + ' WHERE ' );
    qUpdate.SQL.Add( ' CAMPO = ' + QuotedStr ( CAMPO ) + ' AND CODEMP = 0' );
    qUpdate.ExecSQL;
    Result := qUltReg.FieldByName ('VALOR').AsInteger + 1;
  end
  else
  begin
    if CODEMP = 0 then
      CODEMP :=  fgetColigada;

    qUltReg.Close;
    qUltReg.SQL.Clear;
    qUltReg.SQL.Add( ' SELECT VALOR FROM CRMAUTOINC ');
    qUltReg.SQL.Add( ' WHERE CAMPO = ' + QuotedStr( CAMPO ) + ' AND CODEMP = ' + IntToStr(CODEMP) );
    qUltReg.Open;

    qUpdate.Close;
    qUpdate.SQL.Clear;
    qUpdate.SQL.Add( ' UPDATE CRMAUTOINC SET VALOR = ' + IntToStr ( qUltReg.FieldByName ('VALOR').AsInteger + 1 ) + ' WHERE ' );
    qUpdate.SQL.Add( ' CAMPO = ' + QuotedStr ( CAMPO ) + ' AND CODEMP = ' + IntToStr(CODEMP) );
    qUpdate.ExecSQL(true);
    Result := qUltReg.FieldByName ('VALOR').AsInteger + 1;
  end;
end;


Procedure pMonitor(Status :Boolean);
begin
  dmPrincipal.SQLMonitor1.Active := Status;
end;

function fAjustaTop(TamTela :integer):Integer;
var
  disp, fixo :integer;
begin
  disp := frmprincipal.Height - frmPrincipal.ribMenu.Height - frmPrincipal.StatusBar1.Height;
  fixo := frmPrincipal.ribMenu.Height + frmPrincipal.StatusBar1.Height;
  //
  result := (round(disp/2) - round(TamTela/2) - 30);
end;

Procedure pExec(txt :String);
begin
  with dmPrincipal.sqlAux1 do
  begin
    close;
    CommandText := txt;
    ExecSQL(true);
  end;
end;

procedure AchaComponente (Form : TForm);
var i : integer;
 //   Comp : TJvDBLookupCombo;
begin
  for i := 0 to Form.ComponentCount -1 do
    if Form.Components[i] is TJvDBLookupCombo then
    begin
      if (TJvDBLookupCombo(Form.Components[i]).Focused) and (TJvDBLookupCombo(Form.Components[i]).IsDropDown) then
      begin
         frmPrincipal.flag_comp_Enter := true;
         frmPrincipal.flag_comp       := true;
         exit;
      end
      else
      if (TJvDBLookupCombo(Form.Components[i]).Focused) and (TJvDBLookupCombo(Form.Components[i]).Text = '') then
      begin
         frmPrincipal.flag_comp := false;
         exit;
      end
      else
      if (TJvDBLookupCombo(Form.Components[i]).Focused) or (TJvDBLookupCombo(Form.Components[i]).IsDropDown) then
      begin
         frmPrincipal.flag_comp := true;
         exit;
      end;
    end
    else
    if Form.Components[i] is TJvDBDateEdit then
    begin
      if (TJvDBDateEdit(Form.Components[i]).Focused) and (TJvDBDateEdit(Form.Components[i]).PopupVisible) then
      begin
         frmPrincipal.flag_comp_Enter := true;
         frmPrincipal.flag_comp       := true;
         exit;
      end;
    end
    else
    if Form.Components[i] is TJvDateEdit then
    begin
      if (TJvDBDateEdit(Form.Components[i]).Focused) and (TJvDBDateEdit(Form.Components[i]).PopupVisible) then
      begin
         frmPrincipal.flag_comp_Enter := true;
         frmPrincipal.flag_comp       := true;
         exit;
      end;
    end;
end;

function IsInteger(TestaString: String) : boolean;
begin
  try
    result := true;
    StrToInt(TestaString);
  except
    On EConvertError do
       result := False;
    else
       result := True;
    end;
end;

procedure AjustaTela(Form :TForm);
var BarraIniciar, Janela: HWND; {Barra Iniciar}
    tmAltura, tmFixo: Integer;
    tmRect: TRect;
begin
 // fmain.nomeForm := Form;
  //localiza o Handle da janela iniciar
  BarraIniciar := FindWindow('Shell_TrayWnd', nil);
  //Pega o "ret�ngulo" que envolve a barra e sua altura
  GetWindowRect(BarraIniciar, tmRect);

 // IF IsTaskbarAutoHideOn THEN
 //    tmAltura := 0
//  ELSE
     tmAltura := tmRect.Bottom - tmRect.Top;

  tmFixo :=  frmPrincipal.ribMenu.Height + frmPrincipal.StatusBar1.Height + 35;

  Form.Top := 0;
  Form.Left := 0;

  Form.Height := Screen.Height - (tmAltura + tmFixo) ;
  Form.Width  := Screen.Width - 8;
end;

procedure AjustaForm(Form :TForm);
Const
nTamOriginal = 1600; // Ser� o 100% da escala
Var
nEscala : Double; // Vai me dar o percentual de Transforma��o escalar
nPorcento : Integer; // Vai me dar em percentual inteiro o valor
begin
  With Form do
  begin
    if nTamOriginal <> Screen.Width then
    begin
      nEscala := ((Screen.Width-nTamOriginal)/nTamOriginal);
      nPorcento := Round((nEscala*100) + 100);
      Width := Round(Width * (nEscala+1));
      Height := Round(Height * (nEscala+1));
      ScaleBy(nPorcento,100);
    end;
  end;
end;

function SQLExecute (SQL :String):Integer;
var
  qAux :TSQLQuery;
begin
  qAux := TSQLQuery.Create(nil);
  qAux.SQLConnection := dmPrincipal.Conn;
  qAux.Close;

  qAux.SQL.Clear;
  qAux.SQL.Add(SQL);
  Result := qAux.ExecSQL(True);

  FreeAndNil (qAux);
end;


procedure pAbreVisao(oForm: TComponentClass; var oReferencia);
begin
  if not Assigned(TForm(oReferencia)) then
    Application.CreateForm(oForm, oReferencia);

 if TForm(oReferencia).WindowState = wsMinimized then
     TForm(oReferencia).WindowState := wsMaximized;
  TForm(oReferencia).Show;
end;


procedure pAbreCadastro(oForm: TComponentClass; var oReferencia);
begin
  if not Assigned(TForm(oReferencia)) then
    Application.CreateForm(oForm, oReferencia);

   if TForm(oReferencia).WindowState = wsMinimized then
     TForm(oReferencia).WindowState := wsNormal;
  TForm(oReferencia).ShowModal;
end;

procedure CriaQuery(var Query :TSQLQuery);
begin
  Query := TSQLQuery.create(nil);
  Query.SQLConnection := dmprincipal.Conn;
  Query.close;
  Query.sql.clear;
end;



function ZerosEsq;
var i:integer;
begin
  if tam>length(S)
  then  for i:=1 to tam-length(S)
        do S:='0'+S
  else delete(S,tam,length(S)-tam);
  result:= S
end;

procedure AlinharPanel(AForm: TForm; APanel: TCustomPanel; ACentro: Boolean);
begin
  if ACentro then
  begin
    APanel.Left := (AForm.ClientWidth div 2) - (APanel.Width div 2);
    APanel.Top := (AForm.ClientHeight div 2) - (APanel.Height div 2);
  end
  else
  begin
    APanel.Left := (AForm.ClientWidth + 100);
    APanel.Top := (AForm.ClientHeight + 100);
  end;
  AForm.Update;
  APanel.Update;
end;

Function DataProximoMes(xData: TDateTime): Integer;
Var
   Year, Month, Day: Word;
   MesAtual : Integer;
   NovMes : Integer;

Begin
   Result := -1;
   Try
      DecodeDate(xData, Year, Month, Day);
      MesAtual := Integer(Month);
      NovMes := ((MesAtual + 12 + 1) mod 12);
      If NovMes = 0 Then NovMes := 12;
        Result := NovMes;
   Except
      Result := -1;
   End;
End;

function CopiaRegistro(oDataSetOrigem: TDataSet; oDataSetDestino: TDataSet; cCamposExecoes: string): Boolean;
var
   iInd: Integer;
   cCampoSetado: string;
begin
   Result := True;
   if (oDataSetOrigem.Active) and (oDataSetDestino.Active) and not (oDataSetDestino.State in [dsInactive, dsBrowse]) then
   begin
      // Campos que nao ter�o os valores modificados.
      cCamposExecoes := UpperCase(Trim(cCamposExecoes));

      for iInd := 0 to oDataSetOrigem.FieldCount - 1 do
      begin
         cCampoSetado := UpperCase(oDataSetOrigem.Fields[iInd].FieldName);
         if ArraylocateItem(cCamposExecoes, cCampoSetado, ',') = 0 then
         begin
            if oDataSetDestino.FindField(cCampoSetado) <> nil then
              oDataSetDestino.FieldByName(cCampoSetado).Value := oDataSetOrigem.FieldByName(cCampoSetado).Value;
         end;
      end;
   end
   else
   begin
      ShowMessage('CopiaRegistro - oDataSetDestino (' + oDataSetDestino.Name + ') n�o est� em modo de edi��o');
      Result := False;
   end;
end;

function EstaContido(const cVerificar, cString: string): Boolean;
begin
  Result := Pos(LowerCase(cVerificar), LowerCase(cString)) > 0;
end;

function PesquisaCodDescricao(edt1,edt2 :TEdit;Tabela,Chave,Condicao:String;Sender:TObject):Variant ;
var
  Campo1,Campo2,CampoPesq,Script :String;

  procedure AbreTelaPesquisa;
  begin
    Application.CreateForm(TfrmPesquisa,frmPesquisa);
    frmPesquisa.ValorCampo := (Sender as TEdit).Text;
    frmPesquisa.CampoPesq  := CampoPesq;
    SetLength(frmPesquisa.AliasCampos,2);
    frmPesquisa.AliasCampos[0] := edt1.Hint;
    frmPesquisa.AliasCampos[1] := edt2.Hint;
    frmPesquisa.ShowModal;
    if dmPesquisa.cdsPesq.IsEmpty then
    begin
      edt1.Text := '';
      edt2.Text := '';
    end
    else
    begin
      edt1.Text := dmPesquisa.cdsPesq.fields[0].Value;
      edt2.Text := dmPesquisa.cdsPesq.Fields[1].Value;
    end;
  end;

begin
  Application.CreateForm(TdmPesquisa,dmPesquisa);
  Campo1 := Copy(edt1.Name,Pos('_',edt1.Name)+1);
  Campo2 := Copy(edt2.Name,Pos('_',edt2.Name)+1);
  CampoPesq := Copy((Sender as TEdit).name,Pos('_',(Sender as TEdit).name)+1);

  With dmPesquisa do
  begin
    if (Sender as TEdit).text = '' then
    begin
      edt1.Text := '';
      edt2.Text := '';
      FreeAndNil(dmPesquisa);
      Exit;
    end;

    if Chave <> '' then
      Script := 'SELECT ' + Campo1 + ',' + Campo2  + ',' + Chave + ' FROM ' + tabela + ' (NOLOCK) '
    else
      Script := 'SELECT ' + Campo1 + ',' + Campo2  + ' FROM ' + tabela + ' (NOLOCK) ';

    cdsPesq.Close;
    sqlPesq.SQL.Add(Script + ' WHERE (CODCOLIGADA = 0 or CODCOLIGADA = ' +InttoStr(FgetColigada) + ')' + Condicao );
    sqlPesq.SQL.Add(' AND ' + CampoPesq + ' LIKE ( ' + QuotedStr((Sender as TEdit).Text + '%' ) + ')' );
    cdsPesq.Open;

    if not cdsPesq.IsEmpty then
    begin
      if cdsPesq.RecordCount = 1 then
      begin
        edt1.Text := cdsPesq.fields[0].Value;
        edt2.Text := cdsPesq.Fields[1].Value;
      end
      else
        AbreTelaPesquisa;
    end
    else
      AbreTelaPesquisa;

    if (Chave <> '') and (cdsPesq.Fields[2].AsString  <> '') then
      Result := cdsPesq.Fields[2].Value
    else
      Result := '';

  end;
  FreeAndNil(dmPesquisa);
end;

function Pesquisa(DSPrincipal :TClientDataSet;Tabela,Chave:String;Campos,CamposPesq,AliasCampos :array of string;
         CondicaoWhere :String;Sender:TObject):Variant;

var
  Script,cCamposPesq,CampoChave,ValorChave :String;
  i,j :Integer;
  Cancelado :Boolean;


  procedure AbreTelaPesquisa;
  var
   i: Integer;
  begin
    Application.CreateForm(TfrmPesquisa,frmPesquisa);
    frmPesquisa.ValorCampo := ValorChave;
    frmPesquisa.CampoPesq  := UpperCase(CampoChave);

 {   SetLength(frmPesquisa.CamposInvisiveis, Length(CamposInvisiveis));
    for i := Low(CamposInvisiveis) to High(CamposInvisiveis) do
      frmPesquisa.CamposInvisiveis[i] := CamposInvisiveis[i];}

    SetLength(frmPesquisa.AliasCampos, Length(AliasCampos));
    for i := Low(AliasCampos) to High(AliasCampos) do
      frmPesquisa.AliasCampos[i] := AliasCampos[i];

    frmPesquisa.ShowModal;
    Cancelado := dmPesquisa.cdsPesq.IsEmpty;

    for i := 0 to High(Campos) do
      if Cancelado then
         DSPrincipal.FieldByName(Campos[i]).Clear
      else
         DSPrincipal.FieldByName(Campos[i]).value := dmPesquisa.cdsPesq.FieldByName(CamposPesq[i]).Value;
  end;
begin
  begin
    Cancelado := False;
    CampoChave := Copy((Sender as TDBEdit).Name ,Pos('_',(Sender as TDBEdit).Name)+1);
    ValorChave := (Sender as TDBEdit).Text;


    Application.CreateForm(TdmPesquisa,dmPesquisa);
    if not(DSPrincipal.State in [dsInsert,dsEdit]) then
      DSPrincipal.Edit;

    for I := 0 to High(CamposPesq) do
    begin
      if cCamposPesq = '' then
        cCamposPesq := CamposPesq[i]
      else
        cCamposPesq := cCamposPesq + ',' + CamposPesq[i];

    end;
    dmPesquisa.cdsPesq.Close;


    if not (UPPERCASE(Copy(Trim(CondicaoWhere),0,5)) = 'WHERE') then
      CondicaoWhere :=  ' WHERE (CODCOLIGADA = ' + IntToStr(fgetColigada) + ' OR CODCOLIGADA = 0) '  +
                      CondicaoWhere;

    Script := 'SELECT ' +  cCamposPesq + ' FROM ' + Tabela + ' (NOLOCK) ' + CondicaoWhere ;


    dmPesquisa.sqlPesq.SQL.Add(Script);
    dmPesquisa.sqlPesq.SQL.Add(' AND ' + CampoChave +  ' like (' + QuotedStr(ValorChave + '%') + ')');

    if (ValorChave = '') then
    begin
      for i := 0 to High(Campos) do
         DSPrincipal.FieldByName(Campos[i]).Clear;
      FreeAndNil(dmPesquisa);
      Exit;
    end
    else
    begin
      dmPesquisa.cdsPesq.Open;

      if (dmPesquisa.cdsPesq.IsEmpty) or (dmPesquisa.cdsPesq.RecordCount > 1)   then
        AbreTelaPesquisa
      else
       for i := 0 to High(Campos) do
           DSPrincipal.FieldByName(Campos[i]).value := dmPesquisa.cdsPesq.FieldByName(CamposPesq[i]).Value;
    end;
    if Cancelado then
      Result := ''
    else
      Result := DSPrincipal.FieldByName(Chave).value;
    FreeAndNil(dmPesquisa);
  end;
end;

function AllTrim( S: String ) : String;
begin
    AllTrim := LTrim( RTrim( s ) );
end;

function RTrim( S: String ) : String;
var i : integer; cRet : String;
begin
   cRet := '';
   for i := 1 to Length( s ) do
       if Copy( s, i, 1 ) <> ' ' then break;
   cRet := Copy( s, i, ( Length( s ) - ( i - 1 ) ) );
   RTrim := cRet;
end;

function LTrim( S: String ) : String;
var i : integer; cRet : String;
begin
   cRet := '';
   for i := Length( s ) downto 1 do
       if Copy( s, i, 1 ) <> ' ' then break;
   cRet := Copy( s, 1, i );
   LTrim := cRet;
end;

procedure AddFiltroCxGrid(Grid: TcxGridDBTableView;Coluna1,Coluna2 :TcxGridDBColumn;Operador1,Operador2 :String;Valor1,Valor2:String;Condicao :String = '');
var
  AItemList: TcxFilterCriteriaItemList;

begin
 // Adicionando Filtro Default
 Grid.DataController.Filter.BeginUpdate ;
 try
   Grid.DataController.Filter.Root.Clear ;

   if Condicao = 'e' then
     AItemList :=  Grid.DataController.Filter.Root.AddItemList(fboand)
   else
     AItemList :=  Grid.DataController.Filter.Root.AddItemList(fboOr);

   if Operador1 = '=' then
     AItemList.AddItem(Coluna1,foEqual,Valor1,Valor1);

   if Coluna2 <> nil then
   begin
     if Operador2 = '=' then
      AItemList.AddItem(Coluna2,foEqual,Valor2,Valor2);
   end;

 finally
   Grid.DataController.Filter.EndUpdate;
   Grid.DataController.Filter.Active := True;
 end;

end;

function Decimal( Valor: Double ): String;
begin
  Result := StringReplace( StringReplace( FloatToStr( Valor ), '.','', [rfReplaceAll,rfIgnoreCase] ), ',','.',[rfReplaceAll,rfIgnoreCase] );
end;

function cpf(num: string): boolean;
var
    n1,n2,n3,n4,n5,n6,n7,n8,n9: integer;
   d1,d2: integer;
   digitado, calculado: string;
begin
   n1:=StrToInt(num[1]);
   n2:=StrToInt(num[2]);
   n3:=StrToInt(num[3]);
   n4:=StrToInt(num[4]);
   n5:=StrToInt(num[5]);
   n6:=StrToInt(num[6]);
   n7:=StrToInt(num[7]);
   n8:=StrToInt(num[8]);
   n9:=StrToInt(num[9]);
   d1:=n9*2+n8*3+n7*4+n6*5+n5*6+n4*7+n3*8+n2*9+n1*10;
   d1:=11-(d1 mod 11);
   if d1>=10 then
      d1:=0;
      d2:=d1*2+n9*3+n8*4+n7*5+n6*6+n5*7+n4*8+n3*9+n2*10+n1*11;
      d2:=11-(d2 mod 11);
      if d2>=10 then
         d2:=0;
         calculado:=inttostr(d1)+inttostr(d2);
         digitado:=num[10]+num[11];
         if calculado=digitado then
                  cpf:=true
            else
                  cpf:=false;
end;


//Verificando se um CNPJ � v�lido.
//Passe o valor do CNPJ em quest�o para a fun��o abaixo (em forma de string), caso seja retornado o valor de FALSE o CNPJ n�o � v�lido.

function Cnpj(xCNPJ: String):Boolean;
Var
  d1,d4,xx,nCount,fator,resto,digito1,digito2 : Integer;
   Check : String;
begin
d1 := 0;
d4 := 0;
xx := 1;
if Trim(xCNPJ) = '' then
  Exit;

for nCount := 1 to Length( xCNPJ )-2 do
    begin
    if Pos( Copy( xCNPJ, nCount, 1 ), '/-.' ) = 0 then
    begin
    if xx < 5 then
    begin
    fator := 6 - xx;
    end
    else
   begin
   fator := 14 - xx;
   end;
   d1 := d1 + StrToInt( Copy( xCNPJ, nCount, 1 ) ) * fator;
   if xx < 6 then
    begin
    fator := 7 - xx;
   end
   else
   begin
   fator := 15 - xx;    end;
   d4 := d4 + StrToInt( Copy( xCNPJ, nCount, 1 ) ) * fator;
   xx := xx+1;
   end;
   end;
   resto := (d1 mod 11);
   if resto < 2 then
   begin
   digito1 := 0;
   end
   else
   begin
   digito1 := 11 - resto;
   end;
   d4 := d4 + 2 * digito1;
   resto := (d4 mod 11);
   if resto < 2 then
    begin
    digito2 := 0;
   end
   else
    begin
    digito2 := 11 - resto;
   end;
    Check := IntToStr(Digito1) + IntToStr(Digito2);
   if Check <> copy(xCNPJ,succ(length(xCNPJ)-2),2) then
    begin
    Result := False;
   end
   else
    begin
    Result := True;
   end;
 end;

 function EspacosDir( S:String;tam:Integer ) : String;
 var i:integer;
 begin
  if tam>length(S)
  then  for i:=1 to tam-length(S)
        do S:=S+' '
  else delete(S,tam,length(S)-tam);
  result:= S
 end;

 function SoCaracteresPermitidos(s:string):string;
 var
  x:Integer;
  sAux:String;
begin
  sAux := '';
  for x := 1 to Length(s) do
    //Aqui eu testo se s�o caracteres alphanum�ricos comuns ou virgula (,)
    if s[x]  in ['a'..'z','A'..'Z', '0'..'9', ',' , ' '] then
      sAux := sAux + s[x];
  result := sAux;
end;

function VersaoExe: String;
type
  PFFI = ^vs_FixedFileInfo;
var
  F       : PFFI;
  Handle  : Dword;
  Len     : Longint;
  Data    : Pchar;
  Buffer  : Pointer;
  Tamanho : Dword;
  Parquivo: Pchar;
  Arquivo : String;
begin
  Arquivo  := Application.ExeName;
  Parquivo := StrAlloc(Length(Arquivo) + 1);
  StrPcopy(Parquivo, Arquivo);
  Len := GetFileVersionInfoSize(Parquivo, Handle);
  Result := '';
  if Len > 0 then
  begin
     Data:=StrAlloc(Len+1);
     if GetFileVersionInfo(Parquivo,Handle,Len,Data) then
     begin
        VerQueryValue(Data, '\',Buffer,Tamanho);
        F := PFFI(Buffer);
        Result := Format('%d.%d.%d.%d',
                         [HiWord(F^.dwFileVersionMs),
                          LoWord(F^.dwFileVersionMs),
                          HiWord(F^.dwFileVersionLs),
                          Loword(F^.dwFileVersionLs)]
                        );
     end;
     StrDispose(Data);
  end;
  StrDispose(Parquivo);
end;

procedure CarregaRelatorio(NomeRel:String;Parametros,Valores:Array of String; SoImprime :Boolean = False);
var
  MS :TMemoryStream;
  qAux :TSQLQuery;
  i,x,y:integer;
  frDataSet : TfrxDBXQuery;
begin
   if dmGeradorRelatorios = nil then
    Application.CreateForm(TdmGeradorRelatorios,dmGeradorRelatorios);

   CriaQuery(qAux);
   qAux.SQL.Add(' SELECT LAYOUT FROM CRMREL WHERE NOME = :NOME ');
   qAux.Params[0].AsString := NomeRel;
   qAux.Open;

   if not qAux.IsEmpty then
   begin
     MS := TMemoryStream.Create;
     TBlobField(qAux.Fields[0]).SaveToStream(MS);
     MS.Position := 0;

     With dmGeradorRelatorios do
     begin
       frxReport1.LoadFromStream(MS);

       if High(Parametros) > 0 then
       begin
         for i:= 0 to frxReport1.DataSets.Count - 1 do
         begin
           if frxReport1.DataSets[i].DataSet is TfrxDBXQuery then
           begin
             frDataSet := TfrxDBXQuery( frxReport1.DataSets[i].DataSet );

             for x := 0 to frDataSet.Params.Count -1 do
             begin
               for y := 0 to High(Parametros) do
               begin
                  if frDataSet.Params[x].Name = Parametros[y] then
                    frDataSet.Params[x].Value := Valores[y]
               end;
             end;
           end;
         end;
       end;

       dmPrincipal.Conn.CloseDataSets;

       if SoImprime then
       begin
        frxReport1.PrepareReport;
        frxReport1.Print;
       end
       else
       begin
         frxReport1.ShowReport;
         frxReport1.clear;
       end;
     end;
     MS.Free;
   end
   else
     Application.MessageBox(PCHAR('Relat�rio ' + NomeRel + ' n�o Encontrado.'),'SANKHYA', mb_ok + mb_iconinformation);
   FreeAndNil(qAux);
end;

function CarregaSetor :String;
var arq :TextFile;
    linha :string;
begin
  AssignFile(arq,ExtractFilePath(Application.ExeName) + 'SETOR.TXT');
  Reset(arq);
  Readln(arq,linha);

  Result := linha;
  CloseFile(arq);
end;

procedure AtualizaStatusPedido(NuNota :Integer;Status:String);
var
 sSQL :String;
begin
  sSQL := 'UPDATE TGFCAB SET AD_STATUS_DISTRIBUICAO = ' + QuotedStr(Status) +
          ' WHERE NUNOTA = ' + IntToStr(Nunota) + ' AND CODEMP = ' + IntToStr(fGetColigada);

 // SQLExecute(sSQL);
end;

{$ENDREGION Uteis}

{$REGION 'Seguranca'}
Function Cripto(Texto :String):String;
begin
  With dmPrincipal do
  begin
    Cipher.Key := CiPherKey;
    Cipher.Decoded := Texto;
    Result := Cipher.Encoded;
    //
    Cipher.Decoded := '';
    Cipher.Encoded := '';
  end;
end;

Function DeCripto(Texto :String):String;
begin
  With dmPrincipal do
  begin
    Cipher.Key := CiPherKey;
    Cipher.Encoded := Texto;
    Result := Cipher.Decoded;
    //
    Cipher.Decoded := '';
    Cipher.Encoded := '';
  end;
end;
{$ENDREGION Seguranca}

{$REGION 'Arrays'}

function ArrayItens(cString, cSeparador: string; iIndice: Integer): string;
var
   cItem: string;
   aStringPicada: array of string;
   cAbre, cFecha: string[1];
   iQuantidade, iComeco, iFim, iTamanho, iCont: Integer;
begin
   Result := '';
   if (Length(cSeparador) = 2) and (EstaContido(cSeparador, '[]{}()<>��')) then
   begin
      cAbre := Copy(cSeparador, 1, 1);
      cFecha := Copy(cSeparador, 2, 1);
      iQuantidade := ArrayCont(cString, cSeparador);

      // Se o Item solicitado for menor que a quantidade, procura
      if iIndice <= iQuantidade then
      begin
         for iCont := 1 to iQuantidade do
         begin
            iComeco := Pos(cAbre, cString);
            iFim := Pos(cFecha, cString);
            iTamanho := iFim - iComeco;
            cItem := Copy(cString, iComeco + 1, iTamanho - 1);
            SetLength(aStringPicada, iCont);
            aStringPicada[iCont - 1] := cItem;
            Delete(cString, iComeco, iTamanho + 1);
         end;
         // Para nao acessar uma posicao -1 no array
         if iIndice - 1 = -1 then
            Result := ''
         else
            Result := aStringPicada[iIndice - 1];
      end;
      // Se o Item solicitado for maior que a quantidade, sai sem procurar
   end
   else
   begin
      cString := '�' + StringReplace(cString, cSeparador, '��', [rfReplaceAll, rfIgnoreCase]) + '�';
      Result := ArrayItens(cString, '��', iIndice);
   end;
end;


function ArrayCont(cString, cSeparador: string): Integer;
begin
  Result := Length(cString) - Length(StringReplace(cString, Copy(cSeparador, 1, 1),'', [rfReplaceAll, rfIgnoreCase]));

  if (Length(cSeparador) = 1) and (cString <> '') then
  begin
    Result := Result + 1;
  end;
end;




function ArrayLocateItem(cString, cItem, cSeparador: string): Integer;
var
  iInd,
  iCount : integer;
begin
  // In�cio - Criado por: Eduardo Deperon Afonso - 25-09-2009 - In�cio
  cString := UpperCase(cString);
  cItem   := UpperCase(cItem);
  // Fim - Criado por: Eduardo Deperon Afonso - 25-09-2009 - Fim

  // Esta funcao ignora espa�os entre os separadores
  Result := 0;
  iCount := ArrayCont(cString, cSeparador);

  for iInd := 1 to iCount do
  begin
    if AnsiMatchText(Trim(cItem), [Trim(ArrayItens(cString, cSeparador, iInd))]) then
    begin
      Result := iInd;
      Exit;
    end;
  end;
end;



{$ENDREGION 'Arrays'}


(*
procedure CFSaveGridViewFilterLayoutToTable(//AName : string;
                                            //ASaveToField : TField;
                                            AGridView : TcxCustomGridView);
begin
  if Assigned(AGridView) then
  begin
    //if Assigned(ASaveToField.Dataset) then
    begin
      msStream := TStringStream.Create(EmptyStr);
      try
        AGridView.DataController.Filter.SaveToStream(msStream);
        ShowMessage(msStream.DataString);
        msStream.Seek(0,sofrombeginning);
        {with TDataset(ASaveToField.DataSet) do
        begin
          Edit;
          ASaveToField.Value := msStream.DataString;
          Post;
        end;}

      finally
        //msStream.Free;
      end;
    end;
  end;
end;

procedure CFLoadGridViewFilterLayoutFromTable(//AName : string;
                                              //ALoadFromField : TField;
                                              AGridView : TcxCustomGridView);
begin
  if Assigned(AGridView) then
  begin
    //if Assigned(ALoadFromField.Dataset) then
    begin
      //msStream := TStringStream.Create(ALoadFromField.AsString);
//ShowMessage(msStream.DataString);
      try
        msStream.Seek(0,sofrombeginning);
        AGridView.DataController.Filter.LoadFromStream(msStream);
        AGridView.DataController.Filter.Active := True;
      finally
        //msStream.Free;
      end;
    end;
  end;
end;
*)

{$REGION 'Gets'}
Function fGetColigada:Integer;
begin
  result := dmPrincipal.cdsEmpresa.FieldByName('CODEMP').AsInteger;
end;

Function fGetUsuario:Integer;
begin
  result := dmPrincipal.cdsUsuario.FieldByName('IDCRMUSUARIO').AsInteger;
end;

Function fGetNomeColigada:String;
begin
  result := dmPrincipal.cdsEmpresa.FieldByName('NOMEFANTASIA').AsString;
end;

Function fGetNomeUsuario:String;
begin
  result := dmPrincipal.cdsUsuario.FieldByName('USERNAME').AsString;
end;


{$ENDREGION 'Gets'}


{$REGION 'JvTreeView Manipulacao'}
Procedure pExpandTree(var tree :TJvTreeView);
var
  i :integer;
begin
  for I := 0 to tree.Items.Count - 1 do
    tree.Items.Item[i].Expanded := true;
end;

Procedure pColapseTree(var tree :TJvTreeView);
var
  i :integer;
begin
  for I := 0 to tree.Items.Count - 1 do
    tree.Items.Item[i].Expanded := false;
end;
{$ENDREGION 'JvTreeView Manipulacao'}

procedure InsertMov(Tabela : string; sCampos : String; sValores : Variant );
var qInsere : TSQLQuery;
    aux : string;
    i, j, maxTam : Integer;
    FDesc : Array[0..1000] of TFieldDesc;
begin
 //FDesc := Nil;
 if sCampos <> '' then maxTam := 1 else maxTam := 0;
 for i := 1 to Length(sCampos) do if sCampos[i] = ';' then inc(maxTam);

 for i := 0  to maxTam - 1 do begin
   if pos(';',sCampos) > 0 then
     j := pos(';',sCampos) - 1
   else
     j := Length(sCampos);

   FDesc[i].FName := Copy(sCampos,1,j);
   Delete(sCampos,1,j+1);
   if VarIsArray(sValores) then FDesc[i].FValue := QuotedStr(sValores[i]);
 end;

 CriaQuery(qInsere);
 qInsere.SQL.Text := 'INSERT INTO ' + Tabela +' ( ';

 for i:=0 to maxTam - 1 do
 begin
   if i = 0 then
      qInsere.SQL.Text := qInsere.SQL.Text + FDesc[i].FName
   else
      qInsere.SQL.Text := qInsere.SQL.Text + ', ' + FDesc[i].FName;
 end;

 qInsere.SQL.Text := qInsere.SQL.Text + ') VALUES ( ';

 for i:=0 to maxTam - 1 do
 begin
  if i = 0 then
  begin
     aux := ifthen( FDesc[i].FValue='''''','NULL',ifthen( FDesc[i].FValue='''','NULL',FDesc[i].FValue));
     if (pos('TO_CHAR', aux) > 0 ) or (pos('TO_DATE', aux) > 0) then
        qInsere.SQL.Text := qInsere.SQL.Text + StringReplace ( copy (aux, 2, length(aux) -2), '''''', '''', [rfReplaceAll, rfIgnoreCase] )
     else
        qInsere.SQL.Text := qInsere.SQL.Text + aux;
  end
  else
  begin
     aux := ifthen( FDesc[i].FValue='''''','NULL',ifthen( FDesc[i].FValue='''','NULL',FDesc[i].FValue));
     if (pos('TO_CHAR', aux) > 0 ) or (pos('TO_DATE', aux) > 0) then
        qInsere.SQL.Text := qInsere.SQL.Text + ', ' + StringReplace ( copy (aux, 2, length(aux) -2), '''''', '''', [rfReplaceAll, rfIgnoreCase] )
     else
        qInsere.SQL.Text := qInsere.SQL.Text + ', ' + aux;
   end;
 end;

 qInsere.SQL.Text := qInsere.SQL.Text + ')';
 qInsere.SQL.SaveToFile( ExtractFilePath(Application.ExeName) + fgetnomeusuario + 'CRMi.sql');
 qInsere.ExecSQL;
end;




end.
