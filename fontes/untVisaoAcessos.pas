unit untVisaoAcessos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untBaseVisao, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls,
  ComCtrls, dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons, JvLabel,
  ExtCtrls, Grids, DBGrids, dxSkinsCore,dxSkinTheAsphaltWorld, dxSkinscxPCPainter,
  dxSkiniMaginary, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmVisaoAcessos = class(TfrmBaseVisao)
    cxGrid1DBTableView1CODIGO: TcxGridDBColumn;
    cxGrid1DBTableView1NOME: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEUSU: TcxGridDBColumn;
    procedure cxGrid1DBTableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure pSetaDireitos; override;
    //
    procedure pNovo; override;
    procedure pEditar; override;
    procedure pExcluir; override;
    procedure pRefresh; override;
    //--
    Procedure pAbrirTela;
  end;

var
  frmVisaoAcessos: TfrmVisaoAcessos;

implementation

uses udmAcessos, untCadAcessos, untFuncoes, udmPrincipal, untPrincipal;

{$R *.dfm}

procedure TfrmVisaoAcessos.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
pAbrirTela;
end;

procedure TfrmVisaoAcessos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if frmCadAcessos <> nil then
    frmCadAcessos.Close;
  frmVisaoAcessos := nil;
  dmAcessos.Free;
end;

procedure TfrmVisaoAcessos.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmAcessos, dmAcessos);
  cdsPad := dmAcessos.cdsAcessos;
  sqlPad := dmAcessos.sqlAcessos;
  Tabela := 'CRMACESSO';
end;

procedure TfrmVisaoAcessos.FormShow(Sender: TObject);
begin
  inherited;
  pRefresh;
  btnFiltrar.Visible := false;
  dmAcessos.cdsUsu.Open;
end;

procedure TfrmVisaoAcessos.pAbrirTela;
begin
  if frmCadAcessos <> nil then
    frmCadAcessos.Show
  else
  begin
    Application.CreateForm(TfrmCadAcessos, frmCadAcessos);
    frmCadAcessos.Show;
  end;
end;

procedure TfrmVisaoAcessos.pEditar;
begin
  inherited;
  if (dmAcessos.cdsAcessos.RecNo <= 0) then
    Exit;

  pAbrirTela;
  if dmAcessos.cdsAcessos.State <> dsBrowse then
    frmCadAcessos.btnCancelarClick(self);
  //frmCadAcessos.btnEditarClick(self);
end;

procedure TfrmVisaoAcessos.pExcluir;
var
  txt :string;
begin
  inherited;
  with dmAcessos.cdsAcessos do
  begin
    Try
      txt := 'delete '+
               'from CRMACESSO '+
              'where CODCOLIGADA = '+inttostr(fGetColigada)+' '+
                'and IDCRMUSUARIO = '+dmAcessos.cdsAcessosIDCRMUSUARIO.AsString;
      //
      pExec(txt);
    Except
    End;
    //Delete;
    //if ApplyUpdates(0) > 0 then
    //  raise Exception.Create('Erro ao Excluir Registro.');
  end;
end;

procedure TfrmVisaoAcessos.pNovo;
begin
  inherited;
  pAbrirTela;
  if dmAcessos.cdsAcessos.State <> dsBrowse then
    frmCadAcessos.btnCancelarClick(self);
  //frmCadAcessos.btnAdicionarClick(self);
end;

procedure TfrmVisaoAcessos.pRefresh;
var
  chv, chv2 :Integer;
begin
  inherited;
  chv := -1;
  chv2 := -1;
  with dmAcessos.cdsAcessos do
  begin
    if Active then
    begin
      chv := FieldByName('IDCRMUSUARIO').AsInteger;
      chv2 := FieldByName('IDCRMPERFIL').AsInteger;
    end;

    Close;
    Params.ParamByName('PCOLIG').AsInteger := fGetColigada;
    Open;
    if (chv > -1) then
      Locate('IDCRMUSUARIO;IDCRMPERFIL', VarArrayOf([chv,chv2]), []);
  end;
  pRegistros;
end;

procedure TfrmVisaoAcessos.pSetaDireitos;
begin
  inherited;
  btnAdicionar.Enabled := frmprincipal.A00305.Enabled;
  btnEditar.Enabled := frmprincipal.A00305.Enabled;
  dmAcessos.dtsAcess.AutoEdit := frmprincipal.A00305.Enabled;
  btnExcluir.Enabled := frmprincipal.A00305.Enabled;
  panel1.Enabled := frmprincipal.A00305.Enabled;
end;

end.
