unit untBase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls;

type
  TfrmBase = class(TForm)
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBase: TfrmBase;

implementation

uses untFuncoes,udmPrincipal, untPrincipal;

{$R *.dfm}

procedure TfrmBase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  action := cafree;
end;

procedure TfrmBase.FormCreate(Sender: TObject);
begin
  self.KeyPreview := True;
  StatusBar1.Panels[0].Text := 'Registros: 0/0';
end;

procedure TfrmBase.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if (Key = #13) and (dmPrincipal.cdsParametros.FieldByName('ENTERCOMOTAB').Value = 1) and
  (not frmPrincipal.flag_comp_enter) then
  begin
    Key := #0;
    try
     Perform(Wm_NextDlgCtl,0,0);
    Except
    end;
  end;

  if (key = #27) and not ( frmPrincipal.flag_comp ) then
     Close;

  frmPrincipal.flag_comp_Enter := False;
end;

procedure TfrmBase.FormPaint(Sender: TObject);
begin
//  self.Top := fAjustaTop(self.Height);
end;
end.



