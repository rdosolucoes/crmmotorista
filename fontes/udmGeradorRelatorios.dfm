object dmGeradorRelatorios: TdmGeradorRelatorios
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 317
  Width = 811
  object DsPasta: TDataSource
    DataSet = cdsPasta
    Left = 96
    Top = 72
  end
  object dspPasta: TDataSetProvider
    DataSet = sqlPasta
    BeforeApplyUpdates = dspPastaBeforeApplyUpdates
    Left = 96
    Top = 16
  end
  object DsRel: TDataSource
    DataSet = cdsRel
    Left = 240
    Top = 72
  end
  object sqlRel: TSQLDataSet
    SchemaName = 'rm'
    CommandText = 'SELECT * FROM CRMREL'#13#10'ORDER BY NOME'
    MaxBlobSize = 1
    Params = <>
    SQLConnection = dmPrincipal.Conn
    Left = 160
    Top = 16
  end
  object cdsRel: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRel'
    AfterPost = cdsRelAfterPost
    AfterDelete = cdsRelAfterPost
    OnNewRecord = cdsRelNewRecord
    Left = 160
    Top = 72
    object cdsRelIDREL: TFMTBCDField
      FieldName = 'IDREL'
      Precision = 32
    end
    object cdsRelIDPASTA: TFMTBCDField
      FieldName = 'IDPASTA'
      Precision = 32
    end
    object cdsRelNOME: TWideStringField
      FieldName = 'NOME'
      Size = 40
    end
    object cdsRelLAYOUT: TMemoField
      FieldName = 'LAYOUT'
      BlobType = ftOraClob
      Size = 1
    end
  end
  object dspRel: TDataSetProvider
    DataSet = sqlRel
    BeforeApplyUpdates = dspRelBeforeApplyUpdates
    Left = 240
    Top = 16
  end
  object cdsPasta: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspPasta'
    AfterPost = cdsPastaAfterPost
    AfterDelete = cdsPastaAfterPost
    OnNewRecord = cdsPastaNewRecord
    Left = 24
    Top = 72
    object cdsPastaIDPASTA: TFMTBCDField
      FieldName = 'IDPASTA'
      Precision = 32
    end
    object cdsPastaNOME: TWideStringField
      FieldName = 'NOME'
      Size = 30
    end
  end
  object sqlPasta: TSQLDataSet
    SchemaName = 'rm'
    CommandText = 'SELECT * FROM CRMPASTA'#13#10'ORDER BY IDPASTA'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = dmPrincipal.Conn
    Left = 24
    Top = 16
  end
  object frxReport1: TfrxReport
    Version = '6.3.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41332.423439780090000000
    ReportOptions.LastChange = 41332.423439780090000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 16
    Top = 136
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
    end
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 86
    Top = 136
  end
  object frxDialogControls1: TfrxDialogControls
    Left = 166
    Top = 136
  end
  object frxDBXComponents1: TfrxDBXComponents
    DefaultDatabase = dmPrincipal.Conn
    Left = 262
    Top = 136
  end
  object frxRichObject1: TfrxRichObject
    Left = 352
    Top = 136
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 16
    Top = 192
  end
  object frxOLEObject1: TfrxOLEObject
    Left = 88
    Top = 192
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    PDFStandard = psNone
    PDFVersion = pv17
    Left = 184
    Top = 200
  end
  object frxMailExport1: TfrxMailExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ShowExportDialog = True
    SmtpPort = 25
    UseIniFile = True
    TimeOut = 60
    ConfurmReading = False
    UseMAPI = SMTP
    MAPISendFlag = 0
    Left = 280
    Top = 200
  end
  object sdsCliente: TSimpleDataSet
    Aggregates = <>
    Connection = dmPrincipal.Conn
    DataSet.CommandText = 
      'SELECT CODPARC,NOMEPARC FROM TGFPAR'#13#10'WHERE ATIVO = '#39'S'#39#13#10'AND CLIE' +
      'NTE = '#39'S'#39#13#10'ORDER BY NOMEPARC'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 304
    Top = 16
    object sdsClienteCODPARC: TFMTBCDField
      FieldName = 'CODPARC'
      Required = True
      Precision = 10
      Size = 0
    end
    object sdsClienteNOMEPARC: TWideStringField
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
  end
  object frxDBCliente: TfrxDBDataset
    UserName = 'frxDBCliente'
    CloseDataSource = False
    DataSet = sdsCliente
    BCDToCurrency = False
    Left = 304
    Top = 64
  end
  object sdsUsuario: TSimpleDataSet
    Aggregates = <>
    Connection = dmPrincipal.Conn
    DataSet.CommandText = 'SELECT USERNAME FROM CRMUSUARIO'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 384
    Top = 16
    object sdsUsuarioUSERNAME: TWideStringField
      FieldName = 'USERNAME'
      Size = 15
    end
  end
  object frxDBUsuario: TfrxDBDataset
    UserName = 'frxDBUsuario'
    CloseDataSource = False
    DataSet = sdsUsuario
    BCDToCurrency = False
    Left = 384
    Top = 64
  end
  object sdsEntregador: TSimpleDataSet
    Aggregates = <>
    Connection = dmPrincipal.Conn
    DataSet.CommandText = 
      'SELECT CODPARC, NOMEPARC FROM TGFPAR WHERE '#13#10'(AD_MOTOBOY = '#39'S'#39' O' +
      'R MOTORISTA = '#39'S'#39')'#13#10'AND ATIVO = '#39'S'#39#13#10'ORDER BY NOMEPARC'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 464
    Top = 16
    object FMTBCDField1: TFMTBCDField
      FieldName = 'CODPARC'
      Required = True
      Precision = 10
      Size = 0
    end
    object WideStringField1: TWideStringField
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
  end
  object frxDBEntregador: TfrxDBDataset
    UserName = 'frxDBEntregador'
    CloseDataSource = False
    DataSet = sdsEntregador
    BCDToCurrency = False
    Left = 464
    Top = 64
  end
  object sdsMotorista: TSimpleDataSet
    Aggregates = <>
    Connection = dmPrincipal.Conn
    DataSet.CommandText = 
      'SELECT CODPARC, NOMEPARC FROM TGFPAR '#13#10'WHERE MOTORISTA = '#39'S'#39' AND' +
      ' ATIVO = '#39'S'#39#13#10'ORDER BY NOMEPARC'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 552
    Top = 16
    object FMTBCDField2: TFMTBCDField
      FieldName = 'CODPARC'
      Required = True
      Precision = 10
      Size = 0
    end
    object WideStringField2: TWideStringField
      FieldName = 'NOMEPARC'
      Required = True
      Size = 40
    end
  end
  object frxDBMotorista: TfrxDBDataset
    UserName = 'frxDBMotorista'
    CloseDataSource = False
    DataSet = sdsMotorista
    BCDToCurrency = False
    Left = 552
    Top = 64
  end
  object sdsEmpresa: TSimpleDataSet
    Aggregates = <>
    Connection = dmPrincipal.Conn
    DataSet.CommandText = 
      'SELECT EMP1.CODEMP,EMP1.NOMEFANTASIA,EMP1.RAZAOSOCIAL'#13#10'FROM TGFE' +
      'MP EMP, TSIEMP EMP1'#13#10'WHERE EMP.CODEMP = EMP1.CODEMP'#13#10'ORDER BY EM' +
      'P1.CODEMP'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 464
    Top = 136
    object sdsEmpresaCODEMP: TFMTBCDField
      FieldName = 'CODEMP'
      Required = True
      Precision = 5
      Size = 0
    end
    object sdsEmpresaNOMEFANTASIA: TWideStringField
      FieldName = 'NOMEFANTASIA'
      Size = 40
    end
    object sdsEmpresaRAZAOSOCIAL: TWideStringField
      FieldName = 'RAZAOSOCIAL'
      Size = 40
    end
  end
  object frxDBEmpresa: TfrxDBDataset
    UserName = 'frxDBEmpresa'
    CloseDataSource = False
    DataSet = sdsEmpresa
    BCDToCurrency = False
    Left = 464
    Top = 192
  end
end
