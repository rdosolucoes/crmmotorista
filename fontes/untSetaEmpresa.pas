unit untSetaEmpresa;

interface

uses Classes, Controls, ComCtrls, Forms, StdCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, dxGDIPlusClasses, JvExControls,
  JvXPCore, JvXPButtons, ExtCtrls, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxSkinsCore,windows,dxSkinTheAsphaltWorld, dxSkinscxPCPainter,Data.SqlExpr,SysUtils,
  dxSkiniMaginary, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmSetaEmpresa = class(TForm)
    Label1: TLabel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    PageControl1: TPageControl;
    tbs2: TTabSheet;
    Panel2: TPanel;
    Label3: TLabel;
    btnConfirmar: TJvXPButton;
    btnCancelar: TJvXPButton;
    cxGrid1DBTableView1CODCOLIGADA: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEFANTASIA: TcxGridDBColumn;
    cxGrid1DBTableView1NOME: TcxGridDBColumn;
    cxGrid1DBTableView1CGC: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    podefechar: boolean;
  public
    { Public declarations }
  end;

var
  frmSetaEmpresa: TfrmSetaEmpresa;

implementation

{$R *.dfm}

uses udmPrincipal, untPrincipal,untFuncoes;

procedure TfrmSetaEmpresa.btnCancelarClick(Sender: TObject);
begin
  podefechar := True;
  if not(frmPrincipal.trocandoEmpresa) then
    Application.Terminate
  else
  begin
    self.Close;
    frmPrincipal.pSetaAmbiente;
  end;
end;

procedure TfrmSetaEmpresa.btnConfirmarClick(Sender: TObject);
begin
  podefechar := True;
  self.Close;
  frmPrincipal.pSetaAmbiente;
end;

procedure TfrmSetaEmpresa.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  btnConfirmarClick(self);
end;

procedure TfrmSetaEmpresa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if podefechar then
  begin
    Action := caFree;
    frmSetaEmpresa := nil;
  end
  else
    Action := caNone;
end;

procedure TfrmSetaEmpresa.FormCreate(Sender: TObject);
begin
  podefechar := false;
  with dmPrincipal do
  begin
    cdsEmpresa.Close;
    cdsEmpresa.Open;
  end;
end;

procedure TfrmSetaEmpresa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_return then
    btnConfirmarClick(nil);
end;

end.
