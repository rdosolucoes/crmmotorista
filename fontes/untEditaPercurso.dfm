inherited frmEditaPercurso: TfrmEditaPercurso
  Caption = 'Edi'#231#227'o de Percurso'
  ClientHeight = 150
  ClientWidth = 386
  ExplicitWidth = 392
  ExplicitHeight = 179
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 122
    Top = 15
    Width = 56
    Height = 13
    Caption = 'Hora Sa'#237'da:'
  end
  object Label3: TLabel [1]
    Left = 249
    Top = 14
    Width = 47
    Height = 13
    Caption = 'KM Sa'#237'da:'
  end
  object Label5: TLabel [2]
    Left = 122
    Top = 72
    Width = 73
    Height = 13
    Caption = 'Hora Chegada:'
  end
  object Label6: TLabel [3]
    Left = 249
    Top = 72
    Width = 64
    Height = 13
    Caption = 'KM Chegada:'
  end
  inherited StatusBar1: TStatusBar
    Top = 131
    Width = 386
    ExplicitTop = 139
    ExplicitWidth = 506
  end
  inherited pnlBotoes: TPanel
    Height = 131
    ExplicitHeight = 139
    inherited PageControl1: TPageControl
      Height = 131
      ExplicitHeight = 139
      inherited tbs2: TTabSheet
        ExplicitLeft = 4
        ExplicitTop = 6
        ExplicitWidth = 108
        ExplicitHeight = 129
        inherited Panel2: TPanel
          Height = 121
          ExplicitHeight = 129
        end
      end
    end
  end
  object dbkmsaida: TDBEdit
    Left = 249
    Top = 30
    Width = 121
    Height = 21
    DataField = 'KMSAIDA'
    DataSource = dmControleMotoboy.dsPercurso
    TabOrder = 2
  end
  object dbhorasaida: TcxDBTimeEdit
    Left = 122
    Top = 30
    DataBinding.DataField = 'HORASAIDA'
    DataBinding.DataSource = dmControleMotoboy.dsPercurso
    TabOrder = 3
    Width = 121
  end
  object dbkmchegada: TDBEdit
    Left = 249
    Top = 88
    Width = 121
    Height = 21
    DataField = 'KMCHEGADA'
    DataSource = dmControleMotoboy.dsPercurso
    TabOrder = 4
  end
  object dbhorachegada: TcxDBTimeEdit
    Left = 122
    Top = 88
    DataBinding.DataField = 'HORACHEGADA'
    DataBinding.DataSource = dmControleMotoboy.dsPercurso
    TabOrder = 5
    Width = 121
  end
end
