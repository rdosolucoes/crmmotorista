unit udmUsuarios;

interface

uses
  SysUtils, Classes, FMTBcd, DB, SqlExpr, DBClient, Provider, SimpleDS,
  Data.DBXOracle;

type
  TdmUsuarios = class(TDataModule)
    dspUsu: TDataSetProvider;
    cdsUsu: TClientDataSet;
    sqlUsu: TSQLDataSet;
    dtsUsu: TDataSource;
    sqlUsuIDCRMUSUARIO: TFMTBCDField;
    sqlUsuNOME: TWideStringField;
    sqlUsuUSERNAME: TWideStringField;
    sqlUsuSENHA: TWideStringField;
    sqlUsuEMAIL: TWideStringField;
    sqlUsuATIVO: TWideStringField;
    sqlUsuMASTERUSER: TWideStringField;
    sqlUsuTELEFONE: TWideStringField;
    sqlUsuCELULAR: TWideStringField;
    cdsUsuIDCRMUSUARIO: TFMTBCDField;
    cdsUsuNOME: TWideStringField;
    cdsUsuUSERNAME: TWideStringField;
    cdsUsuSENHA: TWideStringField;
    cdsUsuEMAIL: TWideStringField;
    cdsUsuATIVO: TWideStringField;
    cdsUsuMASTERUSER: TWideStringField;
    cdsUsuTELEFONE: TWideStringField;
    cdsUsuCELULAR: TWideStringField;
    sqlEmp: TSimpleDataSet;
    sqlEmpCODEMP: TFMTBCDField;
    sqlEmpNOMEFANTASIA: TWideStringField;
    sqlEmpRAZAOSOCIAL: TWideStringField;
    sqlUsuCODCOLIGADA: TFMTBCDField;
    cdsUsuCODCOLIGADA: TFMTBCDField;
    dtsEmp: TDataSource;
    sdsMotorista: TSimpleDataSet;
    dsMotorista: TDataSource;
    sqlUsuCODPARC: TFMTBCDField;
    cdsUsuCODPARC: TFMTBCDField;
    sqlUsuPERMITEINCPEDIDO: TFMTBCDField;
    cdsUsuPERMITEINCPEDIDO: TFMTBCDField;
    procedure cdsUsuAfterScroll(DataSet: TDataSet);
    procedure cdsUsuBeforePost(DataSet: TDataSet);
    procedure cdsUsuNewRecord(DataSet: TDataSet);
    procedure dtsUsuStateChange(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmUsuarios: TdmUsuarios;

implementation

uses udmPrincipal, untCadUsuarios, untFuncoes;

{$R *.dfm}

procedure TdmUsuarios.cdsUsuAfterScroll(DataSet: TDataSet);
begin
  if (frmCadUsuarios <> nil) then
    if (cdsUsu.FieldByName('IDCRMUSUARIO').AsInteger > 0) then
    begin
      frmcadUsuarios.edtSenha2.Text := frmCadUsuarios.edtSenha.Text;
      frmCadUsuarios.edtSenha.Enabled := false;
      frmCadUsuarios.edtSenha2.Enabled := false;
    end
    else
    begin
      frmCadUsuarios.edtSenha.Enabled := true;
      frmCadUsuarios.edtSenha2.Enabled := true;
    end;
end;

procedure TdmUsuarios.cdsUsuBeforePost(DataSet: TDataSet);
begin
  if (cdsUsu.FieldByName('IDCRMUSUARIO').IsNull) then
  begin
    cdsUsu.FieldByName('IDCRMUSUARIO').AsInteger := GeraCodigo('IDCRMUSUARIO');
    cdsUsu.FieldByName('SENHA').AsString := Cripto(cdsUsu.FieldByName('SENHA').AsString);
  end;
end;

procedure TdmUsuarios.cdsUsuNewRecord(DataSet: TDataSet);
begin
  cdsUsu.FieldByName('ATIVO').AsString := 'S';
  cdsUsu.FieldByName('MASTERUSER').AsString := 'N';
end;

procedure TdmUsuarios.DataModuleCreate(Sender: TObject);
begin
  sqlEmp.Close;
  sqlEmp.Open;

  sdsMotorista.Close;
  sdsMotorista.Open;
end;

procedure TdmUsuarios.dtsUsuStateChange(Sender: TObject);
begin
  if dtsUsu.State = dsEdit then
    if (frmCadUsuarios <> nil) then
      frmCadUsuarios.PageControl1.ActivePageIndex := 1;
end;

end.
