object dmPerfil: TdmPerfil
  OldCreateOrder = False
  Height = 164
  Width = 275
  object dspPerfil: TDataSetProvider
    DataSet = sqlPerfil
    Left = 86
    Top = 16
  end
  object cdsPerfil: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'PCOLIGADA'
        ParamType = ptInput
      end>
    ProviderName = 'dspPerfil'
    BeforePost = cdsPerfilBeforePost
    AfterScroll = cdsPerfilAfterScroll
    OnNewRecord = cdsPerfilNewRecord
    Left = 139
    Top = 16
    object cdsPerfilIDCRMPERFIL: TFMTBCDField
      DisplayLabel = 'ID'
      FieldName = 'IDCRMPERFIL'
      Required = True
      Precision = 32
    end
    object cdsPerfilCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      Required = True
      Precision = 32
    end
    object cdsPerfilCODPERFIL: TWideStringField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'CODPERFIL'
      Size = 10
    end
    object cdsPerfilNOME: TWideStringField
      Tag = 1
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'NOME'
      Size = 50
    end
    object cdsPerfilMENUS: TMemoField
      Tag = 1
      DisplayLabel = 'Menus'
      FieldName = 'MENUS'
      BlobType = ftOraClob
      Size = 1
    end
  end
  object sqlPerfil: TSQLDataSet
    SchemaName = 'dbo'
    CommandText = 
      'SELECT'#13#10'       IDCRMPERFIL,'#13#10'       CODCOLIGADA,'#13#10'       CODPERF' +
      'IL,'#13#10'       NOME,'#13#10'       MENUS'#13#10'  FROM CRMPERFIL '#13#10' WHERE (CODC' +
      'OLIGADA = :PCOLIGADA OR CODCOLIGADA = 0 )'
    MaxBlobSize = 1
    Params = <
      item
        DataType = ftInteger
        Name = 'PCOLIGADA'
        ParamType = ptInput
      end>
    SQLConnection = dmPrincipal.Conn
    Left = 35
    Top = 16
    object sqlPerfilIDCRMPERFIL: TFMTBCDField
      FieldName = 'IDCRMPERFIL'
      Required = True
      Precision = 32
    end
    object sqlPerfilCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      Required = True
      Precision = 32
    end
    object sqlPerfilCODPERFIL: TWideStringField
      FieldName = 'CODPERFIL'
      Size = 10
    end
    object sqlPerfilNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object sqlPerfilMENUS: TMemoField
      FieldName = 'MENUS'
      BlobType = ftOraClob
      Size = 1
    end
  end
  object dtsPerfil: TDataSource
    DataSet = cdsPerfil
    OnStateChange = dtsPerfilStateChange
    Left = 191
    Top = 16
  end
  object cdsAcoes: TClientDataSet
    PersistDataPacket.Data = {
      5D0000009619E0BD0100000018000000030000000000030000005D0005496E64
      6578040001000000000005546578746F01004900000001000557494454480200
      0200320006436F6469676F0100490000000100055749445448020002000A0000
      00}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Index'
        DataType = ftInteger
      end
      item
        Name = 'Texto'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Codigo'
        DataType = ftString
        Size = 10
      end>
    IndexDefs = <
      item
        Name = 'idxcdsAcoesCod'
        Fields = 'CODIGO'
      end>
    IndexName = 'idxcdsAcoesCod'
    Params = <>
    StoreDefs = True
    Left = 139
    Top = 63
    object cdsAcoesIndex: TIntegerField
      FieldName = 'Index'
    end
    object cdsAcoesTexto: TStringField
      FieldName = 'Texto'
      Size = 50
    end
    object cdsAcoescodigo: TStringField
      FieldName = 'Codigo'
      Size = 10
    end
  end
end
