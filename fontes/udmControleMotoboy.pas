unit udmControleMotoboy;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.DB, Data.SqlExpr,
  Datasnap.Provider, Datasnap.DBClient, Vcl.ImgList, Vcl.Controls, cxGraphics,
  System.ImageList, cxImageList;

type
  TdmControleMotoboy = class(TDataModule)
    cdsMotoboy: TClientDataSet;
    dspMotoboy: TDataSetProvider;
    dsMotoboy: TDataSource;
    sqlMotoboy: TSQLDataSet;
    sqlMotoboyCODEMP: TFMTBCDField;
    sqlMotoboyIDCONTROLE: TFMTBCDField;
    sqlMotoboyCODPARC: TFMTBCDField;
    sqlMotoboyNOMEPARC: TWideStringField;
    sqlMotoboyDATA: TSQLTimeStampField;
    sqlMotoboySTATUS: TWideStringField;
    sqlMotoboyORDEM: TFMTBCDField;
    cdsMotoboyCODEMP: TFMTBCDField;
    cdsMotoboyIDCONTROLE: TFMTBCDField;
    cdsMotoboyCODPARC: TFMTBCDField;
    cdsMotoboyNOMEPARC: TWideStringField;
    cdsMotoboyDATA: TSQLTimeStampField;
    cdsMotoboySTATUS: TWideStringField;
    cdsMotoboyORDEM: TFMTBCDField;
    imgList: TcxImageList;
    cdsPercurso: TClientDataSet;
    dsPercurso: TDataSource;
    dspPercurso: TDataSetProvider;
    sqlPercurso: TSQLDataSet;
    sqlPercursoCODEMP: TFMTBCDField;
    sqlPercursoIDCONTROLE: TFMTBCDField;
    sqlPercursoIDPERCURSO: TFMTBCDField;
    sqlPercursoHORASAIDA: TSQLTimeStampField;
    sqlPercursoKMSAIDA: TFMTBCDField;
    sqlPercursoHORACHEGADA: TSQLTimeStampField;
    sqlPercursoKMCHEGADA: TFMTBCDField;
    sqlPercursoIDROMANEIO: TFMTBCDField;
    sqlPercursoTEMPO: TFMTBCDField;
    sqlPercursoPERCURSO: TFMTBCDField;
    cdsPercursoCODEMP: TFMTBCDField;
    cdsPercursoIDCONTROLE: TFMTBCDField;
    cdsPercursoIDPERCURSO: TFMTBCDField;
    cdsPercursoHORASAIDA: TSQLTimeStampField;
    cdsPercursoKMSAIDA: TFMTBCDField;
    cdsPercursoHORACHEGADA: TSQLTimeStampField;
    cdsPercursoKMCHEGADA: TFMTBCDField;
    cdsPercursoIDROMANEIO: TFMTBCDField;
    cdsPercursoTEMPO: TFMTBCDField;
    cdsPercursoPERCURSO: TFMTBCDField;
    sqlPercursoHORASAIDADESC: TWideStringField;
    sqlPercursoHORACHEGADADESC: TWideStringField;
    cdsPercursoHORASAIDADESC: TWideStringField;
    cdsPercursoHORACHEGADADESC: TWideStringField;
    sqlPercursoSTATUS: TWideStringField;
    cdsPercursoSTATUS: TWideStringField;
    sqlPercursoKMCHEGADAAUSENCIA: TFMTBCDField;
    cdsPercursoKMCHEGADAAUSENCIA: TFMTBCDField;
    procedure cdsMotoboyAfterScroll(DataSet: TDataSet);
    function GeraIdPercurso: Integer;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmControleMotoboy: TdmControleMotoboy;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses udmPrincipal,untFuncoes;

{$R *.dfm}

procedure TdmControleMotoboy.cdsMotoboyAfterScroll(DataSet: TDataSet);
begin
  if not dataset.ControlsDisabled then
  begin
    cdsPercurso.Close;
    sqlPercurso.Params[0].AsInteger := cdsMotoboyIDCONTROLE.AsInteger;
    sqlPercurso.Params[1].AsInteger := fgetcoligada;
    cdsPercurso.Open;
  end;
end;



function TdmControleMotoboy.GeraIdPercurso: Integer;
var
  qAux :TSQLQuery;
begin
  CriaQuery(qAux);
  qAux.SQL.Add('SELECT NVL(MAX(IDPERCURSO),0) + 1 FROM CRMPERCURSOMOTOBOY');
  qAux.SQL.Add(' WHERE IDCONTROLE = :IDCONTROLE AND CODEMP = :CODEMP ');
  qAux.Params[0].AsInteger := cdsMotoboyIDCONTROLE.AsInteger;
  qAux.Params[1].AsInteger := fGetColigada;
  qAux.Open;

  Result := qAux.Fields[0].AsInteger;
  FreeAndNil(qAux);
end;

end.
