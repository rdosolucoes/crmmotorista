object dmFiltros: TdmFiltros
  OldCreateOrder = False
  Height = 77
  Width = 282
  object sqlFiltro: TSQLDataSet
    SchemaName = 'rm'
    CommandText = 
      'SELECT * FROM CRMFILTROS WHERE CODCOLIGADA = :CODCOLIGADA'#13#10'AND F' +
      'ORMULARIO = :FORM'#13#10'AND IDUSUARIO = :USUARIO'
    MaxBlobSize = 1
    Params = <
      item
        DataType = ftInteger
        Name = 'CODCOLIGADA'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'FORM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'USUARIO'
        ParamType = ptInput
      end>
    SQLConnection = dmPrincipal.Conn
    Left = 96
    Top = 8
    object sqlFiltroCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 32
    end
    object sqlFiltroIDFILTRO: TFMTBCDField
      FieldName = 'IDFILTRO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 32
    end
    object sqlFiltroID: TFMTBCDField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Precision = 32
    end
    object sqlFiltroDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 60
    end
    object sqlFiltroIDUSUARIO: TFMTBCDField
      FieldName = 'IDUSUARIO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Precision = 32
    end
    object sqlFiltroFORMULARIO: TWideStringField
      FieldName = 'FORMULARIO'
      Size = 30
    end
    object sqlFiltroFILTROTXT: TMemoField
      FieldName = 'FILTROTXT'
      BlobType = ftOraClob
      Size = 1
    end
    object sqlFiltroLAYOUT: TBlobField
      FieldName = 'LAYOUT'
      Size = 1
    end
    object sqlFiltroULTIMOID2: TFMTBCDField
      FieldName = 'ULTIMOID'
      Precision = 32
    end
  end
  object dsFiltro: TDataSource
    DataSet = cdsFiltro
    Left = 224
    Top = 8
  end
  object cdsFiltro: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspFiltro'
    BeforePost = cdsFiltroBeforePost
    AfterPost = cdsFiltroAfterPost
    AfterDelete = cdsFiltroAfterPost
    AfterScroll = cdsFiltroAfterScroll
    OnNewRecord = cdsFiltroNewRecord
    OnReconcileError = cdsFiltroReconcileError
    Left = 24
    Top = 8
    object cdsFiltroCODCOLIGADA: TFMTBCDField
      FieldName = 'CODCOLIGADA'
      Required = True
      Precision = 32
    end
    object cdsFiltroIDFILTRO: TFMTBCDField
      FieldName = 'IDFILTRO'
      Required = True
      Precision = 32
    end
    object cdsFiltroID: TFMTBCDField
      FieldName = 'ID'
      Required = True
      Precision = 32
    end
    object cdsFiltroDESCRICAO: TWideStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Size = 60
    end
    object cdsFiltroIDUSUARIO: TFMTBCDField
      FieldName = 'IDUSUARIO'
      Precision = 32
    end
    object cdsFiltroFORMULARIO: TWideStringField
      FieldName = 'FORMULARIO'
      Size = 30
    end
    object cdsFiltroFILTROTXT: TMemoField
      FieldName = 'FILTROTXT'
      BlobType = ftOraClob
      Size = 1
    end
    object cdsFiltroLAYOUT: TBlobField
      FieldName = 'LAYOUT'
      Size = 1
    end
    object cdsFiltroULTIMOID: TFMTBCDField
      FieldName = 'ULTIMOID'
      Precision = 32
    end
  end
  object dspFiltro: TDataSetProvider
    DataSet = sqlFiltro
    UpdateMode = upWhereKeyOnly
    BeforeApplyUpdates = dspFiltroBeforeApplyUpdates
    Left = 160
    Top = 8
  end
end
