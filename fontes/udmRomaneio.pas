unit udmRomaneio;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.DB, Datasnap.Provider,
  Datasnap.DBClient, Data.SqlExpr, SimpleDS, Vcl.ImgList, Vcl.Controls,
  cxGraphics,forms,Windows,StrUtils, frxClass, frxDBSet, Data.DBXOracle,
  System.ImageList, cxImageList;

type
  TdmRomaneio = class(TDataModule)
    sqlPedidos: TSQLDataSet;
    cdsPedidos: TClientDataSet;
    dspPedidos: TDataSetProvider;
    dsPedidos: TDataSource;
    sqlPedidosNUNOTA: TFMTBCDField;
    sqlPedidosNUMNOTA: TFMTBCDField;
    sqlPedidosNOMEPARC: TWideStringField;
    sqlPedidosDTMOV: TSQLTimeStampField;
    sqlPedidosNOMEREG: TWideStringField;
    cdsPedidosNUNOTA: TFMTBCDField;
    cdsPedidosNUMNOTA: TFMTBCDField;
    cdsPedidosNOMEPARC: TWideStringField;
    cdsPedidosDTMOV: TSQLTimeStampField;
    cdsPedidosNOMEREG: TWideStringField;
    sqlItensRomaneio: TSQLDataSet;
    cdsItensRomaneio: TClientDataSet;
    dspItensRomaneio: TDataSetProvider;
    dsItensRomaneio: TDataSource;
    sqlItensRomaneioNUMNOTA: TFMTBCDField;
    sqlItensRomaneioNOMEPARC: TWideStringField;
    sqlItensRomaneioNOMEREG: TWideStringField;
    sqlItensRomaneioTEMPO: TFMTBCDField;
    cdsItensRomaneioNUMNOTA: TFMTBCDField;
    cdsItensRomaneioNOMEPARC: TWideStringField;
    cdsItensRomaneioNOMEREG: TWideStringField;
    cdsItensRomaneioTEMPO: TFMTBCDField;
    sqlItensRomaneioNUNOTA: TFMTBCDField;
    cdsItensRomaneioNUNOTA: TFMTBCDField;
    sqlRomaneio: TSQLDataSet;
    cdsRomaneio: TClientDataSet;
    dspRomaneio: TDataSetProvider;
    dsRomaneio: TDataSource;
    sqlRomaneioIDROMANEIO: TFMTBCDField;
    sqlRomaneioNOMEPARC: TWideStringField;
    sqlRomaneioTIPO: TWideStringField;
    cdsRomaneioIDROMANEIO: TFMTBCDField;
    cdsRomaneioNOMEPARC: TWideStringField;
    cdsRomaneioTIPO: TWideStringField;
    sqlRomaneioDATACRIACAO: TSQLTimeStampField;
    cdsRomaneioDATACRIACAO: TSQLTimeStampField;
    sqlRomaneioCODPARC: TFMTBCDField;
    cdsRomaneioCODPARC: TFMTBCDField;
    sqlRomaneioCODEMP: TFMTBCDField;
    sqlRomaneioUSUARIOCRIACAO: TWideStringField;
    cdsRomaneioCODEMP: TFMTBCDField;
    cdsRomaneioUSUARIOCRIACAO: TWideStringField;
    sqlItensRomaneioIDROMANEIO: TFMTBCDField;
    sqlItensRomaneioCODEMP: TFMTBCDField;
    cdsItensRomaneioIDROMANEIO: TFMTBCDField;
    cdsItensRomaneioCODEMP: TFMTBCDField;
    sqlPedidosSTATUS: TWideStringField;
    cdsPedidosSTATUS: TWideStringField;
    imgList: TcxImageList;
    cdsPedidosTEMPO: TFMTBCDField;
    sqlPedidosTEMPO: TFMTBCDField;
    sqlRomaneioSTATUS: TWideStringField;
    cdsRomaneioSTATUS: TWideStringField;
    sqlRomaneioDATAROTEIRIZACAO: TSQLTimeStampField;
    cdsRomaneioDATAROTEIRIZACAO: TSQLTimeStampField;
    sqlPedidosAD_LOJA: TWideStringField;
    cdsPedidosAD_LOJA: TWideStringField;
    sqlPedidosCLIENTE: TWideStringField;
    cdsPedidosCLIENTE: TWideStringField;
    sdsMotoboy: TSimpleDataSet;
    dsMotoboy: TDataSource;
    dsCliente: TDataSource;
    dsLoja: TDataSource;
    sqlItensRomaneioCODPARC: TFMTBCDField;
    cdsItensRomaneioCODPARC: TFMTBCDField;
    sqlItensRomaneioCODREG: TFMTBCDField;
    cdsItensRomaneioCODREG: TFMTBCDField;
    sdsCliente: TSimpleDataSet;
    sdsClienteCODPARC: TFMTBCDField;
    sdsClienteNOMEPARC: TWideStringField;
    sdsClienteCODREG: TFMTBCDField;
    sdsClienteNOMEREG: TWideStringField;
    sqlPedidosCODREG: TFMTBCDField;
    sqlPedidosCODPARC: TFMTBCDField;
    cdsPedidosCODREG: TFMTBCDField;
    cdsPedidosCODPARC: TFMTBCDField;
    sqlRomaneioCLILOJA: TWideStringField;
    cdsRomaneioCLILOJA: TWideStringField;
    sdsLoja: TSimpleDataSet;
    sdsLojaCODPARC: TFMTBCDField;
    sdsLojaNOMEPARC: TWideStringField;
    sdsLojaCODREG: TFMTBCDField;
    sdsLojaNOMEREG: TWideStringField;
    sdsMotoboyCODPARC: TFMTBCDField;
    sdsMotoboyNOMEPARC: TWideStringField;
    sdsMotoboyIDCONTROLE: TFMTBCDField;
    sqlRomaneioDATAFECHAMENTO: TSQLTimeStampField;
    cdsRomaneioDATAFECHAMENTO: TSQLTimeStampField;
    frxRel: TfrxReport;
    frxItensRomaneio: TfrxDBDataset;
    frxRomaneio: TfrxDBDataset;
    sqlRomaneioENTREGADOR: TWideStringField;
    sqlItensRomaneioPEDIDOREL: TWideStringField;
    cdsItensRomaneioPEDIDOREL: TWideStringField;
    cdsRomaneioENTREGADOR: TWideStringField;
    cdsItensRomaneioTOTALREG: TAggregateField;
    sdsMotorista: TSimpleDataSet;
    dsMotorista: TDataSource;
    sdsMotoristaCODPARC: TFMTBCDField;
    sdsMotoristaNOMEPARC: TWideStringField;
    sqlRomaneioTIPOMOTORISTA: TFMTBCDField;
    cdsRomaneioTIPOMOTORISTA: TFMTBCDField;
    sqlItensRomaneioOBSERVACOES: TWideStringField;
    cdsItensRomaneioOBSERVACOES: TWideStringField;
    sqlPedidosCODTIPOPER: TFMTBCDField;
    cdsPedidosCODTIPOPER: TFMTBCDField;
    procedure cdsRomaneioAfterScroll(DataSet: TDataSet);
    procedure cdsRomaneioNewRecord(DataSet: TDataSet);
    procedure cdsRomaneioBeforePost(DataSet: TDataSet);
    procedure cdsRomaneioAfterApplyUpdates(Sender: TObject;
      var OwnerData: OleVariant);
    procedure dsRomaneioStateChange(Sender: TObject);
    procedure cdsRomaneioBeforeEdit(DataSet: TDataSet);
    procedure cdsItensRomaneioBeforeEdit(DataSet: TDataSet);
    procedure cdsItensRomaneioAfterDelete(DataSet: TDataSet);
    procedure cdsItensRomaneioAfterPost(DataSet: TDataSet);
    procedure cdsRomaneioBeforeDelete(DataSet: TDataSet);
    procedure cdsRomaneioSTATUSGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure dspRomaneioGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: string);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    SQLPad :String;
  end;

var
  dmRomaneio: TdmRomaneio;

implementation

uses untFuncoes, untCadRomaneio, udmPrincipal;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmRomaneio.cdsItensRomaneioAfterDelete(DataSet: TDataSet);
begin
  // if frmCadRomaneio <> nil then
  //  frmCadRomaneio.rgCliLoja.Enabled := cdsItensRomaneio.IsEmpty;
end;

procedure TdmRomaneio.cdsItensRomaneioAfterPost(DataSet: TDataSet);
begin
//  if frmCadRomaneio <> nil then
//   frmCadRomaneio.rgCliLoja.Enabled := cdsItensRomaneio.IsEmpty;
end;

procedure TdmRomaneio.cdsItensRomaneioBeforeEdit(DataSet: TDataSet);
begin
  if cdsRomaneioSTATUS.AsString <> 'A' then
  begin
    Application.MessageBox('N�o � Poss�vel Alterar um Romaneio j� Roteirizado!!','SANKHYA', mb_ok + mb_iconerror);
    Abort;
  end;
end;

procedure TdmRomaneio.cdsRomaneioAfterApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  cdsItensRomaneio.ApplyUpdates(0);
end;

procedure TdmRomaneio.cdsRomaneioAfterScroll(DataSet: TDataSet);
begin
  if not DataSet.ControlsDisabled then
  begin
    cdsItensRomaneio.Close;
    sqlItensRomaneio.ParamByName('CODEMP').AsInteger := fGetColigada;
    sqlItensRomaneio.ParamByName('IDROMANEIO').AsInteger := cdsRomaneioIDROMANEIO.AsInteger;
    cdsItensRomaneio.Open;

    if frmCadRomaneio <> nil then
    begin
      frmCadRomaneio.btnRoteirizar.visible := cdsRomaneioSTATUS.AsString = 'A';
      frmCadRomaneio.pnlPedReal.Visible :=  cdsRomaneioSTATUS.AsString = 'A';
      frmCadRomaneio.pnlSel.Visible :=  cdsRomaneioSTATUS.AsString = 'A';
      frmCadRomaneio.btnImprimir.Visible :=  cdsRomaneioSTATUS.AsString = 'R';
      frmCadRomaneio.DBRGTipoPropertiesChange(nil);
    //  frmCadRomaneio.rgCliLoja.Enabled := cdsItensRomaneio.IsEmpty;
    end;
  end;
end;

procedure TdmRomaneio.cdsRomaneioBeforeDelete(DataSet: TDataSet);
begin
  if cdsRomaneioSTATUS.AsString <> 'A'  then
  begin
    Application.MessageBox('N�o � poss�vel Excluir um Romaneio ja Roteirizado!!','SANKHYA', mb_ok + mb_iconerror);
    Abort;
  end;
end;

procedure TdmRomaneio.cdsRomaneioBeforeEdit(DataSet: TDataSet);
begin
  if cdsRomaneioSTATUS.AsString <> 'A' then
  begin
    Application.MessageBox('N�o � poss�vel Alterar um Romaneio ja Roteirizado!!','SANKHYA', mb_ok + mb_iconerror);
    Abort;
  end;
end;

procedure TdmRomaneio.cdsRomaneioBeforePost(DataSet: TDataSet);
begin
  if cdsRomaneio.State = dsInsert then
  begin
    cdsRomaneioCODEMP.AsInteger := fGetColigada;
    cdsRomaneioDATACRIACAO.AsDateTime := now;
    cdsRomaneioUSUARIOCRIACAO.AsString := fGetNomeUsuario;
  end;
end;

procedure TdmRomaneio.cdsRomaneioNewRecord(DataSet: TDataSet);
begin
  cdsRomaneioIDROMANEIO.AsInteger := GeraCodigo('IDROMANEIO');
  cdsRomaneioTIPO.AsString := 'ENTREGA';
  cdsRomaneioSTATUS.AsString := 'A';
  cdsRomaneioCLILOJA.AsString := 'A';
  cdsRomaneioTIPOMOTORISTA.AsInteger := 0;

  sdsMotoboy.Close;
  sdsMotoboy.DataSet.Params[0].Value := fgetColigada;
  sdsMotoboy.Open;

  if sdsMotoboy.FieldByName('CODPARC').AsInteger > 0 then
    cdsRomaneioCODPARC.AsInteger := sdsMotoboy.FieldByName('CODPARC').AsInteger;
end;

procedure TdmRomaneio.cdsRomaneioSTATUSGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if DisplayText and not(Sender.IsNull) then
  begin
    case AnsiIndexStr(UpperCase(Sender.Value), ['A', 'R','F']) of
      0: Text := 'ABERTO';
      1: Text := 'ROTEIRIZADO';
      2: Text := 'FINALIZADO';
    end;
  end;
end;

procedure TdmRomaneio.DataModuleCreate(Sender: TObject);
begin
  With dmPrincipal do
  begin
    cdsParametros.Close;
    cdsParametros.Params[0].Value := fGetColigada;
    cdsParametros.Open;
  end;

  SQLPad := ' SELECT  S.STATUS,C.NUNOTA,C.NUMNOTA, P.NOMEPARC,DTMOV , ' +
            ' CASE WHEN  P.AD_LOJA = ''S'' THEN ''LOJA'' ELSE R.NOMEREG END NOMEREG, ' +
            ' (TRUNC(1440 * (SYSDATE - to_date((C.DTMOV || LPAD(C.HRMOV,6,0)),''DD/MM/YYHH24MISS'' ))))  ' +
            ' TEMPO,P.AD_LOJA,P.CLIENTE,P.CODREG,C.CODPARC,  ' +
            ' C.CODTIPOPER  ' +
            ' FROM TGFCAB C ' +
            ' INNER JOIN TGFPAR P ON C.CODPARC = P.CODPARC  ' +
            ' INNER JOIN TSIREG R ON R.CODREG  = P.CODREG   ' +
            ' LEFT JOIN CRMSEPARACAO S ON S.NUNOTA = C.NUNOTA   ' +
            ' WHERE  ' +
            '  ( ' +
            '      (     C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPER').AsString + ' ) ' +
            '           AND C.PENDENTE = ''S'' ' +
            '      ) ' +
            '      OR ( C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPERTODOS').AsString + ' ) ) ' +
            '  ) ' +
            ' AND C.STATUSNOTA = ''L''  ' +
            ' AND C.NUNOTA NOT IN( SELECT NUNOTA FROM CRMITENSROMANEIO) ' +
            ' AND C.CODEMP = :CODEMP ' +
            ' AND C.AD_CODPARC IS NULL   ' +

            ' UNION  ' +

            ' SELECT  S.STATUS,C.NUNOTA,C.NUMNOTA, P.NOMEPARC,DTMOV ,CASE WHEN  P.AD_LOJA = ''S''   ' +
            ' THEN ''LOJA'' ELSE R.NOMEREG END NOMEREG,                                             ' +
            ' (TRUNC(1440 * (SYSDATE - to_date((C.DTMOV || LPAD(C.HRMOV,6,0)),''DD/MM/YYHH24MISS'' ))))   ' +
            ' TEMPO,P.AD_LOJA,P.CLIENTE,P.CODREG,C.CODPARC, ' +
            ' C.CODTIPOPER                                  ' +
            ' FROM TGFCAB C         ' +
            ' INNER JOIN TGFPAR P ON C.AD_CODPARC = P.CODPARC  ' +
            ' INNER JOIN TSIREG R ON R.CODREG  = P.CODREG      ' +
            ' LEFT JOIN CRMSEPARACAO S ON S.NUNOTA = C.NUNOTA  ' +
            ' WHERE                                            ' +
            '  ( ' +
            '      (     C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPER').AsString + ' ) ' +
            '           AND C.PENDENTE = ''S'' ' +
            '      ) ' +
            '      OR ( C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPERTODOS').AsString + ' ) ) ' +
            '  ) ' +
            ' AND C.STATUSNOTA = ''L''                           ' +
            ' AND C.NUNOTA NOT IN( SELECT NUNOTA FROM CRMITENSROMANEIO)  ' +
            ' AND C.CODEMP = :CODEMP  ' +

            ' ORDER BY NOMEREG  ' ;

  sqlPedidos.CommandText := SQLPad;
end;

procedure TdmRomaneio.dspRomaneioGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: string);
begin
  TableName := 'CRMROMANEIO';
end;

procedure TdmRomaneio.dsRomaneioStateChange(Sender: TObject);
begin
  if dsRomaneio.State = dsEdit then
    if (frmCadRomaneio <> nil) then
      frmCadRomaneio.PageControl1.ActivePageIndex := 1;
end;

end.
