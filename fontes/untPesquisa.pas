unit untPesquisa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBase, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,dxSkinTheAsphaltWorld,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, dxGDIPlusClasses, JvExControls,
  JvXPCore, JvXPButtons, Vcl.StdCtrls, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, Vcl.ComCtrls, Data.SqlExpr, Vcl.Grids, Vcl.DBGrids, JvExDBGrids,
  JvDBGrid, Datasnap.Provider, Datasnap.DBClient, Data.FMTBcd, cxContainer,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, dxSkiniMaginary;

type
  TfrmPesquisa = class(TfrmBase)
    Panel1: TPanel;
    Panel2: TPanel;
    edtPesquisa: TEdit;
    btnPesquisar: TJvXPButton;
    JvXPButton2: TJvXPButton;
    btok: TJvXPButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cbCampos: TcxComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure JvXPButton2Click(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure btokClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    CPesq :String;
  public
    ValorCampo,CampoPesq :String;
    CamposInvisiveis,AliasCampos :array of string;
  end;


var
  frmPesquisa: TfrmPesquisa;

implementation

{$R *.dfm}
uses untFuncoes,udmPesquisa;

procedure TfrmPesquisa.btnPesquisarClick(Sender: TObject);
var
  Campo :String;
  i :Integer;
begin
  inherited;
  for I := 0 to cxGrid1DBTableView1.ColumnCount -1 do
    if cxGrid1DBTableView1.Columns[i].Caption = cbCampos.Text then
      Campo := cxGrid1DBTableView1.Columns[i].DataBinding.FieldName;


  dmPesquisa.cdsPesq.Close;

  if edtPesquisa.Text = '' then
     dmPesquisa.sqlPesq.SQL[1] := ' AND CODCOLIGADA < 0'
  else
  if EstaContido(edtPesquisa.Text,'%') then
    DmPesquisa.SQLPesq.SQL[1] := ' AND ' +  CAMPO + ' LIKE (' + QuotedStr(edtPesquisa.Text) + ')'
  else
    DmPesquisa.SQLPesq.SQL[1] := ' AND ' +  CAMPO + ' LIKE ('  + QuotedStr( edtPesquisa.Text + '%')  + ')';
  dmPesquisa.cdsPesq.Open;
end;

procedure TfrmPesquisa.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
  close;
end;

procedure TfrmPesquisa.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if key = VK_RETURN then
    btokClick(nil);
end;

procedure TfrmPesquisa.edtPesquisaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = VK_RETURN then
    btnPesquisarClick(nil);

end;

procedure TfrmPesquisa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  frmPesquisa := nil;
end;

procedure TfrmPesquisa.FormShow(Sender: TObject);
var
 i,j,y :Integer;
begin
  inherited;
  edtPesquisa.Text := ValorCampo;
  dmPesquisa.cdsPesq.Open;
  cxGrid1DBTableView1.DataController.CreateAllItems();

  for j := 0 to cxGrid1DBTableView1.ColumnCount -1 do
  begin
    if cxGrid1DBTableView1.Columns[j].Visible then
    begin
      cxGrid1DBTableView1.Columns[j].Caption :=  AliasCampos[j];
      if cxGrid1DBTableView1.Columns[j].DataBinding.FieldName = CampoPesq then
         cPesq :=  cxGrid1DBTableView1.Columns[j].Caption;
      cbCampos.Properties.Items.Add(cxGrid1DBTableView1.Columns[j].Caption);
      if cxGrid1DBTableView1.Columns[j].Caption = 'I'   then
        cxGrid1DBTableView1.Columns[j].Visible := False;
    end;
  end;
  cbCampos.ItemIndex :=  cbCampos.Properties.Items.IndexOf(cPesq);
    btnPesquisarClick(nil);
  if edtPesquisa.CanFocus then
    edtPesquisa.SetFocus;

end;

procedure TfrmPesquisa.JvXPButton2Click(Sender: TObject);
begin
  inherited;
  dmPesquisa.cdsPesq.Close;
  close;
end;

procedure TfrmPesquisa.btokClick(Sender: TObject);
begin
  inherited;
  close;
end;

end.
