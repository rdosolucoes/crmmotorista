unit udmConferencia;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Data.DB, Datasnap.Provider,
  Datasnap.DBClient, Data.SqlExpr, Vcl.ImgList, Vcl.Controls, cxGraphics,
  System.ImageList, cxImageList;

type
  TdmConferencia = class(TDataModule)
    sqlConferencia: TSQLDataSet;
    cdsConferencia: TClientDataSet;
    dspConferencia: TDataSetProvider;
    dsConferencia: TDataSource;
    sqlConferenciaNUNOTA: TFMTBCDField;
    sqlConferenciaNUMNOTA: TFMTBCDField;
    sqlConferenciaNOMEPARC: TWideStringField;
    sqlConferenciaDATASEPARACAO: TSQLTimeStampField;
    sqlConferenciaSTATUS: TWideStringField;
    sqlConferenciaATRASO: TWideStringField;
    cdsConferenciaNUNOTA: TFMTBCDField;
    cdsConferenciaNUMNOTA: TFMTBCDField;
    cdsConferenciaNOMEPARC: TWideStringField;
    cdsConferenciaDATASEPARACAO: TSQLTimeStampField;
    cdsConferenciaSTATUS: TWideStringField;
    cdsConferenciaATRASO: TWideStringField;
    cdsConferenciaSEL: TIntegerField;
    imgList: TcxImageList;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    SQLPad :String;
  end;

var
  dmConferencia: TdmConferencia;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}
uses udmPrincipal,untFuncoes;

procedure TdmConferencia.DataModuleCreate(Sender: TObject);
begin
  With dmPrincipal do
  begin
    cdsParametros.Close;
    cdsParametros.Params[0].Value := fGetColigada;
    cdsParametros.Open;
  end;


  SQLPad := ' SELECT  C.NUNOTA,C.NUMNOTA, P.NOMEPARC,S.DATASEPARACAO,S.STATUS,  ' +
            ' CASE WHEN  (TRUNC(1440 * (SYSDATE - to_date((to_char(S.DATASEPARACAO,''DD/MM/YY HH24:MI:SS'')), ' +
            ' ''DD/MM/YY HH24:MI:SS'' ))) > (SELECT TEMPOCONFERE FROM CRMPARAMETROS WHERE CODCOLIGADA = 0))   THEN ''A'' ELSE '''' END ATRASO ' +
            ' FROM TGFCAB C   ' +
            ' INNER JOIN TGFPAR P ON C.CODPARC = P.CODPARC  ' +
            ' INNER JOIN TSIREG R ON R.CODREG  = P.CODREG   ' +
            ' LEFT OUTER JOIN CRMSEPARACAO S ON S.NUNOTA = C.NUNOTA  ' +
            ' WHERE   ' +

            '  ( ' +
            '      (     C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPER').AsString + ' ) ' +
            '           AND C.PENDENTE = ''S'' ' +
            '      ) ' +
            '      OR ( C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPERTODOS').AsString + ' ) ) ' +
            '  ) ' +

            ' AND C.STATUSNOTA = ''L''    ' +
            ' AND C.CODEMP = :CODEMP    ' +
            ' AND (S.STATUS <> ''A''  OR S.STATUS IS NULL)  ' +
            ' AND AD_CODPARC IS NULL  ' +

            ' UNION    ' +

            ' SELECT  C.NUNOTA,C.NUMNOTA, P.NOMEPARC,S.DATASEPARACAO,S.STATUS,  ' +
            ' CASE WHEN  (TRUNC(1440 * (SYSDATE - to_date((to_char(S.DATASEPARACAO,''DD/MM/YY HH24:MI:SS'')), ' +
            ' ''DD/MM/YY HH24:MI:SS'' ))) > (SELECT TEMPOCONFERE FROM CRMPARAMETROS WHERE CODCOLIGADA = 0))   THEN ''A'' ELSE '''' END ATRASO  ' +
            ' FROM TGFCAB C  ' +
            ' INNER JOIN TGFPAR P ON C.AD_CODPARC = P.CODPARC  ' +
            ' INNER JOIN TSIREG R ON R.CODREG  = P.CODREG  ' +
            ' LEFT OUTER JOIN CRMSEPARACAO S ON S.NUNOTA = C.NUNOTA  ' +
            ' WHERE  ' +

            '  ( ' +
            '      (     C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPER').AsString + ' ) ' +
            '           AND C.PENDENTE = ''S'' ' +
            '      ) ' +
            '      OR ( C.CODTIPOPER IN ( ' + dmPrincipal.cdsParametros.FieldByName('CODTIPOSPERTODOS').AsString + ' ) ) ' +
            '  ) ' +

            ' AND C.STATUSNOTA = ''L''  ' +
            ' AND C.CODEMP = :CODEMP  ' +
            ' AND (S.STATUS <> ''A''  OR S.STATUS IS NULL)   ';

   sqlConferencia.CommandText := SQLPad;
end;

end.
