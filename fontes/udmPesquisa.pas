unit udmPesquisa;

interface

uses
  System.SysUtils, System.Classes, Datasnap.Provider, Data.DB, Datasnap.DBClient,
  Data.FMTBcd, Data.SqlExpr;

type
  TdmPesquisa = class(TDataModule)
    cdsPesq: TClientDataSet;
    dsPesq: TDataSource;
    dspPesq: TDataSetProvider;
    sqlPesq: TSQLQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmPesquisa: TdmPesquisa;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses udmPrincipal;

{$R *.dfm}

end.
