unit untSeparacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseVisao, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkiniMaginary,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  Data.DB, cxDBData, Vcl.StdCtrls, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ComCtrls, dxGDIPlusClasses, JvExControls, JvXPCore, JvXPButtons, JvLabel,
  Vcl.ExtCtrls, cxCheckBox, dxSkinSilver, cxImageComboBox,SQLExpr,StrUtils,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmSeparacao = class(TfrmBaseVisao)
    btnSeparar: TJvXPButton;
    cxGrid1DBTableView1NUMNOTA: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEPARC: TcxGridDBColumn;
    cxGrid1DBTableView1HRENTSAI: TcxGridDBColumn;
    cxGrid1DBTableView1APELIDO: TcxGridDBColumn;
    cxGrid1DBTableView1NOMEREG: TcxGridDBColumn;
    cxGrid1DBTableView1SEL: TcxGridDBColumn;
    cxGrid1DBTableView1ATRASO: TcxGridDBColumn;
    cxGrid1DBTableView1SETORA: TcxGridDBColumn;
    cxGrid1DBTableView1SETORB: TcxGridDBColumn;
    cxGrid1DBTableView1SETORC: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSepararClick(Sender: TObject);
    procedure cxGrid1DBTableView1SETORAGetCellHint(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
      var AHintText: TCaption; var AIsHintMultiLine: Boolean;
      var AHintTextRect: TRect);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure pSetaDireitos; override;
    procedure pRefresh; override;
  end;

var
  frmSeparacao: TfrmSeparacao;

implementation

{$R *.dfm}

uses udmSeparacao,untFuncoes,untPrincipal,untLogin,udmPrincipal;

procedure TfrmSeparacao.btnSepararClick(Sender: TObject);
var result :integer;
    sSql,sID :String;
    qAux :TSQLQuery;
begin
  inherited;
 // Application.CreateForm(TfrmLogin,frmLogin);
 // result := frmLogin.ShowModal;

 // if result = 6 then
//  begin
    With dmSeparacao do
    begin
      cdsSeparacao.DisableControls;
      cdsSeparacao.First;
      CriaQuery(qAux);

      while not cdsSeparacao.Eof do
      begin
        if cdsSeparacaoSEL.AsInteger <> 1 then
        begin
          cdsSeparacao.Next;
          Continue;
        end;
          
        sID := cdsSeparacaoNUNOTA.AsString;

        if cdsSeparacaoSTATUS.AsString = 'R' then
        begin
          sSQL := 'UPDATE CRMSEPARACAO SET STATUS = ''S'', DATASEPARACAO = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                  ',''DD/MM/YYYY HH24:MI:SS'') WHERE NUNOTA = ' + sID +
                  ' AND CODEMP = ' + IntToStr(fGetColigada) ;
          SQLExecute(sSQL);
          cdsSeparacao.Next;
          Continue;
        end;



        sSQL := 'SELECT NUNOTA FROM CRMSEPARACAO WHERE NUNOTA = ' +  sID +  ' AND CODEMP = ' + IntToStr(fGetColigada) ;
        qAux.Close;
        qAux.SQL.Clear;
        qAux.SQL.Text := sSQL;
        qAux.Open;

        if qAux.IsEmpty then
        begin
          sSQL := 'INSERT INTO CRMSEPARACAO(CODEMP,NUNOTA,SETORA,SETORB,SETORC) VALUES (' + IntToStr(fGetColigada) + ',' + sID + ',' +
                  '0,0,0)';

          SQLExecute(sSQL);
        end;

        case AnsiIndexStr(CarregaSetor, ['A', 'B','C','BC','ABC']) of
           0: sSQL := 'UPDATE CRMSEPARACAO SET SETORA = 1, DATASEPARACAOA = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                      ',''DD/MM/YYYY HH24:MI:SS'') WHERE NUNOTA = ' + sID +  ' AND CODEMP = ' + IntToStr(fGetColigada);

           1: sSQL := 'UPDATE CRMSEPARACAO SET SETORB = 1, DATASEPARACAOB = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                      ',''DD/MM/YYYY HH24:MI:SS'') WHERE NUNOTA = '  + sID + ' AND CODEMP = ' + IntToStr(fGetColigada);

           2: sSQL := 'UPDATE CRMSEPARACAO SET SETORC = 1, DATASEPARACAOC = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                      ',''DD/MM/YYYY HH24:MI:SS'') WHERE NUNOTA = ' + sID +  ' AND CODEMP = ' + IntToStr(fGetColigada);

           3: sSQL := 'UPDATE CRMSEPARACAO SET SETORB = 1, DATASEPARACAOB = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                      ',''DD/MM/YYYY HH24:MI:SS''),SETORC = 1, DATASEPARACAOC = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                      ',''DD/MM/YYYY HH24:MI:SS'') WHERE NUNOTA = ' + sID +  ' AND CODEMP = ' + IntToStr(fGetColigada);

           4: sSQL := 'UPDATE CRMSEPARACAO SET SETORB = 1, DATASEPARACAOB = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                      ',''DD/MM/YYYY HH24:MI:SS''),SETORC = 1, DATASEPARACAOC = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +  ',''DD/MM/YYYY HH24:MI:SS'')' +
                      ',SETORA = 1, DATASEPARACAOA = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +   ',''DD/MM/YYYY HH24:MI:SS'')' +
                      ' WHERE NUNOTA = ' + sID +  ' AND CODEMP = ' + IntToStr(fGetColigada);

        end;
        SQLExecute(sSQL);

        sSQL := ' SELECT NUNOTA FROM CRMSEPARACAO WHERE SETORA = 1 AND SETORB = 1 AND SETORC = 1 ' +
                ' AND NUNOTA = ' + sID +  ' AND CODEMP = ' + IntToStr(fGetColigada) ;
        qAux.Close;
        qAux.SQL.Clear;
        qAux.SQL.Text := sSQL;
        qAux.Open;

        if not qAux.IsEmpty then
        begin
          sSQL := 'UPDATE CRMSEPARACAO SET STATUS = ''S'', DATASEPARACAO = To_Date(' + QuotedStr(FormatDateTime('DD/MM/YYYY HH:MM:SS',now)) +
                  ',''DD/MM/YYYY HH24:MI:SS'') WHERE NUNOTA = ' + sID +
                  ' AND CODEMP = ' + IntToStr(fGetColigada) ;
          SQLExecute(sSQL);
          AtualizaStatusPedido(StrToInt(sID),'Conferência');
        end;
        cdsSeparacao.Next;
      end;
      cdsSeparacao.First;
      cdsSeparacao.EnableControls;
    end;
    FreeAndNil(qAux);
    btnAtualizarClick(nil);
 // end;
end;

procedure TfrmSeparacao.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
 // inherited;

end;

procedure TfrmSeparacao.cxGrid1DBTableView1SETORAGetCellHint(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  ACellViewInfo: TcxGridTableDataCellViewInfo; const AMousePos: TPoint;
  var AHintText: TCaption; var AIsHintMultiLine: Boolean;
  var AHintTextRect: TRect);
begin
  inherited;
  AHintText := VarToStr(ARecord.DisplayTexts[Sender.Index]);
  AIsHintMultiLine := True;
end;

procedure TfrmSeparacao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  frmSeparacao := nil;
  FreeAndNil(dmSeparacao);
end;

procedure TfrmSeparacao.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TdmSeparacao,dmSeparacao);
  cdsPad := dmSeparacao.cdsSeparacao;
  sqlPad := dmSeparacao.sqlseparacao;
  Tabela := 'TGFCAB';
end;

procedure TfrmSeparacao.FormShow(Sender: TObject);
begin
  inherited;
  pRefresh;
  dmPrincipal.cdsParametros.Close;
  dmPrincipal.cdsParametros.Params[0].AsInteger := fGetColigada;
  dmPrincipal.cdsParametros.Open;
end;

procedure TfrmSeparacao.pRefresh;
var
  chv :Integer;
begin
  inherited;
  chv := -1;
  with dmSeparacao.cdsSeparacao do
  begin
    if Active then
      chv := FieldByName('NUNOTA').AsInteger;

    Close;
   // dmSeparacao.sqlSeparacao.ParamByName('CODTIPOPER').AsInteger := dmPrincipal.cdsParametros.FieldByName('CODTIPOPER').AsInteger;
  //  dmSeparacao.sqlSeparacao.ParamByName('CODTIPOPERTRANSF').AsInteger := dmPrincipal.cdsParametros.FieldByName('CODTIPOPERTRANSF').AsInteger;
    dmSeparacao.sqlSeparacao.ParamByName('CODEMP').AsInteger := fGetColigada;
    Open;

    if (chv > -1) then
      Locate('NUNOTA', chv, []);
  end;
  pRegistros;
end;

procedure TfrmSeparacao.pSetaDireitos;
begin
  inherited;
  btnSeparar.Enabled := frmprincipal.A01601.Enabled;
end;

end.
