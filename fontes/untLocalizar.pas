unit untLocalizar;

interface

uses untBase, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  Classes, ActnList, PlatformDefaultStyleActnCtrls, ActnMan, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView, SysUtils,
  cxGridDBTableView, cxGrid, Controls, StdCtrls, dxGDIPlusClasses, JvExControls,
  JvXPCore, JvXPButtons, ExtCtrls, ComCtrls, Dialogs, SqlExpr, Forms,
  dxSkinsCore,dxSkinTheAsphaltWorld,dxSkinscxPCPainter, System.Actions;

Type
  TPesq = record
    Display :string;
    Campo :String;
    Tipo :TFieldType;
end;

type
  TfrmLocalizar = class(TfrmBase)
    pnlBotoes: TPanel;
    btnConfirmar: TJvXPButton;
    btnCancelar: TJvXPButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    acmBotoes: TActionManager;
    actConfirmar: TAction;
    actCancelar: TAction;
    edtPesquisar: TEdit;
    Label1: TLabel;
    btnLocalizar: TJvXPButton;
    actLocalizar: TAction;
    Label2: TLabel;
    cmbCampos: TComboBox;
    procedure actLocalizarExecute(Sender: TObject);
    procedure actConfirmarExecute(Sender: TObject);
    procedure actCancelarExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    Sql :TSQLDataSet;
    CampoDefault, CampoRetorno :String;
    Retorno :Variant;
    CamposPesquisa :Array Of TPesq;
  end;

var
  frmLocalizar: TfrmLocalizar;

implementation

uses udmPrincipal;

{$R *.dfm}

procedure TfrmLocalizar.actCancelarExecute(Sender: TObject);
begin
  inherited;
  Retorno := '<CANCELADO>';
  self.Close;
end;

procedure TfrmLocalizar.actConfirmarExecute(Sender: TObject);
begin
  inherited;
  if (sql = nil) then
    exit;

  Retorno := dmPrincipal.cdsLocalizar.FieldByName(CampoRetorno).AsVariant;
  self.Close;
end;

procedure TfrmLocalizar.actLocalizarExecute(Sender: TObject);
var
  posi :integer;
  txt, cmp :String;
  i: Integer;
begin
  inherited;
  //SQL
  if (sql = nil) then
    exit;
  
  posi := pos('--1=1', sql.CommandText);
  if (posi > 0) then
  begin
    i := cmbCampos.ItemIndex;
    if (CamposPesquisa[i].Tipo = ftString) or
       (CamposPesquisa[i].Tipo = ftDateTime) or
       (CamposPesquisa[i].Tipo = ftMemo) then
      cmp := 'and '+CamposPesquisa[i].Campo+' = '''+edtPesquisar.Text+''' '
    else if (CamposPesquisa[i].Tipo = ftFloat) or
            (CamposPesquisa[i].Tipo = ftFMTBcd) or
            (CamposPesquisa[i].Tipo = ftInteger) or
            (CamposPesquisa[i].Tipo = ftSmallint) then
      cmp := 'and '+CamposPesquisa[i].Campo+' = '+StringReplace(edtPesquisar.Text,',','.',[rfReplaceAll])+' ';
        
    txt := sql.CommandText;
    StringReplace(txt, '--1=1', cmp, [rfReplaceAll]);
  end
  else
    exit;
  //
  with dmPrincipal do
  begin
    sqlLocalizar.Close;
    sqlLocalizar.CommandText := txt;
    cdsLocalizar.Close;
    cdsLocalizar.Open;  
  end;
end;

procedure TfrmLocalizar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //inherited;
  //Fechar e nao liberar mem�ria
  //Liberar mem�ria depois de ler valor retorno
end;

procedure TfrmLocalizar.FormShow(Sender: TObject);
var
  i, x: Integer;
begin
  inherited;
  if (sql = nil) then
    exit;

  SetLength(CamposPesquisa, sql.FieldCount);
  cmbCampos.Items.Clear;
  x := -1;
  //Campos Pesquisa
  for i := 0 to sql.FieldCount - 1 do
  begin
    cmbCampos.Items.Add(sql.Fields.Fields[i].DisplayLabel);
    //
    CamposPesquisa[i].Display := sql.Fields.Fields[i].DisplayLabel;
    CamposPesquisa[i].Campo   := sql.Fields.Fields[i].FieldName;
    CamposPesquisa[i].Tipo    := sql.Fields.Fields[i].DataType;

    if (CampoDefault <> '') and
       (CampoDefault = sql.Fields.Fields[i].FieldName) then
      x := i;
  end;

  //Padrao localizacao
  if (x > -1) then
    cmbCampos.ItemIndex := x;

  if pos('--1=1', sql.CommandText) < 0 then
  begin
    btnLocalizar.Enabled := false;
    raise Exception.Create('Parametro de Pesquisa n�o encontrado no SQL (--1=1)');
  end;  
end;

end.
